package com.ptgproduct.mapper;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.common.CommonConstants;
import com.ptgproduct.common.DateUtils;
import com.ptgproduct.domain.PersonDetails;
import com.ptgproduct.entity.PsJobRef;
import com.ptgproduct.entity.PsPersonAddressesRef;
import com.ptgproduct.entity.PsPersonEmailRef;
import com.ptgproduct.entity.PsPersonNamesRef;
import com.ptgproduct.entity.PsPersonNidRef;
import com.ptgproduct.entity.PsPersonPhoneRef;
import com.ptgproduct.entity.PsPersonRef;
import com.ptgproduct.entity.PsPersonalDataRef;
import com.ptgproduct.exception.PtgproductParseException;

public class PersonDetailsMapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonDetailsMapper.class);

	public static PersonDetails importData(PsPersonRef psPersonRef) {
		LOGGER.debug("importData begin : "+psPersonRef.getPEmplid());
		PersonDetails personDetails = null;
		if (psPersonRef != null) {
			personDetails = new PersonDetails();

			List<PsJobRef> jobRefs = psPersonRef.getPsJobRefs();
			for (PsJobRef psJobRef : jobRefs) {
				personDetails.setCompanyId(psJobRef.getPjCompany());
				personDetails.setEntryReason(psJobRef.getPjActionReason());
				personDetails.setEntryDate(psJobRef.getPjEntryDate().toString());
				personDetails.setEmplType(psJobRef.getPjEmplType());
				personDetails.setEmpReqTemp(psJobRef.getPjRegTemp());
				personDetails.setEmpClass(psJobRef.getPjEmplClass());
				personDetails.setAnnual_wages(psJobRef.getPjAnnlBenefBaseRt().toString());
				personDetails.setStdHours(psJobRef.getPjStdHours().toString());
				personDetails.setBussinessTitle(psJobRef.getPjBasGroupId());
				personDetails.setJobCode(psJobRef.getPjJobcode());
				personDetails.setFlsaStatus(psJobRef.getPjFlsaStatus());
				personDetails.setSupervisorId(psJobRef.getPjSupervisorId());
				personDetails.setCompensationBasicId(psJobRef.getPjCompFrequency());
				personDetails.setCompensationRate(psJobRef.getPjCtgRate().toString());
				personDetails.setDepartmentId(psJobRef.getPjDeptid());
				personDetails.setWorkLocationId(psJobRef.getPjLocation());
				personDetails.setPayGroupsIds(psJobRef.getPjPaygroup());
				personDetails.setBenefitClassId(psJobRef.getPjBenefitSystem());
				personDetails.setLevel(psJobRef.getPjGrpA());
				personDetails.setSponsor(psJobRef.getPjGrpB());
				personDetails.setVacation(psJobRef.getVacation());
				personDetails.setSick(psJobRef.getSick());
				personDetails.setPersonalTime(psJobRef.getPersonalTime());
				personDetails.setFloatingHolidays(psJobRef.getFloatingHolidays());
				personDetails.setIsBased(psJobRef.getBasedOnJobDuties());
				personDetails.setEmpVariableHour(psJobRef.getVariableHour());
				personDetails.setEmpJobDes(psJobRef.getDutiesDesc());
			}
			PsPersonalDataRef pDataRef = psPersonRef.getPsPersonalDataRef();
			personDetails.setGenderId(pDataRef.getPpdSex());
			personDetails.setEthnicityId(pDataRef.getPpdEthinicity());
			personDetails.setMilitryStatus(pDataRef.getPpdMilitaryStatus());
			personDetails.setAlternateEmpId(pDataRef.getPpdAlterEmplid());
			List<PsPersonNidRef> psPersonNidRefs = psPersonRef.getPsPersonNidRefs();
			for (PsPersonNidRef psPersonNidRef : psPersonNidRefs) {
				personDetails.setNationalId(psPersonNidRef.getPniNationalId());
				personDetails.setCountryId(psPersonNidRef.getPniCountry());
			}
			List<PsPersonNamesRef> psPersonNamesRefs = psPersonRef.getPsPersonNamesRefs();
			for (PsPersonNamesRef psPersonNamesRef : psPersonNamesRefs) {
				personDetails.setFirstName(psPersonNamesRef.getPnFirstName());
				personDetails.setLastName(psPersonNamesRef.getPnLastName());
				personDetails.setMiddleName(psPersonNamesRef.getPnMiddleName());
			}
			List<PsPersonAddressesRef> psPersonAddressesRefs = psPersonRef.getPsPersonAddressesRefs();
			for (PsPersonAddressesRef psPersonAddressesRef : psPersonAddressesRefs) {
				personDetails.setFormOfAddressId(psPersonAddressesRef.getPaAddressType());
				personDetails.setAddress1(psPersonAddressesRef.getPaAddress1());
				personDetails.setAddress2(psPersonAddressesRef.getPaAddress2());
				personDetails.setCountry(psPersonAddressesRef.getPaCounty());
				personDetails.setCity(psPersonAddressesRef.getPaCity());
				personDetails.setStateId(psPersonAddressesRef.getPaState());
				personDetails.setPostalCode(psPersonAddressesRef.getPaPostal());
			}
			List<PsPersonPhoneRef> psPersonPhoneRefs = psPersonRef.getPsPersonPhoneRefs();
			for (PsPersonPhoneRef psPersonPhoneRef : psPersonPhoneRefs) {
				personDetails.setHomePhone(psPersonPhoneRef.getPpPhone());
			}
			List<PsPersonEmailRef> psPersonEmailRefs = psPersonRef.getPsPersonEmailRefs();
			for (PsPersonEmailRef pEmailRef : psPersonEmailRefs) {
				if (pEmailRef.getPeEAddrType().equalsIgnoreCase("home")) {
					personDetails.setHomeEmail(pEmailRef.getPeEmailAddr());
				}
				if (pEmailRef.getPeEAddrType().equalsIgnoreCase("work")) {
					personDetails.setWokfEmailAddress(pEmailRef.getPeEmailAddr());
				}
			}
			personDetails.setDateOfBirth(psPersonRef.getPBirthDate().toString());
			personDetails.setpEmplid(psPersonRef.getPEmplid());
			LOGGER.debug("importData end");
			return personDetails;
		} else {
			return personDetails;
		}
	}

	public static PsPersonRef exportData(PersonDetails personDetails) throws PtgproductParseException {
		LOGGER.debug("exportData begin");
		PsPersonRef psPersonRef = null;
		if (personDetails != null) {
			psPersonRef = new PsPersonRef();
			if (personDetails.getpEmplid() != null) {
				psPersonRef.setPEmplid(personDetails.getpEmplid());
			}
			psPersonRef.setPBirthCountry("AUS");
			psPersonRef.setPBirthState("ON");
				psPersonRef.setPBirthDate(DateUtils.convertStringToDate(personDetails.getDateOfBirth()));
				if (StringUtils.isNotBlank(personDetails.getEntryDate())) {
					// PsJobRef
					List<PsJobRef> psJobRefs = new ArrayList<PsJobRef>();
					PsJobRef psJobRef = new PsJobRef();
					psJobRef.setPjEffdt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjEffseq(1);
					psJobRef.setPjEmplRcd(1);
					psJobRef.setPjPerOrg("IT");
					psJobRef.setPjPositionNbr("0");
					psJobRef.setPjHrStatus("A");
					psJobRef.setPjApptType("");
					psJobRef.setPjMainApptNumJpn(new BigDecimal("0"));
					psJobRef.setPjPositionOverride("0");
					psJobRef.setPjPosnChangeRecord("0");
					psJobRef.setPjEmplStatus("0");
					psJobRef.setPjAction("0");
					psJobRef.setPjActionDt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjTaxLocationCd("0");
					psJobRef.setPjDeptEntryDt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjPositionEntryDt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjShift("0");
					psJobRef.setPjPaygroup("0");
					psJobRef.setPjEligConfig1("0");
					psJobRef.setPjEligConfig2("0");
					psJobRef.setPjEligConfig3("0");
					psJobRef.setPjEligConfig4("0");
					psJobRef.setPjEligConfig5("0");
					psJobRef.setPjEligConfig6("0");
					psJobRef.setPjEligConfig7("0");
					psJobRef.setPjEligConfig8("0");
					psJobRef.setPjEligConfig9("0");
					psJobRef.setPjBenStatus("0");
					psJobRef.setPjBasAction("0");
					psJobRef.setPjCobraAction("0");
					psJobRef.setPjStdHrsFrequency("0");
					psJobRef.setPjOfficerCd("0");
					psJobRef.setPjSalAdminPlan("0");
					psJobRef.setPjGrade("0");
					psJobRef.setPjGradeEntryDt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjStep(new BigDecimal("0"));
					psJobRef.setPjStepEntryDt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjGlPayType("0");
					psJobRef.setPjAcctCd("0");
					psJobRef.setPjEarnsDistType("0");
					psJobRef.setPjCompFrequency("0");
					psJobRef.setPjComprate(new BigDecimal("0"));
					psJobRef.setPjChangeAmt(new BigDecimal("0"));
					psJobRef.setPjChangePct(new BigDecimal("0"));
					psJobRef.setPjAnnualRt(new BigDecimal("0"));
					psJobRef.setPjMonthlyRt(new BigDecimal("0"));
					psJobRef.setPjDailyRt(new BigDecimal("0"));
					psJobRef.setPjHourlyRt(new BigDecimal("0"));
					psJobRef.setPjShiftRt(BigDecimal.ONE);
					psJobRef.setPjShiftFactor(BigDecimal.ONE);
					psJobRef.setPjCurrencyCd("0");
					psJobRef.setPjBusinessUnit("0");
					psJobRef.setPjSetidDept("0");
					psJobRef.setPjSetidEmplClass("0");
					psJobRef.setPjSetidJobcode("0");
					psJobRef.setPjSetidLbrAgrmnt("0");
					psJobRef.setPjSetidLocation("0");
					psJobRef.setPjSetidSalary("0");
					psJobRef.setPjSetidSupvLvl("0");
					psJobRef.setPjRegRegion("0");
					psJobRef.setPjDirectlyTipped("0");
					psJobRef.setPjEeoClass("0");
					psJobRef.setPjFunctionCd("0");
					psJobRef.setPjTariffGer("0");
					psJobRef.setPjTariffAreaGer("0");
					psJobRef.setPjPerformGroupGer("0");
					psJobRef.setPjLaborTypeGer("0");
					psJobRef.setPjSpkCommIdGer("0");
					psJobRef.setPjHourlyRtFra("0");
					psJobRef.setPjAccdntCdFra("0");
					psJobRef.setPjValue1Fra("0");
					psJobRef.setPjValue2Fra("0");
					psJobRef.setPjValue3Fra("0");
					psJobRef.setPjValue4Fra("0");
					psJobRef.setPjValue5Fra("0");
					psJobRef.setPjCtgRate(BigDecimal.ONE);
					psJobRef.setPjPaidHours(BigDecimal.ONE);
					psJobRef.setPjPaidFte(BigDecimal.ONE);
					psJobRef.setPjPaidHrsFrequency("0");
					psJobRef.setPjUnionFullPart("0");
					psJobRef.setPjUnionPos("0");
					psJobRef.setPjMatriculaNbr(BigDecimal.ONE);
					psJobRef.setPjSocSecRiskCode("0");
					psJobRef.setPjUnionFeeAmount(BigDecimal.ONE);
					psJobRef.setPjUnionFeeStartDt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjUnionFeeEndDt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjExemptJobLbr("0");
					psJobRef.setPjExemptHoursMonth(BigDecimal.ONE);
					psJobRef.setPjWrksCnclFunction("0");
					psJobRef.setPjInterctrWrksCncl("0");
					psJobRef.setPjCurrencyCd1("0");
					psJobRef.setPjPayUnionFee("0");
					psJobRef.setPjUnionCd("0");
					psJobRef.setPjBargUnit("0");
					psJobRef.setPjUnionSeniorityDt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjLaborAgreement("0");
					psJobRef.setPjEmplCtg("0");
					psJobRef.setPjEmplCtgL1("0");
					psJobRef.setPjEmplCtgL2("0");
					psJobRef.setPjSetidLbrAgrmnt("0");
					psJobRef.setPjWppStopFlag("0");
					psJobRef.setPjLaborFacilityId("0");
					psJobRef.setPjLbrFacEntryDt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjLayoffExemptFlag("0");
					psJobRef.setPjLayoffExemptRsn("0");
					psJobRef.setPjGpPaygroup("0");
					psJobRef.setPjGpDfltEligGrp("0");
					psJobRef.setPjGpEligGrp("0");
					psJobRef.setPjGpDfltCurrttyp("0");
					psJobRef.setPjCurRtType("0");
					psJobRef.setPjGpDfltExrtdt("0");
					psJobRef.setPjGpAsofDtExgRt("0");
					psJobRef.setPjAddsToFteActual("0");
					psJobRef.setPjClassIndc("0");
					psJobRef.setPjEncumbOverride("0");
					psJobRef.setPjFicaStatusEe("0");
					psJobRef.setPjFte(BigDecimal.ONE);
					psJobRef.setPjProrateCntAmt("0");
					psJobRef.setPjPaySystemFlg("0");
					psJobRef.setPjBorderWalker("0");
					psJobRef.setPjLumpSumPay("0");
					psJobRef.setPjContractNum("0");
					psJobRef.setPjJobIndicator("0");
					psJobRef.setPjWrksCnclRoleChe("0");
					psJobRef.setPjWorkDayHours(BigDecimal.ONE);
					psJobRef.setPjReportsTo("0");
					psJobRef.setPjJobDataSrcCd("0");
					psJobRef.setPjEstabid("0");
					psJobRef.setPjSupvLvlId("0");
					psJobRef.setPjSetidSupvLvl("0");
					psJobRef.setPjAbsenceSystemCd("0");
					psJobRef.setPjPoiType("0");
					psJobRef.setPjLdwOvr("0");
					psJobRef.setPjAutoEndFlg("0");
					psJobRef.setPjLastupdoprid("0");
					// Field Not available0
					psJobRef.setPjCompany(personDetails.getCompanyId());
					psJobRef.setPjActionReason(personDetails.getEntryReason());
					psJobRef.setPjDeptid(personDetails.getDepartmentId());
					psJobRef.setPjFullPartTime(personDetails.getEmplType());
					psJobRef.setPjEntryDate(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psJobRef.setPjEmplType(personDetails.getEmplType());
					psJobRef.setPjHolidaySchedule(personDetails.getFloatingHolidays());
					psJobRef.setPjRegTemp(personDetails.getEmpReqTemp());
					psJobRef.setPjEmplClass(personDetails.getEmpClass());
					psJobRef.setPjAnnlBenefBaseRt(new BigDecimal(personDetails.getAnnual_wages()));
					psJobRef.setPjStdHours(new BigDecimal(personDetails.getStdHours()));
					psJobRef.setPjBasGroupId(personDetails.getBussinessTitle());
					psJobRef.setPjJobcode(personDetails.getJobCode());
					psJobRef.setPjFlsaStatus(personDetails.getFlsaStatus());
					psJobRef.setPjSupervisorId(personDetails.getSupervisorId());
					psJobRef.setPjCompFrequency(personDetails.getCompensationBasicId());
					psJobRef.setPjCtgRate(new BigDecimal(personDetails.getCompensationRate()));
					psJobRef.setPjDeptid(personDetails.getDepartmentId());
					psJobRef.setPjLocation(personDetails.getWorkLocationId());
					psJobRef.setPjPaygroup(personDetails.getPayGroupsIds());
					psJobRef.setPjBenefitSystem(personDetails.getBenefitClassId());
					psJobRef.setPjGrpA(personDetails.getLevel());
					psJobRef.setPjGrpB(personDetails.getSponsor());
					psJobRef.setVacation(personDetails.getVacation());
					psJobRef.setSick(personDetails.getSick());
					psJobRef.setPersonalTime(personDetails.getPersonalTime());
					psJobRef.setFloatingHolidays(personDetails.getFloatingHolidays());
					psJobRef.setBasedOnJobDuties(personDetails.getIsBased());
					psJobRef.setVariableHour(personDetails.getEmpVariableHour());
					psJobRef.setDutiesDesc(personDetails.getEmpJobDes());
					psJobRef.setPsPersonRef(psPersonRef);
					psJobRefs.add(psJobRef);
					psPersonRef.setPsJobRefs(psJobRefs);
					// PsPersonNidRef
					PsPersonNidRef psPersonNidRef = new PsPersonNidRef();
					List<PsPersonNidRef> psPersonNidRefs = new ArrayList<PsPersonNidRef>();
					
					psPersonNidRef.setPniNationalIdType("0");
					psPersonNidRef.setPniNationalId(personDetails.getNationalId());
					psPersonNidRef.setPniCountry(personDetails.getCountryId());
					psPersonNidRef.setPniTaxRefIdSgp("s");
					psPersonNidRef.setPniLastUpdoprid(0);
					psPersonNidRef.setPniLastupdoprid("0");
					psPersonNidRef.setPniLastUpddtm(new Timestamp(0));
					psPersonNidRef.setPniLastupddttm(new Timestamp(0));
					psPersonNidRef.setPsPersonRef(psPersonRef);
					psPersonNidRefs.add(psPersonNidRef);
					psPersonRef.setPsPersonNidRefs(psPersonNidRefs);
					// PsPersonNamesRef
					PsPersonNamesRef psPersonNamesRef = new PsPersonNamesRef();
					List<PsPersonNamesRef> psPersonNamesRefs = new ArrayList<PsPersonNamesRef>();
					psPersonNamesRef.setPnFirstName(personDetails.getFirstName());
					psPersonNamesRef.setPnLastName(personDetails.getLastName());
					psPersonNamesRef.setPnMiddleName(personDetails.getMiddleName());
					psPersonNamesRef.setPnNamePrefix("0");
					psPersonNamesRef.setPnNameType("0");
					psPersonNamesRef.setPnCountryNmFormat(personDetails.getCountryId());
					psPersonNamesRef.setPnNameSuffix("0");
					psPersonNamesRef.setPnEffdt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psPersonNamesRef.setPnEffStatus(true);
					psPersonNamesRef.setPnFirstNameSrch("0");
					psPersonNamesRef.setPnLastNamePrefNld("0");
					psPersonNamesRef.setPnLastNameSrch("0");
					psPersonNamesRef.setPnLastUpdoprid("0");
					psPersonNamesRef.setPnName("0");
					psPersonNamesRef.setPnNameAc("0");
					psPersonNamesRef.setPnNameDisplay("0");
					psPersonNamesRef.setPnNameDisplaySrch("0");
					psPersonNamesRef.setPnNameFormal("0");
					psPersonNamesRef.setPnNameInitials("0");
					psPersonNamesRef.setPnNameRoyalPrefix("0");
					psPersonNamesRef.setPnNameRoyalSuffix("0");
					psPersonNamesRef.setPnNameTittle("0");
					psPersonNamesRef.setPnPartnerLastName("0");
					psPersonNamesRef.setPnPartnerRoyPrefix("0");
					psPersonNamesRef.setPnPrefFirstName("0");
					psPersonNamesRef.setPnSecondLastName("0");
					psPersonNamesRef.setPnSecondLastSrch("0");
					psPersonNamesRef.setPsPersonRef(psPersonRef);
					psPersonNamesRefs.add(psPersonNamesRef);
					psPersonRef.setPsPersonNamesRefs(psPersonNamesRefs);
					// PsPersonAddressesRef
					PsPersonAddressesRef psPersonAddressesRef = new PsPersonAddressesRef();
					List<PsPersonAddressesRef> psPersonAddressesRefs = new ArrayList<PsPersonAddressesRef>();
					psPersonAddressesRef.setPaAddressType(personDetails.getFormOfAddressId());
					psPersonAddressesRef.setPaAddress1(personDetails.getAddress1());
					psPersonAddressesRef.setPaAddress2(personDetails.getAddress2());
					psPersonAddressesRef.setPaAddress3("0");
					psPersonAddressesRef.setPaAddress4("0");
					psPersonAddressesRef.setPaCountry(personDetails.getCountryId());
					psPersonAddressesRef.setPaCity(personDetails.getCity());
					psPersonAddressesRef.setPaState(personDetails.getStateId());
					psPersonAddressesRef.setPaPostal(personDetails.getPostalCode());
					psPersonAddressesRef.setPaEffdt(DateUtils.convertStringToDate(personDetails.getEntryDate()));
					psPersonAddressesRef.setPaAddress1Ac("0");
					psPersonAddressesRef.setPaAddress2Ac("0");
					psPersonAddressesRef.setPaAddress3Ac("0");
					psPersonAddressesRef.setPaAddrField1("0");
					psPersonAddressesRef.setPaAddrField2("0");
					psPersonAddressesRef.setPaAddrField3("0");
					psPersonAddressesRef.setPaCityAc("0");
					psPersonAddressesRef.setPaEffStatus(true);
					psPersonAddressesRef.setPaCounty(personDetails.getCountry());
					psPersonAddressesRef.setPaGeoCode("0");
					psPersonAddressesRef.setPaHouseType("0");
					psPersonAddressesRef.setPaInCityLimit("0");
					psPersonAddressesRef.setPaLastupddtm(new Timestamp(0));
					psPersonAddressesRef.setPaLastUpddtm(new Timestamp(0));
					psPersonAddressesRef.setPaLastupdoprid("0");
					psPersonAddressesRef.setPaLastUpdoprid("0");
					psPersonAddressesRef.setPaNum1("0");
					psPersonAddressesRef.setPaNum2("0");
					psPersonAddressesRef.setPaRegRegion("0");
					psPersonAddressesRef.setPsPersonRef(psPersonRef);
					psPersonAddressesRefs.add(psPersonAddressesRef);
					psPersonRef.setPsPersonAddressesRefs(psPersonAddressesRefs);
					// PsPersonPhoneRef
					PsPersonPhoneRef psPersonPhoneRef = new PsPersonPhoneRef();
					List<PsPersonPhoneRef> psPersonPhoneRefs = new ArrayList<PsPersonPhoneRef>();
					psPersonPhoneRef.setPpPhone(personDetails.getHomePhone());
					psPersonPhoneRef.setPpPhoneType("HOME");
					psPersonPhoneRef.setPsPersonRef(psPersonRef);
					psPersonPhoneRefs.add(psPersonPhoneRef);
					psPersonRef.setPsPersonPhoneRefs(psPersonPhoneRefs);
					// PsPersonalDataRef
					PsPersonalDataRef psPersonalDataRef = new PsPersonalDataRef();
					psPersonalDataRef.setPpdSex(personDetails.getGenderId());
					psPersonalDataRef.setPpdEthinicity(personDetails.getEthnicityId());
					psPersonalDataRef.setPpdMilitaryStatus(personDetails.getMilitryStatus());
					psPersonalDataRef.setPpdAlterEmplid(personDetails.getAlternateEmpId());
					psPersonalDataRef.setPpdCountryNmFormat(personDetails.getCompanyId());
					psPersonalDataRef.setPpdName("0");
					psPersonalDataRef.setPpdNameInitials("0");
					psPersonalDataRef.setPpdNamePrefix("0");
					psPersonalDataRef.setPpdNameSuffix("0");
					psPersonalDataRef.setPpdNameRoyalPrefix("0");
					psPersonalDataRef.setPpdNameRoyalSuffix("0");
					psPersonalDataRef.setPpdNameTitle("0");
					psPersonalDataRef.setPpdFirstName("0");
					psPersonalDataRef.setPpdLastNameSrch("0");
					psPersonalDataRef.setPpdFirstNameSrch("0");
					psPersonalDataRef.setPpdLastName("0");
					psPersonalDataRef.setPpdLastName("0");
					psPersonalDataRef.setPpdMiddleName("0");
					psPersonalDataRef.setPpdSecondLastName("0");
					psPersonalDataRef.setPpdSecondLastSrch("0");
					psPersonalDataRef.setPpdNameAc("0");
					psPersonalDataRef.setPpdPrefFirstName("0");
					psPersonalDataRef.setPpdPartnerLastName("0");
					psPersonalDataRef.setPpdPartnerRoyPrefix("0");
					psPersonalDataRef.setPpdLastNamePrefNld("0");
					psPersonalDataRef.setPpdNameDisplay("0");
					psPersonalDataRef.setPpdNameFormal("0");
					psPersonalDataRef.setPpdCountry(personDetails.getCountryId());
					psPersonalDataRef.setPpdAddress1("0");
					psPersonalDataRef.setPpdAddress2("0");
					psPersonalDataRef.setPpdAddress3("0");
					psPersonalDataRef.setPpdAddress4("0");
					psPersonalDataRef.setPpdCity(personDetails.getCity());
					psPersonalDataRef.setPpdNum1("0");
					psPersonalDataRef.setPpdNum2("0");
					psPersonalDataRef.setPpdHouseType("0");
					psPersonalDataRef.setPpdAddrField1("0");
					psPersonalDataRef.setPpdAddrField2("0");
					psPersonalDataRef.setPpdAddrField3("0");
					psPersonalDataRef.setPpdCounty(personDetails.getCountry());
					psPersonalDataRef.setPpdState(personDetails.getStateId());
					psPersonalDataRef.setPpdPostal("0");
					psPersonalDataRef.setPpdGeoCode("0");
					psPersonalDataRef.setPpdInCityLimit("0");
					psPersonalDataRef.setPpdMarStatus("0");
					psPersonalDataRef.setPpdMarStatusDt(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdBirthdate(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdBirthplace("0");
					psPersonalDataRef.setPpdBirthcountry("0");
					psPersonalDataRef.setPpdBirthstate("0");
					psPersonalDataRef.setPpdDtOfDeath(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdHighestEducLvl("0");
					psPersonalDataRef.setPpdFtStudent("0");
					psPersonalDataRef.setPpdLangCd("0");
					psPersonalDataRef.setPpdAddress1Ac("0");
					psPersonalDataRef.setPpdAddress2Ac("0");
					psPersonalDataRef.setPpdAddress3Ac("0");
					psPersonalDataRef.setPpdCityAc("0");
					psPersonalDataRef.setPpdCountryOther("0");
					psPersonalDataRef.setPpdAddress1Other("0");
					psPersonalDataRef.setPpdAddress2Other("0");
					psPersonalDataRef.setPpdAddress3Other("0");
					psPersonalDataRef.setPpdAddress4Other("0");
					psPersonalDataRef.setPpdCityOther("0");
					psPersonalDataRef.setPpdCountryOther("0");
					psPersonalDataRef.setPpdStateOther("0");
					psPersonalDataRef.setPpdPostalOther("0");
					psPersonalDataRef.setPpdNum1Other("0");
					psPersonalDataRef.setPpdNum2Other("0");
					psPersonalDataRef.setPpdHouseTypeOther("0");
					psPersonalDataRef.setPpdAddrField1Other("0");
					psPersonalDataRef.setPpdAddrField2Other("0");
					psPersonalDataRef.setPpdAddrField3Other("0");
					psPersonalDataRef.setPpdInCityLmtOther("0");
					psPersonalDataRef.setPpdGeoCodeOther("0");
					psPersonalDataRef.setPpdCountryCode("0");
					psPersonalDataRef.setPpdPhone("0");
					psPersonalDataRef.setPpdExtension("0");
					psPersonalDataRef.setPpdVaBenefit("0");
					psPersonalDataRef.setPpdCampusId("0");
					psPersonalDataRef.setPpdDeathCertifNbr("0");
					psPersonalDataRef.setPpdFerpa("0");
					psPersonalDataRef.setPpdPlaceOfDeath("0");
					psPersonalDataRef.setPpdUsWorkEligibilty("0");
					psPersonalDataRef.setPpdCitizenProof1("0");
					psPersonalDataRef.setPpdCitizenProof2("0");
					psPersonalDataRef.setPpdMedicareEntldDt(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdHonsekiJpn("0");
					psPersonalDataRef.setPpdMilitaryStatIta("0");
					psPersonalDataRef.setPpdMilitaryTypeIta("0");
					psPersonalDataRef.setPpdMilitaryRankIta("0");
					psPersonalDataRef.setPpdMilitaryEndIta(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdEntryDtFra(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdMilitSituatnFra("0");
					psPersonalDataRef.setPpdCpamid("0");
					psPersonalDataRef.setPpdBilingualismCode("0");
					psPersonalDataRef.setPpdHealthCareNbr("0");
					psPersonalDataRef.setPpdHealthCareState("0");
					psPersonalDataRef.setPpdMilitSituatnEsp("0");
					psPersonalDataRef.setPpdSocSecAffDt(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdMilitaryStatGer("0");
					psPersonalDataRef
							.setPpdExpctdMilitaryDt(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdHrResponsibleId("0");
					psPersonalDataRef.setPpdSmoker("0");
					psPersonalDataRef.setPpdSmokerDt(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdGvtCredMilSvce("0");
					psPersonalDataRef.setPpdGvtMilitaryComp("0");
					psPersonalDataRef.setPpdGvtMilGrade("0");
					psPersonalDataRef.setPpdGvtMilResrveCat("0");
					psPersonalDataRef.setPpdGvtMilSepRet("0");
					psPersonalDataRef.setPpdGvtMilSvceEnd(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdGvtMilSvceStart(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdGvtMilVerify("0");
					psPersonalDataRef.setPpdGvtParNbrLast(new BigDecimal("0"));
					psPersonalDataRef.setPpdGvtUnifSvcCtr("0");
					psPersonalDataRef.setPpdGvtVetPrefAppt("0");
					psPersonalDataRef.setPpdGvtVetPrefRif("0");
					psPersonalDataRef.setPpdGvtChangeFlag("0");
					psPersonalDataRef.setPpdGvtDraftStatus("0");
					psPersonalDataRef.setPpdGvtYrAttained(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdDisabledVet("0");
					psPersonalDataRef.setPpdDisabled("0");
					psPersonalDataRef.setPpdGvtDisabilityCd("0");
					psPersonalDataRef.setPpdGrade("0");
					psPersonalDataRef.setPpdSalAdminPlan("0");
					psPersonalDataRef.setPpdGvtCurrAgcyEmpl("0");
					psPersonalDataRef.setPpdGvtCurrFedEmpl("0");
					psPersonalDataRef.setPpdGvtHighPayPlan("0");
					psPersonalDataRef.setPpdGvtHighGrade("0");
					psPersonalDataRef.setPpdGvtPrevAgcyEmpl("0");
					psPersonalDataRef.setPpdGvtPrevFedEmpl("0");
					psPersonalDataRef.setPpdGvtSepIncentive("0");
					psPersonalDataRef.setPpdGvtSepIncentDt(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdGvtTenure("0");
					psPersonalDataRef.setPpdGvtPayPlan("0");
					psPersonalDataRef.setPpdBargUnit("0");
					psPersonalDataRef.setPpdEffdt(DateUtils.convertStringToDate(CommonConstants.defaultDate));
					psPersonalDataRef.setPpdCountyOther("0");
					psPersonalDataRef.setPsPersonRef(psPersonRef);
					psPersonRef.setPsPersonalDataRef(psPersonalDataRef);
					// PsPersonEmailRef
					
					List<PsPersonEmailRef> psPersonEmailRefs = new ArrayList<PsPersonEmailRef>();
					if (personDetails.getHomeEmail() != null) {
						PsPersonEmailRef psPersonEmailRef = new PsPersonEmailRef();
						System.out.println("Home Flag");
						psPersonEmailRef.setPeEmailAddr(personDetails.getHomeEmail());
						psPersonEmailRef.setPeEAddrType("HOME");
						psPersonEmailRef.setPePrefEmailFlag(true);
						psPersonEmailRef.setPsPersonRef(psPersonRef);
						psPersonEmailRefs.add(psPersonEmailRef);
					}
					
					if (personDetails.getWokfEmailAddress() != null) {
						PsPersonEmailRef psPersonEmailRef = new PsPersonEmailRef();
						System.out.println("Work Flag");
						psPersonEmailRef.setPeEmailAddr(personDetails.getWokfEmailAddress());
						psPersonEmailRef.setPeEAddrType("WORK");
						psPersonEmailRef.setPePrefEmailFlag(true);
						psPersonEmailRef.setPsPersonRef(psPersonRef);
						psPersonEmailRefs.add(psPersonEmailRef);
					}
					
					psPersonRef.setPsPersonEmailRefs(psPersonEmailRefs);
				}
			
			
			LOGGER.debug("importData if END");
			return psPersonRef;
		} else {
			LOGGER.debug("importData ELSE END");
			return psPersonRef;
		}

	}

}
