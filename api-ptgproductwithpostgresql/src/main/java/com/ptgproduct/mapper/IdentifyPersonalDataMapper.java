/**
 * Program Name: IdentifyPersonalDataMapper 
 *                                                                 
 * Program Description / functionality: This is mapper class for identify to import and export data for psjobref and pspersonnid tables
 *                            
 * Modules Impacted: Identify
 *                                                                    
 * Tables affected:  ps_person_nid_ref                                                                 
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Bindu       29/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.mapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.domain.IdentifyPersonalData;
import com.ptgproduct.entity.PsPersonRef;
import com.ptgproduct.entity.PsJobRef;
import com.ptgproduct.entity.PsPersonNidRef;

public class IdentifyPersonalDataMapper {
private static final Logger LOGGER = LoggerFactory.getLogger(IdentifyPersonalDataMapper.class);
	
	/**
	 *
	 * Purpose: This method is to import data for identify module
	 * 
	 * @param psPersonRef
	 * 
	 * @return: List<IdentifyPersonalData>
	 */
	public static List<IdentifyPersonalData> importData(PsPersonRef psPersonRef) {
		LOGGER.debug("import Identify Personal Data begin");
		List<IdentifyPersonalData> identifyList=new ArrayList<IdentifyPersonalData>();
		if(psPersonRef!=null){
			List<PsPersonNidRef> psPersonNidRefs=psPersonRef.getPsPersonNidRefs();
			List<PsJobRef> psJobRefs=psPersonRef.getPsJobRefs();
			for(PsPersonNidRef psPersonNidRef:psPersonNidRefs){
				for(PsJobRef psJobRef:psJobRefs){
					IdentifyPersonalData identifyPersonalData=new IdentifyPersonalData();
					identifyPersonalData.setEmployeeStatus(psJobRef.getPjEmplStatus());
					identifyPersonalData.setSocSecUsId(psPersonNidRef.getPniNationalId());
					identifyPersonalData.setEmployeeId(psPersonRef.getPEmplid());
					identifyPersonalData.setAlternateEmpid("0");
					identifyPersonalData.setCitizenshipStatus("0");
					identifyList.add(identifyPersonalData);
				}
			}
		}
		LOGGER.debug("import Identify Personal Data end");
		return identifyList;
	}

	/**
	 *
	 * Purpose: This method is to export data for identify module
	 * 
	 * @param psPersonRef
	 * @param identifyPersonalData
	 * 
	 * @return: PsPersonRef
	 */
	public static PsPersonRef exportData(PsPersonRef psPersonRef, IdentifyPersonalData identifyPersonalData) {
		LOGGER.debug("export Person Identify Data begin");
		if (identifyPersonalData.getEmployeeId()!= null) {
			List<PsJobRef> psJobRefs=psPersonRef.getPsJobRefs();
			for(PsJobRef psJobRef:psJobRefs){
				List<PsPersonNidRef> psPersonNidRefs =psPersonRef.getPsPersonNidRefs();
				PsPersonNidRef personNidRef = null;
				for(PsPersonNidRef psPersonNidRef:psPersonNidRefs){
					if(identifyPersonalData.getAlternateEmpid().equalsIgnoreCase("0") && identifyPersonalData.getCitizenshipStatus().equalsIgnoreCase("0") && identifyPersonalData.getEmployeeStatus().equalsIgnoreCase(psJobRef.getPjEmplStatus())){
						personNidRef=psPersonNidRef;
						personNidRef.setPniNationalId(identifyPersonalData.getSocSecUsId());
					}
				}
				personNidRef.setPsPersonRef(psPersonRef);
				psPersonRef.setPsPersonNidRefs(psPersonNidRefs);
		}
			}
		LOGGER.debug("export Person Identify Data end");
		return psPersonRef;  
	}
}
