/**
 * Program Name: PersonNameMapper 
 *                                                                 
 * Program Description / functionality: This class is to import data from PsPersonRef to PersonName and export 
 * 										data from PersonName to PsPersonRef
 *                            
 * Modules Impacted: person names module
 *                                                                    
 * Tables affected:  ps_person_names_ref                                                                    
 *                                                                                                         
 * Developer            Created             /Modified Date       Purpose
  *******************************************************************************
 * Pavani & kiran       23/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.mapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.common.DateUtils;
import com.ptgproduct.domain.PersonName;
import com.ptgproduct.entity.PsPersonNamesRef;
import com.ptgproduct.entity.PsPersonRef;
import com.ptgproduct.exception.PtgproductParseException;

public class PersonNameMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonNameMapper.class);

	/**
	 *
	 * Purpose: This method is to import data from PsPersonRef to PersonName
	 * 
	 * @param psPersonRef
	 * 
	 * @return: List<PersonName>
	 */
	public static List<PersonName> importPersonName(PsPersonRef psPersonRef) {
		LOGGER.debug("import person name begin");
		List<PersonName> personNameList = null;
		if (psPersonRef.getPsPersonNamesRefs() != null && psPersonRef.getPsPersonNamesRefs().size() > 0) {
			personNameList = new ArrayList<PersonName>();
			for (PsPersonNamesRef personNamesRef : psPersonRef.getPsPersonNamesRefs()) {
				PersonName personName = new PersonName();
				personName.setPemplId(psPersonRef.getPEmplid());
				personName.setEffectiveDate(DateUtils.convertDateToString(personNamesRef.getPnEffdt()).toString());
				personName.setFirstName(personNamesRef.getPnFirstName());
				personName.setFormOfAddress(personNamesRef.getPnNameTittle());
				personName.setLastName(personNamesRef.getPnLastName());
				personName.setMiddleName(personNamesRef.getPnMiddleName());
				personName.setNameType(personNamesRef.getPnNameType());
				personName.setSuffix(personNamesRef.getPnNameSuffix());
				personNameList.add(personName);
			}
			LOGGER.debug("import person name end");
			return personNameList;
		} else {
			LOGGER.debug("import person name end");
			return personNameList;
		}
	}

	/**
	 *
	 * Purpose: This method is to export data from PersonName to PsPersonRef
	 * 
	 * @param psPersonRef
	 * @param personName
	 * 
	 * @return: PsPersonRef
	 */
	public static PsPersonRef exportPersonName(PsPersonRef psPersonRef, PersonName personName)
			throws PtgproductParseException {
		LOGGER.debug("exportPersonName begin");
		if (personName != null) {
			PsPersonNamesRef psPersonNamesRef = null;
			for (PsPersonNamesRef psPersonNamesRefObject : psPersonRef.getPsPersonNamesRefs()) {
				if (personName.getNameType().equalsIgnoreCase(psPersonNamesRefObject.getPnNameType())
						&& personName.getEffectiveDate()
								.equalsIgnoreCase(DateUtils.convertDateToString(psPersonNamesRefObject.getPnEffdt()))) {
					psPersonNamesRef = psPersonNamesRefObject;
					psPersonNamesRef.setPnFirstName(personName.getFirstName());
					psPersonNamesRef.setPnLastName(personName.getLastName());
					psPersonNamesRef.setPnMiddleName(personName.getMiddleName());
					psPersonNamesRef.setPnNameSuffix(personName.getSuffix());
					psPersonNamesRef.setPnNameTittle(personName.getFormOfAddress());
					LOGGER.debug("exportPersonName end");
				} else {
					psPersonNamesRef = new PsPersonNamesRef();
					psPersonNamesRef.setPnEffdt(DateUtils.convertStringToDate(personName.getEffectiveDate()));
					psPersonNamesRef.setPnNameType(personName.getNameType());
					psPersonNamesRef.setPnFirstName(personName.getFirstName());
					psPersonNamesRef.setPnLastName(personName.getLastName());
					psPersonNamesRef.setPnMiddleName(personName.getMiddleName());
					psPersonNamesRef.setPnNameSuffix(personName.getSuffix());
					psPersonNamesRef.setPnNameTittle(personName.getFormOfAddress());
					LOGGER.debug("exportPersonName end");
				}
			}
			return psPersonRef;
		} else {
			return psPersonRef;
		}
	}

	/**
	 *
	 * Purpose: This method is to export data from PersonName to PsPersonRef
	 * 
	 * @param psPersonRef
	 * @param personName
	 * 
	 * @return: PsPersonRef
	 */
	public static PsPersonRef exportUpdatePersonName(PsPersonRef psPersonRef, PersonName personName)
			throws PtgproductParseException {
		LOGGER.debug("exportPersonName begin");
		if (personName != null) {
			PsPersonNamesRef updatePersonNames = null;
			for (PsPersonNamesRef psPersonNamesRefObject : psPersonRef.getPsPersonNamesRefs()) {
				if (personName.getNameType().equalsIgnoreCase(psPersonNamesRefObject.getPnNameType())
						&& personName.getEffectiveDate()
								.equalsIgnoreCase(DateUtils.convertDateToString(psPersonNamesRefObject.getPnEffdt()))) {
					updatePersonNames = psPersonNamesRefObject;
					updatePersonNames.setPnFirstName(personName.getFirstName());
					updatePersonNames.setPnLastName(personName.getLastName());
					updatePersonNames.setPnMiddleName(personName.getMiddleName());
					updatePersonNames.setPnNameSuffix(personName.getSuffix());
					updatePersonNames.setPnNameTittle(personName.getFormOfAddress());
				}
			}
			psPersonRef.addPsPersonNamesRef(updatePersonNames);
			return psPersonRef;
		} else {
			LOGGER.debug("exportPersonName end");
			return psPersonRef;
		}
	}

	/**
	 *
	 * Purpose: This method is to import data from PersonName to PsPersonRef
	 * 
	 * @param psPersonRef
	 * @param personNameCheck
	 * 
	 * @return: PersonName
	 */
	public static PersonName importUpdatedPersonName(PsPersonRef psPersonRef, PersonName personNameCheck) {
		LOGGER.debug("import UpdatedPersonName begin");
		PersonName personName = new PersonName();
		if (psPersonRef.getPsPersonNamesRefs() != null && psPersonRef.getPsPersonNamesRefs().size() > 0) {

			for (PsPersonNamesRef psPersonNamesRefObject : psPersonRef.getPsPersonNamesRefs()) {
				if (personNameCheck.getNameType().equalsIgnoreCase(psPersonNamesRefObject.getPnNameType())
						&& personNameCheck.getEffectiveDate()
								.equalsIgnoreCase(DateUtils.convertDateToString(psPersonNamesRefObject.getPnEffdt()))) {
					personName = new PersonName();
					personName.setNameType(psPersonNamesRefObject.getPnNameType());
					personName.setEffectiveDate(
							DateUtils.convertDateToString(psPersonNamesRefObject.getPnEffdt()).toString());
				} else {
					personName.setEffectiveDate(null);
					personName.setNameType(null);
				}
				personName.setPemplId(psPersonRef.getPEmplid());
				personName.setFirstName(psPersonNamesRefObject.getPnFirstName());
				personName.setFormOfAddress(psPersonNamesRefObject.getPnNameTittle());
				personName.setLastName(psPersonNamesRefObject.getPnLastName());
				personName.setMiddleName(psPersonNamesRefObject.getPnMiddleName());
				personName.setSuffix(psPersonNamesRefObject.getPnNameSuffix());
				LOGGER.debug("import person name end");
			}
			return personName;
		} else {
			LOGGER.debug("importUpdatedPersonName end");
			return personName;
		}
	}
}