/**
 * Program Name: PersonAddressMapper 
 *                                                                 
 * Program Description / functionality: This is mapper class for person address to import and export data for pspersonaddress table
 *                            
 * Modules Impacted: Person Address
 *                                                                    
 * Tables affected:  ps_person_addresses_ref                                                                  
 *                                                                                                         
 * Developer         Created             /Modified Date       Purpose
  *******************************************************************************
 * Bindu&Rakesh       23/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.mapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.common.DateUtils;
import com.ptgproduct.domain.PersonAddress;
import com.ptgproduct.entity.PsPersonAddressesRef;
import com.ptgproduct.entity.PsPersonRef;
import com.ptgproduct.exception.PtgproductParseException;


public class PersonAddressMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonAddressMapper.class);
	
	/**
	   *
	   * Purpose: This method is to import data for person address module
	   * 
	   * @param psPersonRef
	   * 
	   * @return: List<PersonAddress>
	   */
	public static List<PersonAddress> importData(PsPersonRef psPersonRef){
		LOGGER.debug("import Address Data begin");
		List<PersonAddress> personAddressList=new ArrayList<PersonAddress>();
		if(psPersonRef!=null){
			List<PsPersonAddressesRef> psPersonAddressesRefs=psPersonRef.getPsPersonAddressesRefs();
			for (PsPersonAddressesRef psPersonAddressesRef : psPersonAddressesRefs) {
				PersonAddress personAddress=new PersonAddress();
				personAddress.setPaAddress1(psPersonAddressesRef.getPaAddress1());
				personAddress.setPaAddress2(psPersonAddressesRef.getPaAddress2());
				personAddress.setPaCity(psPersonAddressesRef.getPaCity());
				personAddress.setPaCountry(psPersonAddressesRef.getPaCountry());
				personAddress.setPaState(psPersonAddressesRef.getPaState());
				personAddress.setPaPostal(psPersonAddressesRef.getPaPostal());
				personAddress.setPaAddressType(psPersonAddressesRef.getPaAddressType());
				personAddress.setPaEffdt(psPersonAddressesRef.getPaEffdt().toString());
				personAddress.setpEmplid(psPersonRef.getPEmplid());
				personAddressList.add(personAddress);
			}
		}
			LOGGER.debug("import Address Data end");
			return personAddressList;
		}
		
	/**
	   *
	   * Purpose: This method is to export data for person address module
	   * 
	   * @param psPersonRef
	   * @param personAddress
	   * @param pEmplid
	   * 
	   * @return: PsPersonRef
	   */
	public static PsPersonRef exportData(PsPersonRef psPersonRef,PersonAddress personAddress,String pEmplid){
		LOGGER.debug("export Address Data begin");
		if (pEmplid != null) {
			try{
			List<PsPersonAddressesRef> personAddressRefs=psPersonRef.getPsPersonAddressesRefs();
			PsPersonAddressesRef addressRef=null;
			for(PsPersonAddressesRef personAddressRef:personAddressRefs){
				if(personAddress.getPaAddressType().equalsIgnoreCase(personAddressRef.getPaAddressType())){
					addressRef=personAddressRef;
					addressRef.setPaAddressType(personAddress.getPaAddressType());
					addressRef.setPaAddress1(personAddress.getPaAddress1());
					addressRef.setPaAddress2(personAddress.getPaAddress2());
					addressRef.setPaCity(personAddress.getPaCity());
					addressRef.setPaCountry(personAddress.getPaCountry());
					addressRef.setPaPostal(personAddress.getPaPostal());
					addressRef.setPaState(personAddress.getPaState());
					addressRef.setPaEffdt(DateUtils.convertStringToDate(personAddress.getPaEffdt()));
				}
			}
			addressRef.setPsPersonRef(psPersonRef);
			psPersonRef.setPsPersonAddressesRefs(personAddressRefs);
		}catch (PtgproductParseException e) {
			e.printStackTrace();
		}
	}
		LOGGER.debug("export Address Data end");
		return psPersonRef;   
	}	
}
			