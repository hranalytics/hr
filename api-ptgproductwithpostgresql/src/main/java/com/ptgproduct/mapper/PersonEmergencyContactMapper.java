/**
 * Program Name: PersonEmergencyContactMapper 
 *                                                                 
 * Program Description / functionality: This is mapper class for Emergency Contact Details Services
 *                            
 * Modules Impacted: Person phone Details Services
 *                                                                    
 * Tables affected: Ps_Person_Phone_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Harish      28/12/2016  
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.mapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.domain.PersonEmergencyContactDetails;
import com.ptgproduct.entity.PsEmergencyCntctRef;
import com.ptgproduct.entity.PsPersonRef;

public class PersonEmergencyContactMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonEmergencyContactMapper.class);

	/**
	 *
	 * Purpose: This method is to get Person Emergency Contact Details
	 * 
	 * @param PsPersonRef
	 * 
	 * @return: List of PersonEmergencyContactDetails object
	 */
	public static List<PersonEmergencyContactDetails> importEmergencyContactData(PsPersonRef psPersonRef) {
		LOGGER.debug("Person Phone importPhoneData begin in PersonPhoneMapper");
		PersonEmergencyContactDetails emergencyContactDetails = null;
		List<PersonEmergencyContactDetails> emergencyContactDetailsList = new ArrayList<PersonEmergencyContactDetails>();
		if (psPersonRef != null) {
			List<PsEmergencyCntctRef> psEmergencyCntctRef = psPersonRef.getPsEmergencyCntctRef();
			for (PsEmergencyCntctRef PsEmergencyCntctRef : psEmergencyCntctRef) {
				emergencyContactDetails = new PersonEmergencyContactDetails();
				emergencyContactDetails.setEmplid(psPersonRef.getPEmplid());
				emergencyContactDetails.setContactName(PsEmergencyCntctRef.getPecContactName());
				emergencyContactDetails.setPrimaryContact(PsEmergencyCntctRef.getPecPrimaryContact());
				emergencyContactDetails.setRelationship(PsEmergencyCntctRef.getPecRelationship());
				emergencyContactDetails.setSameAddressEmpl(PsEmergencyCntctRef.getPecSameAddressEmpl());
				emergencyContactDetails.setCountry(PsEmergencyCntctRef.getPecCountry());
				emergencyContactDetails.setAddress1(PsEmergencyCntctRef.getPecAddress1());
				emergencyContactDetails.setAddress2(PsEmergencyCntctRef.getPecAddress2());
				emergencyContactDetails.setAddress3(PsEmergencyCntctRef.getPecAddress3());
				emergencyContactDetails.setCity(PsEmergencyCntctRef.getPecCity());
				emergencyContactDetails.setState(PsEmergencyCntctRef.getPecState());
				emergencyContactDetails.setPostal(PsEmergencyCntctRef.getPecPostal());
				emergencyContactDetails.setSamePhoneEmpl(PsEmergencyCntctRef.getPecSamePhoneEmpl());
				emergencyContactDetails.setPhone(PsEmergencyCntctRef.getPecPhone());
				emergencyContactDetails.setNum1(PsEmergencyCntctRef.getPecNum1());
				emergencyContactDetails.setNum2(PsEmergencyCntctRef.getPecNum2());
				emergencyContactDetailsList.add(emergencyContactDetails);
			}
			LOGGER.debug("Person Phone importData End in PersonPhoneMapper");
		}
		return emergencyContactDetailsList;
	}

	/**
	 *
	 * Purpose: This method is to post Person Emergency Contact Details
	 * 
	 * @param PsPersonRef
	 * @param PersonEmergencyContactDetails
	 * 
	 * @return: List of PersonEmergencyContactDetails object
	 */
	public static PsPersonRef exportPhoneData(PsPersonRef psPersonRef,
			PersonEmergencyContactDetails emergencyContactDetails) {
		LOGGER.debug("Person Emergency Contact export  begin");
		PsEmergencyCntctRef emergencyCntctRef = new PsEmergencyCntctRef();
		emergencyCntctRef.setPecContactName(emergencyContactDetails.getContactName());
		emergencyCntctRef.setPecPrimaryContact(emergencyContactDetails.getPrimaryContact());
		emergencyCntctRef.setPecRelationship(emergencyContactDetails.getRelationship());
		emergencyCntctRef.setPecSameAddressEmpl(emergencyContactDetails.getSameAddressEmpl());
		emergencyCntctRef.setPecCountry(emergencyContactDetails.getCountry());
		emergencyCntctRef.setPecAddrField1("0");
		emergencyCntctRef.setPecAddrField2("0");
		emergencyCntctRef.setPecAddrField3("0");
		emergencyCntctRef.setPecAddress1(emergencyContactDetails.getAddress1());
		emergencyCntctRef.setPecAddress2(emergencyContactDetails.getAddress2());
		emergencyCntctRef.setPecAddress3(emergencyContactDetails.getAddress3());
		emergencyCntctRef.setPecAddress4("0");
		emergencyCntctRef.setPecCity(emergencyContactDetails.getCity());
		emergencyCntctRef.setPecState(emergencyContactDetails.getState());
		emergencyCntctRef.setPecPostal(emergencyContactDetails.getPostal());
		emergencyCntctRef.setPecSamePhoneEmpl(emergencyContactDetails.getSamePhoneEmpl());
		emergencyCntctRef.setPecPhone(emergencyContactDetails.getPhone());
		emergencyCntctRef.setPecNum1(emergencyContactDetails.getNum1());
		emergencyCntctRef.setPecNum2(emergencyContactDetails.getNum2());
		emergencyCntctRef.setPecPhoneType("0");
		emergencyCntctRef.setPecCountryCode("0");
		emergencyCntctRef.setPecInCityLimit("0");
		emergencyCntctRef.setPecGeoCode("0");
		emergencyCntctRef.setPecHouseType("0");
		emergencyCntctRef.setPecCounty("0");
		emergencyCntctRef.setPecAddressType("0");
		emergencyCntctRef.setPecExtension("0");
		emergencyCntctRef.setPsPersonRef(psPersonRef);
		psPersonRef.addPsEmergencyCntctRef(emergencyCntctRef);
		LOGGER.debug("Person Emergency Contact export  end");
		return psPersonRef;
	}

	/**
	 *
	 * Purpose: This method is to post Person Emergency Contact Details
	 * 
	 * @param PsPersonRef
	 * @param PersonEmergencyContactDetails
	 * 
	 * @return: PsPersonRef object
	 */
	public static PsPersonRef exportEmergencyContactUpdate(PsPersonRef psPersonRef,
			PersonEmergencyContactDetails emergencyContactDetails) {
		LOGGER.debug("Person Phone exportUpdate begin");
		if (emergencyContactDetails.getEmplid() != null) {
			List<PsEmergencyCntctRef> psEmergencyCntctRef = psPersonRef.getPsEmergencyCntctRef();
			PsEmergencyCntctRef EmergencyCntctRef = null;
			for (PsEmergencyCntctRef psEmergencyCntctRefs : psEmergencyCntctRef) {
				if ((emergencyContactDetails.getContactName().equalsIgnoreCase(psEmergencyCntctRefs.getPecContactName())
						&& (emergencyContactDetails.getRelationship()
								.equalsIgnoreCase(psEmergencyCntctRefs.getPecRelationship())))) {
					EmergencyCntctRef = psEmergencyCntctRefs;
					EmergencyCntctRef.setPecContactName(emergencyContactDetails.getContactName());
					EmergencyCntctRef.setPecPrimaryContact(emergencyContactDetails.getPrimaryContact());
					EmergencyCntctRef.setPecRelationship(emergencyContactDetails.getRelationship());
					EmergencyCntctRef.setPecSameAddressEmpl(emergencyContactDetails.getSameAddressEmpl());
					EmergencyCntctRef.setPecCountry(emergencyContactDetails.getCountry());
					EmergencyCntctRef.setPecAddrField1("0");
					EmergencyCntctRef.setPecAddrField2("0");
					EmergencyCntctRef.setPecAddrField3("0");
					EmergencyCntctRef.setPecAddress1(emergencyContactDetails.getAddress1());
					EmergencyCntctRef.setPecAddress2(emergencyContactDetails.getAddress2());
					EmergencyCntctRef.setPecAddress3(emergencyContactDetails.getAddress3());
					EmergencyCntctRef.setPecAddress4("0");
					EmergencyCntctRef.setPecCity(emergencyContactDetails.getCity());
					EmergencyCntctRef.setPecState(emergencyContactDetails.getState());
					EmergencyCntctRef.setPecPostal(emergencyContactDetails.getPostal());
					EmergencyCntctRef.setPecSamePhoneEmpl(emergencyContactDetails.getSamePhoneEmpl());
					EmergencyCntctRef.setPecPhone(emergencyContactDetails.getPhone());
					EmergencyCntctRef.setPecNum1(emergencyContactDetails.getNum1());
					EmergencyCntctRef.setPecNum2(emergencyContactDetails.getNum2());
					EmergencyCntctRef.setPecPhoneType("0");
					EmergencyCntctRef.setPecCountryCode("0");
					EmergencyCntctRef.setPecInCityLimit("0");
					EmergencyCntctRef.setPecGeoCode("0");
					EmergencyCntctRef.setPecHouseType("0");
					EmergencyCntctRef.setPecCounty("0");
					EmergencyCntctRef.setPecAddressType("0");
					EmergencyCntctRef.setPecExtension("0");
					EmergencyCntctRef.setPsPersonRef(psPersonRef);
				}
			}
			EmergencyCntctRef.setPsPersonRef(psPersonRef);
			psPersonRef.setPsEmergencyCntctRef(psEmergencyCntctRef);
		}
		LOGGER.debug("Person Phone exportUpdate end");
		return psPersonRef;
	}
}