/**
 * Program Name: EmployeeOptionsMapper 
 *                                                                 
 * Program Description / functionality: This is mapper class for employee options to import and export data for employee options table
 *                            
 * Modules Impacted: Employee options
 *                                                                    
 * Tables affected:  ps_emp_options_trans                                                                   
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Bindu&Astha       04/01/2017 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.mapper;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ptgproduct.domain.EmployeeOptions;
import com.ptgproduct.entity.PsEmpOptionsRef;
import com.ptgproduct.entity.PsEmpOptionsTran;
import com.ptgproduct.entity.PsEmpOptionsTranPK;
import com.ptgproduct.entity.PsPersonRef;

public class EmployeeOptionsMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeOptionsMapper.class);
	
	/**
	   *
	   * Purpose: This method is to import data for employee options module
	   * 
	   * @param psPersonRef
	   * 
	   * @return: List<EmployeeOptions>
	   */
	public static List<EmployeeOptions> importData(PsPersonRef psPersonRef) {
		LOGGER.debug("import Employee Options begin");
		List<EmployeeOptions> employeeOptionsList = new ArrayList<EmployeeOptions>();
		if (psPersonRef != null) {
			if(psPersonRef.getPsEmpOptionsTrans()!=null && psPersonRef.getPsEmpOptionsTrans().size() >0){
				for (PsEmpOptionsTran psEmpOptionsTran : psPersonRef.getPsEmpOptionsTrans()) {
					EmployeeOptions employeeOptions = new EmployeeOptions();
					employeeOptions.setOptionEmplId(psPersonRef.getPEmplid());
					employeeOptions.setOptionId(psEmpOptionsTran.getPsEmpOptionsRef().getEoOpId());
					employeeOptions.setOptionName(psEmpOptionsTran.getPsEmpOptionsRef().getEoOptionsname());
					employeeOptions.setOptionDetails(psEmpOptionsTran.getPsEmpOptionsRef().getEoOptiondetails());
					employeeOptions.setOptionPreferences(psEmpOptionsTran.getEotPreferences());
					employeeOptionsList.add(employeeOptions);
			}
			}
			else{
				for (PsEmpOptionsTran psEmpOptionsTran : psPersonRef.getPsEmpOptionsTrans()) {
					EmployeeOptions employeeOptions = new EmployeeOptions();						
					employeeOptions.setOptionEmplId(psPersonRef.getPEmplid());
					employeeOptions.setOptionId(psEmpOptionsTran.getPsEmpOptionsRef().getEoOpId());
					employeeOptions.setOptionName(psEmpOptionsTran.getPsEmpOptionsRef().getEoOptionsname());
					employeeOptions.setOptionDetails(psEmpOptionsTran.getPsEmpOptionsRef().getEoOptiondetails());
					employeeOptions.setOptionPreferences(psEmpOptionsTran.getEotPreferences());
					employeeOptionsList.add(employeeOptions);	
			}
			}
		}
		LOGGER.debug("import Employee Options end");
		return employeeOptionsList;
	}
	
	/**
	   *
	   * Purpose: This method is to import default data for employee options module
	   * 
	   * @param empId
	   * @param empOptionsRefs
	   * 
	   * @return: List<EmployeeOptions>
	   */
	public static List<EmployeeOptions> importDefaultEmployeeOptions(String empId,List<PsEmpOptionsRef>  empOptionsRefs) {
		LOGGER.debug("import Employee Options begin");
		List<EmployeeOptions> employeeOptionsList = new ArrayList<EmployeeOptions>();
		if (empId != null) {
				for (PsEmpOptionsRef psEmpOptionsRef : empOptionsRefs) {
					EmployeeOptions employeeOptions = new EmployeeOptions();
					employeeOptions.setOptionEmplId(empId);
					employeeOptions.setOptionId(psEmpOptionsRef.getEoOpId());
					employeeOptions.setOptionName(psEmpOptionsRef.getEoOptionsname());
					employeeOptions.setOptionDetails(psEmpOptionsRef.getEoOptiondetails());
					employeeOptions.setOptionPreferences(psEmpOptionsRef.getEoPreferences());
					employeeOptionsList.add(employeeOptions);
  		}
		}
		LOGGER.debug("import Employee Options end");
		return employeeOptionsList;
	}
	
	/**
	   *
	   * Purpose: This method is to export data for employee options module
	   * 
	   * @param personRef
	   * @param employeeOptions
	   * @param psEmpOptionsRef
	   * 
	   * @return: List<PsEmpOptionsTran>
	   */
	public static PsEmpOptionsTran exportEmpOptionData(PsPersonRef personRef, EmployeeOptions employeeOptions,
			PsEmpOptionsRef psEmpOptionsRef) {
		PsEmpOptionsTran psEmpOptionsTran = new PsEmpOptionsTran();
		PsEmpOptionsTranPK psEmpOptionsTranPK = new PsEmpOptionsTranPK();
		psEmpOptionsTranPK.setEotEmplid(employeeOptions.getOptionEmplId());
		psEmpOptionsTranPK.setEotOptid(employeeOptions.getOptionId());
		psEmpOptionsTran.setEotPreferences(employeeOptions.getOptionPreferences());
		psEmpOptionsTran.setPsEmpOptionsRef(psEmpOptionsRef);
		psEmpOptionsTran.setPsPersonRef(personRef);
		psEmpOptionsTran.setId(psEmpOptionsTranPK);
		return psEmpOptionsTran;
	}
}
