/**
 * Program Name: PersonMyRolesMapper 
 *                                                                 
 * Program Description / functionality: This is mapper class for person my role to import and export data for myroles table
 *                            
 * Modules Impacted: MyRoles
 *                                                                    
 * Tables affected:  ps_myrole_ref                                                                  
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Rakesh       29/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.mapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.domain.PersonMyRoles;
import com.ptgproduct.entity.PsMyroleRef;
import com.ptgproduct.entity.PsPersonRef;

public class PersonMyRolesMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonMyRolesMapper.class);
	
	/**
	   *
	   * Purpose: This method is to import data for Myroles module
	   * 
	   * @param psPersonRef
	   * 
	   * @return: List<PersonMyRoles>
	   */
	public static List<PersonMyRoles> importMyroleData(PsPersonRef psPersonRef) {
		LOGGER.debug("import MyRoles begin");
		PersonMyRoles personMyrole = null;
		List<PersonMyRoles> personMyRolesList = new ArrayList<PersonMyRoles>();
		if (psPersonRef != null) {
			List<PsMyroleRef> psMyroleRefs = psPersonRef.getPsMyroleRef();
			for (PsMyroleRef psMyroleRef : psMyroleRefs) {
				personMyrole = new PersonMyRoles();
				personMyrole.setRoleDescription(psMyroleRef.getMrRoleDesc());
				personMyrole.setOrganization(psMyroleRef.getMrOrganization());
				personMyrole.setDepartment(psMyroleRef.getMrDept());
				personMyrole.setLocation(psMyroleRef.getMrLocation());
				personMyrole.setEmplid(psPersonRef.getPEmplid());
				personMyRolesList.add(personMyrole);
			}
		}
		LOGGER.debug("import MyRoles end");
		return personMyRolesList;
	}
	
	/**
	   *
	   * Purpose: This method is to export data for Myroles module
	   * 
	   * @param psPersonRef
	   * @param personMyRoles
	   * 
	   * @return: PsMyroleRef
	   */
	public static PsMyroleRef exportMyroleData(PsPersonRef psPersonRef, PersonMyRoles personMyRoles) {
		LOGGER.debug("export MyRoles begin");
		PsMyroleRef myroleRef = new PsMyroleRef();
		myroleRef.setMrRoleDesc(personMyRoles.getRoleDescription());
		myroleRef.setMrOrganization(personMyRoles.getOrganization());
		myroleRef.setMrLocation(personMyRoles.getLocation());
		myroleRef.setMrDept(personMyRoles.getDepartment());
		myroleRef.setPsPersonRef(psPersonRef);
		LOGGER.debug("export MyRoles end");
		return myroleRef;
	}
}