/**
 * Program Name: EmailDetailsMapper 
 *                                                                 
 * Program Description / functionality: This is class for Email Details Mapper
 *                            
 * Modules Impacted: Manage Email Details
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Astha     20/12/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.mapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.domain.EmailDetails;
import com.ptgproduct.entity.PsPersonEmailRef;
import com.ptgproduct.entity.PsPersonRef;

public class EmailDetailsMapper {

	static Logger LOGGER = LoggerFactory.getLogger(EmailDetailsMapper.class);

	/**
	 *
	 * Purpose: This method is to import data for Email Module
	 * 
	 * @param personRef
	 * 
	 * @return: List<EmailDetails>
	 */
	public static List<EmailDetails> importEmailData(PsPersonRef psPersonRef) {
		LOGGER.debug("importEmailData begin");
		List<EmailDetails> emailDetails = new ArrayList<EmailDetails>();
		if (psPersonRef != null) {
			List<PsPersonEmailRef> emailRefs = psPersonRef.getPsPersonEmailRefs();
			for (PsPersonEmailRef pEmailRef : emailRefs) {
				EmailDetails eDetails = new EmailDetails();
				eDetails.setPeemplId(psPersonRef.getPEmplid());
				eDetails.setAccessType(pEmailRef.getPeEAddrType());
				eDetails.setEmail(pEmailRef.getPeEmailAddr());
				eDetails.setMedia("E-mail Address");
				eDetails.setEmailFlag(String.valueOf(pEmailRef.getPePrefEmailFlag()));
				emailDetails.add(eDetails);
			}
		}
		LOGGER.debug("importEmailData begin");
		return emailDetails;
	}

	/**
	 *
	 * Purpose: This method is to export data for Email Module
	 * 
	 * @param personRef
	 * @param emailDetails
	 * 
	 * @return: List<PsPersonEmailRef>
	 */
	public static PsPersonRef exportEmailData(PsPersonRef psPersonRef, EmailDetails emailDetails) {
		LOGGER.debug("exportEmailData begin");
		PsPersonEmailRef personEmailRef = new PsPersonEmailRef();
		personEmailRef.setPeEAddrType(emailDetails.getAccessType());
		personEmailRef.setPeEmailAddr(emailDetails.getEmail());
		personEmailRef.setPePrefEmailFlag(Boolean.parseBoolean(emailDetails.getEmailFlag()));
		personEmailRef.setPsPersonRef(psPersonRef);
		psPersonRef.addPsPersonEmailRef(personEmailRef);
		LOGGER.debug("exportEmailData end");
		return psPersonRef;
	}

	/**
	 *
	 * Purpose: This method is to export data for Email Module Update
	 * 
	 * @param personRef
	 * @param employeeOptions
	 * @param psEmpOptionsRef
	 * 
	 * @return: List<PsEmpOptionsTran>
	 */
	public static PsPersonRef exportEmailDataUpdate(PsPersonRef psPersonRef, EmailDetails emailDetails) {
		LOGGER.debug("exportEmailDataUpdate begin");
		if (emailDetails.getPeemplId() != null) {
			List<PsPersonEmailRef> personEmailRefs = psPersonRef.getPsPersonEmailRefs();
			PsPersonEmailRef eRef = null;
			for (PsPersonEmailRef emailRef : personEmailRefs) {
				if (emailDetails.getAccessType().equalsIgnoreCase(emailRef.getPeEAddrType())) {
					eRef = emailRef;
					eRef.setPeEmailAddr(emailDetails.getEmail());
					eRef.setPePrefEmailFlag(Boolean.parseBoolean(emailDetails.getEmailFlag()));
				}
			}
			eRef.setPsPersonRef(psPersonRef);
			psPersonRef.setPsPersonEmailRefs(personEmailRefs);
		}
		LOGGER.debug("exportEmailDataUpdate end");
		return psPersonRef;
	}
}
