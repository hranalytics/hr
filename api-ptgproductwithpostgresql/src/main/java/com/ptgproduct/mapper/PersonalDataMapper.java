/**
 * Program Name: PersonalDataMapper 
 *                                                                 
 * Program Description / functionality: This class is to import data from PsPersonalDataRef to PersonalData and export 
 * 										data from PersonalData to PsPersonRef
 *                            
 * Modules Impacted: personal data module
 *                                                                    
 * Tables affected:  ps_personal_data_ref                                                                    
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Pavani       27/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.mapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.common.DateUtils;
import com.ptgproduct.domain.PersonalData;
import com.ptgproduct.entity.PsPersonRef;
import com.ptgproduct.entity.PsPersonalDataRef;
import com.ptgproduct.exception.PtgproductParseException;

public class PersonalDataMapper {
	public static final Logger LOGGER = LoggerFactory.getLogger(PsPersonalDataRef.class);

	/**
	 *
	 * Purpose: This method is to import data from PsPersonalDataRef to
	 * PersonalData
	 * 
	 * @param psPersonalDataRef
	 * 
	 * @return: PersonalData
	 */
	public static PersonalData importPersonalData(PsPersonalDataRef psPersonalDataRef) {
		LOGGER.debug("import person data begin");
		PersonalData personalData = new PersonalData();
		personalData.setpEmplid(psPersonalDataRef.getPsPersonRef().getPEmplid());
		personalData.setBirthDate(DateUtils.convertDateToString(psPersonalDataRef.getPpdBirthdate()));
		personalData.setMilitaryStatus(psPersonalDataRef.getPpdMilitaryStatus());
		personalData.setSex(psPersonalDataRef.getPpdSex());
		personalData.setEffectiveDate(DateUtils.convertDateToString(psPersonalDataRef.getPpdEffdt()));
		personalData.setMaritalStatus(psPersonalDataRef.getPpdMaritalStatus());
		personalData.setEthinicity(psPersonalDataRef.getPpdEthinicity());
		LOGGER.debug("import person data end");
		return personalData;
	}

	/**
	 *
	 * Purpose: This method is to export data from PersonalData to PsPersonRef
	 * 
	 * @param psPersonRef
	 * @param personalData
	 * 
	 * @return: PsPersonalDataRef
	 */
	public static PsPersonalDataRef exportPersonalData(PsPersonRef psPersonRef, PersonalData personalData) {
		PsPersonalDataRef psPersonaldataRef = psPersonRef.getPsPersonalDataRef();
		LOGGER.debug("export personal data begin");
		if (psPersonaldataRef != null) {
			try {
				psPersonaldataRef.setPpdBirthdate(DateUtils.convertStringToDate(personalData.getBirthDate()));
				psPersonaldataRef.setPpdEffdt(DateUtils.convertStringToDate(personalData.getEffectiveDate()));

			} catch (PtgproductParseException e) {
				e.printStackTrace();
			}
			psPersonaldataRef.setPpdMilitaryStatus(personalData.getMilitaryStatus());
			psPersonaldataRef.setPpdSex(personalData.getSex());
			psPersonaldataRef.setPpdMaritalStatus(personalData.getMaritalStatus());
			psPersonaldataRef.setPpdEthinicity(personalData.getEthinicity());
		}
		LOGGER.debug("export personal data end");
		return psPersonaldataRef;
	}
}
