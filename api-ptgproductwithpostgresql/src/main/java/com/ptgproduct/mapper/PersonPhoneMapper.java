/**
 * Program Name: PersonPhoneMapper 
 *                                                                 
 * Program Description / functionality: This is mapper class for person phone details services
 *                            
 * Modules Impacted: Person phone details services
 *                                                                    
 * Tables affected: ps_person_phone_ref                                                                     
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Harish       23/12/2016
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.mapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.domain.PersonPhoneDetails;
import com.ptgproduct.entity.PsPersonPhoneRef;
import com.ptgproduct.entity.PsPersonRef;

public class PersonPhoneMapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonPhoneMapper.class);

	/**
	 *
	 * Purpose: This method is to get Person phone Details
	 * 
	 * @param PsPersonRef
	 * 
	 * @return: List of PersonPhoneDetails object
	 */
	public static List<PersonPhoneDetails> importPhoneData(PsPersonRef psPersonRef) {
		LOGGER.debug("Person Phone importPhoneData begin in PersonPhoneMapper");
		PersonPhoneDetails personPhoneDetails = null;
		List<PersonPhoneDetails> personPhoneDetailsList = new ArrayList<PersonPhoneDetails>();
		if (psPersonRef != null) {
			List<PsPersonPhoneRef> PersonPhoneRef = psPersonRef.getPsPersonPhoneRefs();
			for (PsPersonPhoneRef PsPersonPhoneRef : PersonPhoneRef) {
				personPhoneDetails = new PersonPhoneDetails();
				personPhoneDetails.setpEmplid(psPersonRef.getPEmplid());
				personPhoneDetails.setAccesType(PsPersonPhoneRef.getPpPhoneType());
				personPhoneDetails.setMedia("Telephone");
				personPhoneDetails.setTelePhone(PsPersonPhoneRef.getPpPhone());
				personPhoneDetailsList.add(personPhoneDetails);
			}
			LOGGER.debug("Person Phone importData End in PersonPhoneMapper");
		}
		return personPhoneDetailsList;
	}

	/**
	 *
	 * Purpose: This method is to post Person phone Details
	 * 
	 * @param PsPersonRef
	 * @param PersonPhoneDetails
	 * 
	 * @return: PsPersonPhoneRef object that is inserted
	 */

	public static PsPersonRef exportPhoneData(PsPersonRef psPersonRef, PersonPhoneDetails personPhoneDetails) {
		LOGGER.debug("Person Phone export  begin");
		PsPersonPhoneRef phoneRef = new PsPersonPhoneRef();
		phoneRef.setPpPhoneType(personPhoneDetails.getAccesType());
		phoneRef.setPpPhone(personPhoneDetails.getTelePhone());
		phoneRef.setPsPersonRef(psPersonRef);
		psPersonRef.addPsPersonPhoneRef(phoneRef);
		LOGGER.debug("Person Phone export  end");
		return psPersonRef;
	}

	/**
	 *
	 * Purpose: This method is to put Person phone Details
	 * 
	 * @param PsPersonRef
	 * @param PersonPhoneDetails
	 * 
	 * @return: PsPersonRef object that is updated
	 */
	public static PsPersonRef exportPhoneDataUpdate(PsPersonRef psPersonRef, PersonPhoneDetails personPhoneDetails) {
		LOGGER.debug("Person Phone exportUpdate begin");
		if (personPhoneDetails.getpEmplid() != null) {
			List<PsPersonPhoneRef> psPersonPhoneRef = psPersonRef.getPsPersonPhoneRefs();
			PsPersonPhoneRef phoneRef = null;
			for (PsPersonPhoneRef personPhoneRef : psPersonPhoneRef) {
				if (personPhoneDetails.getAccesType().equalsIgnoreCase(personPhoneRef.getPpPhoneType())) {
					phoneRef = personPhoneRef;
					phoneRef.setPpPhone(personPhoneDetails.getTelePhone());
				}
			}
			phoneRef.setPsPersonRef(psPersonRef);
			psPersonRef.setPsPersonPhoneRefs(psPersonPhoneRef);
		}
		LOGGER.debug("Person Phone exportUpdate end");
		return psPersonRef;
	}
}