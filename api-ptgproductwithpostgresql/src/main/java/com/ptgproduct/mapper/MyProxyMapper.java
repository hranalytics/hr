/**
 * Program Name: MyProxyMapper 
 *                                                                 
 * Program Description / functionality: importing data from psPersonRef to MyProxy & exporting data from MyProxy to PsPersonNamesRef 
 *         
 * Modules Impacted:MyProxy  
 * 
 * Tables affected:ps_person_names_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
 *******************************************************************************
 * Kiran     27/12/2016 
 * 
 * Associated Defects Raised : 
 *
 */
package com.ptgproduct.mapper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ptgproduct.common.DateUtils;
import com.ptgproduct.domain.MyProxy;
import com.ptgproduct.entity.PsPersonNamesRef;
import com.ptgproduct.entity.PsPersonRef;
import com.ptgproduct.exception.PtgproductParseException;

public class MyProxyMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(MyProxyMapper.class);

	/**
	 *
	 * Purpose: This method is to import data from psPersonRef to MyProxy
	 * 
	 * @param psPersonRef
	 * 
	 * @param effectiveDate
	 * 
	 * @return: List<MyProxy>
	 */
	public static List<MyProxy> importProxyData(PsPersonRef psPersonRef, String effectiveDate) {
		LOGGER.debug("import mapper begin");
		List<PsPersonNamesRef> psPersonNamesRefs = psPersonRef.getPsPersonNamesRefs();
		List<MyProxy> myProxies = new ArrayList<MyProxy>();
		MyProxy myProxy = null;
		if (psPersonNamesRefs != null) {
			for (PsPersonNamesRef psPersonNamesRef : psPersonNamesRefs) {
				myProxy = new MyProxy();
				if (effectiveDate != null && effectiveDate
						.equalsIgnoreCase(DateUtils.convertDateToString(psPersonNamesRef.getPnEffdt()))) {
					myProxy.setPpEmplId(psPersonNamesRef.getPsPersonRef().getPEmplid());
					myProxy.setFirstName(psPersonNamesRef.getPnFirstName());
					myProxy.setLastNmae(psPersonNamesRef.getPnLastName());
					myProxy.setEffectiveDate(DateUtils.convertDateToString(psPersonNamesRef.getPnEffdt()));
					myProxy.setEndDate(DateUtils.convertDateToString(psPersonNamesRef.getEndDate()));
					myProxies.add(myProxy);
					return myProxies;
				} else {
					myProxy.setPpEmplId(psPersonNamesRef.getPsPersonRef().getPEmplid());
					myProxy.setFirstName(psPersonNamesRef.getPnFirstName());
					myProxy.setLastNmae(psPersonNamesRef.getPnLastName());
					myProxy.setEffectiveDate(DateUtils.convertDateToString(psPersonNamesRef.getPnEffdt()));
					myProxy.setEndDate(DateUtils.convertDateToString(psPersonNamesRef.getEndDate()));
				}
				myProxies.add(myProxy);
			}
			LOGGER.debug("mapper import end");
			return myProxies;
		} else {
			return myProxies;
		}
	}

	/**
	 *
	 * Purpose: This method is to export data from MyProxy to PsPersonNamesRef
	 * 
	 * @param myProxy
	 * 
	 * @param psPersonRef
	 * 
	 * @return: PsPersonNamesRef
	 */
	public static PsPersonNamesRef exportProxyData(MyProxy myProxy, PsPersonRef psPersonRef) {
		PsPersonNamesRef updatepersonNamesRef = null;
		for (PsPersonNamesRef psPersonNamesRef : psPersonRef.getPsPersonNamesRefs()) {
			if (myProxy.getEffectiveDate()
					.equalsIgnoreCase(DateUtils.convertDateToString(psPersonNamesRef.getPnEffdt()))) {
				updatepersonNamesRef = psPersonNamesRef;
				updatepersonNamesRef.setPnFirstName(myProxy.getFirstName());
				updatepersonNamesRef.setPnLastName(myProxy.getLastNmae());
				try {
					updatepersonNamesRef.setEndDate(DateUtils.convertStringToDate(myProxy.getEndDate()));
				} catch (PtgproductParseException e) {
					e.printStackTrace();
				}
			}
		}
		return updatepersonNamesRef;
	}
}
