package com.ptgproduct.exception;

import com.ptgproduct.common.ExceptionConstants;

public class PtgProductBaseException extends RuntimeException {

	/**
	 * Generated UID
	 */
	private static final long serialVersionUID = -3868232127218537194L;

	protected String errorCode;

	protected Object[] messageParams;

	protected ExceptionConstants exceptionConstants;

	public PtgProductBaseException() {
		super();
	}

	public PtgProductBaseException(String msg) {
		super(msg);
	}

	public PtgProductBaseException(String msg, String errorCode) {
		super(msg);
		this.errorCode = errorCode;

	}

	public PtgProductBaseException(Throwable e) {
		super(e);
	}

	public PtgProductBaseException(String msg, Throwable e) {
		super(msg, e);
		this.errorCode = e.getMessage();
	}

	public static String getStack(Throwable e) {
		StackTraceElement[] st = Thread.currentThread().getStackTrace();
		return st[4].getClassName() + "." + st[4].getMethodName() + "():" + st[4].getLineNumber() + " ->"
				+ e.getMessage();
	}

	public static String getStack() {
		StackTraceElement[] st = Thread.currentThread().getStackTrace();
		return st[4].getClassName() + "." + st[4].getMethodName() + "():" + st[4].getLineNumber() + " -> ";
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	/**
	 * @return Returns the messageParams.
	 */
	public Object[] getMessageParams() {
		return messageParams;
	}

	public ExceptionConstants getExceptionConstants() {
		return exceptionConstants;
	}

}
