/**
 * Program Name: PtgproductParseException 
 *                                                                 
 * Program Description / functionality: This is the Exception handling class for my company service   
 *                            
 * Modules Impacted: My Company
 *                                                                    
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Naresh     06/01/2017
 *
 * 
 * Associated Defects Raised : 
 *
 */ 

package com.ptgproduct.exception;

public class PtgproductParseException extends PtgproductCheckedBaseException {

  private static final long serialVersionUID = 1L;

  public PtgproductParseException(String errorCode) {
    this.errorCode = errorCode;
  }

  public PtgproductParseException(String message, String errorCode) {
    super(message, errorCode);

  }



}
