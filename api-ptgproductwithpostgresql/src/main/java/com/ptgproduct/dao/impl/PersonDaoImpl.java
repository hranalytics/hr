/**
 * Program Name: PersonDaoImpl 
 *                                                                 
 * Program Description / functionality: This class implements PersonDao interface 
 *         
 * Modules Impacted: Manage Person Details
 * 
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * --------     15/12/2016 
 * 
 * Associated Defects Raised : 
 *
 */

package com.ptgproduct.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.CannotCreateTransactionException;

import com.ptgproduct.common.DateUtils;
import com.ptgproduct.common.ExceptionConstants;
import com.ptgproduct.dao.PersonDao;
import com.ptgproduct.entity.PsEmpOptionsRef;
import com.ptgproduct.entity.PsEmpOptionsTran;
import com.ptgproduct.entity.PsJobRef;
import com.ptgproduct.entity.PsMyroleRef;
import com.ptgproduct.entity.PsPersonNidRef;
import com.ptgproduct.entity.PsPersonRef;
import com.ptgproduct.entity.PsPersonalDataRef;
import com.ptgproduct.exception.DataAccessException;
import com.ptgproduct.exception.DataBaseException;
import com.ptgproduct.exception.PtgproductParseException;

public class PersonDaoImpl extends HibernateDaoSupport implements PersonDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonDaoImpl.class);

	@SuppressWarnings("unchecked")

	/**
	 *
	 * Purpose: This method is to get all Person Information
	 * 
	 * @return: PersonDetails List
	 */
	@Override
	public List<PsPersonRef> getAllPersons() {
		try {
			LOGGER.debug("get list of Person details begin");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			List<PsPersonRef> listOfCompany = session.createCriteria(PsPersonRef.class).list();
			LOGGER.debug("Get list of Person details end");
			return listOfCompany;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getAllPersons", ex);
		}
	}

	/**
	 *
	 * Purpose: This method is to get all Person Information by ID
	 * 
	 * @param id
	 * 
	 * @return: PersonDetails
	 */
	@Override
	public PsPersonRef getPersonById(String id) {
		try {
			LOGGER.debug("get Person details by id begin");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			PsPersonRef psPersonRef = (PsPersonRef) session.get(PsPersonRef.class, id);
			LOGGER.debug("get Person details by id end");
			return psPersonRef;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getPersonById", ex);
		}
	}

	/**
	 *
	 * Purpose: This method is to add Person Information
	 * 
	 * @param personDetails
	 * 
	 * @return: Employee Id
	 */
	@Override
	public String addPerson(PsPersonRef psPersonRef) {
		try {
			LOGGER.debug("addPerson begin");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			String psPersonRefSaved = (String) session.save(psPersonRef);
			LOGGER.debug("addPerson end");
			return psPersonRefSaved;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @addPerson", ex);
		}
	}

	/**
	 *
	 * Purpose: This method is to remove Person Information
	 * 
	 * @param id
	 * 
	 * @return: Flag Value
	 */
	@Override
	public boolean deletePerson(String id) {
		try {
			LOGGER.debug("delete Person details begin");
			boolean flag = false;
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			PsPersonRef psPersonRef = (PsPersonRef) session.get(PsPersonRef.class, id);
			if (psPersonRef != null) {
				session.delete(psPersonRef);
				flag = true;
			}
			LOGGER.debug("delete Person details end");
			return flag;
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @deletePerson", ce);
		}
	}

	/**
	 *
	 * Purpose: This method is to update Person Information
	 * 
	 * @param personDetails
	 * 
	 * @return: Flag Value
	 */
	@Override
	public boolean updatePerson(PsPersonRef psPersonRef) {
		try {
			LOGGER.debug("update Person details begin");
			if (psPersonRef != null) {
				getHibernateTemplate().getSessionFactory().getCurrentSession().update(psPersonRef);
				LOGGER.debug("update Person details end");
				return true;
			}
			return false;
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @deletePerson", ce);
		}
	}

	/**
	 *
	 * Purpose: This method is to update the person address
	 * 
	 * @param psPersonRef
	 * 
	 * @return: void
	 */
	@Override
	public void updatePersonAddress(PsPersonRef psPersonRef) {
		try {
			LOGGER.debug("update Person Address DAO begin");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			LOGGER.debug("Person session");
			session.clear();
			session.update(psPersonRef);
			LOGGER.debug("update Person Address DAO end");
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @UpdatePersonAddress", ce);
		}
	}

	/**
	 *
	 * Purpose: This method is to delete person address
	 * 
	 * @param id
	 * @param paAddressType
	 * @param paEffdt
	 * 
	 * @return: boolean object
	 */
	@Override
	public boolean deletePersonAddress(String id, String paAddressType, String paEffdt) {
		boolean flag = false;
		try {
			Date paEffdt1 = DateUtils.convertStringToDate(paEffdt);
			LOGGER.debug("Remove Person Address DAO begin");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			String remove_hql = "delete from PsPersonAddressesRef p where p.paAddressType = :paAddressType and p.paEffdt =:paEffdt and p.psPersonRef.pEmplid=:pEmplid ";
			Query query = session.createQuery(remove_hql);
			query.setString("paAddressType", paAddressType);
			query.setDate("paEffdt", paEffdt1);
			query.setString("pEmplid", id);
			int updated = query.executeUpdate();
			session.flush();
			LOGGER.debug("Remove Person Address DAO end");
			if (updated != 0) {
				flag = true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @DeletePersonAddress", ce);
		} catch (PtgproductParseException e) {
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 *
	 * Purpose: This method is to delete person name
	 * 
	 * @param pemplId
	 * @param pnEffdt
	 * @param nameType
	 * 
	 * @return: boolean
	 */
	@Override
	public boolean deletePersonName(String pemplId, String pnEffdt, String nameType) {
		boolean flag = false;
		try {
			Date pnEffdt1 = DateUtils.convertStringToDate(pnEffdt);
			LOGGER.debug("DAO delete PersonName begin ");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			String removeHql = "delete from PsPersonNamesRef p where p.pnNameType = :pnNameType and p.psPersonRef.pEmplid=:pEmplid and p.pnEffdt = :pnEffdt";
			Query query = session.createQuery(removeHql);
			query.setString("pnNameType", nameType);
			query.setString("pEmplid", pemplId);
			query.setDate("pnEffdt", pnEffdt1);
			int updated = query.executeUpdate();
			session.flush();
			if (updated != 0) {
				flag = true;
			}
			LOGGER.debug(" DAO delete PersonName  end");
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @Delete Person Nmae", ce);
		} catch (PtgproductParseException e) {
			e.printStackTrace();
		}
		return flag;

	}

	/**
	 *
	 * Purpose: This method is to delete Person Phone Information
	 * 
	 * @param id
	 * 
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	@Override
	public boolean removePersonPhone(String pEmplid, String accesType) {
		boolean flag = false;
		try {
			LOGGER.debug("Remove PersonPhone details begin DAO");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			String remove_hql = "delete from PsPersonPhoneRef p where p.ppPhoneType = :ppPhoneType and p.psPersonRef.pEmplid=:pEmplid";
			Query query = session.createQuery(remove_hql);
			query.setString("ppPhoneType", accesType);
			query.setString("pEmplid", pEmplid);
			int updated = query.executeUpdate();
			session.flush();
			LOGGER.debug("Remove PersonPhone details end DAO");
			if (updated != 0) {
				flag = true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @Delete Person Phone", ce);
		}
		return flag;
	}

	/**
	 *
	 * Purpose: This method is to remove Person EmergencyConatct Details
	 * 
	 * @param id
	 * 
	 * @param ContactName
	 * 
	 * @param Relationship
	 * 
	 * @return: flag value
	 */
	@Override
	public boolean removePersonEmergencyContact(String pEmplid, String ContactName, String Relationship) {
		try {
			LOGGER.debug("delete Person Emergency Contact details begin In DAO");
			boolean flag = false;
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			String remove_hql = "delete from PsEmergencyCntctRef p where p.psPersonRef.pEmplid=:pEmplid and p.pecContactName=:ContactName and p.pecRelationship=:Relationship";
			Query query = session.createQuery(remove_hql);
			query.setString("pEmplid", pEmplid);
			query.setString("ContactName", ContactName);
			query.setString("Relationship", Relationship);
			int del = query.executeUpdate();
			LOGGER.debug("delete Person Emergency Contact details end In DAO");
			if (del != 0) {
				flag = true;
			}
			return flag;
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @deletePerson Emergency Contact", ce);
		}
	}

	/**
	 *
	 * Purpose: This method is to delete Person Email Information
	 * 
	 * @param pEmplid
	 * 
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	@Override
	public int deleteEmailInfo(String pEmplid, String accessType) {
		LOGGER.debug("deleteEmailInfo dao begin");
		Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		String hql = "DELETE FROM PsPersonEmailRef p WHERE p.psPersonRef.pEmplid = :employee_id AND p.peEAddrType = :peEAddrType";
		Query query = session.createQuery(hql);
		query.setParameter("employee_id", pEmplid);
		query.setParameter("peEAddrType", accessType);
		int result = query.executeUpdate();
		LOGGER.debug("deleteEmailInfo dao end");
		return result;
	}

	/**
	 *
	 * Purpose: This method is to update identify personal data
	 * 
	 * @param psPersonRef
	 * 
	 * @return: boolean object
	 */
	@Override
	public boolean updateIdentifyPersonalData(PsPersonRef psPersonRef) {
		try {
			LOGGER.debug("update Person Identify details begin");
			boolean flag = false;
			List<PsPersonNidRef> psPersonNidRefs = psPersonRef.getPsPersonNidRefs();
			List<PsJobRef> psJobRefs = psPersonRef.getPsJobRefs();
			for (PsJobRef psJobRef : psJobRefs) {
				for (PsPersonNidRef psPersonNidRef : psPersonNidRefs) {
					if (psPersonRef.getPEmplid() != null
							&& psJobRef.getPjEmplid().equals(psPersonNidRef.getPniEmplid())) {
						getHibernateTemplate().saveOrUpdate(psPersonRef);
						flag = true;
					}
				}
			}
			LOGGER.debug("update Person Identify details end");
			return flag;
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @UpdatePersonIdentify", ce);
		}
	}

	/**
	 *
	 * Purpose: This method is to get identify personal data by Id
	 * 
	 * @param id
	 * 
	 * @return: PsPersonRef object
	 */
	@Override
	public PsPersonRef getIdentifyPersonalDataById(String id) {
		try {
			LOGGER.debug("get Identify Personal Data by id begin");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			PsPersonRef psPersonRef = (PsPersonRef) session.get(PsPersonRef.class, id);
			LOGGER.debug("get Identify Personal Data by id end");
			return psPersonRef;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getIdentifyPersonalDataById", ex);
		}
	}

	/**
	 *
	 * Purpose: This method is to add person my roles
	 * 
	 * @param psMyroleRef
	 * 
	 * @return: String object
	 */
	@Override
	public String addPersonMyRoles(PsMyroleRef psMyroleRef) {
		try {
			LOGGER.debug("add Person myroles begin in DAO");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			session.save(psMyroleRef);
			LOGGER.debug("add Person myroles end in DAO");
			return "success";
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @addPerson myroles", ex);
		}
	}

	/**
	 *
	 * Purpose: This method is to delete person my roles by id and organization
	 * 
	 * @param pEmplid
	 * @param Organization
	 * 
	 * @return: boolean object
	 */
	@Override
	public boolean deletePersonMyRoles(String pEmplid, String Organization) {
		try {
			LOGGER.debug("delete Person MyRoles begin In DAO");
			boolean flag = false;
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			String remove = "delete from PsMyroleRef p where p.psPersonRef.pEmplid=:pEmplid and p.mrOrganization=:Organization";
			Query query = session.createQuery(remove);
			query.setString("pEmplid", pEmplid);
			query.setString("Organization", Organization);
			int del = query.executeUpdate();
			LOGGER.debug("delete Person MyRoles end In DAO");
			if (del != 0) {
				flag = true;
			}
			return flag;
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @deletePerson MyRoles", ce);
		}
	}

	/**
	 *
	 * Purpose: This method is to update personal data
	 * 
	 * @param pspersonaldataRef
	 * 
	 * @return: void
	 */
	public void updatePersonalData(PsPersonalDataRef psPersonalDataRef) {
		try {
			LOGGER.debug("update Person data begin in DAO");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			if (psPersonalDataRef != null) {
				session.clear();
				session.saveOrUpdate(psPersonalDataRef);
			}
			LOGGER.debug("update Person data end in DAO");
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @updatePersonalData", ce);
		}
	}

	/**
	 *
	 * Purpose: This method is to get personal data
	 * 
	 * @param id
	 * 
	 * @return: PsPersonalDataRef
	 */
	@Override
	public PsPersonalDataRef getPersonalDataById(String id) {
		try {
			LOGGER.debug("get Person details by id begin");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			PsPersonalDataRef psPersonalDataRef = (PsPersonalDataRef) session.get(PsPersonalDataRef.class, id);
			LOGGER.debug("get Person details by id end");
			return psPersonalDataRef;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getPersonById", ex);
		}
	}

	/**
	 *
	 * Purpose: This method is to delete Proxy
	 * 
	 * @param pemplId
	 * 
	 * @param pnEffdt
	 * 
	 * @return: boolean
	 */
	@Override
	public boolean deletePersonNameProxy(String pemplId, String pnEffdt) {
		boolean flag = false;
		try {
			Date pnEffdt1 = DateUtils.convertStringToDate(pnEffdt);
			LOGGER.debug("DAO delete PersonName begin ");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			String removeHql = "delete from PsPersonNamesRef p where p.psPersonRef.pEmplid= :pEmplid and p.pnEffdt = :pnEffdt";
			Query query = session.createQuery(removeHql);
			query.setString("pEmplid", pemplId);
			query.setDate("pnEffdt", pnEffdt1);
			int updated = query.executeUpdate();
			session.flush();
			if (updated != 0) {
				flag = true;
			}
			LOGGER.debug(" DAO delete PersonName  end");
		} catch (CannotCreateTransactionException ce) {
			throw new DataAccessException("Error @Delete Person Nmae", ce);
		} catch (PtgproductParseException e) {
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 *
	 * Purpose: This method is to get employee options by employee id
	 * 
	 * @param id
	 * 
	 * @return: PsPersonRef object
	 */
	@Override
	public PsPersonRef getEmployeeOptionsByEmpId(String id) {
		try {
			LOGGER.debug("get Employee Options by id begin");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			PsPersonRef psPersonRef = (PsPersonRef) session.get(PsPersonRef.class, id);
			LOGGER.debug("get Employee Options by id end");
			return psPersonRef;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getEmployeeOptionsById", ex);
		}
	}

	/**
	 *
	 * Purpose: This method is to update Employee Options
	 * 
	 * @param employeeOptions
	 * 
	 * @return:
	 */
	@Override
	public void updateEmployeeOptions(PsEmpOptionsTran psEmpOptionsTran) {
		LOGGER.debug("updateEmployeeOptions dao begin");
		Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		if (psEmpOptionsTran.getPsPersonRef().getPEmplid() != null) {
			session.clear();
			session.saveOrUpdate(psEmpOptionsTran);
		}
		LOGGER.debug("updateEmployeeOptions dao end");
	}

	/**
	 *
	 * Purpose: This method is to get Employee Options by ID
	 * 
	 * @param id
	 * 
	 * @return: EmployeeOptions List
	 */
	@Override
	public PsEmpOptionsRef getEmployeeOptionByOptionId(int id) {
		try {
			LOGGER.debug("getEmployeeOptionByOptionId dao begin");
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
			PsEmpOptionsRef psEmpOptionsRef = (PsEmpOptionsRef) session.get(PsEmpOptionsRef.class, id);
			LOGGER.debug("getEmployeeOptionByOptionId dao end");
			return psEmpOptionsRef;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getEmployeeOptionByOptionId", ex);
		}
	}

	/**
	 *
	 * Purpose: This method is to get employee options
	 * 
	 * 
	 * @return: List<PsEmpOptionsRef>
	 */
	@Override
	public List<PsEmpOptionsRef> getEmployeeOptions() {
		try {
			LOGGER.debug("getEmployeeOptions dao begin");
			List<PsEmpOptionsRef> empOptionsRefs = getHibernateTemplate().loadAll(PsEmpOptionsRef.class);
			LOGGER.debug("getEmployeeOptions dao end");
			return empOptionsRefs;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getEmployeeOptions", ex);
		}
	}
}
