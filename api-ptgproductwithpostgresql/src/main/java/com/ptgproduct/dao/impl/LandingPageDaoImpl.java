package com.ptgproduct.dao.impl;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.CannotCreateTransactionException;

import com.ptgproduct.common.ExceptionConstants;
import com.ptgproduct.dao.LandingPageDao;
import com.ptgproduct.entity.CityRef;
import com.ptgproduct.entity.CompanyEntity;
import com.ptgproduct.entity.CountryRef;
import com.ptgproduct.entity.LandingPage;
import com.ptgproduct.entity.PsJobRef;
import com.ptgproduct.entity.PsPersonAddressesRef;
import com.ptgproduct.entity.StateRef;
import com.ptgproduct.exception.DataAccessException;
import com.ptgproduct.exception.DataBaseException;

public class LandingPageDaoImpl extends HibernateDaoSupport implements LandingPageDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(LandingPageDaoImpl.class);

	public LandingPage getLandingPage() {

		try {
			LOGGER.debug("get getLandingPage details begin");
			LandingPage landingPage=new LandingPage();
			landingPage.setCompanyEntities(getCompanies());
			landingPage.setCountryRefs(getCountries());
			LOGGER.debug("get getLandingPage details end");
			return landingPage;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getLandingPage", ex);
		}
	
		
	}
	
	private List<CompanyEntity> getCompanies(){
		return getHibernateTemplate().loadAll(CompanyEntity.class);
	}
	private List<CountryRef> getCountries() {
		return getHibernateTemplate().loadAll(CountryRef.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StateRef> getStateRefByCountry(String countryId) {
		List<StateRef> stateRefs = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from StateRef stateRef where stateRef.countryRef.countryId = ?").setString(0, countryId).list();
		return stateRefs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CityRef> getCityRefByState(int stateId) {
		List<CityRef> cityRefs = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from CityRef cityRef where cityRef.stateRef.stateId = ?")
			    .setInteger(0, stateId).list();
		return cityRefs;
	}

	@Override
	public LandingPage getLandingPageByEmployee(String employeeId) {

		try {
			LOGGER.debug("get getLandingPage details begin");
			LandingPage landingPage=new LandingPage();
			landingPage.setCompanyEntities(getCompaniesByEmployee(getCompanyByEmployee(employeeId)));
			landingPage.setCountryRefs(getCountriesByEmployee(getCountryByEmployee(employeeId)));
			LOGGER.debug("get getLandingPage details end");
			return landingPage;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getLandingPage", ex);
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<CompanyEntity> getCompaniesByEmployee(String companiyId){
		List<CompanyEntity> companyRef=getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from CompanyEntity companyEntity where companyEntity.companyId = ?")
			    .setString(0,companiyId).list();
		return companyRef;
	}
	
	@SuppressWarnings("unchecked")
	private List<CountryRef> getCountriesByEmployee(String countryId) {
		List<CountryRef> countryRefs = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from CountryRef countryRef where countryRef.countryId = ?").setString(0,countryId).list();
	return countryRefs;
	}
	
	@SuppressWarnings("unchecked")
	private String getCompanyByEmployee(String employeeId){
		List<PsJobRef> jobRefs=getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from PsJobRef psJobRef where psJobRef.psPersonRef.pEmplid = ?")
			    .setString(0,employeeId).list();
		return jobRefs.get(0).getPjCompany();
	}
	@SuppressWarnings("unchecked")
	private String getCountryByEmployee(String employeeId) {
		List<PsPersonAddressesRef> personAddressesRefs = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from PsPersonAddressesRef psPersonAddressesRef where psPersonAddressesRef.psPersonRef.pEmplid = ?").setString(0,employeeId).list();
	return personAddressesRefs.get(0).getPaCountry();
	}

}
