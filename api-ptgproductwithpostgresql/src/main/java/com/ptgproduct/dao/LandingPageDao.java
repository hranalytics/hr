package com.ptgproduct.dao;

import java.util.List;

import com.ptgproduct.entity.CityRef;
import com.ptgproduct.entity.LandingPage;
import com.ptgproduct.entity.StateRef;

public interface LandingPageDao {
LandingPage getLandingPage();
LandingPage getLandingPageByEmployee(String employeeId);
List<StateRef> getStateRefByCountry(String countryId);
List<CityRef> getCityRefByState(int stateId);
	
}