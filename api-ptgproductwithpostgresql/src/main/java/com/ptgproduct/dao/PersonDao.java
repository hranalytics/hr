/**
 * Program Name: PersonDao 
 *                                                                 
 * Program Description / functionality: This is interface for Person Dao
 *                            
 * Modules Impacted: Manage Person Details
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * --------     15/12/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.dao;

import java.util.List;

import com.ptgproduct.entity.PsEmpOptionsRef;
import com.ptgproduct.entity.PsEmpOptionsTran;
import com.ptgproduct.entity.PsMyroleRef;
import com.ptgproduct.entity.PsPersonRef;
import com.ptgproduct.entity.PsPersonalDataRef;

public interface PersonDao {

	/**
	 *
	 * Purpose: This method is to get all Person Information
	 * 
	 * @return: PersonDetails List
	 */
	List<PsPersonRef> getAllPersons();

	/**
	 *
	 * Purpose: This method is to get all Person Information by ID
	 * 
	 * @param id
	 * 
	 * @return: PersonDetails
	 */
	PsPersonRef getPersonById(String id);

	/**
	 *
	 * Purpose: This method is to add Person Information
	 * 
	 * @param personDetails
	 * 
	 * @return: Employee Id
	 */
	String addPerson(PsPersonRef psPersonRef);

	/**
	 *
	 * Purpose: This method is to remove Person Information
	 * 
	 * @param id
	 * 
	 * @return: Flag Value
	 */
	boolean deletePerson(String id);

	/**
	 *
	 * Purpose: This method is to update Person Information
	 * 
	 * @param personDetails
	 * 
	 * @return: Flag Value
	 */
	boolean updatePerson(PsPersonRef psPersonRef);

	/**
	 *
	 * Purpose: This method is to delete person address
	 * 
	 * @param id
	 * @param paAddressType
	 * @param paEffdt
	 * 
	 * @return: boolean object
	 */
	boolean deletePersonAddress(String id, String paAddressType, String paEffdt);

	/**
	 *
	 * Purpose: This method is to update the person address
	 * 
	 * @param psPersonRef
	 * 
	 * @return: void
	 */
	void updatePersonAddress(PsPersonRef psPersonRef);

	/**
	 *
	 * Purpose: This method is to delete person name
	 * 
	 * @param pemplId
	 * @param pnEffdt
	 * @param nameType
	 * 
	 * @return: boolean
	 */
	boolean deletePersonName(String pemplId, String pnEffdt, String nameType);

	/**
	 *
	 * Purpose: This method is to delete Person Phone Information
	 * 
	 * @param id
	 * 
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	public boolean removePersonPhone(String id, String accesType);

	/**
	 *
	 * Purpose: This method is to remove Person EmergencyConatct Details
	 * 
	 * @param id
	 * 
	 * @param ContactName
	 * 
	 * @param Relationship
	 * 
	 * @return: flag value
	 */
	public boolean removePersonEmergencyContact(String id, String ContactName, String Relationship);

	/**
	 *
	 * Purpose: This method is to delete Person Email Information
	 * 
	 * @param pEmplid
	 * 
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	public int deleteEmailInfo(String pEmplid, String accessType);

	/**
	 *
	 * Purpose: This method is to update identify personal data
	 * 
	 * @param psPersonRef
	 * 
	 * @return: boolean object
	 */
	boolean updateIdentifyPersonalData(PsPersonRef psPersonRef);

	/**
	 *
	 * Purpose: This method is to get identify personal data by Id
	 * 
	 * @param id
	 * 
	 * @return: PsPersonRef object
	 */
	PsPersonRef getIdentifyPersonalDataById(String id);

	/**
	 *
	 * Purpose: This method is to add person my roles
	 * 
	 * @param psMyroleRef
	 * 
	 * @return: String object
	 */
	public String addPersonMyRoles(PsMyroleRef psMyroleRef);

	/**
	 *
	 * Purpose: This method is to delete person my roles by id and organization
	 * 
	 * @param pEmplid
	 * @param Organization
	 * 
	 * @return: boolean object
	 */
	public boolean deletePersonMyRoles(String id, String Organization);

	/**
	 *
	 * Purpose: This method is to get personal data
	 * 
	 * @param id
	 * 
	 * @return: PsPersonalDataRef
	 */
	PsPersonalDataRef getPersonalDataById(String id);

	/**
	 *
	 * Purpose: This method is to update personal data
	 * 
	 * @param pspersonaldataRef
	 * 
	 * @return: void
	 */
	void updatePersonalData(PsPersonalDataRef pspersonaldataRef);

	/**
	 *
	 * Purpose: This method is to delete Proxy
	 * 
	 * @param pemplId
	 * 
	 * @param pnEffdt
	 * 
	 * @return: boolean
	 */
	public boolean deletePersonNameProxy(String pemplId, String pnEffdt);

	/**
	 *
	 * Purpose: This method is to get employee options by employee id
	 * 
	 * @param id
	 * 
	 * @return: PsPersonRef object
	 */
	PsPersonRef getEmployeeOptionsByEmpId(String empId);

	/**
	 *
	 * Purpose: This method is to get Employee Options by ID
	 * 
	 * @param id
	 * 
	 * @return: EmployeeOptions List
	 */
	PsEmpOptionsRef getEmployeeOptionByOptionId(int optionId);

	/**
	 *
	 * Purpose: This method is to get employee options
	 * 
	 * 
	 * @return: List<PsEmpOptionsRef>
	 */
	List<PsEmpOptionsRef> getEmployeeOptions();

	/**
	 *
	 * Purpose: This method is to update Employee Options
	 * 
	 * @param employeeOptions
	 * 
	 * @return:
	 */
	public void updateEmployeeOptions(PsEmpOptionsTran psEmpOptionsTran);
}
