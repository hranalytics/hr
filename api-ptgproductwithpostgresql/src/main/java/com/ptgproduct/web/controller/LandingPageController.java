package com.ptgproduct.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ptgproduct.common.NavigationURIConstants;
import com.ptgproduct.domain.ReturnResponse;

@RequestMapping(value = NavigationURIConstants.SECURITY)
public interface LandingPageController {

	@RequestMapping(value = NavigationURIConstants.GET_LANDING_PAGE, method = RequestMethod.GET)
	ReturnResponse getLandingPage();
	
	@RequestMapping(value = NavigationURIConstants.GET_LANDING_PAGE_STATE, method = RequestMethod.GET)
	ReturnResponse getStates(String countryId);
	
	@RequestMapping(value = NavigationURIConstants.GET_LANDING_PAGE_CITY, method = RequestMethod.GET)
	ReturnResponse getCities(String stateId);
	
	
	@RequestMapping(value = NavigationURIConstants.GET_LANDING_PAGE_BY_EMPLOYEE, method = RequestMethod.GET)
	ReturnResponse getLandingPageBYEmployee(String pEmplid);
	
	
}
