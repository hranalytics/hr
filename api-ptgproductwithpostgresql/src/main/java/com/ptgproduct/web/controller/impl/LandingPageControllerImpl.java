package com.ptgproduct.web.controller.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.ptgproduct.common.CommonUtils;
import com.ptgproduct.common.NavigationConstants;
import com.ptgproduct.domain.ReturnResponse;
import com.ptgproduct.entity.CityRef;
import com.ptgproduct.entity.LandingPage;
import com.ptgproduct.entity.StateRef;
import com.ptgproduct.service.LandingPageService;
import com.ptgproduct.web.controller.LandingPageController;

@RestController
public class LandingPageControllerImpl implements LandingPageController{
	private static final Logger LOGGER = LoggerFactory.getLogger(LandingPageControllerImpl.class);

	@Autowired
	private LandingPageService landingPageService;
	@Override
	public ReturnResponse getLandingPage() {
		LOGGER.debug("Getting all getLandingPage details controller begin");
		LandingPage landingPages = landingPageService.getLandingPage();
		LOGGER.debug("Getting all getLandingPage details controller end");
		if (landingPages != null) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, landingPages, null);
		} else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					null, null);
	}
	@Override
	public ReturnResponse getStates(@PathVariable String countryId) {
		LOGGER.debug("Getting all getStates details controller begin");
		List<StateRef> stateRef = landingPageService.getStateRefByCountry(countryId);
		LOGGER.debug("Getting all getStates details controller end");
		if (stateRef != null) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, stateRef, null);
		} else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					null, null);
	}
	@Override
	public ReturnResponse getCities(@PathVariable String stateId) {
		LOGGER.debug("Getting all getCities details controller begin");
		List<CityRef> cittRefs = landingPageService.getCityRefByState(Integer.parseInt(stateId));
		LOGGER.debug("Getting all getCities details controller end");
		if (cittRefs != null) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, cittRefs, null);
		} else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					null, null);
	}
	@Override
	public ReturnResponse getLandingPageBYEmployee(@PathVariable String pEmplid) {
		LOGGER.debug("Getting all getLandingPageBYEmployee details controller begin");
		LandingPage landingPages = landingPageService.getLandingPageByEmployee(pEmplid);
		LOGGER.debug("Getting all getLandingPageBYEmployee details controller end");
		if (landingPages != null) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, landingPages, null);
		} else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					null, null);
	}

}
