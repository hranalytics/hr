/**
 * Program Name: PersonController 
 *                                                                 
 * Program Description / functionality: This is interface for Person Controller
 *                            
 * Modules Impacted: Manage Person Details
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * --------     15/12/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.web.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ptgproduct.common.NavigationURIConstants;
import com.ptgproduct.domain.EmailDetails;
import com.ptgproduct.domain.EmployeeOptions;
import com.ptgproduct.domain.IdentifyPersonalData;
import com.ptgproduct.domain.MyProxy;
import com.ptgproduct.domain.PersonAddress;
import com.ptgproduct.domain.PersonDetails;
import com.ptgproduct.domain.PersonEmergencyContactDetails;
import com.ptgproduct.domain.PersonMyRoles;
import com.ptgproduct.domain.PersonName;
import com.ptgproduct.domain.PersonPhoneDetails;
import com.ptgproduct.domain.PersonalData;
import com.ptgproduct.domain.ReturnResponse;
import com.ptgproduct.exception.PtgproductParseException;

@RequestMapping(value = NavigationURIConstants.SECURITY)
public interface PersonController {

	/**
	 *
	 * Purpose: This method is to get all Person Information
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.GET_PERSONS, method = RequestMethod.GET)
	ReturnResponse getPersons();

	/**
	 *
	 * Purpose: This method is to get Person Information By ID
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.GET_PERSON_BY_ID, method = RequestMethod.GET)
	ReturnResponse getPersonById(String pEmplid);

	/**
	 *
	 * Purpose: This method is to add Person Information
	 * 
	 * @param psPersonRef
	 * 
	 * @return: json object
	 * @throws PtgproductParseException 
	 */
	@RequestMapping(value = NavigationURIConstants.ADD_PERSON, method = RequestMethod.POST)
	ReturnResponse addPerson(PersonDetails psPersonRef) throws PtgproductParseException;

	/**
	 *
	 * Purpose: This method is to remove Person Information
	 * 
	 * @param id
	 * 
	 * @return: Flag Value
	 */
	@RequestMapping(value = NavigationURIConstants.REMOVE_PERSON, method = RequestMethod.DELETE)
	ReturnResponse removePerson(String id);

	/**
	 *
	 * Purpose: This method is to update Person Information
	 * 
	 * @param psPersonRef
	 * 
	 * @return: Employee ID
	 * @throws PtgproductParseException 
	 */
	@RequestMapping(value = NavigationURIConstants.UPDATE_PERSON, method = RequestMethod.PUT)
	ReturnResponse updatePerson(PersonDetails psPersonRef) throws PtgproductParseException;

	/**
	   *
	   * Purpose: This method is to get person address by id
	   * 
	   * @param pEmplid
	   * 
	   * @return: json object
	   */
	@RequestMapping(value = NavigationURIConstants.GET_PERSON_ADDRESS_BY_ID, method = RequestMethod.GET)
	ReturnResponse getPersonAddressById(String pEmplid);

	/**
	   *
	   * Purpose: This method is to remove person address by id, paAddressType and paEffdt
	   * 
	   * @param pEmplid
	   * @param paAddressType
	   * @param paEffdt
	   * 
	   * @return: json object
	   */
	@RequestMapping(value = NavigationURIConstants.REMOVE_PERSON_ADDRESS, method = RequestMethod.DELETE)
	ReturnResponse removePersonAddress(String pEmplid, String paAddressType, String paEffdt);

	/**
	   *
	   * Purpose: This method is to update person address 
	   * 
	   * @param person Address
	   * 
	   * @return: json object
	   */
	@RequestMapping(value = NavigationURIConstants.UPDATE_PERSON_ADDRESS, method = RequestMethod.PUT)
	ReturnResponse updatePersonAddress(PersonAddress personAddress, String pEmplid);

	/**
	 *
	 * Purpose: This method is to perform get operation for person names
	 * 
	 * @param pemplId
	 * 
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.GET_PERSON_NAME_BY_ID, method = RequestMethod.GET)
	ReturnResponse getPersonNames(String pemplId);

	/**
	 *
	 * Purpose: This method is to update person names
	 * 
	 * @param personName
	 * @param pemplId
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.UPDATE_PERSON_NAME_BY_ID, method = RequestMethod.PUT)
	ReturnResponse updatePersonNames(PersonName personName, String pemplId);

	/**
	 *
	 * Purpose: This method is to delete person names
	 * 
	 * @param pemplId
	 * @param effectiveDate
	 * @param nameType
	 *
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.DELETE_PERSON_NAME_BY_ID, method = RequestMethod.DELETE)
	ReturnResponse deletePersonNames(String pemplId, String effectiveDate, String nameType);

	// Person Phone Services

	/**
	 *
	 * Purpose: This method is to add Person Phone Information
	 * 
	 * @param personPhoneDetails
	 * 
	 * @return: Employee ID
	 */
	@RequestMapping(value = NavigationURIConstants.ADD_PERSON_PHONE, method = RequestMethod.POST)
	ReturnResponse addPersonPhone(@RequestBody PersonPhoneDetails personPhoneDetails);

	/**
	 *
	 * Purpose: This method is to update Person Phone Information
	 * 
	 * @param personPhoneDetails
	 * 
	 * @return: Flag Value
	 */
	@RequestMapping(value = NavigationURIConstants.UPDATE_PERSON_PHONE, method = RequestMethod.PUT)
	ReturnResponse updatePersonPhone(@RequestBody PersonPhoneDetails personPhoneDetails);

	/**
	 *
	 * Purpose: This method is to get Person Phone Information By ID
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.GET_PERSON_PHONE_BY_ID, method = RequestMethod.GET)
	ReturnResponse getPersonPhoneById(String pEmplid);

	/**
	 *
	 * Purpose: This method is to remove Person Phone Information By ID
	 * 
	 * @param pEmplid
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	@RequestMapping(value = NavigationURIConstants.REMOVE_PERSON_PHONE_BY_ID, method = RequestMethod.DELETE)
	ReturnResponse removePersonPhoneById(String pEmplid, String accessType);

	/**
	 *
	 * Purpose: This method is to add Person Phone Details
	 * 
	 * @param personPhoneDetails
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.ADD_PERSON_EMERGENCY_CONTACT, method = RequestMethod.POST)
	ReturnResponse addPersonEmergencyContact(@RequestBody PersonEmergencyContactDetails emergencyContactDetails);

	/**
	 *
	 * Purpose: This method is to get Person Emergency Contact Details by empID
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.GET_PERSON_EMERGENCY_CONTACT_BY_ID, method = RequestMethod.GET)
	ReturnResponse getPersonEmergencyContactById(String pEmplid);

	/**
	 *
	 * Purpose: This method is to remove Person Emergency Contact Details 
	 * 
	 * 
	 * @param pEmplid
	 * 
	 * @param contactName
	 * 
	 * @param relationship
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.REMOVE_PERSON_EMERGENCY_CONTACT_BY_ID, method = RequestMethod.DELETE)
	ReturnResponse removePersonEmergencyContactById(@PathVariable String pEmplid, @PathVariable String contactName,
			@PathVariable String relationship);

	/**
	 *
	 * Purpose: This method is to update Person Emergency Contact Details
	 * 
	 * 
	 * @param emergencyContactDetails
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.UPDATE_PERSON_EMERGENCY_CONTACT, method = RequestMethod.PUT)
	ReturnResponse updatePersonEmergencyContactById(@RequestBody PersonEmergencyContactDetails emergencyContactDetails);

	/**
	 *
	 * Purpose: This method is to get Person Email Information by Id
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.GET_EMAIL_INFO_BY_ID, method = RequestMethod.GET)
	ReturnResponse getEmailInfoById(String pEmplid);

	/**
	 *
	 * Purpose: This method is to Add Person Email Information
	 * 
	 * @param emailDetails
	 * 
	 * @return: Employee Id
	 */
	@RequestMapping(value = NavigationURIConstants.ADD_EMAIL_INFO, method = RequestMethod.POST)
	ReturnResponse addEmailInfo(EmailDetails emailDetails);

	/**
	 *
	 * Purpose: This method is to remove Person Email Information
	 * 
	 * @param pEmplid
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	@RequestMapping(value = NavigationURIConstants.REMOVE_EMAIL_INFO, method = RequestMethod.DELETE)
	ReturnResponse removeEmailInfo(String pEmplid, String accessType);

	/**
	 *
	 * Purpose: This method is to update Person Email Information
	 * 
	 * @param emailDetails
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.UPDATE_EMAIL_INFO, method = RequestMethod.PUT)
	ReturnResponse updateEmailInfo(EmailDetails emailDetails);

	/**
	   *
	   * Purpose: This method is to update identify personal data 
	   * 
	   * @param identifyPersonalData
	   * 
	   * @return: json object
	   */
	@RequestMapping(value = NavigationURIConstants.UPDATE_IDENTIFY_PERSONAL_DATA, method = RequestMethod.PUT)
	ReturnResponse updateIdentifyPersonalData(IdentifyPersonalData identifyPersonalData);

	/**
	   *
	   * Purpose: This method is to get identify personal address by pEmplid
	   * 
	   * @param pEmplid
	   * 
	   * @return: json object
	   */
	@RequestMapping(value = NavigationURIConstants.GET_IDENTIFY_PERSONAL_DATA, method = RequestMethod.GET)
	ReturnResponse getIdentifyPersonalData(String pEmplid);

	/**
	   *
	   * Purpose: This method is to get my roles by pEmplid
	   * 
	   * @param pEmplid
	   * 
	   * @return: json object
	   */
	@RequestMapping(value = NavigationURIConstants.GET_ROLES_BY_ID, method = RequestMethod.GET)
	ReturnResponse getMyRolesById(String pEmplId);

	/**
	   *
	   * Purpose: This method is to add person my roles
	   * 
	   * @param myroles
	   * 
	   * @return: json object
	   */
	@RequestMapping(value = NavigationURIConstants.ADD_PERSON_ROLES, method = RequestMethod.POST)
	ReturnResponse addPersonMyRoles(@RequestBody PersonMyRoles myroles);

	/**
	   *
	   * Purpose: This method is to remove person my roles by id and organization
	   * 
	   * @param pEmplid
	   * @param Organization
	   * 
	   * @return: json object
	   */
	@RequestMapping(value = NavigationURIConstants.REMOVE_PERSON_ROLES_BY_ID, method = RequestMethod.DELETE)
	ReturnResponse removePersonMyRolesById(String pEmplid, String Organization);

	/**
	 *
	 * Purpose: This method is to perform update operation for personal data
	 * 
	 * @param personalData
	 * 
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.UPDATE_PERSONAL_DATA_BY_ID, method = RequestMethod.PUT)
	ReturnResponse updatePersonalData(PersonalData personalData, String pEmplid);

	/**
	 *
	 * Purpose: This method is to perform get operation for personal data
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.GET_PERSONAL_DATA_BY_ID, method = RequestMethod.GET)
	ReturnResponse getPersonalData(String pEmplid);

	/**
	 *
	 * Purpose: This method is to get Proxies
	 * 
	 * @param ppEmplId
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.GET_ALL_PROXY_BY_ID, method = RequestMethod.GET)
	ReturnResponse allProxy(String ppEmplId);

	/**
	 *
	 * Purpose: This method is to add Proxy
	 * 
	 * @param myProxy
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.ADD_PROXY, method = RequestMethod.POST)
	ReturnResponse addProxy(@RequestBody MyProxy myProxy);

	/**
	 *
	 * Purpose: This method is to get Proxy
	 * 
	 * @param ppEmplId
	 * 
	 * @param effectiveDate
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.GET_PROXY_BY_ID, method = RequestMethod.GET)
	ReturnResponse getProxyById(String ppEmplId, String effectiveDate);

	/**
	 *
	 * Purpose: This method is to delete Proxy
	 * 
	 * @param ppEmplId
	 * 
	 * @param effectiveDate
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.REMOVE_PROXY_BY_ID, method = RequestMethod.DELETE)
	ReturnResponse removeProxyById(String ppEmplId, String effectiveDate);

	/**
	 *
	 * Purpose: This method is to update Proxy
	 *
	 * @param myProxy
	 * 
	 * @param ppEmplId
	 * 
	 * @param effectiveDate
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.UPDATE_PROXY_BY_ID, method = RequestMethod.PUT)
	ReturnResponse updateProxyById(@RequestBody MyProxy myProxy, String ppEmplId, String effectiveDate);

	/**
	   *
	   * Purpose: This method is to get employee options by pEmplid
	   * 
	   * @param pEmplid
	   * 
	   * @return: json object
	   */
	@RequestMapping(value = NavigationURIConstants.GET_EMP_OPTIONS_BY_ID, method = RequestMethod.GET)
	ReturnResponse getEmployeeOptionsById(String pEmplid);

	/**
	 *
	 * Purpose: This method is to update Employee Options
	 * 
	 * @param employeeOptions
	 * 
	 * @return: json object
	 */
	@RequestMapping(value = NavigationURIConstants.UPDATE_EMPLOYEE_OPTIONS, method = RequestMethod.PUT)
	ReturnResponse updateEmployeeOptions(EmployeeOptions employeeOptions);

}
