/**
 * Program Name: PersonControllerImpl 
 *                                                                 
 * Program Description / functionality: This class implements PersonController interface 
 *         
 * Modules Impacted: Manage Person Details
 * 
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * --------     15/12/2016 
 * 
 * Associated Defects Raised : 
 *
 */

package com.ptgproduct.web.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ptgproduct.common.CommonUtils;
import com.ptgproduct.common.EmailUtils;
import com.ptgproduct.common.NavigationConstants;
import com.ptgproduct.domain.EmailDetails;
import com.ptgproduct.domain.EmployeeOptions;
import com.ptgproduct.domain.EmployeeRef;
import com.ptgproduct.domain.IdentifyPersonalData;
import com.ptgproduct.domain.MyProxy;
import com.ptgproduct.domain.PersonAddress;
import com.ptgproduct.domain.PersonDetails;
import com.ptgproduct.domain.PersonEmergencyContactDetails;
import com.ptgproduct.domain.PersonMyRoles;
import com.ptgproduct.domain.PersonName;
import com.ptgproduct.domain.PersonPhoneDetails;
import com.ptgproduct.domain.PersonalData;
import com.ptgproduct.domain.ReturnResponse;
import com.ptgproduct.exception.PtgproductParseException;
import com.ptgproduct.service.PersonService;
import com.ptgproduct.web.controller.PersonController;

@RestController
public class PersonControllerImpl implements PersonController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonControllerImpl.class);

	@Autowired
	private PersonService personService;

	/**
	 *
	 * Purpose: This method is to get all Person Information
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse getPersons() {
		LOGGER.debug("Getting all Persons details controller begin");
		List<PersonDetails> personDetails = personService.getAllPersons();
		LOGGER.debug("Getting all Persons details controller end");
		if (personDetails.size() != 0) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, personDetails, null);
		}else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					null, null);
	}

	/**
	 *
	 * Purpose: This method is to get Person Information By ID
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse getPersonById(@PathVariable String pEmplid) {
		LOGGER.debug("Get Person details by ID controller begin");
		PersonDetails personDetails = personService.getPersonById(pEmplid);
		LOGGER.debug("Get Person details by ID controller end");
		if (personDetails != null)
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, personDetails, null);
		else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					null, null);
	}

	/**
	 *
	 * Purpose: This method is to add Person Information
	 * 
	 * @param psPersonRef
	 * 
	 * @return: json object
	 * @throws PtgproductParseException 
	 */
	@Override
	public ReturnResponse addPerson(@Valid @RequestBody PersonDetails personDetails) throws PtgproductParseException  {
		LOGGER.debug("Add Person details controller begin");
		String pEmplid = personService.addPerson(personDetails);
		if (pEmplid != null && pEmplid.length() > 0) {
			personDetails.setpEmplid(pEmplid);
			EmailUtils.sendEmail(personDetails);
			EmployeeRef employee=new EmployeeRef(personDetails.getpEmplid(), personDetails.getHomeEmail());
			
			LOGGER.debug("Add Person details controller end");
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_SUCCESS, HttpStatus.OK,employee,null);
		} else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_FAILURE, HttpStatus.BAD_REQUEST, null,
					null);
	}

	/**
	 *
	 * Purpose: This method is to remove Person Information
	 * 
	 * @param id
	 * 
	 * @return: Flag Value
	 */
	@Override
	public ReturnResponse removePerson(@PathVariable String pEmplid) {
		LOGGER.debug("Remove Person details controller begin");
		boolean flag = personService.deletePerson(pEmplid);
		LOGGER.debug("Remove Person details controller end");
		if (flag)
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_SUCCESS, HttpStatus.OK, flag, null);
		else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_FAILURE,
					HttpStatus.INTERNAL_SERVER_ERROR, flag, null);
	}

	/**
	 *
	 * Purpose: This method is to update Person Information
	 * 
	 * @param psPersonRef
	 * 
	 * @return: Employee ID
	 * @throws PtgproductParseException 
	 */
	@Override
	public ReturnResponse updatePerson(@Valid @RequestBody PersonDetails personDetails) throws PtgproductParseException {
		LOGGER.debug("Update Person details controller begin");
		boolean flag = personService.updatePerson(personDetails);
		LOGGER.debug("Update Person details controller end");
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_SUCCESS, HttpStatus.OK,
					personDetails.getpEmplid(), null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_FAILURE, HttpStatus.BAD_REQUEST,
					personDetails.getpEmplid(), null);
		}

	}

	/**
	   *
	   * Purpose: This method is to get person address by id
	   * 
	   * @param pEmplid
	   * 
	   * @return: json object
	   */
	@Override
	public ReturnResponse getPersonAddressById(@Valid @PathVariable String pEmplid) {
		LOGGER.debug("Get Person Address by id controller begin");
		List<PersonAddress> personAddress = personService.getPersonAddressById(pEmplid);
		LOGGER.debug("Get Person Address by id controller end");
		if (personAddress.isEmpty() != true)
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, personAddress, null);
		else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS,HttpStatus.PRECONDITION_FAILED, personAddress, null);

	}

	/**
	   *
	   * Purpose: This method is to remove person address by id, paAddressType and paEffdt
	   * 
	   * @param pEmplid
	   * @param paAddressType
	   * @param paEffdt
	   * 
	   * @return: json object
	   */
	@Override
	public ReturnResponse removePersonAddress(@Valid @PathVariable String pEmplid, @PathVariable String paAddressType,
			@PathVariable String paEffdt) {
		LOGGER.debug("Delete Person Address by id controller begin");
		boolean flag = personService.deletePersonAddress(pEmplid, paAddressType, paEffdt);
		LOGGER.debug("Delete Person Address by id controller end");
		if (flag)
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_SUCCESS, HttpStatus.OK, null, null);
		else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_FAILURE, HttpStatus.INTERNAL_SERVER_ERROR, flag, null);
	}

	/**
	   *
	   * Purpose: This method is to update person address 
	   * 
	   * @param person Address
	   * 
	   * @return: json object
	   */
	@Override
	public ReturnResponse updatePersonAddress(@Valid @RequestBody PersonAddress personAddress,
			@PathVariable String pEmplid) {
		LOGGER.debug("Update Person Address by id controller begin");
		personService.updatePersonAddress(personAddress, pEmplid);
		LOGGER.debug("Update Person Address by id controller end");
		return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_SUCCESS, HttpStatus.OK, null, null);
	}

	/**
	 *
	 * Purpose: This method is to perform get operation for person names
	 * 
	 * @param pemplId
	 * 
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse getPersonNames(@PathVariable String pemplId) {
		List<PersonName> personNames = personService.getPersonNames(pemplId);
		if (personNames != null) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, personNames, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.NOT_FOUND, personNames,
					null);
		}
	}

	/**
	 *
	 * Purpose: This method is to update person names
	 * 
	 * @param personName
	 * @param pemplId
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse updatePersonNames(@Valid @RequestBody PersonName personName, @PathVariable String pemplId) {

		personName = personService.updatePersonNames(personName, pemplId);
		if (personName.getEffectiveDate().equals(null) && personName.getNameType().equals(null)) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_FAILURE, HttpStatus.NOT_MODIFIED,
					NavigationConstants.UPDATE_FAILURE, null);
		} else {

			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, personName, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to delete person names
	 * 
	 * @param pemplId
	 * @param effectiveDate
	 * @param nameType
	 *
	 * @return: json object
	 */
	@Override
	public ReturnResponse deletePersonNames(@PathVariable String pemplId, @PathVariable String effectiveDate,
			@PathVariable String nameType) {
		boolean flag = personService.deletePersonNames(pemplId, effectiveDate, nameType);
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, null, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_FAILURE, HttpStatus.NOT_FOUND, null,
					null);
		}
	}

	/**
	 *
	 * Purpose: This method is to add Person Phone Information
	 * 
	 * @param personPhoneDetails
	 * 
	 * @return: Employee ID
	 */
	@Override
	public ReturnResponse addPersonPhone(@Valid @RequestBody PersonPhoneDetails personPhoneDetails) {
		LOGGER.debug("add Person phone  controller begin");
		String pEmplid = personService.addPersonPhone(personPhoneDetails);
		LOGGER.debug("add person Phone controller end");
		if (pEmplid != null && pEmplid.length() > 0) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_SUCCESS, HttpStatus.OK, pEmplid, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_FAILURE, HttpStatus.BAD_REQUEST, null,
					null);
		}
	}

	/**
	 *
	 * Purpose: This method is to get Person Phone Information By ID
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse getPersonPhoneById(@PathVariable String pEmplid) {
		LOGGER.debug("Getting  Person phone details by id In Controller. Start");
		List<PersonPhoneDetails> personPhoneDetails = personService.getPersonPhoneById(pEmplid);
		LOGGER.debug("Getting  Person phone details by id In Controller. End");
		if (personPhoneDetails.isEmpty() != true)
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, personPhoneDetails,
					null);
		else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					personPhoneDetails, null);
	}

	/**
	 *
	 * Purpose: This method is to update Person Phone Information
	 * 
	 * @param personPhoneDetails
	 * 
	 * @return: Flag Value
	 */
	@Override
	public ReturnResponse updatePersonPhone(@Valid @RequestBody PersonPhoneDetails personPhoneDetails) {
		LOGGER.debug("updateContactInfo controller begin");
		boolean flag = personService.updatePersonPhone(personPhoneDetails);
		LOGGER.debug("updateContactInfo controller end");
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_SUCCESS, HttpStatus.OK, flag, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_FAILURE, HttpStatus.BAD_REQUEST, null,
					null);
		}
	}

	/**
	 *
	 * Purpose: This method is to remove Person Phone Information By ID
	 * 
	 * @param pEmplid
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	@Override
	public ReturnResponse removePersonPhoneById(@PathVariable String pEmplid, @PathVariable String accessType) {
		LOGGER.debug("Remove Person phone controller begin");
		boolean flag = personService.deletePersonPhone(pEmplid, accessType);
		LOGGER.debug("Remove Person phone controller End");
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_SUCCESS, HttpStatus.OK, flag, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_FAILURE,
					HttpStatus.INTERNAL_SERVER_ERROR, flag, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to add Person Emergency Contact Details
	 * 
	 * @param emergencyContactDetails
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse addPersonEmergencyContact(
			@Valid @RequestBody PersonEmergencyContactDetails emergencyContactDetails) {
		LOGGER.debug("add Person emergency contact controller begin");
		String pEmplid = personService.addPersonEmergencyConact(emergencyContactDetails);
		LOGGER.debug("add Person emergency contact controller end");
		if (pEmplid != null && pEmplid.length() > 0) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_SUCCESS, HttpStatus.OK, pEmplid, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_FAILURE, HttpStatus.BAD_REQUEST, null,
					null);
		}
	}

	/**
	 *
	 * Purpose: This method is to get Person Emergency Contact Details by empID
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse getPersonEmergencyContactById(@PathVariable String pEmplid) {
		LOGGER.debug("Getting  Person emergency contact details by id In Controller. Start");
		List<PersonEmergencyContactDetails> emergencyContactDetails = personService
				.getPersonEmergencyContactById(pEmplid);
		LOGGER.debug("Getting  Person emergency contact details by id In Controller. End");
		if (emergencyContactDetails.isEmpty() != true)
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK,
					emergencyContactDetails, null);
		else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					emergencyContactDetails, null);
	}

	/**
	 *
	 * Purpose: This method is to remove Person Emergency Contact Details by
	 * empID
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse removePersonEmergencyContactById(@PathVariable String pEmplid,
			@PathVariable String contactName, @PathVariable String relationship) {
		LOGGER.debug("Remove Person Emergency Contact controller begin");
		boolean flag = personService.deletePersonEmergencyContact(pEmplid, contactName, relationship);
		LOGGER.debug("Remove Person Emergency Contact controller End");
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_SUCCESS, HttpStatus.OK, flag, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_FAILURE,
					HttpStatus.INTERNAL_SERVER_ERROR, flag, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to update Person Emergency Contact Details by
	 * empID
	 * 
	 * @param emergencyContactDetails
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse updatePersonEmergencyContactById(
			@Valid @RequestBody PersonEmergencyContactDetails emergencyContactDetails) {
		LOGGER.debug("update Emergency contact controller begin");
		boolean flag = personService.updatePersonEmergencyContact(emergencyContactDetails);
		LOGGER.debug("update Emergency contact controller end");
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_SUCCESS, HttpStatus.OK, flag, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_FAILURE, HttpStatus.OK, null, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to get Person Email Information by Id
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse getEmailInfoById(@PathVariable String pEmplid) {

		LOGGER.debug("getEmailInfoById controller begin");
		List<EmailDetails> emailDetails = personService.getEmailInfoById(pEmplid);
		LOGGER.debug("getEmailInfoById controller end");
		if (emailDetails != null) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, emailDetails, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					null, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to Add Person Email Information
	 * 
	 * @param emailDetails
	 * 
	 * @return: Employee Id
	 */
	@Override
	public ReturnResponse addEmailInfo(@Valid @RequestBody EmailDetails emailDetails) {
		LOGGER.debug("addEmailInfo controller begin");
		if (emailDetails.getMedia().equalsIgnoreCase("E-mail Address")) {
			String pEmplid = personService.addEmailInfo(emailDetails);
			LOGGER.debug("addEmailInfo controller end");
			if (pEmplid != null && pEmplid.length() > 0) {
				return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_SUCCESS, HttpStatus.OK, pEmplid,
						null);
			} else {
				return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_FAILURE, HttpStatus.BAD_REQUEST,
						null, null);
			}
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INVALID_EMAIL_MEDIA, HttpStatus.BAD_REQUEST,
					null, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to remove Person Email Information
	 * 
	 * @param pEmplid
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	@Override
	public ReturnResponse removeEmailInfo(@PathVariable String pEmplid, @PathVariable String accessType) {
		LOGGER.debug("removeContactInfo controller begin");
		boolean flag = personService.removeEmailInfo(pEmplid, accessType);
		LOGGER.debug("removeContactInfo controller end");
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_SUCCESS, HttpStatus.OK, flag, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_FAILURE,
					HttpStatus.INTERNAL_SERVER_ERROR, flag, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to update Person Email Information
	 * 
	 * @param emailDetails
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse updateEmailInfo(@Valid @RequestBody EmailDetails emailDetails) {
		if (emailDetails.getMedia().equalsIgnoreCase("E-mail Address")) {
			LOGGER.debug("updateEmailInfo controller begin");
			boolean flag = personService.updateEmailInfo(emailDetails);
			LOGGER.debug("updateEmailInfo controller end");
			if (flag) {
				return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_SUCCESS, HttpStatus.OK,
						emailDetails, null);
			} else {
				return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_FAILURE, HttpStatus.BAD_REQUEST,
						flag, null);
			}
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INVALID_EMAIL_MEDIA, HttpStatus.BAD_REQUEST,
					null, null);
		}
	}

	/**
	   *
	   * Purpose: This method is to update identify personal data 
	   * 
	   * @param identifyPersonalData
	   * 
	   * @return: json object
	   */
	@Override
	public ReturnResponse updateIdentifyPersonalData(@Valid @RequestBody IdentifyPersonalData identifyPersonalData) {
		LOGGER.debug("update Identify Person controller begin");
		boolean flag = personService.updateIdentifyPersonalData(identifyPersonalData);
		LOGGER.debug("update Identify Person controller end");
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_SUCCESS, HttpStatus.OK, flag, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_FAILURE, HttpStatus.BAD_REQUEST, null, null);
		}
	}

	/**
	   *
	   * Purpose: This method is to get identify personal address by pEmplid
	   * 
	   * @param pEmplid
	   * 
	   * @return: json object
	   */
	@Override
	public ReturnResponse getIdentifyPersonalData(@PathVariable String pEmplid) {
		LOGGER.debug("Get Identify Personal Data by id controller begin");
		List<IdentifyPersonalData> identifyPersonalData = personService.getIdentifyPersonalDataById(pEmplid);
		LOGGER.debug("Get Identify Personal Data by id controller end");
		if (identifyPersonalData != null)
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, identifyPersonalData,
					null);
		else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED, identifyPersonalData, null);

	}

	/**
	   *
	   * Purpose: This method is to get my roles by pEmplid
	   * 
	   * @param pEmplid
	   * 
	   * @return: json object
	   */
	@Override
	public ReturnResponse getMyRolesById(@PathVariable String pEmplId) {
		LOGGER.debug("get Roles by id controller begin");
		List<PersonMyRoles> myRoles = personService.getMyRoleById(pEmplId);
		LOGGER.debug("get Roles by id controller end");
		if (myRoles != null) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, myRoles, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.FAILURE, HttpStatus.PRECONDITION_FAILED, myRoles, null);
		}
	}

	/**
	   *
	   * Purpose: This method is to add person my roles
	   * 
	   * @param myroles
	   * 
	   * @return: json object
	   */
	@Override
	public ReturnResponse addPersonMyRoles(@RequestBody PersonMyRoles myroles) {
		LOGGER.debug("add Person Roles controller begin");
		String pEmplid = personService.addPersonMyRoles(myroles);
		LOGGER.debug("add Person Roles controller end");
		if (pEmplid != null && pEmplid.length() > 0) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_SUCCESS, HttpStatus.OK, pEmplid, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_FAILURE, HttpStatus.BAD_REQUEST, null,
					null);
		}
	}

	/**
	   *
	   * Purpose: This method is to remove person my roles by id and organization
	   * 
	   * @param pEmplid
	   * @param Organization
	   * 
	   * @return: json object
	   */
	@Override
	public ReturnResponse removePersonMyRolesById(@PathVariable String pEmplid, @PathVariable String Organization) {
		LOGGER.debug("Remove Person Roles controller begin");
		boolean flag = personService.deletePersonMyRoles(pEmplid, Organization);
		LOGGER.debug("Remove Person Roles controller End");
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_SUCCESS, HttpStatus.OK, flag, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_FAILURE,
					HttpStatus.INTERNAL_SERVER_ERROR, flag, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to perform update operation for personal data
	 * 
	 * @param personalData
	 * 
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse updatePersonalData(@Valid @RequestBody PersonalData personalData,
			@PathVariable String pEmplid) {
		personService.updatePersonalData(personalData, pEmplid);
		return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_SUCCESS, HttpStatus.OK, personalData, null);
	}

	/**
	 *
	 * Purpose: This method is to perform get operation for personal data
	 * 
	 * @param pEmplid
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse getPersonalData(@PathVariable String pEmplid) {
		LOGGER.debug("Getting  Personal data by id.");
		PersonalData personalData = personService.getPersonalDataById(pEmplid);
		if (personalData != null) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, personalData, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.NOT_FOUND, null, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to add Proxy
	 * 
	 * @param myProxy
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse addProxy(@Valid @RequestBody MyProxy myProxy) {
		LOGGER.debug("add proxy controller begin");
		String s = personService.addProxy(myProxy);
		LOGGER.debug("add proxy controller end");
		if (s != null && s.length() > 0) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_SUCCESS, HttpStatus.OK, s, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.INSERT_FAILURE, HttpStatus.NOT_FOUND, null,
					null);
		}
	}

	/**
	 *
	 * Purpose: This method is to get Proxy
	 * 
	 * @param ppEmplId
	 * 
	 * @param effectiveDate
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse getProxyById(@PathVariable String ppEmplId, @PathVariable String effectiveDate) {
		LOGGER.debug("get procy by id controller begin");
		List<MyProxy> myProxies = personService.getProxyById(ppEmplId, effectiveDate);
		LOGGER.debug("get procy by id controller end");
		if (myProxies != null) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, myProxies, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.FAILURE, HttpStatus.NOT_FOUND, null, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to delete Proxy
	 * 
	 * @param ppEmplId
	 * 
	 * @param effectiveDate
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse removeProxyById(@PathVariable String ppEmplId, @PathVariable String effectiveDate) {
		LOGGER.debug("remove proxy controller begin");
		boolean b = personService.deleteProxy(ppEmplId, effectiveDate);
		LOGGER.debug("remove proxy controller end");
		if (b) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.DELETE_SUCCESS, HttpStatus.OK, b, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.FAILURE, HttpStatus.NOT_FOUND, b, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to update Proxy
	 *
	 * @param myProxy
	 * 
	 * @param ppEmplId
	 * 
	 * @param effectiveDate
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse updateProxyById(@Valid @RequestBody MyProxy myProxy, @PathVariable String ppEmplId,
			@PathVariable String effectiveDate) {
		LOGGER.debug("update allproxies controller begin");

		boolean b = personService.updateProxy(myProxy, ppEmplId, effectiveDate);
		LOGGER.debug("update allproxies controller end");
		if (b) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_SUCCESS, HttpStatus.OK, b, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_FAILURE, HttpStatus.NOT_FOUND, b, null);
		}
	}

	/**
	 *
	 * Purpose: This method is to get Proxies
	 * 
	 * @param ppEmplId
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse allProxy(@Valid @PathVariable String ppEmplId) {
		LOGGER.debug("get allproxies controller begin");
		List<MyProxy> myProxies = personService.getProxyById(ppEmplId, null);
		LOGGER.debug("get allproxies controller end");
		if (myProxies != null && myProxies.size() > 0) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, myProxies, null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.FAILURE, HttpStatus.NOT_FOUND, null, null);
		}
	}

	// Emp options

	/**
	   *
	   * Purpose: This method is to get employee options by pEmplid
	   * 
	   * @param pEmplid
	   * 
	   * @return: json object
	   */
	@Override
	public ReturnResponse getEmployeeOptionsById(@Valid @PathVariable String pEmplid) {
		LOGGER.debug("Get Employee Options by id controller begin");
		List<EmployeeOptions> employeeOptions = personService.getEmployeeOptionsById(pEmplid);
		LOGGER.debug("Get Employee Options by id controller end");
		if (employeeOptions != null)
			return CommonUtils.getHttpStatusResponse(NavigationConstants.SUCCESS, HttpStatus.OK, employeeOptions, null);
		else
			return CommonUtils.getHttpStatusResponse(NavigationConstants.NO_RECORDS, HttpStatus.PRECONDITION_FAILED,
					null, null);
	}

	/**
	 *
	 * Purpose: This method is to update Employee Options
	 * 
	 * @param employeeOptions
	 * 
	 * @return: json object
	 */
	@Override
	public ReturnResponse updateEmployeeOptions(@Valid @RequestBody EmployeeOptions employeeOptions) {
		LOGGER.debug("updateEmployeeOptions controller begin");
		boolean flag = false;
		try {
			personService.updateEmployeeOptions(employeeOptions);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.debug("updateEmployeeOptions controller end");
		if (flag) {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_SUCCESS, HttpStatus.OK,
					"Preference : " + employeeOptions.getOptionPreferences(), null);
		} else {
			return CommonUtils.getHttpStatusResponse(NavigationConstants.UPDATE_FAILURE, HttpStatus.BAD_REQUEST, null,
					null);
		}
	}
}
