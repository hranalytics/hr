/**
 * Program Name: PersonalData
 * 
 * Program Description / functionality: This is POJO class
 * 
 * Modules Impacted: Personal Data
 * 
 * Tables affected: ps_personal_data_ref
 *  
 * Developer       Created        /Modified       Date         Purpose
 *******************************************************************************
 * Pavani		  27/12/2016
 * 
 * * Associated Defects Raised :
 *
 */
package com.ptgproduct.domain;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class PersonalData {
	private String pEmplid;
	@NotEmpty(message = "effective date should not be null")
	@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = " Invalid Date Format. Hint YYYY-MM-DD")
	private String effectiveDate;

	@NotEmpty(message = "maritalStatus should not be null")
	private String maritalStatus;

	@NotEmpty(message = "militaryStatus should not be null")
	@Size(max = 1)
	private String militaryStatus;

	@NotEmpty(message = "sex should not be null")
	@Size(max = 1)
	private String sex;
	@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = " Invalid Date Format. Hint YYYY-MM-DD")
	private String birthDate;

	@NotEmpty(message = "ethinicity should not be null")
	private String ethinicity;

	public PersonalData(String pEmplid, String effectiveDate, String maritalStatus, String militaryStatus, String sex,
			String birthDate, String ethinicity) {
		super();
		this.pEmplid = pEmplid;
		this.effectiveDate = effectiveDate;
		this.maritalStatus = maritalStatus;
		this.militaryStatus = militaryStatus;
		this.sex = sex;
		this.birthDate = birthDate;
		this.ethinicity = ethinicity;
	}

	public PersonalData() {
		// TODO Auto-generated constructor stub
	}

	public String getpEmplid() {
		return pEmplid;
	}

	public void setpEmplid(String pEmplid) {
		this.pEmplid = pEmplid;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getMilitaryStatus() {
		return militaryStatus;
	}

	public void setMilitaryStatus(String militaryStatus) {
		this.militaryStatus = militaryStatus;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getEthinicity() {
		return ethinicity;
	}

	public void setEthinicity(String ethinicity) {
		this.ethinicity = ethinicity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((effectiveDate == null) ? 0 : effectiveDate.hashCode());
		result = prime * result + ((ethinicity == null) ? 0 : ethinicity.hashCode());
		result = prime * result + ((maritalStatus == null) ? 0 : maritalStatus.hashCode());
		result = prime * result + ((militaryStatus == null) ? 0 : militaryStatus.hashCode());
		result = prime * result + ((pEmplid == null) ? 0 : pEmplid.hashCode());
		result = prime * result + ((sex == null) ? 0 : sex.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonalData other = (PersonalData) obj;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (effectiveDate == null) {
			if (other.effectiveDate != null)
				return false;
		} else if (!effectiveDate.equals(other.effectiveDate))
			return false;
		if (ethinicity == null) {
			if (other.ethinicity != null)
				return false;
		} else if (!ethinicity.equals(other.ethinicity))
			return false;
		if (maritalStatus == null) {
			if (other.maritalStatus != null)
				return false;
		} else if (!maritalStatus.equals(other.maritalStatus))
			return false;
		if (militaryStatus == null) {
			if (other.militaryStatus != null)
				return false;
		} else if (!militaryStatus.equals(other.militaryStatus))
			return false;
		if (pEmplid == null) {
			if (other.pEmplid != null)
				return false;
		} else if (!pEmplid.equals(other.pEmplid))
			return false;
		if (sex == null) {
			if (other.sex != null)
				return false;
		} else if (!sex.equals(other.sex))
			return false;
		return true;
	}

}
