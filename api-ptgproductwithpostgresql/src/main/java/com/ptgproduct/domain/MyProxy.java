/**
 * Program Name: MyProxy 
 *                                                                 
 * Program Description / functionality: POJO class 
 *         
 * Modules Impacted:MyProxy  
 * 
 * Tables affected:ps_person_names_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
 *******************************************************************************
 * Kiran     27/12/2016 
 * 
 * Associated Defects Raised : 
 *
 */
package com.ptgproduct.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class MyProxy {
	

	@NotNull(message = "ppEmplId should not be null")
	private String ppEmplId;
	@NotNull(message = "firstName should not be null")
	@Size(min = 3, max = 30)
	private String firstName;
	@NotNull(message = "lastNmae should not be null")
	@Size(min = 3, max = 30)
	private String lastNmae;
	@NotNull(message = "effectiveDate should not be null")
	@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = " Invalid Date Format. Hint YYYY-MM-DD")
	private String effectiveDate;
	@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = " Invalid Date Format. Hint YYYY-MM-DD")
	private String endDate;

	public String getPpEmplId() {
		return ppEmplId;
	}

	public void setPpEmplId(String ppEmplId) {
		this.ppEmplId = ppEmplId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastNmae() {
		return lastNmae;
	}

	public void setLastNmae(String lastNmae) {
		this.lastNmae = lastNmae;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effectiveDate == null) ? 0 : effectiveDate.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastNmae == null) ? 0 : lastNmae.hashCode());
		result = prime * result + ((ppEmplId == null) ? 0 : ppEmplId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MyProxy other = (MyProxy) obj;
		if (effectiveDate == null) {
			if (other.effectiveDate != null)
				return false;
		} else if (!effectiveDate.equals(other.effectiveDate))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastNmae == null) {
			if (other.lastNmae != null)
				return false;
		} else if (!lastNmae.equals(other.lastNmae))
			return false;
		if (ppEmplId == null) {
			if (other.ppEmplId != null)
				return false;
		} else if (!ppEmplId.equals(other.ppEmplId))
			return false;
		return true;
	}
	
}
