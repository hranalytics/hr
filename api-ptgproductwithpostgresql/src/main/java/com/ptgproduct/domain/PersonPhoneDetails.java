/**
 * Program Name: PersonPhoneDetails 
 *                                                                 
 * Program Description / functionality: This is POJO class for Person phone Details Services
 *                            
 * Modules Impacted: Person phone Details Services
 *                                                                    
 * Tables affected: Ps_Person_Phone_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Harish       23/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.ptgproduct.entity.BaseEntity;

public class PersonPhoneDetails extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String pEmplid;
	@NotNull(message = "accestype should not be null")
	@Size(min = 1, max = 4, message = "Size of acces type should be lenght 1 to 4 oly")
	private String accesType;

	private String media;
	@NotNull(message = "telePhone should not be null")
	@Size(min = 10, max = 15, message = "Telephone length should be length of 10 to 15 only")
	@Pattern(regexp = "^(0|[1-9][0-9]*)$", message = "It should be numbers only")
	private String telePhone;

	public String getpEmplid() {
		return pEmplid;
	}

	public void setpEmplid(String pEmplid) {
		this.pEmplid = pEmplid;
	}

	public String getAccesType() {
		return accesType;
	}

	public void setAccesType(String accesType) {
		this.accesType = accesType;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public String getTelePhone() {
		return telePhone;
	}

	public void setTelePhone(String telePhone) {
		this.telePhone = telePhone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accesType == null) ? 0 : accesType.hashCode());
		result = prime * result + ((media == null) ? 0 : media.hashCode());
		result = prime * result + ((pEmplid == null) ? 0 : pEmplid.hashCode());
		result = prime * result + ((telePhone == null) ? 0 : telePhone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonPhoneDetails other = (PersonPhoneDetails) obj;
		if (accesType == null) {
			if (other.accesType != null)
				return false;
		} else if (!accesType.equals(other.accesType))
			return false;
		if (media == null) {
			if (other.media != null)
				return false;
		} else if (!media.equals(other.media))
			return false;
		if (pEmplid == null) {
			if (other.pEmplid != null)
				return false;
		} else if (!pEmplid.equals(other.pEmplid))
			return false;
		if (telePhone == null) {
			if (other.telePhone != null)
				return false;
		} else if (!telePhone.equals(other.telePhone))
			return false;
		return true;
	}
	
	

}
