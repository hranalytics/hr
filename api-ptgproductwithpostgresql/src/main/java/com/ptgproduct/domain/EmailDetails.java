/**
 * Program Name: EmailDetails 
 *                                                                 
 * Program Description / functionality: This is domain for Email Details
 *                            
 * Modules Impacted: Manage Email Details
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Astha        20/12/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.ptgproduct.common.ValidationUtils;

public class EmailDetails {

	@NotNull(message=ValidationUtils.PERSON_EMPID_NOT_NULL)
	@Size(min=4,max=11,message=ValidationUtils.PERSON_EMPID_SIZE)
	private String peemplId;
	@NotNull(message=ValidationUtils.PERSON_EMAIL_ACCESS_TYPE_NOT_NULL)
	@Size(min=1,max=4,message=ValidationUtils.PERSON_EMAIL_ACCESS_TYPE_SIZE)
	private String accessType;
	private String media;
	@NotNull(message=ValidationUtils.PERSON_EMAIL_NOT_NULL)
	@Email(message=ValidationUtils.PERSON_EMAIL_VALIDATION)
	private String email;
	@NotNull(message=ValidationUtils.PERSON_EMAIL_FLAG_NOT_NULL)
	private String emailFlag;

	public String getEmailFlag() {
		return emailFlag;
	}

	public void setEmailFlag(String emailFlag) {
		this.emailFlag = emailFlag;
	}

	public String getPeemplId() {
		return peemplId;
	}

	public void setPeemplId(String peemplId) {
		this.peemplId = peemplId;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accessType == null) ? 0 : accessType.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((emailFlag == null) ? 0 : emailFlag.hashCode());
		result = prime * result + ((media == null) ? 0 : media.hashCode());
		result = prime * result + ((peemplId == null) ? 0 : peemplId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailDetails other = (EmailDetails) obj;
		if (accessType == null) {
			if (other.accessType != null)
				return false;
		} else if (!accessType.equals(other.accessType))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (emailFlag == null) {
			if (other.emailFlag != null)
				return false;
		} else if (!emailFlag.equals(other.emailFlag))
			return false;
		if (media == null) {
			if (other.media != null)
				return false;
		} else if (!media.equals(other.media))
			return false;
		if (peemplId == null) {
			if (other.peemplId != null)
				return false;
		} else if (!peemplId.equals(other.peemplId))
			return false;
		return true;
	}
	
	

}
