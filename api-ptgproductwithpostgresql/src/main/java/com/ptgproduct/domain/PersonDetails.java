package com.ptgproduct.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.ptgproduct.common.CommonConstants;
import com.ptgproduct.common.ValidationConstants;
import com.ptgproduct.entity.BaseEntity;

public class PersonDetails extends BaseEntity {

	private static final long serialVersionUID = -7389585423540910114L;

	private String pEmplid;

	@NotNull(message = ValidationConstants.PERSON_COMPANY_NOT_NULL)
	@Size(min = 1, max = 3, message = ValidationConstants.PERSON_COMPANY_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String companyId;

	@NotNull(message = ValidationConstants.PERSON_NATIONAL_ID_SIZE)
	@Size(max = 20, message = ValidationConstants.PERSON_NATIONAL_ID_SIZE)
	@Pattern(regexp = CommonConstants.NUMBER_REGEX, message = ValidationConstants.PERSON_NUMBER_INVALID)
	private String nationalId;

	@NotNull(message = ValidationConstants.PERSON_FORMOF_ADDRESS_NOT_NULL)
	@Size(min = 1, max = 4, message = ValidationConstants.PERSON_FORMOF_ADDRESS_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String formOfAddressId;

	@NotNull(message = ValidationConstants.PERSON_FIRST_NAME_NOT_NULL)
	@Size(max = 30, message = ValidationConstants.PERSON_FIRST_NAME_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String firstName;

	@NotNull(message = ValidationConstants.PERSON_LAST_NAME_NOT_NULL)
	@Size(max = 30, message = ValidationConstants.PERSON_LAST_NAME_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String lastName;

	@NotNull(message = ValidationConstants.PERSON_MIDDLE_NAME_NOT_NULL)
	@Size(max = 30, message = ValidationConstants.PERSON_MIDDLE_NAME_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String middleName;

	@NotNull(message = ValidationConstants.PERSON_COUNTRY_ID_NOT_NULL)
	@Size(max = 3, message = ValidationConstants.PERSON_COUNTRY_ID_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String countryId;

	@NotNull(message = ValidationConstants.PERSON_ADDRESS1_NOT_NULL)
	@Size(max = 55, message = ValidationConstants.PERSON_ADDRESS1_SIZE)
	private String address1;

	@NotNull(message = ValidationConstants.PERSON_ADDRESS2_NOT_NULL)
	@Size(max = 55, message = ValidationConstants.PERSON_ADDRESS2_SIZE)
	private String address2;

	@NotNull(message = ValidationConstants.PERSON_CITY_NOT_NULL)
	@Size(max = 30, message = ValidationConstants.PERSON_CITY_SIZE)
	@Pattern(regexp = CommonConstants.NUMBER_REGEX, message = ValidationConstants.PERSON_NUMBER_INVALID)
	private String city;

	@NotNull(message = ValidationConstants.PERSON_COUNTRY_NOT_NULL)
	@Size(min = 1, message = ValidationConstants.PERSON_COUNTRY_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX_WITH_SPACE, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String country;

	@NotNull(message = ValidationConstants.PERSON_STATE_NOT_NULL)
	@Size(max = 6, message = ValidationConstants.PERSON_STATE_SIZE)
	@Pattern(regexp = CommonConstants.NUMBER_REGEX, message = ValidationConstants.PERSON_NUMBER_INVALID)
	private String stateId;

	@NotNull(message = ValidationConstants.PERSON_POSTAL_NOT_NULL)
	@Size(max = 12, message = ValidationConstants.PERSON_POSTAL_SIZE)
	@Pattern(regexp = CommonConstants.NUMBER_REGEX, message = ValidationConstants.PERSON_NUMBER_INVALID)
	private String postalCode;

	@Size(max = 24, message = ValidationConstants.PERSON_HOME_PHONE_SIZE)
	@Pattern(regexp = CommonConstants.NUMBER_REGEX, message = ValidationConstants.PERSON_NUMBER_INVALID)
	private String homePhone;

	@NotNull(message = ValidationConstants.PERSON_HOME_EMAIL_NOT_NULL)
	@Size(max = 70, message = ValidationConstants.PERSON_HOME_EMAIL_SIZE)
	@Email(message = ValidationConstants.PERSON_EMAIL_VALIDATION)
	private String homeEmail;

	@NotNull(message = ValidationConstants.PERSON_GENDER_ID_NOT_NULL)
	@Size(max = 1, message = ValidationConstants.PERSON_GENDER_ID_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String genderId;

	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String ethnicityId;

	@NotNull(message = ValidationConstants.PERSON_MILITARY_STATUS_NOT_NULL)
	@Size(max = 1, message = ValidationConstants.PERSON_MILITARY_STATUS_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String militryStatus;

	@NotNull(message = ValidationConstants.PERSON_ENTRY_DATE_NOT_NULL)
	@Pattern(regexp = CommonConstants.DATE_REGEX, message = ValidationConstants.PERSON_DATE_INVALID)
	private String entryDate;

	@NotNull(message = ValidationConstants.PERSON_ENTRY_REASON_NOT_NULL)
	@Size(max = 3, message = ValidationConstants.PERSON_ENTRY_REASON_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String entryReason;

	@NotNull(message = ValidationConstants.PERSON_EMP_TYPE_NOT_NULL)
	@Size(max = 3, message = ValidationConstants.PERSON_EMP_TYPE_SIZE)
	@Pattern(regexp = CommonConstants.ALPHABET_REGEX, message = ValidationConstants.PERSON_ALPHABETS_INVALID)
	private String emplType;

	@NotNull(message = ValidationConstants.PERSON_EMP_REQUEST_TEMP_NOT_NULL)
	@Size(max = 1, message = ValidationConstants.PERSON_EMP_REQUEST_TEMP_SIZE)
	private String empReqTemp;

	@NotNull(message = ValidationConstants.PERSON_EMP_CLASS_NOT_NULL)
	@Size(max = 3, message = ValidationConstants.PERSON_EMP_CLASS_SIZE)
	private String empClass;

	@NotNull(message = ValidationConstants.PERSON_EMP_ANNUAL_WAGES_NOT_NULL)
	@Pattern(regexp = CommonConstants.ANNUAL_WAGES_REGEX, message = ValidationConstants.PERSON_NUMBER_OR_DECIMAL_NUMBER_INVALID)
	private String annual_wages;

	@NotNull(message = ValidationConstants.PERSON_BENIFIT_CLASS_NOT_NULL)
	@Size(max = 2, message = ValidationConstants.PERSON_BENIFIT_CLASS_SIZE)
	private String benefitClassId;

	@NotNull(message = ValidationConstants.PERSON_STANDARD_HOURS_NOT_NULL)
	@Pattern(regexp = CommonConstants.STANDARD_HOURS_REGEX, message = ValidationConstants.PERSON_NUMBER_OR_DECIMAL_NUMBER_INVALID)
	private String stdHours;

	@NotNull(message = ValidationConstants.PERSON_BUSINESS_TITLE_NOT_NULL)
	@Size(max = 5, message = ValidationConstants.PERSON_BUSINESS_TITLE_SIZE)
	private String bussinessTitle;

	@NotNull(message = ValidationConstants.PERSON_JOB_CODE_NOT_NULL)
	@Size(max = 6, message = ValidationConstants.PERSON_JOB_CODE_SIZE)
	private String jobCode;

	@NotNull(message = ValidationConstants.PERSON_FLSA_STATUS_NOT_NULL)
	@Size(max = 1, message = ValidationConstants.PERSON_FLSA_STATUS_SIZE)
	private String flsaStatus;

	@NotNull(message = ValidationConstants.PERSON_SUPERVISOR_NOT_NULL)
	@Size(max = 11, message = ValidationConstants.PERSON_SUPERVISOR_SIZE)
	private String supervisorId;

	@NotNull(message = ValidationConstants.PERSON_COMPENSATION_BASIC_NOT_NULL)
	@Size(max = 5, message = ValidationConstants.PERSON_COMPENSATION_BASIC_SIZE)
	private String compensationBasicId;

	@NotNull(message = ValidationConstants.PERSON_COMPENSATION_RATE_NOT_NULL)
	@Pattern(regexp = CommonConstants.NUMBER_REGEX, message = ValidationConstants.PERSON_NUMBER_INVALID)
	private String compensationRate;

	@NotNull(message = ValidationConstants.PERSON_DEPARTMENT_NOT_NULL)
	@Size(max = 10, message = ValidationConstants.PERSON_DEPARTMENT_SIZE)
	private String departmentId;

	@NotNull(message = ValidationConstants.PERSON_WORK_LOCATION_NOT_NULL)
	@Size(max = 10, message = ValidationConstants.PERSON_WORK_LOCATION_SIZE)
	private String workLocationId;

	@NotNull(message = ValidationConstants.PERSON_PAY_GROUPS_NOT_NULL)
	@Size(max = 3, message = ValidationConstants.PERSON_PAY_GROUPS_SIZE)
	private String payGroupsIds;

	@NotNull(message = ValidationConstants.PERSON_ALTERNATE_EMPID_NOT_NULL)
	@Size(max = 11, message = ValidationConstants.PERSON_ALTERNATE_EMPID_SIZE)
	private String alternateEmpId;

	@NotNull(message = ValidationConstants.PERSON_WORK_EMAIL_NOT_NULL)
	@Size(max = 70, message = ValidationConstants.PERSON_WORK_EMAIL_SIZE)
	@Email(message = ValidationConstants.PERSON_EMAIL_VALIDATION)
	private String wokfEmailAddress;
	private String level;
	private String sponsor;
	private String vacation;
	private String sick;
	private String personalTime;
	private String floatingHolidays;

	// @DateTimeFormat(pattern = "yyyy-MM-DD")
	@Pattern(regexp = CommonConstants.DATE_REGEX, message = ValidationConstants.PERSON_DATE_INVALID)
	private String dateOfBirth;

	private String isBased;
	private String empVariableHour;
	private String empJobDes;

	public String getpEmplid() {
		return pEmplid;
	}

	public void setpEmplid(String pEmplid) {
		this.pEmplid = pEmplid;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getFormOfAddressId() {
		return formOfAddressId;
	}

	public void setFormOfAddressId(String formOfAddressId) {
		this.formOfAddressId = formOfAddressId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getHomeEmail() {
		return homeEmail;
	}

	public void setHomeEmail(String homeEmail) {
		this.homeEmail = homeEmail;
	}

	public String getGenderId() {
		return genderId;
	}

	public void setGenderId(String genderId) {
		this.genderId = genderId;
	}

	public String getEthnicityId() {
		return ethnicityId;
	}

	public void setEthnicityId(String ethnicityId) {
		this.ethnicityId = ethnicityId;
	}

	public String getMilitryStatus() {
		return militryStatus;
	}

	public void setMilitryStatus(String militryStatus) {
		this.militryStatus = militryStatus;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getEntryReason() {
		return entryReason;
	}

	public void setEntryReason(String entryReason) {
		this.entryReason = entryReason;
	}

	public String getEmplType() {
		return emplType;
	}

	public void setEmplType(String emplType) {
		this.emplType = emplType;
	}

	public String getEmpReqTemp() {
		return empReqTemp;
	}

	public void setEmpReqTemp(String empReqTemp) {
		this.empReqTemp = empReqTemp;
	}

	public String getEmpClass() {
		return empClass;
	}

	public void setEmpClass(String empClass) {
		this.empClass = empClass;
	}

	public String getAnnual_wages() {
		return annual_wages;
	}

	public void setAnnual_wages(String annual_wages) {
		this.annual_wages = annual_wages;
	}

	public String getBenefitClassId() {
		return benefitClassId;
	}

	public void setBenefitClassId(String benefitClassId) {
		this.benefitClassId = benefitClassId;
	}

	public String getStdHours() {
		return stdHours;
	}

	public void setStdHours(String stdHours) {
		this.stdHours = stdHours;
	}

	public String getBussinessTitle() {
		return bussinessTitle;
	}

	public void setBussinessTitle(String bussinessTitle) {
		this.bussinessTitle = bussinessTitle;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getFlsaStatus() {
		return flsaStatus;
	}

	public void setFlsaStatus(String flsaStatus) {
		this.flsaStatus = flsaStatus;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getCompensationBasicId() {
		return compensationBasicId;
	}

	public void setCompensationBasicId(String compensationBasicId) {
		this.compensationBasicId = compensationBasicId;
	}

	public String getCompensationRate() {
		return compensationRate;
	}

	public void setCompensationRate(String compensationRate) {
		this.compensationRate = compensationRate;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getWorkLocationId() {
		return workLocationId;
	}

	public void setWorkLocationId(String workLocationId) {
		this.workLocationId = workLocationId;
	}

	public String getPayGroupsIds() {
		return payGroupsIds;
	}

	public void setPayGroupsIds(String payGroupsIds) {
		this.payGroupsIds = payGroupsIds;
	}

	public String getAlternateEmpId() {
		return alternateEmpId;
	}

	public void setAlternateEmpId(String alternateEmpId) {
		this.alternateEmpId = alternateEmpId;
	}

	public String getWokfEmailAddress() {
		return wokfEmailAddress;
	}

	public void setWokfEmailAddress(String wokfEmailAddress) {
		this.wokfEmailAddress = wokfEmailAddress;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getVacation() {
		return vacation;
	}

	public void setVacation(String vacation) {
		this.vacation = vacation;
	}

	public String getSick() {
		return sick;
	}

	public void setSick(String sick) {
		this.sick = sick;
	}

	public String getPersonalTime() {
		return personalTime;
	}

	public void setPersonalTime(String personalTime) {
		this.personalTime = personalTime;
	}

	public String getFloatingHolidays() {
		return floatingHolidays;
	}

	public void setFloatingHolidays(String floatingHolidays) {
		this.floatingHolidays = floatingHolidays;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getIsBased() {
		return isBased;
	}

	public void setIsBased(String isBased) {
		this.isBased = isBased;
	}

	public String getEmpVariableHour() {
		return empVariableHour;
	}

	public void setEmpVariableHour(String empVariableHour) {
		this.empVariableHour = empVariableHour;
	}

	public String getEmpJobDes() {
		return empJobDes;
	}

	public void setEmpJobDes(String empJobDes) {
		this.empJobDes = empJobDes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address1 == null) ? 0 : address1.hashCode());
		result = prime * result + ((address2 == null) ? 0 : address2.hashCode());
		result = prime * result + ((alternateEmpId == null) ? 0 : alternateEmpId.hashCode());
		result = prime * result + ((annual_wages == null) ? 0 : annual_wages.hashCode());
		result = prime * result + ((benefitClassId == null) ? 0 : benefitClassId.hashCode());
		result = prime * result + ((bussinessTitle == null) ? 0 : bussinessTitle.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((companyId == null) ? 0 : companyId.hashCode());
		result = prime * result + ((compensationBasicId == null) ? 0 : compensationBasicId.hashCode());
		result = prime * result + ((compensationRate == null) ? 0 : compensationRate.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((departmentId == null) ? 0 : departmentId.hashCode());
		result = prime * result + ((empClass == null) ? 0 : empClass.hashCode());
		result = prime * result + ((empJobDes == null) ? 0 : empJobDes.hashCode());
		result = prime * result + ((empReqTemp == null) ? 0 : empReqTemp.hashCode());
		result = prime * result + ((empVariableHour == null) ? 0 : empVariableHour.hashCode());
		result = prime * result + ((emplType == null) ? 0 : emplType.hashCode());
		result = prime * result + ((entryDate == null) ? 0 : entryDate.hashCode());
		result = prime * result + ((entryReason == null) ? 0 : entryReason.hashCode());
		result = prime * result + ((ethnicityId == null) ? 0 : ethnicityId.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((floatingHolidays == null) ? 0 : floatingHolidays.hashCode());
		result = prime * result + ((flsaStatus == null) ? 0 : flsaStatus.hashCode());
		result = prime * result + ((formOfAddressId == null) ? 0 : formOfAddressId.hashCode());
		result = prime * result + ((genderId == null) ? 0 : genderId.hashCode());
		result = prime * result + ((homeEmail == null) ? 0 : homeEmail.hashCode());
		result = prime * result + ((homePhone == null) ? 0 : homePhone.hashCode());
		result = prime * result + ((isBased == null) ? 0 : isBased.hashCode());
		result = prime * result + ((jobCode == null) ? 0 : jobCode.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((militryStatus == null) ? 0 : militryStatus.hashCode());
		result = prime * result + ((nationalId == null) ? 0 : nationalId.hashCode());
		result = prime * result + ((pEmplid == null) ? 0 : pEmplid.hashCode());
		result = prime * result + ((payGroupsIds == null) ? 0 : payGroupsIds.hashCode());
		result = prime * result + ((personalTime == null) ? 0 : personalTime.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((sick == null) ? 0 : sick.hashCode());
		result = prime * result + ((sponsor == null) ? 0 : sponsor.hashCode());
		result = prime * result + ((stateId == null) ? 0 : stateId.hashCode());
		result = prime * result + ((stdHours == null) ? 0 : stdHours.hashCode());
		result = prime * result + ((supervisorId == null) ? 0 : supervisorId.hashCode());
		result = prime * result + ((vacation == null) ? 0 : vacation.hashCode());
		result = prime * result + ((wokfEmailAddress == null) ? 0 : wokfEmailAddress.hashCode());
		result = prime * result + ((workLocationId == null) ? 0 : workLocationId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonDetails other = (PersonDetails) obj;
		if (address1 == null) {
			if (other.address1 != null)
				return false;
		} else if (!address1.equals(other.address1))
			return false;
		if (address2 == null) {
			if (other.address2 != null)
				return false;
		} else if (!address2.equals(other.address2))
			return false;
		if (alternateEmpId == null) {
			if (other.alternateEmpId != null)
				return false;
		} else if (!alternateEmpId.equals(other.alternateEmpId))
			return false;
		if (annual_wages == null) {
			if (other.annual_wages != null)
				return false;
		} else if (!annual_wages.equals(other.annual_wages))
			return false;
		if (benefitClassId == null) {
			if (other.benefitClassId != null)
				return false;
		} else if (!benefitClassId.equals(other.benefitClassId))
			return false;
		if (bussinessTitle == null) {
			if (other.bussinessTitle != null)
				return false;
		} else if (!bussinessTitle.equals(other.bussinessTitle))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (companyId == null) {
			if (other.companyId != null)
				return false;
		} else if (!companyId.equals(other.companyId))
			return false;
		if (compensationBasicId == null) {
			if (other.compensationBasicId != null)
				return false;
		} else if (!compensationBasicId.equals(other.compensationBasicId))
			return false;
		if (compensationRate == null) {
			if (other.compensationRate != null)
				return false;
		} else if (!compensationRate.equals(other.compensationRate))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (departmentId == null) {
			if (other.departmentId != null)
				return false;
		} else if (!departmentId.equals(other.departmentId))
			return false;
		if (empClass == null) {
			if (other.empClass != null)
				return false;
		} else if (!empClass.equals(other.empClass))
			return false;
		if (empJobDes == null) {
			if (other.empJobDes != null)
				return false;
		} else if (!empJobDes.equals(other.empJobDes))
			return false;
		if (empReqTemp == null) {
			if (other.empReqTemp != null)
				return false;
		} else if (!empReqTemp.equals(other.empReqTemp))
			return false;
		if (empVariableHour == null) {
			if (other.empVariableHour != null)
				return false;
		} else if (!empVariableHour.equals(other.empVariableHour))
			return false;
		if (emplType == null) {
			if (other.emplType != null)
				return false;
		} else if (!emplType.equals(other.emplType))
			return false;
		if (entryDate == null) {
			if (other.entryDate != null)
				return false;
		} else if (!entryDate.equals(other.entryDate))
			return false;
		if (entryReason == null) {
			if (other.entryReason != null)
				return false;
		} else if (!entryReason.equals(other.entryReason))
			return false;
		if (ethnicityId == null) {
			if (other.ethnicityId != null)
				return false;
		} else if (!ethnicityId.equals(other.ethnicityId))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (floatingHolidays == null) {
			if (other.floatingHolidays != null)
				return false;
		} else if (!floatingHolidays.equals(other.floatingHolidays))
			return false;
		if (flsaStatus == null) {
			if (other.flsaStatus != null)
				return false;
		} else if (!flsaStatus.equals(other.flsaStatus))
			return false;
		if (formOfAddressId == null) {
			if (other.formOfAddressId != null)
				return false;
		} else if (!formOfAddressId.equals(other.formOfAddressId))
			return false;
		if (genderId == null) {
			if (other.genderId != null)
				return false;
		} else if (!genderId.equals(other.genderId))
			return false;
		if (homeEmail == null) {
			if (other.homeEmail != null)
				return false;
		} else if (!homeEmail.equals(other.homeEmail))
			return false;
		if (homePhone == null) {
			if (other.homePhone != null)
				return false;
		} else if (!homePhone.equals(other.homePhone))
			return false;
		if (isBased == null) {
			if (other.isBased != null)
				return false;
		} else if (!isBased.equals(other.isBased))
			return false;
		if (jobCode == null) {
			if (other.jobCode != null)
				return false;
		} else if (!jobCode.equals(other.jobCode))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (level == null) {
			if (other.level != null)
				return false;
		} else if (!level.equals(other.level))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (militryStatus == null) {
			if (other.militryStatus != null)
				return false;
		} else if (!militryStatus.equals(other.militryStatus))
			return false;
		if (nationalId == null) {
			if (other.nationalId != null)
				return false;
		} else if (!nationalId.equals(other.nationalId))
			return false;
		if (pEmplid == null) {
			if (other.pEmplid != null)
				return false;
		} else if (!pEmplid.equals(other.pEmplid))
			return false;
		if (payGroupsIds == null) {
			if (other.payGroupsIds != null)
				return false;
		} else if (!payGroupsIds.equals(other.payGroupsIds))
			return false;
		if (personalTime == null) {
			if (other.personalTime != null)
				return false;
		} else if (!personalTime.equals(other.personalTime))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (sick == null) {
			if (other.sick != null)
				return false;
		} else if (!sick.equals(other.sick))
			return false;
		if (sponsor == null) {
			if (other.sponsor != null)
				return false;
		} else if (!sponsor.equals(other.sponsor))
			return false;
		if (stateId == null) {
			if (other.stateId != null)
				return false;
		} else if (!stateId.equals(other.stateId))
			return false;
		if (stdHours == null) {
			if (other.stdHours != null)
				return false;
		} else if (!stdHours.equals(other.stdHours))
			return false;
		if (supervisorId == null) {
			if (other.supervisorId != null)
				return false;
		} else if (!supervisorId.equals(other.supervisorId))
			return false;
		if (vacation == null) {
			if (other.vacation != null)
				return false;
		} else if (!vacation.equals(other.vacation))
			return false;
		if (wokfEmailAddress == null) {
			if (other.wokfEmailAddress != null)
				return false;
		} else if (!wokfEmailAddress.equals(other.wokfEmailAddress))
			return false;
		if (workLocationId == null) {
			if (other.workLocationId != null)
				return false;
		} else if (!workLocationId.equals(other.workLocationId))
			return false;
		return true;
	}
}