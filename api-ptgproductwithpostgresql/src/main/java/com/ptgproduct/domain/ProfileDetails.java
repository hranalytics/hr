package com.ptgproduct.domain;

public class ProfileDetails {
  
  private Integer id;
  private String username;
  private String password;
  private String email;
  public Integer getId() {
    return id;
  }
  public String getUsername() {
    return username;
  }
  public String getPassword() {
    return password;
  }
  public String getEmail() {
    return email;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  
  
  

}
