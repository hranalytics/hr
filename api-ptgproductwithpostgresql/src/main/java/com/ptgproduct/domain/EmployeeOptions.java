/**
 * Program Name: EmployeeOptions 
 *                                                                 
 * Program Description / functionality: This is domain for EmployeeOptions
 *                            
 * Modules Impacted: Manage Employee Options
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer       Created             /Modified Date       Purpose
  *******************************************************************************
 * Astha & Bindu    04/01/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.ptgproduct.common.ValidationConstants;

public class EmployeeOptions {
	
	@NotNull(message = "optionEmplId should not be null")
	private String optionEmplId;
	 
	@NotNull(message = "optionId should not be null")
	private int optionId;
	
	private String optionName;
	
	private String optionDetails;
	
	@NotEmpty(message = "optionPreferences should not be null")
	@Size(min=2, max=30, message=ValidationConstants.EMPLOYEE_OPTION_PREFERENCE_SIZE)
	private String optionPreferences;

	
	public String getOptionEmplId() {
		return optionEmplId;
	}

	public void setOptionEmplId(String optionEmplId) {
		this.optionEmplId = optionEmplId;
	}

	public int getOptionId() {
		return optionId;
	}

	public void setOptionId(int optionId) {
		this.optionId = optionId;
	}
	
	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public String getOptionDetails() {
		return optionDetails;
	}

	public void setOptionDetails(String optionDetails) {
		this.optionDetails = optionDetails;
	}

	public String getOptionPreferences() {
		return optionPreferences;
	}

	public void setOptionPreferences(String optionPreferences) {
		this.optionPreferences = optionPreferences;
	}
}
