/**
* Program Name: PersonMyRoles 
*                                                                 
* Program Description / functionality: This is pojo class for person my roles
*                            
* Modules Impacted: person my roles
*                                                                    
* Tables affected:  ps_myrole_ref                                                                   
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Rakesh              29/12/2016
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.domain;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.ptgproduct.entity.BaseEntity;

public class PersonMyRoles extends BaseEntity {

	
	private static final long serialVersionUID = 4188403908570581065L;

	private String emplid;
	private String roleDescription;
	@NotEmpty(message = "organization should not be null") 
	@Size(min = 1, max = 6,message="min is 1 and max=6")
	private String organization;
	private String department;
	private String location;

	public String getEmplid() {
		return emplid;
	}

	public void setEmplid(String emplid) {
		this.emplid = emplid;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((emplid == null) ? 0 : emplid.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((organization == null) ? 0 : organization.hashCode());
		result = prime * result + ((roleDescription == null) ? 0 : roleDescription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonMyRoles other = (PersonMyRoles) obj;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (emplid == null) {
			if (other.emplid != null)
				return false;
		} else if (!emplid.equals(other.emplid))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (organization == null) {
			if (other.organization != null)
				return false;
		} else if (!organization.equals(other.organization))
			return false;
		if (roleDescription == null) {
			if (other.roleDescription != null)
				return false;
		} else if (!roleDescription.equals(other.roleDescription))
			return false;
		return true;
	}
}