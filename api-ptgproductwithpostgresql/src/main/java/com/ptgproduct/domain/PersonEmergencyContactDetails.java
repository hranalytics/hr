/**
 * Program Name: PersonEmergencyContactDetails 
 *                                                                 
 * Program Description / functionality: This is POJO class for Emergency Contact Details Services
 *                            
 * Modules Impacted: Emergency Contact Details Services
 *                                                                    
 * Tables affected: ps_emergency_cntct_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Harish      28/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.ptgproduct.entity.BaseEntity;

public class PersonEmergencyContactDetails extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String emplid;
	@NotNull(message = "ContactName should not be null")
	@Size(min = 3, message = "Length should be minmum of 3")
	private String contactName;
	@NotNull(message = "PrimaryContact should not be null")
	@Size(max = 3)
	private String primaryContact;
	@NotNull(message = "Relationship should not be null")
	@Size(min = 3)
	private String relationship;
	@NotNull(message = "SameAddressEmpl should not be null")
	@Size(max = 3)
	private String sameAddressEmpl;
	@NotNull(message = "Country should not be null")
	@Size(min = 3, message = "Length should be minmum of 3")
	private String country;
	@NotNull(message = "address1 should not be null")
	@Size(min = 7, message = "Length should be minmum of 7")
	private String address1;
	@NotNull(message = "address2 should not be null")
	@Size(min = 7, message = "Length should be minmum of 7")
	private String address2;
	@NotNull(message = "address3 should not be null")
	@Size(min = 7, message = "Length should be minmum of 7")
	private String address3;
	@NotNull(message = "City should not be null")
	@Size(min = 5, message = "Length should be minmum of 5")
	private String city;
	@NotNull(message = "State should not be null")
	@Size(min = 3, message = "Length should be minmum of 3")
	private String state;
	@NotNull(message = "Postal  should not be null")
	@Size(min = 3, max = 12, message = "Length should be minmum of 3")
	private String postal;
	@NotNull(message = "SamePhoneEmpl  should not be null")
	@Size(max = 3)
	private String samePhoneEmpl;
	@NotNull(message = "Phone  should not be null")
	@Size(min = 10, max = 24)
	@Pattern(regexp = "^(0|[1-9][0-9]*)$", message = "It should be numbers only")
	private String phone;
	@NotNull(message = "Num1  should not be null")
	@Size(min = 10, max = 10)
	@Pattern(regexp = "^(0|[1-9][0-9]*)$", message = "It should be numbers only")
	private String num1;
	@NotNull(message = "Num2  should not be null")
	@Size(min = 10, max = 10)
	@Pattern(regexp = "^(0|[1-9][0-9]*)$", message = "It should be numbers only")
	private String num2;

	public String getEmplid() {
		return emplid;
	}

	public void setEmplid(String emplid) {
		this.emplid = emplid;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getSameAddressEmpl() {
		return sameAddressEmpl;
	}

	public void setSameAddressEmpl(String sameAddressEmpl) {
		this.sameAddressEmpl = sameAddressEmpl;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getSamePhoneEmpl() {
		return samePhoneEmpl;
	}

	public void setSamePhoneEmpl(String samePhoneEmpl) {
		this.samePhoneEmpl = samePhoneEmpl;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNum1() {
		return num1;
	}

	public void setNum1(String num1) {
		this.num1 = num1;
	}

	public String getNum2() {
		return num2;
	}

	public void setNum2(String num2) {
		this.num2 = num2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address1 == null) ? 0 : address1.hashCode());
		result = prime * result + ((address2 == null) ? 0 : address2.hashCode());
		result = prime * result + ((address3 == null) ? 0 : address3.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((contactName == null) ? 0 : contactName.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((emplid == null) ? 0 : emplid.hashCode());
		result = prime * result + ((num1 == null) ? 0 : num1.hashCode());
		result = prime * result + ((num2 == null) ? 0 : num2.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((postal == null) ? 0 : postal.hashCode());
		result = prime * result + ((primaryContact == null) ? 0 : primaryContact.hashCode());
		result = prime * result + ((relationship == null) ? 0 : relationship.hashCode());
		result = prime * result + ((sameAddressEmpl == null) ? 0 : sameAddressEmpl.hashCode());
		result = prime * result + ((samePhoneEmpl == null) ? 0 : samePhoneEmpl.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonEmergencyContactDetails other = (PersonEmergencyContactDetails) obj;
		if (address1 == null) {
			if (other.address1 != null)
				return false;
		} else if (!address1.equals(other.address1))
			return false;
		if (address2 == null) {
			if (other.address2 != null)
				return false;
		} else if (!address2.equals(other.address2))
			return false;
		if (address3 == null) {
			if (other.address3 != null)
				return false;
		} else if (!address3.equals(other.address3))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (contactName == null) {
			if (other.contactName != null)
				return false;
		} else if (!contactName.equals(other.contactName))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (emplid == null) {
			if (other.emplid != null)
				return false;
		} else if (!emplid.equals(other.emplid))
			return false;
		if (num1 == null) {
			if (other.num1 != null)
				return false;
		} else if (!num1.equals(other.num1))
			return false;
		if (num2 == null) {
			if (other.num2 != null)
				return false;
		} else if (!num2.equals(other.num2))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (postal == null) {
			if (other.postal != null)
				return false;
		} else if (!postal.equals(other.postal))
			return false;
		if (primaryContact == null) {
			if (other.primaryContact != null)
				return false;
		} else if (!primaryContact.equals(other.primaryContact))
			return false;
		if (relationship == null) {
			if (other.relationship != null)
				return false;
		} else if (!relationship.equals(other.relationship))
			return false;
		if (sameAddressEmpl == null) {
			if (other.sameAddressEmpl != null)
				return false;
		} else if (!sameAddressEmpl.equals(other.sameAddressEmpl))
			return false;
		if (samePhoneEmpl == null) {
			if (other.samePhoneEmpl != null)
				return false;
		} else if (!samePhoneEmpl.equals(other.samePhoneEmpl))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}
}
