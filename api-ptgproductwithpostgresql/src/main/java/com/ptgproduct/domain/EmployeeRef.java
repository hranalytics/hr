package com.ptgproduct.domain;

public class EmployeeRef {

	private String employeeId;
	private String emailId;
	public EmployeeRef(String employeeId, String emailId) {
		this.employeeId = employeeId;
		this.emailId = emailId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
	
	
}
