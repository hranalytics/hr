/**
* Program Name: PersonAddress 
*                                                                 
* Program Description / functionality: This is pojo class for person address
*                            
* Modules Impacted: person address
*                                                                    
* Tables affected:  ps_person_addresses_ref                                                                   
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Bindu & Rakesh        23/12/2016
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.domain;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import com.ptgproduct.entity.BaseEntity;

public class PersonAddress extends BaseEntity{
	
	private static final long serialVersionUID = -7389585423540910114L;
	
	private String pEmplid;
	@NotEmpty(message = "paAddress1 should not be null") 
	@Size(min=5,max=55,message="min is 5 and max=55")
	private String paAddress1;
	@NotEmpty(message = "paAddress2 should not be null")
	@Size(min=5,max=55,message="min is 5 and max=55")
	private String paAddress2;
	@NotEmpty(message = "pacity should not be null")
	@Size(max=30,message="max=30")
	private String paCity;
	@NotEmpty(message = "pacountry should not be null")
	@Size(max=3,message="max=3")
	private String paCountry;
	@NotEmpty(message = "pastate should not be null")
	@Size(max=6,message="max=6")
	private String paState;
	@NotEmpty(message = "papostal should not be null")
	@Size(max=12,message="max=12")
	private String paPostal;
	@NotEmpty(message = "paAddressType should not be null")
	@Size(max=4,message="max=4")
	private String paAddressType;
	@NotEmpty(message = "effective date should not be null")
	@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = " Invalid Date Format. Hint YYYY-MM-DD")
	private String paEffdt;
	
	public String getpEmplid() {
		return pEmplid;
	}
	public void setpEmplid(String pEmplid) {
		this.pEmplid = pEmplid;
	}
	
	public String getPaAddress1() {
		return paAddress1;
	}
	public void setPaAddress1(String paAddress1) {
		this.paAddress1 = paAddress1;
	}
	public String getPaAddress2() {
		return paAddress2;
	}
	public void setPaAddress2(String paAddress2) {
		this.paAddress2 = paAddress2;
	}
	public String getPaCity() {
		return paCity;
	}
	public void setPaCity(String paCity) {
		this.paCity = paCity;
	}
	public String getPaCountry() {
		return paCountry;
	}
	public void setPaCountry(String paCountry) {
		this.paCountry = paCountry;
	}
	public String getPaState() {
		return paState;
	}
	public void setPaState(String paState) {
		this.paState = paState;
	}
	public String getPaPostal() {
		return paPostal;
	}
	public void setPaPostal(String paPostal) {
		this.paPostal = paPostal;
	}
	public String getPaAddressType() {
		return paAddressType;
	}
	public void setPaAddressType(String paAddressType) {
		this.paAddressType = paAddressType;
	}
	public String getPaEffdt() {
		return paEffdt;
	}
	public void setPaEffdt(String paEffdt) {
		this.paEffdt = paEffdt;
	}
}
