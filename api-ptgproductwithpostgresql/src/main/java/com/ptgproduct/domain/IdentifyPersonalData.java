/**
* Program Name: IdentifyPersonalData 
*                                                                 
* Program Description / functionality: This is pojo class for identify
*                            
* Modules Impacted: Identify
*                                                                    
* Tables affected:  ps_person_nid_ref                                                                   
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Bindu                 23/12/2016
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.domain;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class IdentifyPersonalData {
	@NotEmpty(message = "employeeId should not be null")
	private String employeeId;
	@Size(max = 20,message="max=20")
	private String socSecUsId;
	@NotEmpty(message = "employeeStatus should not be null")
	private String employeeStatus;
	@NotEmpty(message = "alternateEmpid should not be null")
	private String alternateEmpid;
	@NotEmpty(message = "citizenshipStatus should not be null")
	private String citizenshipStatus;

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getSocSecUsId() {
		return socSecUsId;
	}

	public void setSocSecUsId(String socSecUsId) {
		this.socSecUsId = socSecUsId;
	}

	public String getEmployeeStatus() {
		return employeeStatus;
	}

	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}

	public String getAlternateEmpid() {
		return alternateEmpid;
	}

	public void setAlternateEmpid(String alternateEmpid) {
		this.alternateEmpid = alternateEmpid;
	}

	public String getCitizenshipStatus() {
		return citizenshipStatus;
	}

	public void setCitizenshipStatus(String citizenshipStatus) {
		this.citizenshipStatus = citizenshipStatus;
	}
}
