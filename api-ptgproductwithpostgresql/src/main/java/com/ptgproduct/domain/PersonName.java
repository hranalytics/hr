/**
 * Program Name: PersonName 
 *                                                                 
 * Program Description / functionality: This is PersonName POJO
 *                            
 * Modules Impacted: PersonName module
 *                                                                    
 * Tables affected: ps_person_names_ref                                                                   
 *                                                                                                         
 * Developer              Created             /Modified Date         Purpose
  *******************************************************************************
 * Pavani & Kiran        23/12/2016	 			
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class PersonName {
	private String pemplId;
	@NotNull(message = "effective date should not be null")
	@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = " Invalid Date Format. Hint YYYY-MM-DD")
	private String effectiveDate;
	private String formOfAddress;
	@Size(min = 1, max = 30)
	private String firstName;
	@Size(min = 1, max = 30)
	private String middleName;
	@Size(min = 1, max = 30)
	private String lastName;
	@Size(min = 1, max = 15)
	private String suffix;
	@NotNull(message = "nameType shoud not be null")
	@Size(min = 1, max = 3)
	private String nameType;

	public PersonName(String pemplId, String effectiveDate, String formOfAddress, String firstName, String middleName,
			String lastName, String suffix, String nameType) {
		super();
		this.pemplId = pemplId;
		this.effectiveDate = effectiveDate;
		this.formOfAddress = formOfAddress;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.suffix = suffix;
		this.nameType = nameType;
	}

	public PersonName() {
		// TODO Auto-generated constructor stub
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getFormOfAddress() {
		return formOfAddress;
	}

	public void setFormOfAddress(String formOfAddress) {
		this.formOfAddress = formOfAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getNameType() {
		return nameType;
	}

	public void setNameType(String nameType) {
		this.nameType = nameType;
	}

	public String getPemplId() {
		return pemplId;
	}

	public void setPemplId(String pemplid) {
		this.pemplId = pemplid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effectiveDate == null) ? 0 : effectiveDate.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((formOfAddress == null) ? 0 : formOfAddress.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((nameType == null) ? 0 : nameType.hashCode());
		result = prime * result + ((pemplId == null) ? 0 : pemplId.hashCode());
		result = prime * result + ((suffix == null) ? 0 : suffix.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonName other = (PersonName) obj;
		if (effectiveDate == null) {
			if (other.effectiveDate != null)
				return false;
		} else if (!effectiveDate.equals(other.effectiveDate))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (formOfAddress == null) {
			if (other.formOfAddress != null)
				return false;
		} else if (!formOfAddress.equals(other.formOfAddress))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (nameType == null) {
			if (other.nameType != null)
				return false;
		} else if (!nameType.equals(other.nameType))
			return false;
		if (pemplId == null) {
			if (other.pemplId != null)
				return false;
		} else if (!pemplId.equals(other.pemplId))
			return false;
		if (suffix == null) {
			if (other.suffix != null)
				return false;
		} else if (!suffix.equals(other.suffix))
			return false;
		return true;
	}

}
