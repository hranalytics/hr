/**
 * Program Name: PersonService 
 *                                                                 
 * Program Description / functionality: This is interface for Person Service
 *                            
 * Modules Impacted: Manage Person Details
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * --------     15/12/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.service;

import java.util.List;

import com.ptgproduct.domain.EmailDetails;
import com.ptgproduct.domain.EmployeeOptions;
import com.ptgproduct.domain.IdentifyPersonalData;
import com.ptgproduct.domain.MyProxy;
import com.ptgproduct.domain.PersonAddress;
import com.ptgproduct.domain.PersonDetails;
import com.ptgproduct.domain.PersonEmergencyContactDetails;
import com.ptgproduct.domain.PersonMyRoles;
import com.ptgproduct.domain.PersonName;
import com.ptgproduct.domain.PersonPhoneDetails;
import com.ptgproduct.domain.PersonalData;
import com.ptgproduct.exception.PtgproductParseException;

public interface PersonService {

	/**
	 *
	 * Purpose: This method is to get all Person Information
	 * 
	 * @return: PersonDetails List
	 */
	List<PersonDetails> getAllPersons();

	/**
	 *
	 * Purpose: This method is to get all Person Information by ID
	 * 
	 * @param id
	 * 
	 * @return: PersonDetails
	 */
	PersonDetails getPersonById(String id);

	/**
	 *
	 * Purpose: This method is to add Person Information
	 * 
	 * @param personDetails
	 * 
	 * @return: Employee Id
	 */
	String addPerson(PersonDetails personDetails) throws PtgproductParseException;

	/**
	 *
	 * Purpose: This method is to remove Person Information
	 * 
	 * @param id
	 * 
	 * @return: Flag Value
	 */
	boolean deletePerson(String id);

	/**
	 *
	 * Purpose: This method is to update Person Information
	 * 
	 * @param personDetails
	 * 
	 * @return: Flag Value
	 */
	boolean updatePerson(PersonDetails personDetails) throws PtgproductParseException;

	/**
	   *
	   * Purpose: This method is to get person address by id
	   * 
	   * @param id
	   * 
	   * @return: List<PersonAddress>
	   */
	List<PersonAddress> getPersonAddressById(String id);

	/**
	   *
	   * Purpose: This method is to delete person address
	   * 
	   * @param id
	   * @param paAddressType
	   * @param paEffdt
	   * 
	   * @return: boolean object
	   */
	boolean deletePersonAddress(String id, String paAddressType, String paEffdt);

	/**
	   *
	   * Purpose: This method is to update person address
	   * 
	   * @param personAddress
	   * @param pEmplid
	   * 
	   * @return: boolean object
	   */
	boolean updatePersonAddress(PersonAddress personAddress, String pEmplid);

	/**
	 *
	 * Purpose: This method is to get Person names
	 * 
	 * @param id
	 * 
	 * @return: List<PersonName>
	 */
	List<PersonName> getPersonNames(String id);

	/**
	 *
	 * Purpose: This method is to update Person names
	 * 
	 * @param personName
	 * @param id
	 * 
	 * @return: PersonName
	 */
	PersonName updatePersonNames(PersonName personName, String id);

	/**
	 *
	 * Purpose: This method is to delete Person names
	 * 
	 * @param pemplId
	 * @param effectiveDate
	 * @param nameType
	 * 
	 * @return: boolean
	 */
	boolean deletePersonNames(String pemplId, String effectiveDate, String nameType);

	/**
	 *
	 * Purpose: This method is to add Person Phone Information
	 * 
	 * @param personPhoneDetails
	 * 
	 * @return: Employee Id
	 */
	String addPersonPhone(PersonPhoneDetails personPhoneDetails);

	/**
	 *
	 * Purpose: This method is to update Person Phone Information
	 * 
	 * @param personPhoneDetails
	 * 
	 * @return: Flag Value
	 */
	boolean updatePersonPhone(PersonPhoneDetails personPhoneDetails);

	/**
	 *
	 * Purpose: This method is to get Person Phone Information By Id
	 * 
	 * @param id
	 * 
	 * @return: PersonPhoneDetails List
	 */
	List<PersonPhoneDetails> getPersonPhoneById(String id);

	/**
	 *
	 * Purpose: This method is to delete Person Phone Information
	 * 
	 * @param id
	 * 
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	boolean deletePersonPhone(String id, String accessType);

	/**
	 *
	 * Purpose: This method is to add Person EmergencyConact details
	 * 
	 * @param emergencyContactDetails
	 * 
	 * @return: Employee Id
	 */
	String addPersonEmergencyConact(PersonEmergencyContactDetails emergencyContactDetails);

	/**
	 *
	 * Purpose: This method is to get Person EmergencyConact details by ID
	 * 
	 * @param id
	 * 
	 * @return: List of PersonEmergencyContactDetails object
	 */
	List<PersonEmergencyContactDetails> getPersonEmergencyContactById(String id);

	/**
	 *
	 * Purpose: This method is to remove Person Emergency Contact Details
	 * 
	 * 
	 * @param pEmplid
	 * 
	 * @param contactName
	 * 
	 * @param relationship
	 * 
	 * @return: flag value
	 */
	boolean deletePersonEmergencyContact(String id, String ContactName, String Relationship);

	/**
	 *
	 * Purpose: This method is to update Person Emergency Contact Details
	 * 
	 * 
	 * @param emergencyContactDetails
	 * 
	 * @return: flag value
	 */
	boolean updatePersonEmergencyContact(PersonEmergencyContactDetails emergencyContactDetails);

	/**
	 *
	 * Purpose: This method is to get Person Email Information by Id
	 * 
	 * @param id
	 * 
	 * @return: PersonDetails List
	 */
	List<EmailDetails> getEmailInfoById(String id);

	/**
	 *
	 * Purpose: This method is to add Person Email Information
	 * 
	 * @param emailDetails
	 * 
	 * @return: Employee Id
	 */
	String addEmailInfo(EmailDetails emailDetails);

	/**
	 *
	 * Purpose: This method is to update Person Email Information
	 * 
	 * @param emailDetails
	 * 
	 * @return: Flag Value
	 */
	boolean updateEmailInfo(EmailDetails emailDetails);

	/**
	 *
	 * Purpose: This method is to delete Person Email Information
	 * 
	 * @param pEmplid
	 * 
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	boolean removeEmailInfo(String pEmplid, String accessType);

	/**
	   *
	   * Purpose: This method is to update identify personal data
	   * 
	   * @param identifyPersonalData
	   * 
	   * @return: boolean object
	   */
	boolean updateIdentifyPersonalData(IdentifyPersonalData identifyPersonalData);

	/**
	   *
	   * Purpose: This method is to get identify personal data by id
	   * 
	   * @param id
	   * 
	   * @return: List<IdentifyPersonalData>
	   */
	List<IdentifyPersonalData> getIdentifyPersonalDataById(String id);

	/**
	   *
	   * Purpose: This method is to get my role by id
	   * 
	   * @param id
	   * 
	   * @return: List<PersonMyRoles>
	   */
	List<PersonMyRoles> getMyRoleById(String id);

	/**
	   *
	   * Purpose: This method is to add person my roles
	   * 
	   * @param myroles
	   * 
	   * @return: String object
	   */
	String addPersonMyRoles(PersonMyRoles myroles);

	/**
	   *
	   * Purpose: This method is to delete person my roles by id and organization
	   * 
	   * @param id
	   * @param Organization
	   * 
	   * @return: boolean object
	   */
	boolean deletePersonMyRoles(String id, String Organization);

	/**
	 *
	 * Purpose: This method is to get Personal data
	 * 
	 * @param id
	 * 
	 * @return: PersonalData
	 */
	PersonalData getPersonalDataById(String id);

	/**
	 *
	 * Purpose: This method is to update Personal data
	 * 
	 * @param personalData
	 * @param id
	 * 
	 * @return: PersonalData
	 */
	PersonalData updatePersonalData(PersonalData personalData, String id);

	/**
	 *
	 * Purpose: This method is to add Proxy
	 *
	 * @param myProxy
	 * 
	 * @return: String
	 */
	String addProxy(MyProxy myProxy);

	/**
	 *
	 * Purpose: This method is to get Proxy
	 *
	 * @param id
	 * @param effectiveDate
	 * 
	 * @return: List<MyProxy>
	 */
	List<MyProxy> getProxyById(String id, String effectiveDate);

	/**
	 *
	 * Purpose: This method is to delete Proxy
	 *
	 * @param id
	 * @param effectiveDate
	 * @return: boolean
	 */
	boolean deleteProxy(String id, String effectiveDate);

	/**
	 *
	 * Purpose: This method is to update Proxy
	 *
	 * @param myProxy
	 * @param id
	 * @param effectiveDate
	 * 
	 * @return: boolean
	 */
	boolean updateProxy(MyProxy myProxy, String id, String effectiveDate);

	/**
	   *
	   * Purpose: This method is to get employee options by id
	   * 
	   * @param id
	   * 
	   * @return: List<EmployeeOptions> 
	   */
	List<EmployeeOptions> getEmployeeOptionsById(String id);

	/**
	 *
	 * Purpose: This method is to update Employee Options
	 * 
	 * @param employeeOptions
	 * 
	 * @return:
	 */
	void updateEmployeeOptions(EmployeeOptions employeeOptions);

}
