package com.ptgproduct.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ptgproduct.dao.LandingPageDao;
import com.ptgproduct.entity.CityRef;
import com.ptgproduct.entity.LandingPage;
import com.ptgproduct.entity.StateRef;
import com.ptgproduct.service.LandingPageService;

public class LandingPageServiceImpl implements LandingPageService {
	private static final Logger LOGGER = LoggerFactory.getLogger(LandingPageServiceImpl.class);
	@Autowired
	private LandingPageDao landingPageDao;
	
	
	public LandingPageDao getLandingPageDao() {
		return landingPageDao;
	}


	public void setLandingPageDao(LandingPageDao landingPageDao) {
		this.landingPageDao = landingPageDao;
	}


	@Override
	public LandingPage getLandingPage() {
		return landingPageDao.getLandingPage();
	}


	@Override
	public List<StateRef> getStateRefByCountry(String countryId) {
		return landingPageDao.getStateRefByCountry(countryId);
	}


	@Override
	public List<CityRef> getCityRefByState(int stateId) {
		return landingPageDao.getCityRefByState(stateId);
	}


	@Override
	public LandingPage getLandingPageByEmployee(String employeeId) {
		return landingPageDao.getLandingPageByEmployee(employeeId);
	}

}
