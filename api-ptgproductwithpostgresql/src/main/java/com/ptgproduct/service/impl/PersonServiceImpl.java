/**
 * Program Name: PersonServiceImpl 
 *                                                                 
 * Program Description / functionality: This class implements PersonService interface 
 *         
 * Modules Impacted: Manage Person Details
 * 
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * --------     15/12/2016 
 * 
 * Associated Defects Raised : 
 *
 */

package com.ptgproduct.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ptgproduct.common.DateUtils;
import com.ptgproduct.dao.PersonDao;
import com.ptgproduct.domain.EmailDetails;
import com.ptgproduct.domain.EmployeeOptions;
import com.ptgproduct.domain.IdentifyPersonalData;
import com.ptgproduct.domain.MyProxy;
import com.ptgproduct.domain.PersonAddress;
import com.ptgproduct.domain.PersonDetails;
import com.ptgproduct.domain.PersonEmergencyContactDetails;
import com.ptgproduct.domain.PersonMyRoles;
import com.ptgproduct.domain.PersonName;
import com.ptgproduct.domain.PersonPhoneDetails;
import com.ptgproduct.domain.PersonalData;
import com.ptgproduct.entity.PsEmpOptionsRef;
import com.ptgproduct.entity.PsEmpOptionsTran;
import com.ptgproduct.entity.PsMyroleRef;
import com.ptgproduct.entity.PsPersonNamesRef;
import com.ptgproduct.entity.PsPersonRef;
import com.ptgproduct.entity.PsPersonalDataRef;
import com.ptgproduct.exception.PtgproductParseException;
import com.ptgproduct.mapper.EmailDetailsMapper;
import com.ptgproduct.mapper.EmployeeOptionsMapper;
import com.ptgproduct.mapper.IdentifyPersonalDataMapper;
import com.ptgproduct.mapper.MyProxyMapper;
import com.ptgproduct.mapper.PersonAddressMapper;
import com.ptgproduct.mapper.PersonDetailsMapper;
import com.ptgproduct.mapper.PersonEmergencyContactMapper;
import com.ptgproduct.mapper.PersonMyRolesMapper;
import com.ptgproduct.mapper.PersonNameMapper;
import com.ptgproduct.mapper.PersonPhoneMapper;
import com.ptgproduct.mapper.PersonalDataMapper;
import com.ptgproduct.service.PersonService;

@Transactional
public class PersonServiceImpl implements PersonService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);
	@Autowired
	private PersonDao personDao;

	public PersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}

	/**
	 *
	 * Purpose: This method is to get all Person Information
	 * 
	 * @return: PersonDetails List
	 */
	@Override
	public List<PersonDetails> getAllPersons() {
		LOGGER.debug("Get list of Persons details service begin");
		List<PersonDetails> pDetails = null;
		List<PsPersonRef> persons = personDao.getAllPersons();
		if (persons.size() != 0) {
			pDetails = new ArrayList<PersonDetails>();
			for (PsPersonRef psPersonRef : persons) {
				PersonDetails personDetails = PersonDetailsMapper.importData(psPersonRef);
				pDetails.add(personDetails);
			}
			LOGGER.debug("Get list of Persons details service end");
			return pDetails;
		} else {
			return pDetails;
		}
	}

	/**
	 *
	 * Purpose: This method is to get all Person Information by ID
	 * 
	 * @param id
	 * 
	 * @return: PersonDetails
	 */
	@Override
	public PersonDetails getPersonById(String id) {
		LOGGER.debug("Get Person details by id service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		PersonDetails personDetails = PersonDetailsMapper.importData(psPersonRef);
		LOGGER.debug("Get Person details by id service end");
		return personDetails;
	}

	/**
	 *
	 * Purpose: This method is to add Person Information
	 * 
	 * @param personDetails
	 * 
	 * @return: Employee Id
	 * @throws PtgproductParseException 
	 */
	@Override
	public String addPerson(PersonDetails personDetails) throws PtgproductParseException {
		LOGGER.debug("add Person details service begin");
		PsPersonRef psPersonRefs = PersonDetailsMapper.exportData(personDetails);
		String status = personDao.addPerson(psPersonRefs);
		LOGGER.debug("add Person details service end");
		return status;
	}

	/**
	 *
	 * Purpose: This method is to remove Person Information
	 * 
	 * @param id
	 * 
	 * @return: Flag Value
	 */
	@Override
	public boolean deletePerson(String id) {
		LOGGER.debug("Delete Person details service begin");
		boolean flag = personDao.deletePerson(id);
		LOGGER.debug("Delete Person details service end");
		return flag;
	}

	/**
	 *
	 * Purpose: This method is to update Person Information
	 * 
	 * @param personDetails
	 * 
	 * @return: Flag Value
	 * @throws PtgproductParseException 
	 */
	@Override
	public boolean updatePerson(PersonDetails personDetails) throws PtgproductParseException {
		LOGGER.debug("update Person details service begin");
		PsPersonRef psPersonRef = PersonDetailsMapper.exportData(personDetails);
		boolean flag = personDao.updatePerson(psPersonRef);
		LOGGER.debug("update Person details service end");
		return flag;
	}

	/**
	 *
	 * Purpose: This method is to get person address by id
	 * 
	 * @param id
	 * 
	 * @return: List<PersonAddress>
	 */
	@Override
	public List<PersonAddress> getPersonAddressById(String id) {
		LOGGER.debug("Get Person Address by id service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		List<PersonAddress> personAddress = PersonAddressMapper.importData(psPersonRef);
		LOGGER.debug("Get Person Address by id service end");
		return personAddress;
	}

	/**
	 *
	 * Purpose: This method is to delete person address
	 * 
	 * @param id
	 * @param paAddressType
	 * @param paEffdt
	 * 
	 * @return: boolean object
	 */
	@Override
	public boolean deletePersonAddress(String id, String paAddressType, String paEffdt) {
		LOGGER.debug("Delete Person Address by id service begin");
		boolean flag = personDao.deletePersonAddress(id, paAddressType, paEffdt);
		LOGGER.debug("Delete Person Address by id service end");
		return flag;
	}

	/**
	 *
	 * Purpose: This method is to update person address
	 * 
	 * @param personAddress
	 * @param pEmplid
	 * 
	 * @return: boolean object
	 */
	@Override
	public boolean updatePersonAddress(PersonAddress personAddress, String pEmplid) {
		LOGGER.debug("update Person Address service begin");
		PsPersonRef psPersonRefs = personDao.getPersonById(pEmplid);
		psPersonRefs = PersonAddressMapper.exportData(psPersonRefs, personAddress, pEmplid);
		personDao.updatePersonAddress(psPersonRefs);
		if (psPersonRefs.getPEmplid() != null) {
			return true;
		}
		LOGGER.debug("update Person Address service end");
		return false;
	}

	/**
	 *
	 * Purpose: This method is to get Person names
	 * 
	 * @param id
	 * 
	 * @return: List<PersonName>
	 */
	@Override
	public List<PersonName> getPersonNames(String id) {
		List<PersonName> personName = null;
		LOGGER.debug("Get Person name by id begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		if (psPersonRef.getPEmplid() != null) {
			personName = PersonNameMapper.importPersonName(psPersonRef);
			LOGGER.debug("Get Person name by id end");
		}
		LOGGER.debug("Get Person name by id end");
		return personName;
	}

	/**
	 *
	 * Purpose: This method is to delete Person names
	 * 
	 * @param personName
	 * @param id
	 * 
	 * @return: PersonName
	 */
	@Override
	public PersonName updatePersonNames(PersonName personName, String id) {
		LOGGER.debug("update Person name begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		try {
			psPersonRef = PersonNameMapper.exportPersonName(psPersonRef, personName);
		} catch (PtgproductParseException e) {
			e.printStackTrace();
		}
		personDao.updatePerson(psPersonRef);
		psPersonRef = personDao.getPersonById(id);
		LOGGER.debug("update Person name end");
		return PersonNameMapper.importUpdatedPersonName(psPersonRef, personName);
	}

	/**
	 *
	 * Purpose: This method is to update Person names
	 * 
	 * @param pemplId
	 * @param effectiveDate
	 * @param nameType
	 * 
	 * @return: boolean
	 */
	@Override
	public boolean deletePersonNames(String pemplId, String effectiveDate, String nameType) {
		LOGGER.debug("delete Person Name begin");
		boolean flag = personDao.deletePersonName(pemplId, effectiveDate, nameType);
		LOGGER.debug("delete Person Name end");
		return flag;
	}

	/**
	 *
	 * Purpose: This method is to add Person Phone Information
	 * 
	 * @param personPhoneDetails
	 * 
	 * @return: Employee Id
	 */
	@Override
	public String addPersonPhone(PersonPhoneDetails personPhoneDetails) {
		LOGGER.debug("add Person Phone service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(personPhoneDetails.getpEmplid());
		psPersonRef = PersonPhoneMapper.exportPhoneData(psPersonRef, personPhoneDetails);
		String result = String.valueOf(personDao.updatePerson(psPersonRef));
		LOGGER.debug("add Person Phone service end");
		return result;
	}

	/**
	 *
	 * Purpose: This method is to update Person Phone Information
	 * 
	 * @param personPhoneDetails
	 * 
	 * @return: Flag Value
	 */
	@Override
	public boolean updatePersonPhone(PersonPhoneDetails personPhoneDetails) {
		LOGGER.debug("update Person phone service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(personPhoneDetails.getpEmplid());
		psPersonRef = PersonPhoneMapper.exportPhoneDataUpdate(psPersonRef, personPhoneDetails);
		boolean flag = personDao.updatePerson(psPersonRef);
		LOGGER.debug("update Person phone service end");
		return flag;
	}

	/**
	 *
	 * Purpose: This method is to get Person Phone Information By Id
	 * 
	 * @param id
	 * 
	 * @return: PersonPhoneDetails List
	 */
	@Override
	public List<PersonPhoneDetails> getPersonPhoneById(String id) {
		LOGGER.debug("Get Person Phone details by id begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		List<PersonPhoneDetails> personPhoneDetails = PersonPhoneMapper.importPhoneData(psPersonRef);
		LOGGER.debug("Get Person Phone details by id end");
		return personPhoneDetails;
	}

	/**
	 *
	 * Purpose: This method is to delete Person Phone Information
	 * 
	 * @param id
	 * 
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	@Override
	public boolean deletePersonPhone(String pEmplid, String accessType) {
		LOGGER.debug("Delete Person Phone details by id begin");
		boolean flag = personDao.removePersonPhone(pEmplid, accessType);
		LOGGER.debug("Delete Person Phone details by id end");
		if (flag)
			return flag;
		else
			return flag;
	}

	/**
	 *
	 * Purpose: This method is to add Person EmergencyConact details
	 * 
	 * @param emergencyContactDetails
	 * 
	 * @return: emplId
	 */
	@Override
	public String addPersonEmergencyConact(PersonEmergencyContactDetails emergencyContactDetails) {
		LOGGER.debug("add Person emergency conact service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(emergencyContactDetails.getEmplid());
		psPersonRef = PersonEmergencyContactMapper.exportPhoneData(psPersonRef, emergencyContactDetails);
		boolean result = personDao.updatePerson(psPersonRef);
		LOGGER.debug("add Person emergency conact service end");
		return String.valueOf(result);
	}

	/**
	 *
	 * Purpose: This method is to update Person Emergency Contact Details
	 * 
	 * @param emergencyContactDetails
	 * 
	 * @return: flag value
	 */
	@Override
	public boolean updatePersonEmergencyContact(PersonEmergencyContactDetails emergencyContactDetails) {
		LOGGER.debug("update Person Emergency Contact service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(emergencyContactDetails.getEmplid());
		psPersonRef = PersonEmergencyContactMapper.exportEmergencyContactUpdate(psPersonRef, emergencyContactDetails);
		boolean flag = personDao.updatePerson(psPersonRef);
		LOGGER.debug("update Person Emergency Contact  service end");
		if (flag) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *
	 * Purpose: This method is to get Person EmergencyConact details by ID
	 * 
	 * @param id
	 * 
	 * @return: List of PersonEmergencyContactDetails object
	 */
	@Override
	public List<PersonEmergencyContactDetails> getPersonEmergencyContactById(String id) {
		LOGGER.debug("Get Person Phone details by id begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		List<PersonEmergencyContactDetails> emergencyContactDetails = PersonEmergencyContactMapper
				.importEmergencyContactData(psPersonRef);
		LOGGER.debug("Get Person Phone details by id end");
		return emergencyContactDetails;
	}

	/**
	 *
	 * Purpose: This method is to remove Person Emergency Contact Details
	 * 
	 * @param pEmplid
	 * 
	 * @param contactName
	 * 
	 * @param relationship
	 * 
	 * @return: flag value
	 */
	@Override
	public boolean deletePersonEmergencyContact(String id, String ContactName, String Relationship) {
		LOGGER.debug("Delete Person Emergency contact details by id begin");
		boolean flag = personDao.removePersonEmergencyContact(id, ContactName, Relationship);
		LOGGER.debug("Delete Person Emergency contact details by id end");
		if (flag)
			return flag;
		else
			return flag;
	}

	/**
	 *
	 * Purpose: This method is to get Person Email Information by Id
	 * 
	 * @param id
	 * 
	 * @return: PersonDetails List
	 */
	@Override
	public List<EmailDetails> getEmailInfoById(String id) {
		LOGGER.debug("getEmailInfoById service begin");
		PsPersonRef personRef = personDao.getPersonById(id);
		List<EmailDetails> emailDetails = EmailDetailsMapper.importEmailData(personRef);
		LOGGER.debug("getEmailInfoById service end");
		return emailDetails;
	}

	/**
	 *
	 * Purpose: This method is to add Person Email Information
	 * 
	 * @param emailDetails
	 * 
	 * @return: Employee Id
	 */
	@Override
	public String addEmailInfo(EmailDetails emailDetails) {
		LOGGER.debug("addEmailInfo service begin - " + emailDetails.getAccessType() + "," + emailDetails.getPeemplId());
		PsPersonRef psPersonRef = personDao.getPersonById(emailDetails.getPeemplId());
		psPersonRef = EmailDetailsMapper.exportEmailData(psPersonRef, emailDetails);
		boolean result = personDao.updatePerson(psPersonRef);
		LOGGER.debug("addEmailInfo service end");
		return String.valueOf(result);
	}

	/**
	 *
	 * Purpose: This method is to update Person Email Information
	 * 
	 * @param emailDetails
	 * 
	 * @return: Flag Value
	 */
	@Override
	public boolean updateEmailInfo(EmailDetails emailDetails) {
		LOGGER.debug("updateEmailInfo service begin");

		PsPersonRef psPersonRef = personDao.getPersonById(emailDetails.getPeemplId());
		psPersonRef = EmailDetailsMapper.exportEmailDataUpdate(psPersonRef, emailDetails);
		boolean flag = personDao.updatePerson(psPersonRef);
		LOGGER.debug("updateEmailInfo service end");
		return flag;

	}

	/**
	 *
	 * Purpose: This method is to delete Person Email Information
	 * 
	 * @param pEmplid
	 * 
	 * @param accessType
	 * 
	 * @return: Flag Value
	 */
	@Override
	public boolean removeEmailInfo(String pEmplid, String accessType) {
		LOGGER.debug("removeEmailInfo service begin");
		int result = personDao.deleteEmailInfo(pEmplid, accessType);
		LOGGER.debug("removeEmailInfo service end");
		if (result > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *
	 * Purpose: This method is to update identify personal data
	 * 
	 * @param identifyPersonalData
	 * 
	 * @return: boolean object
	 */
	@Override
	public boolean updateIdentifyPersonalData(IdentifyPersonalData identifyPersonalData) {
		LOGGER.debug("update Person Identify service begin");
		PsPersonRef psPersonRefs = personDao.getPersonById(identifyPersonalData.getEmployeeId());
		psPersonRefs = IdentifyPersonalDataMapper.exportData(psPersonRefs, identifyPersonalData);
		boolean flag = personDao.updateIdentifyPersonalData(psPersonRefs);
		LOGGER.debug("update Person Identify service end");
		return flag;
	}

	/**
	 *
	 * Purpose: This method is to get identify personal data by id
	 * 
	 * @param id
	 * 
	 * @return: List<IdentifyPersonalData>
	 */
	@Override
	public List<IdentifyPersonalData> getIdentifyPersonalDataById(String id) {
		LOGGER.debug("Get Identify Personal Data by id service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		List<IdentifyPersonalData> identifyPersonalData = IdentifyPersonalDataMapper.importData(psPersonRef);
		LOGGER.debug("Get Identify Personal Data by id service end");
		return identifyPersonalData;
	}

	/**
	 *
	 * Purpose: This method is to get my role by id
	 * 
	 * @param id
	 * 
	 * @return: List<PersonMyRoles>
	 */
	@Override
	public List<PersonMyRoles> getMyRoleById(String id) {
		LOGGER.debug("get Roles byid service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		LOGGER.debug("get Roles byid service end");
		return PersonMyRolesMapper.importMyroleData(psPersonRef);
	}

	/**
	 *
	 * Purpose: This method is to add person my roles
	 * 
	 * @param myroles
	 * 
	 * @return: String object
	 */
	@Override
	public String addPersonMyRoles(PersonMyRoles myroles) {
		LOGGER.debug("add Person My Roles service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(myroles.getEmplid());
		PsMyroleRef psMyroleRef = PersonMyRolesMapper.exportMyroleData(psPersonRef, myroles);
		String result = personDao.addPersonMyRoles(psMyroleRef);
		LOGGER.debug("add Person My Roles service end");
		return result;
	}

	/**
	 *
	 * Purpose: This method is to delete person my roles by id and organization
	 * 
	 * @param id
	 * @param Organization
	 * 
	 * @return: boolean object
	 */
	@Override
	public boolean deletePersonMyRoles(String id, String Organization) {
		LOGGER.debug("Delete Person My Roles details by id begin");
		boolean flag = personDao.deletePersonMyRoles(id, Organization);
		LOGGER.debug("Delete Person My Roles details by id end");
		if (flag)
			return flag;
		else
			return flag;
	}

	/**
	 *
	 * Purpose: This method is to update Personal data
	 * 
	 * @param personalData
	 * @param id
	 * 
	 * @return: PersonalData
	 */
	@Override
	public PersonalData updatePersonalData(PersonalData personalData, String id) {
		LOGGER.debug("update Person data begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		PsPersonalDataRef personalDataRef = PersonalDataMapper.exportPersonalData(psPersonRef, personalData);
		psPersonRef.setPsPersonalDataRef(personalDataRef);
		personDao.updatePerson(psPersonRef);
		LOGGER.debug("update Person data end");
		return PersonalDataMapper.importPersonalData(personalDataRef);
	}

	/**
	 *
	 * Purpose: This method is to get Personal data
	 * 
	 * @param id
	 * 
	 * @return: PersonalData
	 */
	@Override
	public PersonalData getPersonalDataById(String id) {
		LOGGER.debug("Get Personal data by id begin");
		PsPersonalDataRef psPersonalDataRef = personDao.getPersonalDataById(id);
		PersonalData personalData = PersonalDataMapper.importPersonalData(psPersonalDataRef);
		LOGGER.debug("Get Personal data by id end");
		return personalData;
	}

	/**
	 *
	 * Purpose: This method is to add Proxy
	 *
	 * @param myProxy
	 * 
	 * @return: String
	 */
	@Override
	public String addProxy(MyProxy myProxy) {
		PsPersonNamesRef addPersonNamesRef = null;
		LOGGER.debug("add proxy service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(myProxy.getPpEmplId());
		List<PsPersonNamesRef> psPersonNamesRefs = psPersonRef.getPsPersonNamesRefs();
		for (PsPersonNamesRef psPersonNamesRef : psPersonNamesRefs) {
			if (!psPersonNamesRef.getPnEffdt().equals(myProxy.getEffectiveDate())) {
				addPersonNamesRef = new PsPersonNamesRef();
				try {
					addPersonNamesRef.setEndDate(DateUtils.convertStringToDate(myProxy.getEndDate()));
					addPersonNamesRef.setPnEffdt(DateUtils.convertStringToDate(myProxy.getEffectiveDate()));
				} catch (PtgproductParseException e) {
					e.printStackTrace();
				}
				addPersonNamesRef.setPnCountryNmFormat("0");
				addPersonNamesRef.setPnNameType("PRI");
				addPersonNamesRef.setPnEffStatus(false);
				addPersonNamesRef.setPnFirstNameSrch("0");
				addPersonNamesRef.setPnLastNamePrefNld("0");
				addPersonNamesRef.setPnMiddleName("0");
				addPersonNamesRef.setPnLastNameSrch("0");
				addPersonNamesRef.setPnName("0");
				addPersonNamesRef.setPnLastUpdoprid("0");
				addPersonNamesRef.setPnNameAc("0");
				addPersonNamesRef.setPnNameDisplay("0");
				addPersonNamesRef.setPnNameDisplaySrch("0");
				addPersonNamesRef.setPnNameFormal("0");
				addPersonNamesRef.setPnNameInitials("0");
				addPersonNamesRef.setPnNamePrefix("0");
				addPersonNamesRef.setPnNameRoyalPrefix("0");
				addPersonNamesRef.setPnNameRoyalSuffix("0");
				addPersonNamesRef.setPnNameSuffix("0");
				addPersonNamesRef.setPnNameTittle("0");
				addPersonNamesRef.setPnPartnerLastName("0");
				addPersonNamesRef.setPnPartnerRoyPrefix("0");
				addPersonNamesRef.setPnPrefFirstName("0");
				addPersonNamesRef.setPnSecondLastName("0");
				addPersonNamesRef.setPnSecondLastSrch("0");
				addPersonNamesRef.setPnFirstName(myProxy.getFirstName());
				addPersonNamesRef.setPnLastName(myProxy.getLastNmae());
			}
		}
		psPersonRef.addPsPersonNamesRef(addPersonNamesRef);
		psPersonRef.setPsPersonPhoneRefs(null);
		personDao.updatePerson(psPersonRef);
		LOGGER.debug("add proxy service begin");
		return "inserted successfully";
	}

	/**
	 *
	 * Purpose: This method is to get Proxy
	 *
	 * @param id
	 *
	 * @param effectiveDate
	 * 
	 * @return: List<MyProxy>
	 */
	@Override
	public List<MyProxy> getProxyById(String id, String effectiveDate) {
		LOGGER.debug("get proxy byid service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		LOGGER.debug("get proxy byid service end");
		return MyProxyMapper.importProxyData(psPersonRef, effectiveDate);
	}

	/**
	 *
	 * Purpose: This method is to delete Proxy
	 *
	 * @param id
	 *
	 * @param effectiveDate
	 * 
	 * @return: boolean
	 */
	@Override
	public boolean deleteProxy(String id, String effectiveDate) {
		LOGGER.debug("delete proxy service begin");
		return personDao.deletePersonNameProxy(id, effectiveDate);
	}

	/**
	 *
	 * Purpose: This method is to update Proxy
	 *
	 * @param myProxy
	 *
	 * @param id
	 *
	 * @param effectiveDate
	 * 
	 * @return: boolean
	 */
	@Override
	public boolean updateProxy(MyProxy myProxy, String id, String effectiveDate) {
		LOGGER.debug("update proxy service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(id);
		PsPersonNamesRef updatePersonNamesRef = MyProxyMapper.exportProxyData(myProxy, psPersonRef);
		psPersonRef.addPsPersonNamesRef(updatePersonNamesRef);
		psPersonRef.setPsPersonPhoneRefs(null);
		boolean b = personDao.updatePerson(psPersonRef);
		LOGGER.debug("update proxy service begin");
		return b;
	}

	/**
	 *
	 * Purpose: This method is to get employee options by id
	 * 
	 * @param id
	 * 
	 * @return: List<EmployeeOptions>
	 */
	@Override
	public List<EmployeeOptions> getEmployeeOptionsById(String id) {
		LOGGER.debug("Get Employee Options by id service begin");
		PsPersonRef psPersonRef = personDao.getEmployeeOptionsByEmpId(id);
		List<EmployeeOptions> employeeOptions = new ArrayList<EmployeeOptions>();
		if (psPersonRef.getPsEmpOptionsTrans() != null && psPersonRef.getPsEmpOptionsTrans().size() > 0) {
			employeeOptions = EmployeeOptionsMapper.importData(psPersonRef);
		} else {
			List<PsEmpOptionsRef> empOptionsRefs = personDao.getEmployeeOptions();
			employeeOptions = EmployeeOptionsMapper.importDefaultEmployeeOptions(psPersonRef.getPEmplid(),
					empOptionsRefs);
		}
		LOGGER.debug("Get Employee Options by id service end");
		return employeeOptions;
	}

	/**
	 *
	 * Purpose: This method is to update Employee Options
	 * 
	 * @param employeeOptions
	 * 
	 * @return:
	 */
	@Override
	public void updateEmployeeOptions(EmployeeOptions employeeOptions) {
		LOGGER.debug("updateEmployeeOptions service begin");
		PsPersonRef psPersonRef = personDao.getPersonById(employeeOptions.getOptionEmplId());
		PsEmpOptionsRef psEmpOptionsRef = personDao.getEmployeeOptionByOptionId(employeeOptions.getOptionId());
		PsEmpOptionsTran psEmpOptionsTran = EmployeeOptionsMapper.exportEmpOptionData(psPersonRef, employeeOptions,
				psEmpOptionsRef);
		personDao.updateEmployeeOptions(psEmpOptionsTran);
		LOGGER.debug("updateEmployeeOptions service end");
	}
}
