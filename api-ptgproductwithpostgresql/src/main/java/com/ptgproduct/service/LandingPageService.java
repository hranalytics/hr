package com.ptgproduct.service;

import java.util.List;

import com.ptgproduct.entity.CityRef;
import com.ptgproduct.entity.LandingPage;
import com.ptgproduct.entity.StateRef;

public interface LandingPageService {
LandingPage getLandingPage();
LandingPage getLandingPageByEmployee(String employeeId);
List<StateRef> getStateRefByCountry(String countryId);
List<CityRef> getCityRefByState(int stateId);
	
}
