/**
* Program Name: PsPersonAddressesRefPK 
*                                                                 
* Program Description / functionality: This is entity class for person Address
*                            
* Modules Impacted: Person Address
*                                                                    
* Tables affected:  ps_person_addresses_ref                                                                    
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Rakesh & Bindu        23/12/2016 
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.entity;

public class PsPersonAddressesRefPK extends BaseEntity {
	private static final long serialVersionUID = 1L;

	private String psPersonRef;

	private java.util.Date paEffdt;

	private String paAddressType;

	public PsPersonAddressesRefPK() {
	}

	public String getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(String psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	public java.util.Date getPaEffdt() {
		return this.paEffdt;
	}

	public void setPaEffdt(java.util.Date paEffdt) {
		this.paEffdt = paEffdt;
	}

	public String getPaAddressType() {
		return this.paAddressType;
	}

	public void setPaAddressType(String paAddressType) {
		this.paAddressType = paAddressType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PsPersonAddressesRefPK)) {
			return false;
		}
		PsPersonAddressesRefPK castOther = (PsPersonAddressesRefPK) other;
		return this.psPersonRef.equals(castOther.psPersonRef) && this.paEffdt.equals(castOther.paEffdt)
				&& this.paAddressType.equals(castOther.paAddressType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.psPersonRef.hashCode();
		hash = hash * prime + this.paEffdt.hashCode();
		hash = hash * prime + this.paAddressType.hashCode();
		return hash;
	}
}