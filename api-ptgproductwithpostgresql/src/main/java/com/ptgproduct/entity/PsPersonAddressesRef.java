
/**
* Program Name: PsPersonAddressesRef 
*                                                                 
* Program Description / functionality: This is entity class for person Address
*                            
* Modules Impacted: Person Address
*                                                                    
* Tables affected:  ps_person_addresses_ref                                                                    
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Rakesh & Bindu        23/12/2016 
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ps_person_addresses_ref")
@NamedQuery(name = "PsPersonAddressesRef.findAll", query = "SELECT p FROM PsPersonAddressesRef p")
@IdClass(PsPersonAddressesRefPK.class)
public class PsPersonAddressesRef extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name = "pa_effdt")
	private java.util.Date paEffdt;

	@Id
	@Column(name = "pa_address_type")
	private String paAddressType;

	@Column(name = "pa_addr_field1")
	private String paAddrField1;

	@Column(name = "pa_addr_field2")
	private String paAddrField2;

	@Column(name = "pa_addr_field3")
	private String paAddrField3;

	@Column(name = "pa_address1")
	private String paAddress1;

	@Column(name = "pa_address1_ac")
	private String paAddress1Ac;

	@Column(name = "pa_address2")
	private String paAddress2;

	@Column(name = "pa_address2_ac")
	private String paAddress2Ac;

	@Column(name = "pa_address3")
	private String paAddress3;

	@Column(name = "pa_address3_ac")
	private String paAddress3Ac;

	@Column(name = "pa_address4")
	private String paAddress4;

	@Column(name = "pa_city")
	private String paCity;

	@Column(name = "pa_city_ac")
	private String paCityAc;

	@Column(name = "pa_country")
	private String paCountry;

	@Column(name = "pa_county")
	private String paCounty;

	@Column(name = "pa_eff_status")
	private Boolean paEffStatus;

	@Column(name = "pa_geo_code")
	private String paGeoCode;

	@Column(name = "pa_house_type")
	private String paHouseType;

	@Column(name = "pa_in_city_limit")
	private String paInCityLimit;

	@Column(name = "pa_last_upddtm")
	private Timestamp paLastUpddtm;

	@Column(name = "pa_last_updoprid")
	private String paLastUpdoprid;

	@Column(name = "pa_lastupddtm")
	private Timestamp paLastupddtm;

	@Column(name = "pa_lastupdoprid")
	private String paLastupdoprid;

	@Column(name = "pa_num1")
	private String paNum1;

	@Column(name = "pa_num2")
	private String paNum2;

	@Column(name = "pa_postal")
	private String paPostal;

	@Column(name = "pa_reg_region")
	private String paRegRegion;

	@Column(name = "pa_state")
	private String paState;

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pa_emplid")
	private PsPersonRef psPersonRef;

	public PsPersonAddressesRef() {
	}

	public java.util.Date getPaEffdt() {
		return paEffdt;
	}

	public void setPaEffdt(java.util.Date paEffdt) {
		this.paEffdt = paEffdt;
	}

	public String getPaAddressType() {
		return paAddressType;
	}

	public void setPaAddressType(String paAddressType) {
		this.paAddressType = paAddressType;
	}

	public String getPaAddrField1() {
		return this.paAddrField1;
	}

	public void setPaAddrField1(String paAddrField1) {
		this.paAddrField1 = paAddrField1;
	}

	public String getPaAddrField2() {
		return this.paAddrField2;
	}

	public void setPaAddrField2(String paAddrField2) {
		this.paAddrField2 = paAddrField2;
	}

	public String getPaAddrField3() {
		return this.paAddrField3;
	}

	public void setPaAddrField3(String paAddrField3) {
		this.paAddrField3 = paAddrField3;
	}

	public String getPaAddress1() {
		return this.paAddress1;
	}

	public void setPaAddress1(String paAddress1) {
		this.paAddress1 = paAddress1;
	}

	public String getPaAddress1Ac() {
		return this.paAddress1Ac;
	}

	public void setPaAddress1Ac(String paAddress1Ac) {
		this.paAddress1Ac = paAddress1Ac;
	}

	public String getPaAddress2() {
		return this.paAddress2;
	}

	public void setPaAddress2(String paAddress2) {
		this.paAddress2 = paAddress2;
	}

	public String getPaAddress2Ac() {
		return this.paAddress2Ac;
	}

	public void setPaAddress2Ac(String paAddress2Ac) {
		this.paAddress2Ac = paAddress2Ac;
	}

	public String getPaAddress3() {
		return this.paAddress3;
	}

	public void setPaAddress3(String paAddress3) {
		this.paAddress3 = paAddress3;
	}

	public String getPaAddress3Ac() {
		return this.paAddress3Ac;
	}

	public void setPaAddress3Ac(String paAddress3Ac) {
		this.paAddress3Ac = paAddress3Ac;
	}

	public String getPaAddress4() {
		return this.paAddress4;
	}

	public void setPaAddress4(String paAddress4) {
		this.paAddress4 = paAddress4;
	}

	public String getPaCity() {
		return this.paCity;
	}

	public void setPaCity(String paCity) {
		this.paCity = paCity;
	}

	public String getPaCityAc() {
		return this.paCityAc;
	}

	public void setPaCityAc(String paCityAc) {
		this.paCityAc = paCityAc;
	}

	public String getPaCountry() {
		return this.paCountry;
	}

	public void setPaCountry(String paCountry) {
		this.paCountry = paCountry;
	}

	public String getPaCounty() {
		return this.paCounty;
	}

	public void setPaCounty(String paCounty) {
		this.paCounty = paCounty;
	}

	public Boolean getPaEffStatus() {
		return this.paEffStatus;
	}

	public void setPaEffStatus(Boolean paEffStatus) {
		this.paEffStatus = paEffStatus;
	}

	public String getPaGeoCode() {
		return this.paGeoCode;
	}

	public void setPaGeoCode(String paGeoCode) {
		this.paGeoCode = paGeoCode;
	}

	public String getPaHouseType() {
		return this.paHouseType;
	}

	public void setPaHouseType(String paHouseType) {
		this.paHouseType = paHouseType;
	}

	public String getPaInCityLimit() {
		return this.paInCityLimit;
	}

	public void setPaInCityLimit(String paInCityLimit) {
		this.paInCityLimit = paInCityLimit;
	}

	public Timestamp getPaLastUpddtm() {
		return this.paLastUpddtm;
	}

	public void setPaLastUpddtm(Timestamp paLastUpddtm) {
		this.paLastUpddtm = paLastUpddtm;
	}

	public String getPaLastUpdoprid() {
		return this.paLastUpdoprid;
	}

	public void setPaLastUpdoprid(String paLastUpdoprid) {
		this.paLastUpdoprid = paLastUpdoprid;
	}

	public Timestamp getPaLastupddtm() {
		return this.paLastupddtm;
	}

	public void setPaLastupddtm(Timestamp paLastupddtm) {
		this.paLastupddtm = paLastupddtm;
	}

	public String getPaLastupdoprid() {
		return this.paLastupdoprid;
	}

	public void setPaLastupdoprid(String paLastupdoprid) {
		this.paLastupdoprid = paLastupdoprid;
	}

	public String getPaNum1() {
		return this.paNum1;
	}

	public void setPaNum1(String paNum1) {
		this.paNum1 = paNum1;
	}

	public String getPaNum2() {
		return this.paNum2;
	}

	public void setPaNum2(String paNum2) {
		this.paNum2 = paNum2;
	}

	public String getPaPostal() {
		return this.paPostal;
	}

	public void setPaPostal(String paPostal) {
		this.paPostal = paPostal;
	}

	public String getPaRegRegion() {
		return this.paRegRegion;
	}

	public void setPaRegRegion(String paRegRegion) {
		this.paRegRegion = paRegRegion;
	}

	public String getPaState() {
		return this.paState;
	}

	public void setPaState(String paState) {
		this.paState = paState;
	}

	public PsPersonRef getPsPersonRef() {
		return this.psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((paAddrField1 == null) ? 0 : paAddrField1.hashCode());
		result = prime * result + ((paAddrField2 == null) ? 0 : paAddrField2.hashCode());
		result = prime * result + ((paAddrField3 == null) ? 0 : paAddrField3.hashCode());
		result = prime * result + ((paAddress1 == null) ? 0 : paAddress1.hashCode());
		result = prime * result + ((paAddress1Ac == null) ? 0 : paAddress1Ac.hashCode());
		result = prime * result + ((paAddress2 == null) ? 0 : paAddress2.hashCode());
		result = prime * result + ((paAddress2Ac == null) ? 0 : paAddress2Ac.hashCode());
		result = prime * result + ((paAddress3 == null) ? 0 : paAddress3.hashCode());
		result = prime * result + ((paAddress3Ac == null) ? 0 : paAddress3Ac.hashCode());
		result = prime * result + ((paAddress4 == null) ? 0 : paAddress4.hashCode());
		result = prime * result + ((paAddressType == null) ? 0 : paAddressType.hashCode());
		result = prime * result + ((paCity == null) ? 0 : paCity.hashCode());
		result = prime * result + ((paCityAc == null) ? 0 : paCityAc.hashCode());
		result = prime * result + ((paCountry == null) ? 0 : paCountry.hashCode());
		result = prime * result + ((paCounty == null) ? 0 : paCounty.hashCode());
		result = prime * result + ((paEffStatus == null) ? 0 : paEffStatus.hashCode());
		result = prime * result + ((paEffdt == null) ? 0 : paEffdt.hashCode());
		result = prime * result + ((paGeoCode == null) ? 0 : paGeoCode.hashCode());
		result = prime * result + ((paHouseType == null) ? 0 : paHouseType.hashCode());
		result = prime * result + ((paInCityLimit == null) ? 0 : paInCityLimit.hashCode());
		result = prime * result + ((paLastUpddtm == null) ? 0 : paLastUpddtm.hashCode());
		result = prime * result + ((paLastUpdoprid == null) ? 0 : paLastUpdoprid.hashCode());
		result = prime * result + ((paLastupddtm == null) ? 0 : paLastupddtm.hashCode());
		result = prime * result + ((paLastupdoprid == null) ? 0 : paLastupdoprid.hashCode());
		result = prime * result + ((paNum1 == null) ? 0 : paNum1.hashCode());
		result = prime * result + ((paNum2 == null) ? 0 : paNum2.hashCode());
		result = prime * result + ((paPostal == null) ? 0 : paPostal.hashCode());
		result = prime * result + ((paRegRegion == null) ? 0 : paRegRegion.hashCode());
		result = prime * result + ((paState == null) ? 0 : paState.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PsPersonAddressesRef)) {
			return false;
		}
		PsPersonAddressesRef other = (PsPersonAddressesRef) obj;
		if (paAddrField1 == null) {
			if (other.paAddrField1 != null) {
				return false;
			}
		} else if (!paAddrField1.equals(other.paAddrField1)) {
			return false;
		}
		if (paAddrField2 == null) {
			if (other.paAddrField2 != null) {
				return false;
			}
		} else if (!paAddrField2.equals(other.paAddrField2)) {
			return false;
		}
		if (paAddrField3 == null) {
			if (other.paAddrField3 != null) {
				return false;
			}
		} else if (!paAddrField3.equals(other.paAddrField3)) {
			return false;
		}
		if (paAddress1 == null) {
			if (other.paAddress1 != null) {
				return false;
			}
		} else if (!paAddress1.equals(other.paAddress1)) {
			return false;
		}
		if (paAddress1Ac == null) {
			if (other.paAddress1Ac != null) {
				return false;
			}
		} else if (!paAddress1Ac.equals(other.paAddress1Ac)) {
			return false;
		}
		if (paAddress2 == null) {
			if (other.paAddress2 != null) {
				return false;
			}
		} else if (!paAddress2.equals(other.paAddress2)) {
			return false;
		}
		if (paAddress2Ac == null) {
			if (other.paAddress2Ac != null) {
				return false;
			}
		} else if (!paAddress2Ac.equals(other.paAddress2Ac)) {
			return false;
		}
		if (paAddress3 == null) {
			if (other.paAddress3 != null) {
				return false;
			}
		} else if (!paAddress3.equals(other.paAddress3)) {
			return false;
		}
		if (paAddress3Ac == null) {
			if (other.paAddress3Ac != null) {
				return false;
			}
		} else if (!paAddress3Ac.equals(other.paAddress3Ac)) {
			return false;
		}
		if (paAddress4 == null) {
			if (other.paAddress4 != null) {
				return false;
			}
		} else if (!paAddress4.equals(other.paAddress4)) {
			return false;
		}
		if (paAddressType == null) {
			if (other.paAddressType != null) {
				return false;
			}
		} else if (!paAddressType.equals(other.paAddressType)) {
			return false;
		}
		if (paCity == null) {
			if (other.paCity != null) {
				return false;
			}
		} else if (!paCity.equals(other.paCity)) {
			return false;
		}
		if (paCityAc == null) {
			if (other.paCityAc != null) {
				return false;
			}
		} else if (!paCityAc.equals(other.paCityAc)) {
			return false;
		}
		if (paCountry == null) {
			if (other.paCountry != null) {
				return false;
			}
		} else if (!paCountry.equals(other.paCountry)) {
			return false;
		}
		if (paCounty == null) {
			if (other.paCounty != null) {
				return false;
			}
		} else if (!paCounty.equals(other.paCounty)) {
			return false;
		}
		if (paEffStatus == null) {
			if (other.paEffStatus != null) {
				return false;
			}
		} else if (!paEffStatus.equals(other.paEffStatus)) {
			return false;
		}
		if (paEffdt == null) {
			if (other.paEffdt != null) {
				return false;
			}
		} else if (!paEffdt.equals(other.paEffdt)) {
			return false;
		}
		if (paGeoCode == null) {
			if (other.paGeoCode != null) {
				return false;
			}
		} else if (!paGeoCode.equals(other.paGeoCode)) {
			return false;
		}
		if (paHouseType == null) {
			if (other.paHouseType != null) {
				return false;
			}
		} else if (!paHouseType.equals(other.paHouseType)) {
			return false;
		}
		if (paInCityLimit == null) {
			if (other.paInCityLimit != null) {
				return false;
			}
		} else if (!paInCityLimit.equals(other.paInCityLimit)) {
			return false;
		}
		if (paLastUpddtm == null) {
			if (other.paLastUpddtm != null) {
				return false;
			}
		} else if (!paLastUpddtm.equals(other.paLastUpddtm)) {
			return false;
		}
		if (paLastUpdoprid == null) {
			if (other.paLastUpdoprid != null) {
				return false;
			}
		} else if (!paLastUpdoprid.equals(other.paLastUpdoprid)) {
			return false;
		}
		if (paLastupddtm == null) {
			if (other.paLastupddtm != null) {
				return false;
			}
		} else if (!paLastupddtm.equals(other.paLastupddtm)) {
			return false;
		}
		if (paLastupdoprid == null) {
			if (other.paLastupdoprid != null) {
				return false;
			}
		} else if (!paLastupdoprid.equals(other.paLastupdoprid)) {
			return false;
		}
		if (paNum1 == null) {
			if (other.paNum1 != null) {
				return false;
			}
		} else if (!paNum1.equals(other.paNum1)) {
			return false;
		}
		if (paNum2 == null) {
			if (other.paNum2 != null) {
				return false;
			}
		} else if (!paNum2.equals(other.paNum2)) {
			return false;
		}
		if (paPostal == null) {
			if (other.paPostal != null) {
				return false;
			}
		} else if (!paPostal.equals(other.paPostal)) {
			return false;
		}
		if (paRegRegion == null) {
			if (other.paRegRegion != null) {
				return false;
			}
		} else if (!paRegRegion.equals(other.paRegRegion)) {
			return false;
		}
		if (paState == null) {
			if (other.paState != null) {
				return false;
			}
		} else if (!paState.equals(other.paState)) {
			return false;
		}
		if (psPersonRef == null) {
			if (other.psPersonRef != null) {
				return false;
			}
		} else if (!psPersonRef.equals(other.psPersonRef)) {
			return false;
		}
		return true;
	}
}