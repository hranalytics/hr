/**
 * Program Name: PsPersonRef 
 *                                                                 
 * Program Description / functionality: This is parent entity class for Person phone Details Services
 *                            
 * Modules Impacted: Person phone Details Services
 *                                                                    
 * Tables affected: Ps_Person_Phone_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Harish       23/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.entity;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_person_ref")
@NamedQuery(name = "PsPersonRef.findAll", query = "SELECT p FROM PsPersonRef p")
public class PsPersonRef extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "emp_id_seq", strategy = "com.ptgproduct.common.EmployeeIdGenerator")
	@GeneratedValue(generator = "emp_id_seq")
	@Column(name = "p_emplid")
	private String pEmplid;

	@Column(name = "p_birth_country")
	private String pBirthCountry;

	@Temporal(TemporalType.DATE)
	@Column(name = "p_birth_date")
	private Date pBirthDate;

	@Column(name = "p_birth_state")
	private String pBirthState;

	@Column(name = "p_createddtm")
	private Timestamp pCreateddtm;

	@Temporal(TemporalType.DATE)
	@Column(name = "p_date_of_death")
	private Date pDateOfDeath;

	@Column(name = "p_isactive")
	private Boolean pIsactive;

	@Column(name = "p_last_child_upddtm")
	private Timestamp pLastChildUpddtm;

	@OneToMany(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<PsJobRef> psJobRefs;

	@OneToMany(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<PsPersonAddressesRef> psPersonAddressesRefs;

	@OneToMany(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<PsPersonNamesRef> psPersonNamesRefs;

	@OneToMany(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<PsPersonNidRef> psPersonNidRefs;

	@OneToMany(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<PsPersonPhoneRef> psPersonPhoneRefs;

	@OneToMany(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<PsEmergencyCntctRef> psEmergencyCntctRef;

	@OneToMany(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<PsPersonEmailRef> psPersonEmailRefs;

	@OneToOne(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private PsPersonalDataRef psPersonalDataRef;

	@OneToMany(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<PsMyroleRef> psMyroleRef;

	public PsPersonalDataRef getPsPersonalDataRef() {
		return psPersonalDataRef;
	}

	public void setPsPersonalDataRef(PsPersonalDataRef psPersonalDataRef) {
		this.psPersonalDataRef = psPersonalDataRef;
	}

	public PsPersonRef() {
	}

	public String getPEmplid() {
		return this.pEmplid;
	}

	public void setPEmplid(String pEmplid) {
		this.pEmplid = pEmplid;
	}

	public String getPBirthCountry() {
		return this.pBirthCountry;
	}

	public void setPBirthCountry(String pBirthCountry) {
		this.pBirthCountry = pBirthCountry;
	}

	public Date getPBirthDate() {
		return this.pBirthDate;
	}

	public void setPBirthDate(Date pBirthDate) {
		this.pBirthDate = pBirthDate;
	}

	public String getPBirthState() {
		return this.pBirthState;
	}

	public void setPBirthState(String pBirthState) {
		this.pBirthState = pBirthState;
	}

	public Timestamp getPCreateddtm() {
		return this.pCreateddtm;
	}

	public void setPCreateddtm(Timestamp pCreateddtm) {
		this.pCreateddtm = pCreateddtm;
	}

	public Date getPDateOfDeath() {
		return this.pDateOfDeath;
	}

	public void setPDateOfDeath(Date pDateOfDeath) {
		this.pDateOfDeath = pDateOfDeath;
	}

	public Boolean getPIsactive() {
		return this.pIsactive;
	}

	public void setPIsactive(Boolean pIsactive) {
		this.pIsactive = pIsactive;
	}

	public Timestamp getPLastChildUpddtm() {
		return this.pLastChildUpddtm;
	}

	public void setPLastChildUpddtm(Timestamp pLastChildUpddtm) {
		this.pLastChildUpddtm = pLastChildUpddtm;
	}

	public List<PsJobRef> getPsJobRefs() {
		return this.psJobRefs;
	}

	public void setPsJobRefs(List<PsJobRef> psJobRefs) {
		this.psJobRefs = psJobRefs;
	}

	public PsJobRef addPsJobRef(PsJobRef psJobRef) {
		getPsJobRefs().add(psJobRef);
		psJobRef.setPsPersonRef(this);
		return psJobRef;
	}

	public PsJobRef removePsJobRef(PsJobRef psJobRef) {
		getPsJobRefs().remove(psJobRef);
		psJobRef.setPsPersonRef(null);
		return psJobRef;
	}

	public List<PsPersonAddressesRef> getPsPersonAddressesRefs() {
		return this.psPersonAddressesRefs;
	}

	public void setPsPersonAddressesRefs(List<PsPersonAddressesRef> psPersonAddressesRefs) {
		this.psPersonAddressesRefs = psPersonAddressesRefs;
	}

	public PsPersonAddressesRef addPsPersonAddressesRef(PsPersonAddressesRef psPersonAddressesRef) {
		getPsPersonAddressesRefs().add(psPersonAddressesRef);
		psPersonAddressesRef.setPsPersonRef(this);
		return psPersonAddressesRef;
	}

	public PsPersonAddressesRef removePsPersonAddressesRef(PsPersonAddressesRef psPersonAddressesRef) {
		getPsPersonAddressesRefs().remove(psPersonAddressesRef);
		psPersonAddressesRef.setPsPersonRef(null);
		return psPersonAddressesRef;
	}

	public List<PsPersonNamesRef> getPsPersonNamesRefs() {
		return this.psPersonNamesRefs;
	}

	public void setPsPersonNamesRefs(List<PsPersonNamesRef> psPersonNamesRefs) {
		this.psPersonNamesRefs = psPersonNamesRefs;
	}

	public PsPersonNamesRef addPsPersonNamesRef(PsPersonNamesRef psPersonNamesRef) {
		getPsPersonNamesRefs().add(psPersonNamesRef);
		psPersonNamesRef.setPsPersonRef(this);
		return psPersonNamesRef;
	}

	public PsPersonNamesRef removePsPersonNamesRef(PsPersonNamesRef psPersonNamesRef) {
		getPsPersonNamesRefs().remove(psPersonNamesRef);
		psPersonNamesRef.setPsPersonRef(null);
		return psPersonNamesRef;
	}

	public List<PsPersonNidRef> getPsPersonNidRefs() {
		return this.psPersonNidRefs;
	}

	public void setPsPersonNidRefs(List<PsPersonNidRef> psPersonNidRefs) {
		this.psPersonNidRefs = psPersonNidRefs;
	}

	public PsPersonNidRef addPsPersonNidRef(PsPersonNidRef psPersonNidRef) {
		getPsPersonNidRefs().add(psPersonNidRef);
		psPersonNidRef.setPsPersonRef(this);
		return psPersonNidRef;
	}

	public PsPersonNidRef removePsPersonNidRef(PsPersonNidRef psPersonNidRef) {
		getPsPersonNidRefs().remove(psPersonNidRef);
		psPersonNidRef.setPsPersonRef(null);
		return psPersonNidRef;
	}

	public List<PsPersonPhoneRef> getPsPersonPhoneRefs() {
		return this.psPersonPhoneRefs;
	}

	public void setPsPersonPhoneRefs(List<PsPersonPhoneRef> psPersonPhoneRefs) {
		this.psPersonPhoneRefs = psPersonPhoneRefs;
	}

	public PsPersonPhoneRef addPsPersonPhoneRef(PsPersonPhoneRef psPersonPhoneRef) {
		getPsPersonPhoneRefs().add(psPersonPhoneRef);
		psPersonPhoneRef.setPsPersonRef(this);
		return psPersonPhoneRef;
	}

	public PsPersonPhoneRef removePsPersonPhoneRef(PsPersonPhoneRef psPersonPhoneRef) {
		getPsPersonPhoneRefs().remove(psPersonPhoneRef);
		psPersonPhoneRef.setPsPersonRef(null);
		return psPersonPhoneRef;
	}

	public List<PsEmergencyCntctRef> getPsEmergencyCntctRef() {
		return psEmergencyCntctRef;
	}

	public void setPsEmergencyCntctRef(List<PsEmergencyCntctRef> psEmergencyCntctRef) {
		this.psEmergencyCntctRef = psEmergencyCntctRef;
	}

	public PsEmergencyCntctRef addPsEmergencyCntctRef(PsEmergencyCntctRef psEmergencyCntctRef) {
		getPsEmergencyCntctRef().add(psEmergencyCntctRef);
		psEmergencyCntctRef.setPsPersonRef(this);
		return psEmergencyCntctRef;
	}

	public PsEmergencyCntctRef removePsEmergencyCntctRef(PsEmergencyCntctRef psEmergencyCntctRef) {
		getPsEmergencyCntctRef().remove(psEmergencyCntctRef);
		psEmergencyCntctRef.setPsPersonRef(null);
		return psEmergencyCntctRef;
	}

	public List<PsPersonEmailRef> getPsPersonEmailRefs() {
		return psPersonEmailRefs;
	}

	public void setPsPersonEmailRefs(List<PsPersonEmailRef> psPersonEmailRefs) {
		this.psPersonEmailRefs = psPersonEmailRefs;
	}

	public PsPersonEmailRef addPsPersonEmailRef(PsPersonEmailRef psPersonEmailRef) {
		getPsPersonEmailRefs().add(psPersonEmailRef);
		psPersonEmailRef.setPsPersonRef(this);
		return psPersonEmailRef;
	}

	public PsPersonEmailRef removePsPersonEmailRef(PsPersonEmailRef psPersonEmailRef) {
		getPsPersonEmailRefs().remove(psPersonEmailRef);
		psPersonEmailRef.setPsPersonRef(null);
		return psPersonEmailRef;
	}

	public List<PsMyroleRef> getPsMyroleRef() {
		return psMyroleRef;
	}

	public void setPsMyroleRef(List<PsMyroleRef> psMyroleRef) {
		this.psMyroleRef = psMyroleRef;
	}

	public PsMyroleRef addPsMyroleRef(PsMyroleRef psMyroleRef) {
		getPsMyroleRef().add(psMyroleRef);
		psMyroleRef.setPsPersonRef(this);
		return psMyroleRef;
	}

	public PsMyroleRef removePsMyroleRef(PsMyroleRef psMyroleRef) {
		getPsMyroleRef().remove(psMyroleRef);
		psMyroleRef.setPsPersonRef(null);
		return psMyroleRef;
	}

	@OneToMany(mappedBy = "psPersonRef", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<PsEmpOptionsTran> psEmpOptionsTrans;

	public List<PsEmpOptionsTran> getPsEmpOptionsTrans() {
		return this.psEmpOptionsTrans;
	}

	public void setPsEmpOptionsTrans(List<PsEmpOptionsTran> psEmpOptionsTrans) {
		this.psEmpOptionsTrans = psEmpOptionsTrans;
	}

	public PsEmpOptionsTran addPsEmpOptionsTran(PsEmpOptionsTran psEmpOptionsTran) {
		getPsEmpOptionsTrans().add(psEmpOptionsTran);
		psEmpOptionsTran.setPsPersonRef(this);
		return psEmpOptionsTran;
	}

	public PsEmpOptionsTran removePsEmpOptionsTran(PsEmpOptionsTran psEmpOptionsTran) {
		getPsEmpOptionsTrans().remove(psEmpOptionsTran);
		psEmpOptionsTran.setPsPersonRef(null);
		return psEmpOptionsTran;
	}
}