package com.ptgproduct.entity;

import java.util.List;

public class LandingPage extends BaseEntity {
	private static final long serialVersionUID = 7502740376887577247L;
	
	private List<CompanyEntity> companyEntities;
	
	private List<CountryRef> countryRefs;

	public List<CompanyEntity> getCompanyEntities() {
		return companyEntities;
	}

	public List<CountryRef> getCountryRefs() {
		return countryRefs;
	}

	public LandingPage(List<CompanyEntity> companyEntities, List<CountryRef> countryRefs) {
		super();
		this.companyEntities = companyEntities;
		this.countryRefs = countryRefs;
	}

	public LandingPage() {
	
	}

	public void setCompanyEntities(List<CompanyEntity> companyEntities) {
		this.companyEntities = companyEntities;
	}

	public void setCountryRefs(List<CountryRef> countryRefs) {
		this.countryRefs = countryRefs;
	}
	
	
	
	

}
