/**
* Program Name: PsJobRefPK 
*                                                                 
* Program Description / functionality: This is entity class for person Jobes
*                            
* Modules Impacted: Person Jobes
*                                                                    
* Tables affected:  ps_job_ref                                                                    
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Bindu               23/12/2016 
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.entity;

import java.io.Serializable;

public class PsJobRefPK implements Serializable {

	private static final long serialVersionUID = 1L;
	private String psPersonRef;
	private java.util.Date pjEffdt;
	private long pjEffseq;
	private long pjEmplRcd;

	public PsJobRefPK() {
	}

	public String getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(String psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	public java.util.Date getPjEffdt() {
		return this.pjEffdt;
	}

	public void setPjEffdt(java.util.Date pjEffdt) {
		this.pjEffdt = pjEffdt;
	}

	public long getPjEffseq() {
		return this.pjEffseq;
	}

	public void setPjEffseq(long pjEffseq) {
		this.pjEffseq = pjEffseq;
	}

	public long getPjEmplRcd() {
		return this.pjEmplRcd;
	}

	public void setPjEmplRcd(long pjEmplRcd) {
		this.pjEmplRcd = pjEmplRcd;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PsJobRefPK)) {
			return false;
		}
		PsJobRefPK castOther = (PsJobRefPK) other;
		return this.psPersonRef.equals(castOther.psPersonRef) && this.pjEffdt.equals(castOther.pjEffdt)
				&& (this.pjEffseq == castOther.pjEffseq) && (this.pjEmplRcd == castOther.pjEmplRcd);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.psPersonRef.hashCode();
		hash = hash * prime + this.pjEffdt.hashCode();
		hash = hash * prime + ((int) (this.pjEffseq ^ (this.pjEffseq >>> 32)));
		hash = hash * prime + ((int) (this.pjEmplRcd ^ (this.pjEmplRcd >>> 32)));
		return hash;
	}
}