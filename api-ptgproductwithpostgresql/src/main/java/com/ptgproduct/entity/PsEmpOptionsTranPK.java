/**
 * Program Name: PsEmpOptionsTranPK 
 *                                                                 
 * Program Description / functionality: This is Entity class for PsEmpOptionsTranPK
 *                            
 * Modules Impacted: Manage Employee Option
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Astha&Bindu     04/01/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.entity;

import java.io.Serializable;
import javax.persistence.*;

@Embeddable
public class PsEmpOptionsTranPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="eot_emplid", insertable=false, updatable=false)
	private String eotEmplid;

	@Column(name="eot_optid", insertable=false, updatable=false)
	private Integer eotOptid;

	public PsEmpOptionsTranPK() {
	}
	public String getEotEmplid() {
		return this.eotEmplid;
	}
	public void setEotEmplid(String eotEmplid) {
		this.eotEmplid = eotEmplid;
	}
	public Integer getEotOptid() {
		return this.eotOptid;
	}
	public void setEotOptid(Integer eotOptid) {
		this.eotOptid = eotOptid;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PsEmpOptionsTranPK)) {
			return false;
		}
		PsEmpOptionsTranPK castOther = (PsEmpOptionsTranPK)other;
		return 
			this.eotEmplid.equals(castOther.eotEmplid)
			&& this.eotOptid.equals(castOther.eotOptid);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.eotEmplid.hashCode();
		hash = hash * prime + this.eotOptid.hashCode();
		
		return hash;
	}
}