/**
 * Program Name: PsEmergencyCntctRef 
 *                                                                 
 * Program Description / functionality: This is entity class for Emergency Contact Details Services
 *                            
 * Modules Impacted: Emergency Contact Details Services
 *                                                                    
 * Tables affected: ps_emergency_cntct_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Harish      28/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "ps_emergency_cntct_ref")
@NamedQuery(name = "PsEmergencyCntctRef.findAll", query = "SELECT p FROM PsEmergencyCntctRef p")
@IdClass(PsEmergencyCntctRefPK.class)
public class PsEmergencyCntctRef implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "pec_contact_name")
	private String pecContactName;
	@Id
	@Column(name = "pec_relationship")
	private String pecRelationship;

	@Column(name = "pec_addr_field1")
	private String pecAddrField1;

	@Column(name = "pec_addr_field2")
	private String pecAddrField2;

	@Column(name = "pec_addr_field3")
	private String pecAddrField3;

	@Column(name = "pec_address_type")
	private String pecAddressType;

	@Column(name = "pec_address1")
	private String pecAddress1;

	@Column(name = "pec_address2")
	private String pecAddress2;

	@Column(name = "pec_address3")
	private String pecAddress3;

	@Column(name = "pec_address4")
	private String pecAddress4;

	@Column(name = "pec_city")
	private String pecCity;

	@Column(name = "pec_country")
	private String pecCountry;

	@Column(name = "pec_country_code")
	private String pecCountryCode;

	@Column(name = "pec_county")
	private String pecCounty;

	@Column(name = "pec_extension")
	private String pecExtension;

	@Column(name = "pec_geo_code")
	private String pecGeoCode;

	@Column(name = "pec_house_type")
	private String pecHouseType;

	@Column(name = "pec_in_city_limit")
	private String pecInCityLimit;

	@Column(name = "pec_num1")
	private String pecNum1;

	@Column(name = "pec_num2")
	private String pecNum2;

	@Column(name = "pec_phone")
	private String pecPhone;

	@Column(name = "pec_phone_type")
	private String pecPhoneType;

	@Column(name = "pec_postal")
	private String pecPostal;

	@Column(name = "pec_primary_contact")
	private String pecPrimaryContact;

	@Column(name = "pec_same_address_empl")
	private String pecSameAddressEmpl;

	@Column(name = "pec_same_phone_empl")
	private String pecSamePhoneEmpl;

	@Column(name = "pec_state")
	private String pecState;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pec_emplid")
	private PsPersonRef psPersonRef;

	public PsPersonRef getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	public PsEmergencyCntctRef() {
	}

	public String getPecAddrField1() {
		return this.pecAddrField1;
	}

	public void setPecAddrField1(String pecAddrField1) {
		this.pecAddrField1 = pecAddrField1;
	}

	public String getPecAddrField2() {
		return this.pecAddrField2;
	}

	public void setPecAddrField2(String pecAddrField2) {
		this.pecAddrField2 = pecAddrField2;
	}

	public String getPecAddrField3() {
		return this.pecAddrField3;
	}

	public void setPecAddrField3(String pecAddrField3) {
		this.pecAddrField3 = pecAddrField3;
	}

	public String getPecAddressType() {
		return this.pecAddressType;
	}

	public void setPecAddressType(String pecAddressType) {
		this.pecAddressType = pecAddressType;
	}

	public String getPecAddress1() {
		return this.pecAddress1;
	}

	public void setPecAddress1(String pecAddress1) {
		this.pecAddress1 = pecAddress1;
	}

	public String getPecAddress2() {
		return this.pecAddress2;
	}

	public void setPecAddress2(String pecAddress2) {
		this.pecAddress2 = pecAddress2;
	}

	public String getPecAddress3() {
		return this.pecAddress3;
	}

	public void setPecAddress3(String pecAddress3) {
		this.pecAddress3 = pecAddress3;
	}

	public String getPecAddress4() {
		return this.pecAddress4;
	}

	public void setPecAddress4(String pecAddress4) {
		this.pecAddress4 = pecAddress4;
	}

	public String getPecCity() {
		return this.pecCity;
	}

	public void setPecCity(String pecCity) {
		this.pecCity = pecCity;
	}

	public String getPecContactName() {
		return this.pecContactName;
	}

	public void setPecContactName(String pecContactName) {
		this.pecContactName = pecContactName;
	}

	public String getPecCountry() {
		return this.pecCountry;
	}

	public void setPecCountry(String pecCountry) {
		this.pecCountry = pecCountry;
	}

	public String getPecCountryCode() {
		return this.pecCountryCode;
	}

	public void setPecCountryCode(String pecCountryCode) {
		this.pecCountryCode = pecCountryCode;
	}

	public String getPecCounty() {
		return this.pecCounty;
	}

	public void setPecCounty(String pecCounty) {
		this.pecCounty = pecCounty;
	}

	public String getPecExtension() {
		return this.pecExtension;
	}

	public void setPecExtension(String pecExtension) {
		this.pecExtension = pecExtension;
	}

	public String getPecGeoCode() {
		return this.pecGeoCode;
	}

	public void setPecGeoCode(String pecGeoCode) {
		this.pecGeoCode = pecGeoCode;
	}

	public String getPecHouseType() {
		return this.pecHouseType;
	}

	public void setPecHouseType(String pecHouseType) {
		this.pecHouseType = pecHouseType;
	}

	public String getPecInCityLimit() {
		return this.pecInCityLimit;
	}

	public void setPecInCityLimit(String pecInCityLimit) {
		this.pecInCityLimit = pecInCityLimit;
	}

	public String getPecNum1() {
		return this.pecNum1;
	}

	public void setPecNum1(String pecNum1) {
		this.pecNum1 = pecNum1;
	}

	public String getPecNum2() {
		return this.pecNum2;
	}

	public void setPecNum2(String pecNum2) {
		this.pecNum2 = pecNum2;
	}

	public String getPecPhone() {
		return this.pecPhone;
	}

	public void setPecPhone(String pecPhone) {
		this.pecPhone = pecPhone;
	}

	public String getPecPhoneType() {
		return this.pecPhoneType;
	}

	public void setPecPhoneType(String pecPhoneType) {
		this.pecPhoneType = pecPhoneType;
	}

	public String getPecPostal() {
		return this.pecPostal;
	}

	public void setPecPostal(String pecPostal) {
		this.pecPostal = pecPostal;
	}

	public String getPecPrimaryContact() {
		return this.pecPrimaryContact;
	}

	public void setPecPrimaryContact(String pecPrimaryContact) {
		this.pecPrimaryContact = pecPrimaryContact;
	}

	public String getPecRelationship() {
		return this.pecRelationship;
	}

	public void setPecRelationship(String pecRelationship) {
		this.pecRelationship = pecRelationship;
	}

	public String getPecSameAddressEmpl() {
		return this.pecSameAddressEmpl;
	}

	public void setPecSameAddressEmpl(String pecSameAddressEmpl) {
		this.pecSameAddressEmpl = pecSameAddressEmpl;
	}

	public String getPecSamePhoneEmpl() {
		return this.pecSamePhoneEmpl;
	}

	public void setPecSamePhoneEmpl(String pecSamePhoneEmpl) {
		this.pecSamePhoneEmpl = pecSamePhoneEmpl;
	}

	public String getPecState() {
		return this.pecState;
	}

	public void setPecState(String pecState) {
		this.pecState = pecState;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pecAddrField1 == null) ? 0 : pecAddrField1.hashCode());
		result = prime * result + ((pecAddrField2 == null) ? 0 : pecAddrField2.hashCode());
		result = prime * result + ((pecAddrField3 == null) ? 0 : pecAddrField3.hashCode());
		result = prime * result + ((pecAddress1 == null) ? 0 : pecAddress1.hashCode());
		result = prime * result + ((pecAddress2 == null) ? 0 : pecAddress2.hashCode());
		result = prime * result + ((pecAddress3 == null) ? 0 : pecAddress3.hashCode());
		result = prime * result + ((pecAddress4 == null) ? 0 : pecAddress4.hashCode());
		result = prime * result + ((pecAddressType == null) ? 0 : pecAddressType.hashCode());
		result = prime * result + ((pecCity == null) ? 0 : pecCity.hashCode());
		result = prime * result + ((pecContactName == null) ? 0 : pecContactName.hashCode());
		result = prime * result + ((pecCountry == null) ? 0 : pecCountry.hashCode());
		result = prime * result + ((pecCountryCode == null) ? 0 : pecCountryCode.hashCode());
		result = prime * result + ((pecCounty == null) ? 0 : pecCounty.hashCode());
		result = prime * result + ((pecExtension == null) ? 0 : pecExtension.hashCode());
		result = prime * result + ((pecGeoCode == null) ? 0 : pecGeoCode.hashCode());
		result = prime * result + ((pecHouseType == null) ? 0 : pecHouseType.hashCode());
		result = prime * result + ((pecInCityLimit == null) ? 0 : pecInCityLimit.hashCode());
		result = prime * result + ((pecNum1 == null) ? 0 : pecNum1.hashCode());
		result = prime * result + ((pecNum2 == null) ? 0 : pecNum2.hashCode());
		result = prime * result + ((pecPhone == null) ? 0 : pecPhone.hashCode());
		result = prime * result + ((pecPhoneType == null) ? 0 : pecPhoneType.hashCode());
		result = prime * result + ((pecPostal == null) ? 0 : pecPostal.hashCode());
		result = prime * result + ((pecPrimaryContact == null) ? 0 : pecPrimaryContact.hashCode());
		result = prime * result + ((pecRelationship == null) ? 0 : pecRelationship.hashCode());
		result = prime * result + ((pecSameAddressEmpl == null) ? 0 : pecSameAddressEmpl.hashCode());
		result = prime * result + ((pecSamePhoneEmpl == null) ? 0 : pecSamePhoneEmpl.hashCode());
		result = prime * result + ((pecState == null) ? 0 : pecState.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PsEmergencyCntctRef other = (PsEmergencyCntctRef) obj;
		if (pecAddrField1 == null) {
			if (other.pecAddrField1 != null)
				return false;
		} else if (!pecAddrField1.equals(other.pecAddrField1))
			return false;
		if (pecAddrField2 == null) {
			if (other.pecAddrField2 != null)
				return false;
		} else if (!pecAddrField2.equals(other.pecAddrField2))
			return false;
		if (pecAddrField3 == null) {
			if (other.pecAddrField3 != null)
				return false;
		} else if (!pecAddrField3.equals(other.pecAddrField3))
			return false;
		if (pecAddress1 == null) {
			if (other.pecAddress1 != null)
				return false;
		} else if (!pecAddress1.equals(other.pecAddress1))
			return false;
		if (pecAddress2 == null) {
			if (other.pecAddress2 != null)
				return false;
		} else if (!pecAddress2.equals(other.pecAddress2))
			return false;
		if (pecAddress3 == null) {
			if (other.pecAddress3 != null)
				return false;
		} else if (!pecAddress3.equals(other.pecAddress3))
			return false;
		if (pecAddress4 == null) {
			if (other.pecAddress4 != null)
				return false;
		} else if (!pecAddress4.equals(other.pecAddress4))
			return false;
		if (pecAddressType == null) {
			if (other.pecAddressType != null)
				return false;
		} else if (!pecAddressType.equals(other.pecAddressType))
			return false;
		if (pecCity == null) {
			if (other.pecCity != null)
				return false;
		} else if (!pecCity.equals(other.pecCity))
			return false;
		if (pecContactName == null) {
			if (other.pecContactName != null)
				return false;
		} else if (!pecContactName.equals(other.pecContactName))
			return false;
		if (pecCountry == null) {
			if (other.pecCountry != null)
				return false;
		} else if (!pecCountry.equals(other.pecCountry))
			return false;
		if (pecCountryCode == null) {
			if (other.pecCountryCode != null)
				return false;
		} else if (!pecCountryCode.equals(other.pecCountryCode))
			return false;
		if (pecCounty == null) {
			if (other.pecCounty != null)
				return false;
		} else if (!pecCounty.equals(other.pecCounty))
			return false;
		if (pecExtension == null) {
			if (other.pecExtension != null)
				return false;
		} else if (!pecExtension.equals(other.pecExtension))
			return false;
		if (pecGeoCode == null) {
			if (other.pecGeoCode != null)
				return false;
		} else if (!pecGeoCode.equals(other.pecGeoCode))
			return false;
		if (pecHouseType == null) {
			if (other.pecHouseType != null)
				return false;
		} else if (!pecHouseType.equals(other.pecHouseType))
			return false;
		if (pecInCityLimit == null) {
			if (other.pecInCityLimit != null)
				return false;
		} else if (!pecInCityLimit.equals(other.pecInCityLimit))
			return false;
		if (pecNum1 == null) {
			if (other.pecNum1 != null)
				return false;
		} else if (!pecNum1.equals(other.pecNum1))
			return false;
		if (pecNum2 == null) {
			if (other.pecNum2 != null)
				return false;
		} else if (!pecNum2.equals(other.pecNum2))
			return false;
		if (pecPhone == null) {
			if (other.pecPhone != null)
				return false;
		} else if (!pecPhone.equals(other.pecPhone))
			return false;
		if (pecPhoneType == null) {
			if (other.pecPhoneType != null)
				return false;
		} else if (!pecPhoneType.equals(other.pecPhoneType))
			return false;
		if (pecPostal == null) {
			if (other.pecPostal != null)
				return false;
		} else if (!pecPostal.equals(other.pecPostal))
			return false;
		if (pecPrimaryContact == null) {
			if (other.pecPrimaryContact != null)
				return false;
		} else if (!pecPrimaryContact.equals(other.pecPrimaryContact))
			return false;
		if (pecRelationship == null) {
			if (other.pecRelationship != null)
				return false;
		} else if (!pecRelationship.equals(other.pecRelationship))
			return false;
		if (pecSameAddressEmpl == null) {
			if (other.pecSameAddressEmpl != null)
				return false;
		} else if (!pecSameAddressEmpl.equals(other.pecSameAddressEmpl))
			return false;
		if (pecSamePhoneEmpl == null) {
			if (other.pecSamePhoneEmpl != null)
				return false;
		} else if (!pecSamePhoneEmpl.equals(other.pecSamePhoneEmpl))
			return false;
		if (pecState == null) {
			if (other.pecState != null)
				return false;
		} else if (!pecState.equals(other.pecState))
			return false;
		if (psPersonRef == null) {
			if (other.psPersonRef != null)
				return false;
		} else if (!psPersonRef.equals(other.psPersonRef))
			return false;
		return true;
	}

}