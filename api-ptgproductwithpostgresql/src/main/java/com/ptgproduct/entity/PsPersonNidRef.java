/**
* Program Name: PsPersonNidRef 
*                                                                 
* Program Description / functionality: This is entity class for person national identity
*                            
* Modules Impacted: Identify
*                                                                    
* Tables affected:  ps_person_nid_ref                                                                    
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Bindu                 23/12/2016 
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "ps_person_nid_ref")
@NamedQuery(name = "PsPersonNidRef.findAll", query = "SELECT p FROM PsPersonNidRef p")
@IdClass(PsPersonNidRefPK.class)
public class PsPersonNidRef extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "pni_emplid", insertable = false, updatable = false)
	private String pniEmplid;

	@Id
	@Column(name = "pni_country")
	private String pniCountry;

	@Id
	@Column(name = "pni_national_id_type")
	private String pniNationalIdType;

	@Column(name = "pni_last_upddtm")
	private Timestamp pniLastUpddtm;

	@Column(name = "pni_last_updoprid")
	private Integer pniLastUpdoprid;

	@Column(name = "pni_lastupddttm")
	private Timestamp pniLastupddttm;

	@Column(name = "pni_lastupdoprid")
	private String pniLastupdoprid;

	@Column(name = "pni_national_id")
	private String pniNationalId;

	@Column(name = "pni_primary_nid")
	private String pniPrimaryNid;

	@Column(name = "pni_tax_ref_id_sgp")
	private String pniTaxRefIdSgp;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pni_emplid")
	@JsonBackReference("psPersonRef-psPersonNidRefs")
	private PsPersonRef psPersonRef;

	public PsPersonNidRef() {
	}

	public String getPniEmplid() {
		return pniEmplid;
	}

	public void setPniEmplid(String pniEmplid) {
		this.pniEmplid = pniEmplid;
	}

	public String getPniCountry() {
		return pniCountry;
	}

	public void setPniCountry(String pniCountry) {
		this.pniCountry = pniCountry;
	}

	public String getPniNationalIdType() {
		return pniNationalIdType;
	}

	public void setPniNationalIdType(String pniNationalIdType) {
		this.pniNationalIdType = pniNationalIdType;
	}

	public Timestamp getPniLastUpddtm() {
		return this.pniLastUpddtm;
	}

	public void setPniLastUpddtm(Timestamp pniLastUpddtm) {
		this.pniLastUpddtm = pniLastUpddtm;
	}

	public Integer getPniLastUpdoprid() {
		return this.pniLastUpdoprid;
	}

	public void setPniLastUpdoprid(Integer pniLastUpdoprid) {
		this.pniLastUpdoprid = pniLastUpdoprid;
	}

	public Timestamp getPniLastupddttm() {
		return this.pniLastupddttm;
	}

	public void setPniLastupddttm(Timestamp pniLastupddttm) {
		this.pniLastupddttm = pniLastupddttm;
	}

	public String getPniLastupdoprid() {
		return this.pniLastupdoprid;
	}

	public void setPniLastupdoprid(String pniLastupdoprid) {
		this.pniLastupdoprid = pniLastupdoprid;
	}

	public String getPniNationalId() {
		return this.pniNationalId;
	}

	public void setPniNationalId(String pniNationalId) {
		this.pniNationalId = pniNationalId;
	}

	public String getPniPrimaryNid() {
		return this.pniPrimaryNid;
	}

	public void setPniPrimaryNid(String pniPrimaryNid) {
		this.pniPrimaryNid = pniPrimaryNid;
	}

	public String getPniTaxRefIdSgp() {
		return this.pniTaxRefIdSgp;
	}

	public void setPniTaxRefIdSgp(String pniTaxRefIdSgp) {
		this.pniTaxRefIdSgp = pniTaxRefIdSgp;
	}

	public PsPersonRef getPsPersonRef() {
		return this.psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pniCountry == null) ? 0 : pniCountry.hashCode());
		result = prime * result + ((pniLastUpddtm == null) ? 0 : pniLastUpddtm.hashCode());
		result = prime * result + ((pniLastUpdoprid == null) ? 0 : pniLastUpdoprid.hashCode());
		result = prime * result + ((pniLastupddttm == null) ? 0 : pniLastupddttm.hashCode());
		result = prime * result + ((pniLastupdoprid == null) ? 0 : pniLastupdoprid.hashCode());
		result = prime * result + ((pniNationalId == null) ? 0 : pniNationalId.hashCode());
		result = prime * result + ((pniNationalIdType == null) ? 0 : pniNationalIdType.hashCode());
		result = prime * result + ((pniPrimaryNid == null) ? 0 : pniPrimaryNid.hashCode());
		result = prime * result + ((pniTaxRefIdSgp == null) ? 0 : pniTaxRefIdSgp.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PsPersonNidRef)) {
			return false;
		}
		PsPersonNidRef other = (PsPersonNidRef) obj;
		if (pniCountry == null) {
			if (other.pniCountry != null) {
				return false;
			}
		} else if (!pniCountry.equals(other.pniCountry)) {
			return false;
		}
		if (pniLastUpddtm == null) {
			if (other.pniLastUpddtm != null) {
				return false;
			}
		} else if (!pniLastUpddtm.equals(other.pniLastUpddtm)) {
			return false;
		}
		if (pniLastUpdoprid == null) {
			if (other.pniLastUpdoprid != null) {
				return false;
			}
		} else if (!pniLastUpdoprid.equals(other.pniLastUpdoprid)) {
			return false;
		}
		if (pniLastupddttm == null) {
			if (other.pniLastupddttm != null) {
				return false;
			}
		} else if (!pniLastupddttm.equals(other.pniLastupddttm)) {
			return false;
		}
		if (pniLastupdoprid == null) {
			if (other.pniLastupdoprid != null) {
				return false;
			}
		} else if (!pniLastupdoprid.equals(other.pniLastupdoprid)) {
			return false;
		}
		if (pniNationalId == null) {
			if (other.pniNationalId != null) {
				return false;
			}
		} else if (!pniNationalId.equals(other.pniNationalId)) {
			return false;
		}
		if (pniNationalIdType == null) {
			if (other.pniNationalIdType != null) {
				return false;
			}
		} else if (!pniNationalIdType.equals(other.pniNationalIdType)) {
			return false;
		}
		if (pniPrimaryNid == null) {
			if (other.pniPrimaryNid != null) {
				return false;
			}
		} else if (!pniPrimaryNid.equals(other.pniPrimaryNid)) {
			return false;
		}
		if (pniTaxRefIdSgp == null) {
			if (other.pniTaxRefIdSgp != null) {
				return false;
			}
		} else if (!pniTaxRefIdSgp.equals(other.pniTaxRefIdSgp)) {
			return false;
		}
		if (psPersonRef == null) {
			if (other.psPersonRef != null) {
				return false;
			}
		} else if (!psPersonRef.equals(other.psPersonRef)) {
			return false;
		}
		return true;
	}
}