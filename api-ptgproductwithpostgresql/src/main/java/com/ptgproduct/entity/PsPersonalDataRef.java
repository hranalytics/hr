/**
 * Program Name: PsPersonalDataRef 
 *                                                                 
 * Program Description / functionality: This is entity class for personal data
 *                            
 * Modules Impacted: Personal data
 *                                                                    
 * Tables affected:  ps_personal_data_ref                                                                    
 *                                                                                                         
 * Developer           Created             /Modified Date       Purpose
  *******************************************************************************
 * Pavani             27/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */ 
package com.ptgproduct.entity;

import java.io.Serializable;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ps_personal_data_ref")
@NamedQuery(name = "PsPersonalDataRef.findAll", query = "SELECT p FROM PsPersonalDataRef p")
public class PsPersonalDataRef implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "ppd_addr_field1")
	private String ppdAddrField1;

	@Column(name = "ppd_addr_field1_other")
	private String ppdAddrField1Other;

	@Column(name = "ppd_addr_field2")
	private String ppdAddrField2;

	@Column(name = "ppd_addr_field2_other")
	private String ppdAddrField2Other;

	@Column(name = "ppd_addr_field3")
	private String ppdAddrField3;

	@Column(name = "ppd_addr_field3_other")
	private String ppdAddrField3Other;

	@Column(name = "ppd_address1")
	private String ppdAddress1;

	@Column(name = "ppd_address1_ac")
	private String ppdAddress1Ac;

	@Column(name = "ppd_address1_other")
	private String ppdAddress1Other;

	@Column(name = "ppd_address2")
	private String ppdAddress2;

	@Column(name = "ppd_address2_ac")
	private String ppdAddress2Ac;

	@Column(name = "ppd_address2_other")
	private String ppdAddress2Other;

	@Column(name = "ppd_address3")
	private String ppdAddress3;

	@Column(name = "ppd_address3_ac")
	private String ppdAddress3Ac;

	@Column(name = "ppd_address3_other")
	private String ppdAddress3Other;

	@Column(name = "ppd_address4")
	private String ppdAddress4;

	@Column(name = "ppd_address4_other")
	private String ppdAddress4Other;

	@Column(name = "ppd_alter_emplid")
	private String ppdAlterEmplid;

	@Column(name = "ppd_barg_unit")
	private String ppdBargUnit;

	@Column(name = "ppd_bilingualism_code")
	private String ppdBilingualismCode;

	@Column(name = "ppd_birthcountry")
	private String ppdBirthcountry;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_birthdate")
	private Date ppdBirthdate;

	@Column(name = "ppd_birthplace")
	private String ppdBirthplace;

	@Column(name = "ppd_birthstate")
	private String ppdBirthstate;

	@Column(name = "ppd_campus_id")
	private String ppdCampusId;

	@Column(name = "ppd_citizen_proof1")
	private String ppdCitizenProof1;

	@Column(name = "ppd_citizen_proof2")
	private String ppdCitizenProof2;

	@Column(name = "ppd_city")
	private String ppdCity;

	@Column(name = "ppd_city_ac")
	private String ppdCityAc;

	@Column(name = "ppd_city_other")
	private String ppdCityOther;

	@Column(name = "ppd_country")
	private String ppdCountry;

	@Column(name = "ppd_country_code")
	private String ppdCountryCode;

	@Column(name = "ppd_country_nm_format")
	private String ppdCountryNmFormat;

	@Column(name = "ppd_country_other")
	private String ppdCountryOther;

	@Column(name = "ppd_county")
	private String ppdCounty;

	@Column(name = "ppd_county_other")
	private String ppdCountyOther;

	@Column(name = "ppd_cpamid")
	private String ppdCpamid;

	@Column(name = "ppd_death_certif_nbr")
	private String ppdDeathCertifNbr;

	@Column(name = "ppd_disabled")
	private String ppdDisabled;

	@Column(name = "ppd_disabled_vet")
	private String ppdDisabledVet;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_dt_of_death")
	private Date ppdDtOfDeath;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_entry_dt_fra")
	private Date ppdEntryDtFra;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_expctd_military_dt")
	private Date ppdExpctdMilitaryDt;

	@Column(name = "ppd_extension")
	private String ppdExtension;

	@Column(name = "ppd_ferpa")
	private String ppdFerpa;

	@Column(name = "ppd_first_name")
	private String ppdFirstName;

	@Column(name = "ppd_first_name_srch")
	private String ppdFirstNameSrch;

	@Column(name = "ppd_ft_student")
	private String ppdFtStudent;

	@Column(name = "ppd_geo_code")
	private String ppdGeoCode;

	@Column(name = "ppd_geo_code_other")
	private String ppdGeoCodeOther;

	@Column(name = "ppd_grade")
	private String ppdGrade;

	@Column(name = "ppd_gvt_change_flag")
	private String ppdGvtChangeFlag;

	@Column(name = "ppd_gvt_cred_mil_svce")
	private String ppdGvtCredMilSvce;

	@Column(name = "ppd_gvt_curr_agcy_empl")
	private String ppdGvtCurrAgcyEmpl;

	@Column(name = "ppd_gvt_curr_fed_empl")
	private String ppdGvtCurrFedEmpl;

	@Column(name = "ppd_gvt_disability_cd")
	private String ppdGvtDisabilityCd;

	@Column(name = "ppd_gvt_draft_status")
	private String ppdGvtDraftStatus;

	@Column(name = "ppd_gvt_high_grade")
	private String ppdGvtHighGrade;

	@Column(name = "ppd_gvt_high_pay_plan")
	private String ppdGvtHighPayPlan;

	@Column(name = "ppd_gvt_mil_grade")
	private String ppdGvtMilGrade;

	@Column(name = "ppd_gvt_mil_resrve_cat")
	private String ppdGvtMilResrveCat;

	@Column(name = "ppd_gvt_mil_sep_ret")
	private String ppdGvtMilSepRet;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_gvt_mil_svce_end")
	private Date ppdGvtMilSvceEnd;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_gvt_mil_svce_start")
	private Date ppdGvtMilSvceStart;

	@Column(name = "ppd_gvt_mil_verify")
	private String ppdGvtMilVerify;

	@Column(name = "ppd_gvt_military_comp")
	private String ppdGvtMilitaryComp;

	@Column(name = "ppd_gvt_par_nbr_last")
	private BigDecimal ppdGvtParNbrLast;

	@Column(name = "ppd_gvt_pay_plan")
	private String ppdGvtPayPlan;

	@Column(name = "ppd_gvt_prev_agcy_empl")
	private String ppdGvtPrevAgcyEmpl;

	@Column(name = "ppd_gvt_prev_fed_empl")
	private String ppdGvtPrevFedEmpl;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_gvt_sep_incent_dt")
	private Date ppdGvtSepIncentDt;

	@Column(name = "ppd_gvt_sep_incentive")
	private String ppdGvtSepIncentive;

	@Column(name = "ppd_gvt_tenure")
	private String ppdGvtTenure;

	@Column(name = "ppd_gvt_unif_svc_ctr")
	private String ppdGvtUnifSvcCtr;

	@Column(name = "ppd_gvt_vet_pref_appt")
	private String ppdGvtVetPrefAppt;

	@Column(name = "ppd_gvt_vet_pref_rif")
	private String ppdGvtVetPrefRif;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_gvt_yr_attained")
	private Date ppdGvtYrAttained;

	@Column(name = "ppd_health_care_nbr")
	private String ppdHealthCareNbr;

	@Column(name = "ppd_health_care_state")
	private String ppdHealthCareState;

	@Column(name = "ppd_highest_educ_lvl")
	private String ppdHighestEducLvl;

	@Column(name = "ppd_honseki_jpn")
	private String ppdHonsekiJpn;

	@Column(name = "ppd_house_type")
	private String ppdHouseType;

	@Column(name = "ppd_house_type_other")
	private String ppdHouseTypeOther;

	@Column(name = "ppd_hr_responsible_id")
	private String ppdHrResponsibleId;

	@Column(name = "ppd_in_city_limit")
	private String ppdInCityLimit;

	@Column(name = "ppd_in_city_lmt_other")
	private String ppdInCityLmtOther;

	@Column(name = "ppd_lang_cd")
	private String ppdLangCd;

	@Column(name = "ppd_last_name")
	private String ppdLastName;

	@Column(name = "ppd_last_name_pref_nld")
	private String ppdLastNamePrefNld;

	@Column(name = "ppd_last_name_srch")
	private String ppdLastNameSrch;

	@Column(name = "ppd_lastupddttm")
	private Timestamp ppdLastupddttm;

	@Column(name = "ppd_mar_status")
	private String ppdMarStatus;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_mar_status_dt")
	private Date ppdMarStatusDt;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_medicare_entld_dt")
	private Date ppdMedicareEntldDt;

	@Column(name = "ppd_middle_name")
	private String ppdMiddleName;

	@Column(name = "ppd_milit_situatn_esp")
	private String ppdMilitSituatnEsp;

	@Column(name = "ppd_milit_situatn_fra")
	private String ppdMilitSituatnFra;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_military_end_ita")
	private Date ppdMilitaryEndIta;

	@Column(name = "ppd_military_rank_ita")
	private String ppdMilitaryRankIta;

	@Column(name = "ppd_military_stat_ger")
	private String ppdMilitaryStatGer;

	@Column(name = "ppd_military_stat_ita")
	private String ppdMilitaryStatIta;

	@Column(name = "ppd_military_status")
	private String ppdMilitaryStatus;

	@Column(name = "ppd_military_type_ita")
	private String ppdMilitaryTypeIta;

	@Column(name = "ppd_name")
	private String ppdName;

	@Column(name = "ppd_name_ac")
	private String ppdNameAc;

	@Column(name = "ppd_name_display")
	private String ppdNameDisplay;

	@Column(name = "ppd_name_formal")
	private String ppdNameFormal;

	@Column(name = "ppd_name_initials")
	private String ppdNameInitials;

	@Column(name = "ppd_name_prefix")
	private String ppdNamePrefix;

	@Column(name = "ppd_name_royal_prefix")
	private String ppdNameRoyalPrefix;

	@Column(name = "ppd_name_royal_suffix")
	private String ppdNameRoyalSuffix;

	@Column(name = "ppd_name_suffix")
	private String ppdNameSuffix;

	@Column(name = "ppd_name_title")
	private String ppdNameTitle;

	@Column(name = "ppd_num1")
	private String ppdNum1;

	@Column(name = "ppd_num1_other")
	private String ppdNum1Other;

	@Column(name = "ppd_num2")
	private String ppdNum2;

	@Column(name = "ppd_num2_other")
	private String ppdNum2Other;

	@Column(name = "ppd_partner_last_name")
	private String ppdPartnerLastName;

	@Column(name = "ppd_partner_roy_prefix")
	private String ppdPartnerRoyPrefix;

	@Column(name = "ppd_phone")
	private String ppdPhone;

	@Column(name = "ppd_place_of_death")
	private String ppdPlaceOfDeath;

	@Column(name = "ppd_postal")
	private String ppdPostal;

	@Column(name = "ppd_postal_other")
	private String ppdPostalOther;

	@Column(name = "ppd_pref_first_name")
	private String ppdPrefFirstName;

	@Column(name = "ppd_sal_admin_plan")
	private String ppdSalAdminPlan;

	@Column(name = "ppd_second_last_name")
	private String ppdSecondLastName;

	@Column(name = "ppd_second_last_srch")
	private String ppdSecondLastSrch;

	@Column(name = "ppd_sex")
	private String ppdSex;

	@Column(name = "ppd_smoker")
	private String ppdSmoker;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_smoker_dt")
	private Date ppdSmokerDt;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_soc_sec_aff_dt")
	private Date ppdSocSecAffDt;

	@Column(name = "ppd_state")
	private String ppdState;

	@Column(name = "ppd_state_other")
	private String ppdStateOther;

	@Column(name = "ppd_us_work_eligibilty")
	private String ppdUsWorkEligibilty;

	@Column(name = "ppd_va_benefit")
	private String ppdVaBenefit;

	@Id
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ppd_emplid")
	private PsPersonRef psPersonRef;

	@Column(name = "ppd_ethinicity")
	private String ppdEthinicity;
	@Column(name = "ppd_marital_status")
	private String ppdMaritalStatus;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_effdt")
	private Date ppdEffdt;

	public String getPpdEthinicity() {
		return ppdEthinicity;
	}

	public void setPpdEthinicity(String ppdEthinicity) {
		this.ppdEthinicity = ppdEthinicity;
	}

	public String getPpdMaritalStatus() {
		return ppdMaritalStatus;
	}

	public void setPpdMaritalStatus(String ppdMaritalStatus) {
		this.ppdMaritalStatus = ppdMaritalStatus;
	}

	public Date getPpdEffdt() {
		return ppdEffdt;
	}

	public void setPpdEffdt(Date ppdEffdt) {
		this.ppdEffdt = ppdEffdt;
	}

	public PsPersonRef getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	public PsPersonalDataRef() {
	}

	public String getPpdAddrField1() {
		return this.ppdAddrField1;
	}

	public void setPpdAddrField1(String ppdAddrField1) {
		this.ppdAddrField1 = ppdAddrField1;
	}

	public String getPpdAddrField1Other() {
		return this.ppdAddrField1Other;
	}

	public void setPpdAddrField1Other(String ppdAddrField1Other) {
		this.ppdAddrField1Other = ppdAddrField1Other;
	}

	public String getPpdAddrField2() {
		return this.ppdAddrField2;
	}

	public void setPpdAddrField2(String ppdAddrField2) {
		this.ppdAddrField2 = ppdAddrField2;
	}

	public String getPpdAddrField2Other() {
		return this.ppdAddrField2Other;
	}

	public void setPpdAddrField2Other(String ppdAddrField2Other) {
		this.ppdAddrField2Other = ppdAddrField2Other;
	}

	public String getPpdAddrField3() {
		return this.ppdAddrField3;
	}

	public void setPpdAddrField3(String ppdAddrField3) {
		this.ppdAddrField3 = ppdAddrField3;
	}

	public String getPpdAddrField3Other() {
		return this.ppdAddrField3Other;
	}

	public void setPpdAddrField3Other(String ppdAddrField3Other) {
		this.ppdAddrField3Other = ppdAddrField3Other;
	}

	public String getPpdAddress1() {
		return this.ppdAddress1;
	}

	public void setPpdAddress1(String ppdAddress1) {
		this.ppdAddress1 = ppdAddress1;
	}

	public String getPpdAddress1Ac() {
		return this.ppdAddress1Ac;
	}

	public void setPpdAddress1Ac(String ppdAddress1Ac) {
		this.ppdAddress1Ac = ppdAddress1Ac;
	}

	public String getPpdAddress1Other() {
		return this.ppdAddress1Other;
	}

	public void setPpdAddress1Other(String ppdAddress1Other) {
		this.ppdAddress1Other = ppdAddress1Other;
	}

	public String getPpdAddress2() {
		return this.ppdAddress2;
	}

	public void setPpdAddress2(String ppdAddress2) {
		this.ppdAddress2 = ppdAddress2;
	}

	public String getPpdAddress2Ac() {
		return this.ppdAddress2Ac;
	}

	public void setPpdAddress2Ac(String ppdAddress2Ac) {
		this.ppdAddress2Ac = ppdAddress2Ac;
	}

	public String getPpdAddress2Other() {
		return this.ppdAddress2Other;
	}

	public void setPpdAddress2Other(String ppdAddress2Other) {
		this.ppdAddress2Other = ppdAddress2Other;
	}

	public String getPpdAddress3() {
		return this.ppdAddress3;
	}

	public void setPpdAddress3(String ppdAddress3) {
		this.ppdAddress3 = ppdAddress3;
	}

	public String getPpdAddress3Ac() {
		return this.ppdAddress3Ac;
	}

	public void setPpdAddress3Ac(String ppdAddress3Ac) {
		this.ppdAddress3Ac = ppdAddress3Ac;
	}

	public String getPpdAddress3Other() {
		return this.ppdAddress3Other;
	}

	public void setPpdAddress3Other(String ppdAddress3Other) {
		this.ppdAddress3Other = ppdAddress3Other;
	}

	public String getPpdAddress4() {
		return this.ppdAddress4;
	}

	public void setPpdAddress4(String ppdAddress4) {
		this.ppdAddress4 = ppdAddress4;
	}

	public String getPpdAddress4Other() {
		return this.ppdAddress4Other;
	}

	public void setPpdAddress4Other(String ppdAddress4Other) {
		this.ppdAddress4Other = ppdAddress4Other;
	}

	public String getPpdAlterEmplid() {
		return this.ppdAlterEmplid;
	}

	public void setPpdAlterEmplid(String ppdAlterEmplid) {
		this.ppdAlterEmplid = ppdAlterEmplid;
	}

	public String getPpdBargUnit() {
		return this.ppdBargUnit;
	}

	public void setPpdBargUnit(String ppdBargUnit) {
		this.ppdBargUnit = ppdBargUnit;
	}

	public String getPpdBilingualismCode() {
		return this.ppdBilingualismCode;
	}

	public void setPpdBilingualismCode(String ppdBilingualismCode) {
		this.ppdBilingualismCode = ppdBilingualismCode;
	}

	public String getPpdBirthcountry() {
		return this.ppdBirthcountry;
	}

	public void setPpdBirthcountry(String ppdBirthcountry) {
		this.ppdBirthcountry = ppdBirthcountry;
	}

	public Date getPpdBirthdate() {
		return this.ppdBirthdate;
	}

	public void setPpdBirthdate(Date ppdBirthdate) {
		this.ppdBirthdate = ppdBirthdate;
	}

	public String getPpdBirthplace() {
		return this.ppdBirthplace;
	}

	public void setPpdBirthplace(String ppdBirthplace) {
		this.ppdBirthplace = ppdBirthplace;
	}

	public String getPpdBirthstate() {
		return this.ppdBirthstate;
	}

	public void setPpdBirthstate(String ppdBirthstate) {
		this.ppdBirthstate = ppdBirthstate;
	}

	public String getPpdCampusId() {
		return this.ppdCampusId;
	}

	public void setPpdCampusId(String ppdCampusId) {
		this.ppdCampusId = ppdCampusId;
	}

	public String getPpdCitizenProof1() {
		return this.ppdCitizenProof1;
	}

	public void setPpdCitizenProof1(String ppdCitizenProof1) {
		this.ppdCitizenProof1 = ppdCitizenProof1;
	}

	public String getPpdCitizenProof2() {
		return this.ppdCitizenProof2;
	}

	public void setPpdCitizenProof2(String ppdCitizenProof2) {
		this.ppdCitizenProof2 = ppdCitizenProof2;
	}

	public String getPpdCity() {
		return this.ppdCity;
	}

	public void setPpdCity(String ppdCity) {
		this.ppdCity = ppdCity;
	}

	public String getPpdCityAc() {
		return this.ppdCityAc;
	}

	public void setPpdCityAc(String ppdCityAc) {
		this.ppdCityAc = ppdCityAc;
	}

	public String getPpdCityOther() {
		return this.ppdCityOther;
	}

	public void setPpdCityOther(String ppdCityOther) {
		this.ppdCityOther = ppdCityOther;
	}

	public String getPpdCountry() {
		return this.ppdCountry;
	}

	public void setPpdCountry(String ppdCountry) {
		this.ppdCountry = ppdCountry;
	}

	public String getPpdCountryCode() {
		return this.ppdCountryCode;
	}

	public void setPpdCountryCode(String ppdCountryCode) {
		this.ppdCountryCode = ppdCountryCode;
	}

	public String getPpdCountryNmFormat() {
		return this.ppdCountryNmFormat;
	}

	public void setPpdCountryNmFormat(String ppdCountryNmFormat) {
		this.ppdCountryNmFormat = ppdCountryNmFormat;
	}

	public String getPpdCountryOther() {
		return this.ppdCountryOther;
	}

	public void setPpdCountryOther(String ppdCountryOther) {
		this.ppdCountryOther = ppdCountryOther;
	}

	public String getPpdCounty() {
		return this.ppdCounty;
	}

	public void setPpdCounty(String ppdCounty) {
		this.ppdCounty = ppdCounty;
	}

	public String getPpdCountyOther() {
		return this.ppdCountyOther;
	}

	public void setPpdCountyOther(String ppdCountyOther) {
		this.ppdCountyOther = ppdCountyOther;
	}

	public String getPpdCpamid() {
		return this.ppdCpamid;
	}

	public void setPpdCpamid(String ppdCpamid) {
		this.ppdCpamid = ppdCpamid;
	}

	public String getPpdDeathCertifNbr() {
		return this.ppdDeathCertifNbr;
	}

	public void setPpdDeathCertifNbr(String ppdDeathCertifNbr) {
		this.ppdDeathCertifNbr = ppdDeathCertifNbr;
	}

	public String getPpdDisabled() {
		return this.ppdDisabled;
	}

	public void setPpdDisabled(String ppdDisabled) {
		this.ppdDisabled = ppdDisabled;
	}

	public String getPpdDisabledVet() {
		return this.ppdDisabledVet;
	}

	public void setPpdDisabledVet(String ppdDisabledVet) {
		this.ppdDisabledVet = ppdDisabledVet;
	}

	public Date getPpdDtOfDeath() {
		return this.ppdDtOfDeath;
	}

	public void setPpdDtOfDeath(Date ppdDtOfDeath) {
		this.ppdDtOfDeath = ppdDtOfDeath;
	}

	public Date getPpdEntryDtFra() {
		return this.ppdEntryDtFra;
	}

	public void setPpdEntryDtFra(Date ppdEntryDtFra) {
		this.ppdEntryDtFra = ppdEntryDtFra;
	}

	public Date getPpdExpctdMilitaryDt() {
		return this.ppdExpctdMilitaryDt;
	}

	public void setPpdExpctdMilitaryDt(Date ppdExpctdMilitaryDt) {
		this.ppdExpctdMilitaryDt = ppdExpctdMilitaryDt;
	}

	public String getPpdExtension() {
		return this.ppdExtension;
	}

	public void setPpdExtension(String ppdExtension) {
		this.ppdExtension = ppdExtension;
	}

	public String getPpdFerpa() {
		return this.ppdFerpa;
	}

	public void setPpdFerpa(String ppdFerpa) {
		this.ppdFerpa = ppdFerpa;
	}

	public String getPpdFirstName() {
		return this.ppdFirstName;
	}

	public void setPpdFirstName(String ppdFirstName) {
		this.ppdFirstName = ppdFirstName;
	}

	public String getPpdFirstNameSrch() {
		return this.ppdFirstNameSrch;
	}

	public void setPpdFirstNameSrch(String ppdFirstNameSrch) {
		this.ppdFirstNameSrch = ppdFirstNameSrch;
	}

	public String getPpdFtStudent() {
		return this.ppdFtStudent;
	}

	public void setPpdFtStudent(String ppdFtStudent) {
		this.ppdFtStudent = ppdFtStudent;
	}

	public String getPpdGeoCode() {
		return this.ppdGeoCode;
	}

	public void setPpdGeoCode(String ppdGeoCode) {
		this.ppdGeoCode = ppdGeoCode;
	}

	public String getPpdGeoCodeOther() {
		return this.ppdGeoCodeOther;
	}

	public void setPpdGeoCodeOther(String ppdGeoCodeOther) {
		this.ppdGeoCodeOther = ppdGeoCodeOther;
	}

	public String getPpdGrade() {
		return this.ppdGrade;
	}

	public void setPpdGrade(String ppdGrade) {
		this.ppdGrade = ppdGrade;
	}

	public String getPpdGvtChangeFlag() {
		return this.ppdGvtChangeFlag;
	}

	public void setPpdGvtChangeFlag(String ppdGvtChangeFlag) {
		this.ppdGvtChangeFlag = ppdGvtChangeFlag;
	}

	public String getPpdGvtCredMilSvce() {
		return this.ppdGvtCredMilSvce;
	}

	public void setPpdGvtCredMilSvce(String ppdGvtCredMilSvce) {
		this.ppdGvtCredMilSvce = ppdGvtCredMilSvce;
	}

	public String getPpdGvtCurrAgcyEmpl() {
		return this.ppdGvtCurrAgcyEmpl;
	}

	public void setPpdGvtCurrAgcyEmpl(String ppdGvtCurrAgcyEmpl) {
		this.ppdGvtCurrAgcyEmpl = ppdGvtCurrAgcyEmpl;
	}

	public String getPpdGvtCurrFedEmpl() {
		return this.ppdGvtCurrFedEmpl;
	}

	public void setPpdGvtCurrFedEmpl(String ppdGvtCurrFedEmpl) {
		this.ppdGvtCurrFedEmpl = ppdGvtCurrFedEmpl;
	}

	public String getPpdGvtDisabilityCd() {
		return this.ppdGvtDisabilityCd;
	}

	public void setPpdGvtDisabilityCd(String ppdGvtDisabilityCd) {
		this.ppdGvtDisabilityCd = ppdGvtDisabilityCd;
	}

	public String getPpdGvtDraftStatus() {
		return this.ppdGvtDraftStatus;
	}

	public void setPpdGvtDraftStatus(String ppdGvtDraftStatus) {
		this.ppdGvtDraftStatus = ppdGvtDraftStatus;
	}

	public String getPpdGvtHighGrade() {
		return this.ppdGvtHighGrade;
	}

	public void setPpdGvtHighGrade(String ppdGvtHighGrade) {
		this.ppdGvtHighGrade = ppdGvtHighGrade;
	}

	public String getPpdGvtHighPayPlan() {
		return this.ppdGvtHighPayPlan;
	}

	public void setPpdGvtHighPayPlan(String ppdGvtHighPayPlan) {
		this.ppdGvtHighPayPlan = ppdGvtHighPayPlan;
	}

	public String getPpdGvtMilGrade() {
		return this.ppdGvtMilGrade;
	}

	public void setPpdGvtMilGrade(String ppdGvtMilGrade) {
		this.ppdGvtMilGrade = ppdGvtMilGrade;
	}

	public String getPpdGvtMilResrveCat() {
		return this.ppdGvtMilResrveCat;
	}

	public void setPpdGvtMilResrveCat(String ppdGvtMilResrveCat) {
		this.ppdGvtMilResrveCat = ppdGvtMilResrveCat;
	}

	public String getPpdGvtMilSepRet() {
		return this.ppdGvtMilSepRet;
	}

	public void setPpdGvtMilSepRet(String ppdGvtMilSepRet) {
		this.ppdGvtMilSepRet = ppdGvtMilSepRet;
	}

	public Date getPpdGvtMilSvceEnd() {
		return this.ppdGvtMilSvceEnd;
	}

	public void setPpdGvtMilSvceEnd(Date ppdGvtMilSvceEnd) {
		this.ppdGvtMilSvceEnd = ppdGvtMilSvceEnd;
	}

	public Date getPpdGvtMilSvceStart() {
		return this.ppdGvtMilSvceStart;
	}

	public void setPpdGvtMilSvceStart(Date ppdGvtMilSvceStart) {
		this.ppdGvtMilSvceStart = ppdGvtMilSvceStart;
	}

	public String getPpdGvtMilVerify() {
		return this.ppdGvtMilVerify;
	}

	public void setPpdGvtMilVerify(String ppdGvtMilVerify) {
		this.ppdGvtMilVerify = ppdGvtMilVerify;
	}

	public String getPpdGvtMilitaryComp() {
		return this.ppdGvtMilitaryComp;
	}

	public void setPpdGvtMilitaryComp(String ppdGvtMilitaryComp) {
		this.ppdGvtMilitaryComp = ppdGvtMilitaryComp;
	}

	public BigDecimal getPpdGvtParNbrLast() {
		return this.ppdGvtParNbrLast;
	}

	public void setPpdGvtParNbrLast(BigDecimal ppdGvtParNbrLast) {
		this.ppdGvtParNbrLast = ppdGvtParNbrLast;
	}

	public String getPpdGvtPayPlan() {
		return this.ppdGvtPayPlan;
	}

	public void setPpdGvtPayPlan(String ppdGvtPayPlan) {
		this.ppdGvtPayPlan = ppdGvtPayPlan;
	}

	public String getPpdGvtPrevAgcyEmpl() {
		return this.ppdGvtPrevAgcyEmpl;
	}

	public void setPpdGvtPrevAgcyEmpl(String ppdGvtPrevAgcyEmpl) {
		this.ppdGvtPrevAgcyEmpl = ppdGvtPrevAgcyEmpl;
	}

	public String getPpdGvtPrevFedEmpl() {
		return this.ppdGvtPrevFedEmpl;
	}

	public void setPpdGvtPrevFedEmpl(String ppdGvtPrevFedEmpl) {
		this.ppdGvtPrevFedEmpl = ppdGvtPrevFedEmpl;
	}

	public Date getPpdGvtSepIncentDt() {
		return this.ppdGvtSepIncentDt;
	}

	public void setPpdGvtSepIncentDt(Date ppdGvtSepIncentDt) {
		this.ppdGvtSepIncentDt = ppdGvtSepIncentDt;
	}

	public String getPpdGvtSepIncentive() {
		return this.ppdGvtSepIncentive;
	}

	public void setPpdGvtSepIncentive(String ppdGvtSepIncentive) {
		this.ppdGvtSepIncentive = ppdGvtSepIncentive;
	}

	public String getPpdGvtTenure() {
		return this.ppdGvtTenure;
	}

	public void setPpdGvtTenure(String ppdGvtTenure) {
		this.ppdGvtTenure = ppdGvtTenure;
	}

	public String getPpdGvtUnifSvcCtr() {
		return this.ppdGvtUnifSvcCtr;
	}

	public void setPpdGvtUnifSvcCtr(String ppdGvtUnifSvcCtr) {
		this.ppdGvtUnifSvcCtr = ppdGvtUnifSvcCtr;
	}

	public String getPpdGvtVetPrefAppt() {
		return this.ppdGvtVetPrefAppt;
	}

	public void setPpdGvtVetPrefAppt(String ppdGvtVetPrefAppt) {
		this.ppdGvtVetPrefAppt = ppdGvtVetPrefAppt;
	}

	public String getPpdGvtVetPrefRif() {
		return this.ppdGvtVetPrefRif;
	}

	public void setPpdGvtVetPrefRif(String ppdGvtVetPrefRif) {
		this.ppdGvtVetPrefRif = ppdGvtVetPrefRif;
	}

	public Date getPpdGvtYrAttained() {
		return this.ppdGvtYrAttained;
	}

	public void setPpdGvtYrAttained(Date ppdGvtYrAttained) {
		this.ppdGvtYrAttained = ppdGvtYrAttained;
	}

	public String getPpdHealthCareNbr() {
		return this.ppdHealthCareNbr;
	}

	public void setPpdHealthCareNbr(String ppdHealthCareNbr) {
		this.ppdHealthCareNbr = ppdHealthCareNbr;
	}

	public String getPpdHealthCareState() {
		return this.ppdHealthCareState;
	}

	public void setPpdHealthCareState(String ppdHealthCareState) {
		this.ppdHealthCareState = ppdHealthCareState;
	}

	public String getPpdHighestEducLvl() {
		return this.ppdHighestEducLvl;
	}

	public void setPpdHighestEducLvl(String ppdHighestEducLvl) {
		this.ppdHighestEducLvl = ppdHighestEducLvl;
	}

	public String getPpdHonsekiJpn() {
		return this.ppdHonsekiJpn;
	}

	public void setPpdHonsekiJpn(String ppdHonsekiJpn) {
		this.ppdHonsekiJpn = ppdHonsekiJpn;
	}

	public String getPpdHouseType() {
		return this.ppdHouseType;
	}

	public void setPpdHouseType(String ppdHouseType) {
		this.ppdHouseType = ppdHouseType;
	}

	public String getPpdHouseTypeOther() {
		return this.ppdHouseTypeOther;
	}

	public void setPpdHouseTypeOther(String ppdHouseTypeOther) {
		this.ppdHouseTypeOther = ppdHouseTypeOther;
	}

	public String getPpdHrResponsibleId() {
		return this.ppdHrResponsibleId;
	}

	public void setPpdHrResponsibleId(String ppdHrResponsibleId) {
		this.ppdHrResponsibleId = ppdHrResponsibleId;
	}

	public String getPpdInCityLimit() {
		return this.ppdInCityLimit;
	}

	public void setPpdInCityLimit(String ppdInCityLimit) {
		this.ppdInCityLimit = ppdInCityLimit;
	}

	public String getPpdInCityLmtOther() {
		return this.ppdInCityLmtOther;
	}

	public void setPpdInCityLmtOther(String ppdInCityLmtOther) {
		this.ppdInCityLmtOther = ppdInCityLmtOther;
	}

	public String getPpdLangCd() {
		return this.ppdLangCd;
	}

	public void setPpdLangCd(String ppdLangCd) {
		this.ppdLangCd = ppdLangCd;
	}

	public String getPpdLastName() {
		return this.ppdLastName;
	}

	public void setPpdLastName(String ppdLastName) {
		this.ppdLastName = ppdLastName;
	}

	public String getPpdLastNamePrefNld() {
		return this.ppdLastNamePrefNld;
	}

	public void setPpdLastNamePrefNld(String ppdLastNamePrefNld) {
		this.ppdLastNamePrefNld = ppdLastNamePrefNld;
	}

	public String getPpdLastNameSrch() {
		return this.ppdLastNameSrch;
	}

	public void setPpdLastNameSrch(String ppdLastNameSrch) {
		this.ppdLastNameSrch = ppdLastNameSrch;
	}

	public Timestamp getPpdLastupddttm() {
		return this.ppdLastupddttm;
	}

	public void setPpdLastupddttm(Timestamp ppdLastupddttm) {
		this.ppdLastupddttm = ppdLastupddttm;
	}

	public String getPpdMarStatus() {
		return this.ppdMarStatus;
	}

	public void setPpdMarStatus(String ppdMarStatus) {
		this.ppdMarStatus = ppdMarStatus;
	}

	public Date getPpdMarStatusDt() {
		return this.ppdMarStatusDt;
	}

	public void setPpdMarStatusDt(Date ppdMarStatusDt) {
		this.ppdMarStatusDt = ppdMarStatusDt;
	}

	public Date getPpdMedicareEntldDt() {
		return this.ppdMedicareEntldDt;
	}

	public void setPpdMedicareEntldDt(Date ppdMedicareEntldDt) {
		this.ppdMedicareEntldDt = ppdMedicareEntldDt;
	}

	public String getPpdMiddleName() {
		return this.ppdMiddleName;
	}

	public void setPpdMiddleName(String ppdMiddleName) {
		this.ppdMiddleName = ppdMiddleName;
	}

	public String getPpdMilitSituatnEsp() {
		return this.ppdMilitSituatnEsp;
	}

	public void setPpdMilitSituatnEsp(String ppdMilitSituatnEsp) {
		this.ppdMilitSituatnEsp = ppdMilitSituatnEsp;
	}

	public String getPpdMilitSituatnFra() {
		return this.ppdMilitSituatnFra;
	}

	public void setPpdMilitSituatnFra(String ppdMilitSituatnFra) {
		this.ppdMilitSituatnFra = ppdMilitSituatnFra;
	}

	public Date getPpdMilitaryEndIta() {
		return this.ppdMilitaryEndIta;
	}

	public void setPpdMilitaryEndIta(Date ppdMilitaryEndIta) {
		this.ppdMilitaryEndIta = ppdMilitaryEndIta;
	}

	public String getPpdMilitaryRankIta() {
		return this.ppdMilitaryRankIta;
	}

	public void setPpdMilitaryRankIta(String ppdMilitaryRankIta) {
		this.ppdMilitaryRankIta = ppdMilitaryRankIta;
	}

	public String getPpdMilitaryStatGer() {
		return this.ppdMilitaryStatGer;
	}

	public void setPpdMilitaryStatGer(String ppdMilitaryStatGer) {
		this.ppdMilitaryStatGer = ppdMilitaryStatGer;
	}

	public String getPpdMilitaryStatIta() {
		return this.ppdMilitaryStatIta;
	}

	public void setPpdMilitaryStatIta(String ppdMilitaryStatIta) {
		this.ppdMilitaryStatIta = ppdMilitaryStatIta;
	}

	public String getPpdMilitaryStatus() {
		return this.ppdMilitaryStatus;
	}

	public void setPpdMilitaryStatus(String ppdMilitaryStatus) {
		this.ppdMilitaryStatus = ppdMilitaryStatus;
	}

	public String getPpdMilitaryTypeIta() {
		return this.ppdMilitaryTypeIta;
	}

	public void setPpdMilitaryTypeIta(String ppdMilitaryTypeIta) {
		this.ppdMilitaryTypeIta = ppdMilitaryTypeIta;
	}

	public String getPpdName() {
		return this.ppdName;
	}

	public void setPpdName(String ppdName) {
		this.ppdName = ppdName;
	}

	public String getPpdNameAc() {
		return this.ppdNameAc;
	}

	public void setPpdNameAc(String ppdNameAc) {
		this.ppdNameAc = ppdNameAc;
	}

	public String getPpdNameDisplay() {
		return this.ppdNameDisplay;
	}

	public void setPpdNameDisplay(String ppdNameDisplay) {
		this.ppdNameDisplay = ppdNameDisplay;
	}

	public String getPpdNameFormal() {
		return this.ppdNameFormal;
	}

	public void setPpdNameFormal(String ppdNameFormal) {
		this.ppdNameFormal = ppdNameFormal;
	}

	public String getPpdNameInitials() {
		return this.ppdNameInitials;
	}

	public void setPpdNameInitials(String ppdNameInitials) {
		this.ppdNameInitials = ppdNameInitials;
	}

	public String getPpdNamePrefix() {
		return this.ppdNamePrefix;
	}

	public void setPpdNamePrefix(String ppdNamePrefix) {
		this.ppdNamePrefix = ppdNamePrefix;
	}

	public String getPpdNameRoyalPrefix() {
		return this.ppdNameRoyalPrefix;
	}

	public void setPpdNameRoyalPrefix(String ppdNameRoyalPrefix) {
		this.ppdNameRoyalPrefix = ppdNameRoyalPrefix;
	}

	public String getPpdNameRoyalSuffix() {
		return this.ppdNameRoyalSuffix;
	}

	public void setPpdNameRoyalSuffix(String ppdNameRoyalSuffix) {
		this.ppdNameRoyalSuffix = ppdNameRoyalSuffix;
	}

	public String getPpdNameSuffix() {
		return this.ppdNameSuffix;
	}

	public void setPpdNameSuffix(String ppdNameSuffix) {
		this.ppdNameSuffix = ppdNameSuffix;
	}

	public String getPpdNameTitle() {
		return this.ppdNameTitle;
	}

	public void setPpdNameTitle(String ppdNameTitle) {
		this.ppdNameTitle = ppdNameTitle;
	}

	public String getPpdNum1() {
		return this.ppdNum1;
	}

	public void setPpdNum1(String ppdNum1) {
		this.ppdNum1 = ppdNum1;
	}

	public String getPpdNum1Other() {
		return this.ppdNum1Other;
	}

	public void setPpdNum1Other(String ppdNum1Other) {
		this.ppdNum1Other = ppdNum1Other;
	}

	public String getPpdNum2() {
		return this.ppdNum2;
	}

	public void setPpdNum2(String ppdNum2) {
		this.ppdNum2 = ppdNum2;
	}

	public String getPpdNum2Other() {
		return this.ppdNum2Other;
	}

	public void setPpdNum2Other(String ppdNum2Other) {
		this.ppdNum2Other = ppdNum2Other;
	}

	public String getPpdPartnerLastName() {
		return this.ppdPartnerLastName;
	}

	public void setPpdPartnerLastName(String ppdPartnerLastName) {
		this.ppdPartnerLastName = ppdPartnerLastName;
	}

	public String getPpdPartnerRoyPrefix() {
		return this.ppdPartnerRoyPrefix;
	}

	public void setPpdPartnerRoyPrefix(String ppdPartnerRoyPrefix) {
		this.ppdPartnerRoyPrefix = ppdPartnerRoyPrefix;
	}

	public String getPpdPhone() {
		return this.ppdPhone;
	}

	public void setPpdPhone(String ppdPhone) {
		this.ppdPhone = ppdPhone;
	}

	public String getPpdPlaceOfDeath() {
		return this.ppdPlaceOfDeath;
	}

	public void setPpdPlaceOfDeath(String ppdPlaceOfDeath) {
		this.ppdPlaceOfDeath = ppdPlaceOfDeath;
	}

	public String getPpdPostal() {
		return this.ppdPostal;
	}

	public void setPpdPostal(String ppdPostal) {
		this.ppdPostal = ppdPostal;
	}

	public String getPpdPostalOther() {
		return this.ppdPostalOther;
	}

	public void setPpdPostalOther(String ppdPostalOther) {
		this.ppdPostalOther = ppdPostalOther;
	}

	public String getPpdPrefFirstName() {
		return this.ppdPrefFirstName;
	}

	public void setPpdPrefFirstName(String ppdPrefFirstName) {
		this.ppdPrefFirstName = ppdPrefFirstName;
	}

	public String getPpdSalAdminPlan() {
		return this.ppdSalAdminPlan;
	}

	public void setPpdSalAdminPlan(String ppdSalAdminPlan) {
		this.ppdSalAdminPlan = ppdSalAdminPlan;
	}

	public String getPpdSecondLastName() {
		return this.ppdSecondLastName;
	}

	public void setPpdSecondLastName(String ppdSecondLastName) {
		this.ppdSecondLastName = ppdSecondLastName;
	}

	public String getPpdSecondLastSrch() {
		return this.ppdSecondLastSrch;
	}

	public void setPpdSecondLastSrch(String ppdSecondLastSrch) {
		this.ppdSecondLastSrch = ppdSecondLastSrch;
	}

	public String getPpdSex() {
		return this.ppdSex;
	}

	public void setPpdSex(String ppdSex) {
		this.ppdSex = ppdSex;
	}

	public String getPpdSmoker() {
		return this.ppdSmoker;
	}

	public void setPpdSmoker(String ppdSmoker) {
		this.ppdSmoker = ppdSmoker;
	}

	public Date getPpdSmokerDt() {
		return this.ppdSmokerDt;
	}

	public void setPpdSmokerDt(Date ppdSmokerDt) {
		this.ppdSmokerDt = ppdSmokerDt;
	}

	public Date getPpdSocSecAffDt() {
		return this.ppdSocSecAffDt;
	}

	public void setPpdSocSecAffDt(Date ppdSocSecAffDt) {
		this.ppdSocSecAffDt = ppdSocSecAffDt;
	}

	public String getPpdState() {
		return this.ppdState;
	}

	public void setPpdState(String ppdState) {
		this.ppdState = ppdState;
	}

	public String getPpdStateOther() {
		return this.ppdStateOther;
	}

	public void setPpdStateOther(String ppdStateOther) {
		this.ppdStateOther = ppdStateOther;
	}

	public String getPpdUsWorkEligibilty() {
		return this.ppdUsWorkEligibilty;
	}

	public void setPpdUsWorkEligibilty(String ppdUsWorkEligibilty) {
		this.ppdUsWorkEligibilty = ppdUsWorkEligibilty;
	}

	public String getPpdVaBenefit() {
		return this.ppdVaBenefit;
	}

	public void setPpdVaBenefit(String ppdVaBenefit) {
		this.ppdVaBenefit = ppdVaBenefit;
	}
}