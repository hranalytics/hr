/**
 * Program Name: PsPersonPhoneRefPK 
 *                                                                 
 * Program Description / functionality: This is composit Id class for PsPersonPhoneRefPK
 *                            
 * Modules Impacted: Person phone Details Services
 *                                                                    
 * Tables affected: Ps_Person_Phone_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Harish       23/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.entity;

public class PsPersonPhoneRefPK extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String psPersonRef;
	private String ppPhoneType;

	public PsPersonPhoneRefPK() {

	}

	public String getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(String psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	public String getPpPhoneType() {
		return this.ppPhoneType;
	}

	public void setPpPhoneType(String ppPhoneType) {
		this.ppPhoneType = ppPhoneType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ppPhoneType == null) ? 0 : ppPhoneType.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PsPersonPhoneRefPK other = (PsPersonPhoneRefPK) obj;
		if (ppPhoneType == null) {
			if (other.ppPhoneType != null)
				return false;
		} else if (!ppPhoneType.equals(other.ppPhoneType))
			return false;
		if (psPersonRef == null) {
			if (other.psPersonRef != null)
				return false;
		} else if (!psPersonRef.equals(other.psPersonRef))
			return false;
		return true;
	}

}