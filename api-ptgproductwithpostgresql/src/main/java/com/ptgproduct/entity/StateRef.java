package com.ptgproduct.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "gis_state_ref")
public class StateRef extends BaseEntity {

	private static final long serialVersionUID = -4777211606019741466L;

	@Id
	@Column(name = "st_state_id")
	private int stateId;

	@Column(name = "st_state_name")
	private String stateName;

	@Column(name = "st_desc")
	private String description;

	@Column(name = "st_isactive")
	private boolean isActive;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "st_country_name")
	private CountryRef countryRef;

	public StateRef() {
	}

	public StateRef(int stateId, String stateName, String description, CountryRef countryRef) {

		this.stateId = stateId;
		this.stateName = stateName;
		this.description = description;

		this.countryRef = countryRef;
	}

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	

	public CountryRef getCountryRef() {
		return countryRef;
	}

	public void setCountryRef(CountryRef countryRef) {
		this.countryRef = countryRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryRef == null) ? 0 : countryRef.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + stateId;
		result = prime * result + ((stateName == null) ? 0 : stateName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateRef other = (StateRef) obj;
		if (countryRef == null) {
			if (other.countryRef != null)
				return false;
		} else if (!countryRef.equals(other.countryRef))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (stateId != other.stateId)
			return false;
		if (stateName == null) {
			if (other.stateName != null)
				return false;
		} else if (!stateName.equals(other.stateName))
			return false;
		return true;
	}

}
