/**
* Program Name: PsJobRef 
*                                                                 
* Program Description / functionality: This is entity class for person Jobes
*                            
* Modules Impacted: Person Jobes
*                                                                    
* Tables affected:  ps_job_ref                                                                    
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Bindu               23/12/2016 
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "ps_job_ref")
@NamedQuery(name = "PsJobRef.findAll", query = "SELECT p FROM PsJobRef p")
@IdClass(PsJobRefPK.class)
public class PsJobRef extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "pj_emplid", insertable = false, updatable = false)
	private String pjEmplid;

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name = "pj_effdt")
	private java.util.Date pjEffdt;

	@Id
	@Column(name = "pj_effseq")
	private long pjEffseq;

	@Id
	@Column(name = "pj_empl_rcd")
	private long pjEmplRcd;

	@Column(name = "pj_absence_system_cd")
	private String pjAbsenceSystemCd;

	@Column(name = "pj_accdnt_cd_fra")
	private String pjAccdntCdFra;

	@Column(name = "pj_acct_cd")
	private String pjAcctCd;

	@Column(name = "pj_action")
	private String pjAction;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_action_dt")
	private Date pjActionDt;

	@Column(name = "pj_action_reason")
	private String pjActionReason;

	@Column(name = "pj_adds_to_fte_actual")
	private String pjAddsToFteActual;

	@Column(name = "pj_annl_benef_base_rt")
	private BigDecimal pjAnnlBenefBaseRt;

	@Column(name = "pj_annual_rt")
	private BigDecimal pjAnnualRt;

	@Column(name = "pj_appt_type")
	private String pjApptType;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_asgn_end_dt")
	private Date pjAsgnEndDt;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_asgn_start_dt")
	private Date pjAsgnStartDt;

	@Column(name = "pj_auto_end_flg")
	private String pjAutoEndFlg;

	@Column(name = "pj_barg_unit")
	private String pjBargUnit;

	@Column(name = "pj_bas_action")
	private String pjBasAction;

	@Column(name = "pj_bas_group_id")
	private String pjBasGroupId;

	@Column(name = "pj_ben_status")
	private String pjBenStatus;

	@Column(name = "pj_benefit_system")
	private String pjBenefitSystem;

	@Column(name = "pj_border_walker")
	private String pjBorderWalker;

	@Column(name = "pj_business_unit")
	private String pjBusinessUnit;

	@Column(name = "pj_change_amt")
	private BigDecimal pjChangeAmt;

	@Column(name = "pj_change_pct")
	private BigDecimal pjChangePct;

	@Column(name = "pj_class_indc")
	private String pjClassIndc;

	@Column(name = "pj_cobra_action")
	private String pjCobraAction;

	@Column(name = "pj_comp_frequency")
	private String pjCompFrequency;

	@Column(name = "pj_company")
	private String pjCompany;

	@Column(name = "pj_comprate")
	private BigDecimal pjComprate;

	@Column(name = "pj_contract_num")
	private String pjContractNum;

	@Column(name = "pj_ctg_rate")
	private BigDecimal pjCtgRate;

	@Column(name = "pj_cur_rt_type")
	private String pjCurRtType;

	@Column(name = "pj_currency_cd")
	private String pjCurrencyCd;

	@Column(name = "pj_currency_cd1")
	private String pjCurrencyCd1;

	@Column(name = "pj_daily_rt")
	private BigDecimal pjDailyRt;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_dept_entry_dt")
	private Date pjDeptEntryDt;

	@Column(name = "pj_deptid")
	private String pjDeptid;

	@Column(name = "pj_directly_tipped")
	private String pjDirectlyTipped;

	@Column(name = "pj_earns_dist_type")
	private String pjEarnsDistType;

	@Column(name = "pj_eeo_class")
	private String pjEeoClass;

	@Column(name = "pj_elig_config1")
	private String pjEligConfig1;

	@Column(name = "pj_elig_config2")
	private String pjEligConfig2;

	@Column(name = "pj_elig_config3")
	private String pjEligConfig3;

	@Column(name = "pj_elig_config4")
	private String pjEligConfig4;

	@Column(name = "pj_elig_config5")
	private String pjEligConfig5;

	@Column(name = "pj_elig_config6")
	private String pjEligConfig6;

	@Column(name = "pj_elig_config7")
	private String pjEligConfig7;

	@Column(name = "pj_elig_config8")
	private String pjEligConfig8;

	@Column(name = "pj_elig_config9")
	private String pjEligConfig9;

	@Column(name = "pj_empl_class")
	private String pjEmplClass;

	@Column(name = "pj_empl_ctg")
	private String pjEmplCtg;

	@Column(name = "pj_empl_ctg_l1")
	private String pjEmplCtgL1;

	@Column(name = "pj_empl_ctg_l2")
	private String pjEmplCtgL2;

	@Column(name = "pj_empl_status")
	private String pjEmplStatus;

	@Column(name = "pj_empl_type")
	private String pjEmplType;

	@Column(name = "pj_encumb_override")
	private String pjEncumbOverride;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_entry_date")
	private Date pjEntryDate;

	@Column(name = "pj_estabid")
	private String pjEstabid;

	@Column(name = "pj_exempt_hours_month")
	private BigDecimal pjExemptHoursMonth;

	@Column(name = "pj_exempt_job_lbr")
	private String pjExemptJobLbr;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_expected_end_date")
	private Date pjExpectedEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_expected_return_dt")
	private Date pjExpectedReturnDt;

	@Column(name = "pj_fica_status_ee")
	private String pjFicaStatusEe;

	@Column(name = "pj_flsa_status")
	private String pjFlsaStatus;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_force_publish")
	private Date pjForcePublish;

	@Column(name = "pj_fte")
	private BigDecimal pjFte;

	@Column(name = "pj_full_part_time")
	private String pjFullPartTime;

	@Column(name = "pj_function_cd")
	private String pjFunctionCd;

	@Column(name = "pj_gl_pay_type")
	private String pjGlPayType;

	@Column(name = "pj_gp_asof_dt_exg_rt")
	private String pjGpAsofDtExgRt;

	@Column(name = "pj_gp_dflt_currttyp")
	private String pjGpDfltCurrttyp;

	@Column(name = "pj_gp_dflt_elig_grp")
	private String pjGpDfltEligGrp;

	@Column(name = "pj_gp_dflt_exrtdt")
	private String pjGpDfltExrtdt;

	@Column(name = "pj_gp_elig_grp")
	private String pjGpEligGrp;

	@Column(name = "pj_gp_paygroup")
	private String pjGpPaygroup;

	@Column(name = "pj_grade")
	private String pjGrade;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_grade_entry_dt")
	private Date pjGradeEntryDt;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_hire_dt")
	private Date pjHireDt;

	@Column(name = "pj_holiday_schedule")
	private String pjHolidaySchedule;

	@Column(name = "pj_hourly_rt")
	private BigDecimal pjHourlyRt;

	@Column(name = "pj_hourly_rt_fra")
	private String pjHourlyRtFra;

	@Column(name = "pj_hr_status")
	private String pjHrStatus;

	@Column(name = "pj_interctr_wrks_cncl")
	private String pjInterctrWrksCncl;

	@Column(name = "pj_job_data_src_cd")
	private String pjJobDataSrcCd;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_job_entry_dt")
	private Date pjJobEntryDt;

	@Column(name = "pj_job_indicator")
	private String pjJobIndicator;

	@Column(name = "pj_jobcode")
	private String pjJobcode;

	@Column(name = "pj_labor_agreement")
	private String pjLaborAgreement;

	@Column(name = "pj_labor_facility_id")
	private String pjLaborFacilityId;

	@Column(name = "pj_labor_type_ger")
	private String pjLaborTypeGer;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_last_date_worked")
	private Date pjLastDateWorked;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_last_hire_dt")
	private Date pjLastHireDt;

	@Column(name = "pj_lastupddttm")
	private Timestamp pjLastupddttm;

	@Column(name = "pj_lastupdoprid")
	private String pjLastupdoprid;

	@Column(name = "pj_layoff_exempt_flag")
	private String pjLayoffExemptFlag;

	@Column(name = "pj_layoff_exempt_rsn")
	private String pjLayoffExemptRsn;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_lbr_fac_entry_dt")
	private Date pjLbrFacEntryDt;

	@Column(name = "pj_ldw_ovr")
	private String pjLdwOvr;

	@Column(name = "pj_location")
	private String pjLocation;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_lst_asgn_start_dt")
	private Date pjLstAsgnStartDt;

	@Column(name = "pj_lump_sum_pay")
	private String pjLumpSumPay;

	@Column(name = "pj_main_appt_num_jpn")
	private BigDecimal pjMainApptNumJpn;

	@Column(name = "pj_matricula_nbr")
	private BigDecimal pjMatriculaNbr;

	@Column(name = "pj_monthly_rt")
	private BigDecimal pjMonthlyRt;

	@Column(name = "pj_officer_cd")
	private String pjOfficerCd;

	@Column(name = "pj_paid_fte")
	private BigDecimal pjPaidFte;

	@Column(name = "pj_paid_hours")
	private BigDecimal pjPaidHours;

	@Column(name = "pj_paid_hrs_frequency")
	private String pjPaidHrsFrequency;

	@Column(name = "pj_pay_system_flg")
	private String pjPaySystemFlg;

	@Column(name = "pj_pay_union_fee")
	private String pjPayUnionFee;

	@Column(name = "pj_paygroup")
	private String pjPaygroup;

	@Column(name = "pj_per_org")
	private String pjPerOrg;

	@Column(name = "pj_perform_group_ger")
	private String pjPerformGroupGer;

	@Column(name = "pj_poi_type")
	private String pjPoiType;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_position_entry_dt")
	private Date pjPositionEntryDt;

	@Column(name = "pj_position_nbr")
	private String pjPositionNbr;

	@Column(name = "pj_position_override")
	private String pjPositionOverride;

	@Column(name = "pj_posn_change_record")
	private String pjPosnChangeRecord;

	@Column(name = "pj_prorate_cnt_amt")
	private String pjProrateCntAmt;

	@Column(name = "pj_reg_region")
	private String pjRegRegion;

	@Column(name = "pj_reg_temp")
	private String pjRegTemp;

	@Column(name = "pj_reports_to")
	private String pjReportsTo;

	@Column(name = "pj_sal_admin_plan")
	private String pjSalAdminPlan;

	@Column(name = "pj_setid_dept")
	private String pjSetidDept;

	@Column(name = "pj_setid_empl_class")
	private String pjSetidEmplClass;

	@Column(name = "pj_setid_jobcode")
	private String pjSetidJobcode;

	@Column(name = "pj_setid_lbr_agrmnt")
	private String pjSetidLbrAgrmnt;

	@Column(name = "pj_setid_location")
	private String pjSetidLocation;

	@Column(name = "pj_setid_salary")
	private String pjSetidSalary;

	@Column(name = "pj_setid_supv_lvl")
	private String pjSetidSupvLvl;

	@Column(name = "pj_shift")
	private String pjShift;

	@Column(name = "pj_shift_factor")
	private BigDecimal pjShiftFactor;

	@Column(name = "pj_shift_rt")
	private BigDecimal pjShiftRt;

	@Column(name = "pj_soc_sec_risk_code")
	private String pjSocSecRiskCode;

	@Column(name = "pj_spk_comm_id_ger")
	private String pjSpkCommIdGer;

	@Column(name = "pj_std_hours")
	private BigDecimal pjStdHours;

	@Column(name = "pj_std_hrs_frequency")
	private String pjStdHrsFrequency;

	@Column(name = "pj_step")
	private BigDecimal pjStep;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_step_entry_dt")
	private Date pjStepEntryDt;

	@Column(name = "pj_supervisor_id")
	private String pjSupervisorId;

	@Column(name = "pj_supv_lvl_id")
	private String pjSupvLvlId;

	@Column(name = "pj_tariff_area_ger")
	private String pjTariffAreaGer;

	@Column(name = "pj_tariff_ger")
	private String pjTariffGer;

	@Column(name = "pj_tax_location_cd")
	private String pjTaxLocationCd;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_termination_dt")
	private Date pjTerminationDt;

	@Column(name = "pj_union_cd")
	private String pjUnionCd;

	@Column(name = "pj_union_fee_amount")
	private BigDecimal pjUnionFeeAmount;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_union_fee_end_dt")
	private Date pjUnionFeeEndDt;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_union_fee_start_dt")
	private Date pjUnionFeeStartDt;

	@Column(name = "pj_union_full_part")
	private String pjUnionFullPart;

	@Column(name = "pj_union_pos")
	private String pjUnionPos;

	@Temporal(TemporalType.DATE)
	@Column(name = "pj_union_seniority_dt")
	private Date pjUnionSeniorityDt;

	@Column(name = "pj_value_1_fra")
	private String pjValue1Fra;

	@Column(name = "pj_value_2_fra")
	private String pjValue2Fra;

	@Column(name = "pj_value_3_fra")
	private String pjValue3Fra;

	@Column(name = "pj_value_4_fra")
	private String pjValue4Fra;

	@Column(name = "pj_value_5_fra")
	private String pjValue5Fra;

	@Column(name = "pj_work_day_hours")
	private BigDecimal pjWorkDayHours;

	@Column(name = "pj_wpp_stop_flag")
	private String pjWppStopFlag;

	@Column(name = "pj_wrks_cncl_function")
	private String pjWrksCnclFunction;

	@Column(name = "pj_wrks_cncl_role_che")
	private String pjWrksCnclRoleChe;

	@Column(name = "pj_grp_a")
	private String pjGrpA;

	@Column(name = "pj_grp_b")
	private String pjGrpB;

	@Column(name = "vacation")
	private String vacation;

	@Column(name = "sick")
	private String sick;

	@Column(name = "personal_time")
	private String personalTime;

	@Column(name = "floating_holidays")
	private String floatingHolidays;

	@Column(name = "based_on_job_duties")
	private String basedOnJobDuties;

	@Column(name = "variable_hour")
	private String variableHour;

	@Column(name = "duties_desc")
	private String dutiesDesc;

	// bi-directional many-to-one association to PsPersonRef
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pj_emplid")
	@JsonBackReference("psPersonRef-psJobRefs")
	private PsPersonRef psPersonRef;

	public String getPjGrpA() {
		return pjGrpA;
	}

	public void setPjGrpA(String pjGrpA) {
		this.pjGrpA = pjGrpA;
	}

	public String getPjGrpB() {
		return pjGrpB;
	}

	public void setPjGrpB(String pjGrpB) {
		this.pjGrpB = pjGrpB;
	}

	public String getVacation() {
		return vacation;
	}

	public void setVacation(String vacation) {
		this.vacation = vacation;
	}

	public String getSick() {
		return sick;
	}

	public void setSick(String sick) {
		this.sick = sick;
	}

	public String getPersonalTime() {
		return personalTime;
	}

	public void setPersonalTime(String personalTime) {
		this.personalTime = personalTime;
	}

	public String getFloatingHolidays() {
		return floatingHolidays;
	}

	public void setFloatingHolidays(String floatingHolidays) {
		this.floatingHolidays = floatingHolidays;
	}

	public String getBasedOnJobDuties() {
		return basedOnJobDuties;
	}

	public void setBasedOnJobDuties(String basedOnJobDuties) {
		this.basedOnJobDuties = basedOnJobDuties;
	}

	public String getVariableHour() {
		return variableHour;
	}

	public void setVariableHour(String variableHour) {
		this.variableHour = variableHour;
	}

	public String getDutiesDesc() {
		return dutiesDesc;
	}

	public void setDutiesDesc(String dutiesDesc) {
		this.dutiesDesc = dutiesDesc;
	}

	public PsJobRef() {
	}

	public String getPjEmplid() {
		return pjEmplid;
	}

	public void setPjEmplid(String pjEmplid) {
		this.pjEmplid = pjEmplid;
	}

	public java.util.Date getPjEffdt() {
		return pjEffdt;
	}

	public void setPjEffdt(java.util.Date pjEffdt) {
		this.pjEffdt = pjEffdt;
	}

	public long getPjEffseq() {
		return pjEffseq;
	}

	public void setPjEffseq(long pjEffseq) {
		this.pjEffseq = pjEffseq;
	}

	public long getPjEmplRcd() {
		return pjEmplRcd;
	}

	public void setPjEmplRcd(long pjEmplRcd) {
		this.pjEmplRcd = pjEmplRcd;
	}

	public String getPjAbsenceSystemCd() {
		return this.pjAbsenceSystemCd;
	}

	public void setPjAbsenceSystemCd(String pjAbsenceSystemCd) {
		this.pjAbsenceSystemCd = pjAbsenceSystemCd;
	}

	public String getPjAccdntCdFra() {
		return this.pjAccdntCdFra;
	}

	public void setPjAccdntCdFra(String pjAccdntCdFra) {
		this.pjAccdntCdFra = pjAccdntCdFra;
	}

	public String getPjAcctCd() {
		return this.pjAcctCd;
	}

	public void setPjAcctCd(String pjAcctCd) {
		this.pjAcctCd = pjAcctCd;
	}

	public String getPjAction() {
		return this.pjAction;
	}

	public void setPjAction(String pjAction) {
		this.pjAction = pjAction;
	}

	public Date getPjActionDt() {
		return this.pjActionDt;
	}

	public void setPjActionDt(Date pjActionDt) {
		this.pjActionDt = pjActionDt;
	}

	public String getPjActionReason() {
		return this.pjActionReason;
	}

	public void setPjActionReason(String pjActionReason) {
		this.pjActionReason = pjActionReason;
	}

	public String getPjAddsToFteActual() {
		return this.pjAddsToFteActual;
	}

	public void setPjAddsToFteActual(String pjAddsToFteActual) {
		this.pjAddsToFteActual = pjAddsToFteActual;
	}

	public BigDecimal getPjAnnlBenefBaseRt() {
		return this.pjAnnlBenefBaseRt;
	}

	public void setPjAnnlBenefBaseRt(BigDecimal pjAnnlBenefBaseRt) {
		this.pjAnnlBenefBaseRt = pjAnnlBenefBaseRt;
	}

	public BigDecimal getPjAnnualRt() {
		return this.pjAnnualRt;
	}

	public void setPjAnnualRt(BigDecimal pjAnnualRt) {
		this.pjAnnualRt = pjAnnualRt;
	}

	public String getPjApptType() {
		return this.pjApptType;
	}

	public void setPjApptType(String pjApptType) {
		this.pjApptType = pjApptType;
	}

	public Date getPjAsgnEndDt() {
		return this.pjAsgnEndDt;
	}

	public void setPjAsgnEndDt(Date pjAsgnEndDt) {
		this.pjAsgnEndDt = pjAsgnEndDt;
	}

	public Date getPjAsgnStartDt() {
		return this.pjAsgnStartDt;
	}

	public void setPjAsgnStartDt(Date pjAsgnStartDt) {
		this.pjAsgnStartDt = pjAsgnStartDt;
	}

	public String getPjAutoEndFlg() {
		return this.pjAutoEndFlg;
	}

	public void setPjAutoEndFlg(String pjAutoEndFlg) {
		this.pjAutoEndFlg = pjAutoEndFlg;
	}

	public String getPjBargUnit() {
		return this.pjBargUnit;
	}

	public void setPjBargUnit(String pjBargUnit) {
		this.pjBargUnit = pjBargUnit;
	}

	public String getPjBasAction() {
		return this.pjBasAction;
	}

	public void setPjBasAction(String pjBasAction) {
		this.pjBasAction = pjBasAction;
	}

	public String getPjBasGroupId() {
		return this.pjBasGroupId;
	}

	public void setPjBasGroupId(String pjBasGroupId) {
		this.pjBasGroupId = pjBasGroupId;
	}

	public String getPjBenStatus() {
		return this.pjBenStatus;
	}

	public void setPjBenStatus(String pjBenStatus) {
		this.pjBenStatus = pjBenStatus;
	}

	public String getPjBenefitSystem() {
		return this.pjBenefitSystem;
	}

	public void setPjBenefitSystem(String pjBenefitSystem) {
		this.pjBenefitSystem = pjBenefitSystem;
	}

	public String getPjBorderWalker() {
		return this.pjBorderWalker;
	}

	public void setPjBorderWalker(String pjBorderWalker) {
		this.pjBorderWalker = pjBorderWalker;
	}

	public String getPjBusinessUnit() {
		return this.pjBusinessUnit;
	}

	public void setPjBusinessUnit(String pjBusinessUnit) {
		this.pjBusinessUnit = pjBusinessUnit;
	}

	public BigDecimal getPjChangeAmt() {
		return this.pjChangeAmt;
	}

	public void setPjChangeAmt(BigDecimal pjChangeAmt) {
		this.pjChangeAmt = pjChangeAmt;
	}

	public BigDecimal getPjChangePct() {
		return this.pjChangePct;
	}

	public void setPjChangePct(BigDecimal pjChangePct) {
		this.pjChangePct = pjChangePct;
	}

	public String getPjClassIndc() {
		return this.pjClassIndc;
	}

	public void setPjClassIndc(String pjClassIndc) {
		this.pjClassIndc = pjClassIndc;
	}

	public String getPjCobraAction() {
		return this.pjCobraAction;
	}

	public void setPjCobraAction(String pjCobraAction) {
		this.pjCobraAction = pjCobraAction;
	}

	public String getPjCompFrequency() {
		return this.pjCompFrequency;
	}

	public void setPjCompFrequency(String pjCompFrequency) {
		this.pjCompFrequency = pjCompFrequency;
	}

	public String getPjCompany() {
		return this.pjCompany;
	}

	public void setPjCompany(String pjCompany) {
		this.pjCompany = pjCompany;
	}

	public BigDecimal getPjComprate() {
		return this.pjComprate;
	}

	public void setPjComprate(BigDecimal pjComprate) {
		this.pjComprate = pjComprate;
	}

	public String getPjContractNum() {
		return this.pjContractNum;
	}

	public void setPjContractNum(String pjContractNum) {
		this.pjContractNum = pjContractNum;
	}

	public BigDecimal getPjCtgRate() {
		return this.pjCtgRate;
	}

	public void setPjCtgRate(BigDecimal pjCtgRate) {
		this.pjCtgRate = pjCtgRate;
	}

	public String getPjCurRtType() {
		return this.pjCurRtType;
	}

	public void setPjCurRtType(String pjCurRtType) {
		this.pjCurRtType = pjCurRtType;
	}

	public String getPjCurrencyCd() {
		return this.pjCurrencyCd;
	}

	public void setPjCurrencyCd(String pjCurrencyCd) {
		this.pjCurrencyCd = pjCurrencyCd;
	}

	public String getPjCurrencyCd1() {
		return this.pjCurrencyCd1;
	}

	public void setPjCurrencyCd1(String pjCurrencyCd1) {
		this.pjCurrencyCd1 = pjCurrencyCd1;
	}

	public BigDecimal getPjDailyRt() {
		return this.pjDailyRt;
	}

	public void setPjDailyRt(BigDecimal pjDailyRt) {
		this.pjDailyRt = pjDailyRt;
	}

	public Date getPjDeptEntryDt() {
		return this.pjDeptEntryDt;
	}

	public void setPjDeptEntryDt(Date pjDeptEntryDt) {
		this.pjDeptEntryDt = pjDeptEntryDt;
	}

	public String getPjDeptid() {
		return this.pjDeptid;
	}

	public void setPjDeptid(String pjDeptid) {
		this.pjDeptid = pjDeptid;
	}

	public String getPjDirectlyTipped() {
		return this.pjDirectlyTipped;
	}

	public void setPjDirectlyTipped(String pjDirectlyTipped) {
		this.pjDirectlyTipped = pjDirectlyTipped;
	}

	public String getPjEarnsDistType() {
		return this.pjEarnsDistType;
	}

	public void setPjEarnsDistType(String pjEarnsDistType) {
		this.pjEarnsDistType = pjEarnsDistType;
	}

	public String getPjEeoClass() {
		return this.pjEeoClass;
	}

	public void setPjEeoClass(String pjEeoClass) {
		this.pjEeoClass = pjEeoClass;
	}

	public String getPjEligConfig1() {
		return this.pjEligConfig1;
	}

	public void setPjEligConfig1(String pjEligConfig1) {
		this.pjEligConfig1 = pjEligConfig1;
	}

	public String getPjEligConfig2() {
		return this.pjEligConfig2;
	}

	public void setPjEligConfig2(String pjEligConfig2) {
		this.pjEligConfig2 = pjEligConfig2;
	}

	public String getPjEligConfig3() {
		return this.pjEligConfig3;
	}

	public void setPjEligConfig3(String pjEligConfig3) {
		this.pjEligConfig3 = pjEligConfig3;
	}

	public String getPjEligConfig4() {
		return this.pjEligConfig4;
	}

	public void setPjEligConfig4(String pjEligConfig4) {
		this.pjEligConfig4 = pjEligConfig4;
	}

	public String getPjEligConfig5() {
		return this.pjEligConfig5;
	}

	public void setPjEligConfig5(String pjEligConfig5) {
		this.pjEligConfig5 = pjEligConfig5;
	}

	public String getPjEligConfig6() {
		return this.pjEligConfig6;
	}

	public void setPjEligConfig6(String pjEligConfig6) {
		this.pjEligConfig6 = pjEligConfig6;
	}

	public String getPjEligConfig7() {
		return this.pjEligConfig7;
	}

	public void setPjEligConfig7(String pjEligConfig7) {
		this.pjEligConfig7 = pjEligConfig7;
	}

	public String getPjEligConfig8() {
		return this.pjEligConfig8;
	}

	public void setPjEligConfig8(String pjEligConfig8) {
		this.pjEligConfig8 = pjEligConfig8;
	}

	public String getPjEligConfig9() {
		return this.pjEligConfig9;
	}

	public void setPjEligConfig9(String pjEligConfig9) {
		this.pjEligConfig9 = pjEligConfig9;
	}

	public String getPjEmplClass() {
		return this.pjEmplClass;
	}

	public void setPjEmplClass(String pjEmplClass) {
		this.pjEmplClass = pjEmplClass;
	}

	public String getPjEmplCtg() {
		return this.pjEmplCtg;
	}

	public void setPjEmplCtg(String pjEmplCtg) {
		this.pjEmplCtg = pjEmplCtg;
	}

	public String getPjEmplCtgL1() {
		return this.pjEmplCtgL1;
	}

	public void setPjEmplCtgL1(String pjEmplCtgL1) {
		this.pjEmplCtgL1 = pjEmplCtgL1;
	}

	public String getPjEmplCtgL2() {
		return this.pjEmplCtgL2;
	}

	public void setPjEmplCtgL2(String pjEmplCtgL2) {
		this.pjEmplCtgL2 = pjEmplCtgL2;
	}

	public String getPjEmplStatus() {
		return this.pjEmplStatus;
	}

	public void setPjEmplStatus(String pjEmplStatus) {
		this.pjEmplStatus = pjEmplStatus;
	}

	public String getPjEmplType() {
		return this.pjEmplType;
	}

	public void setPjEmplType(String pjEmplType) {
		this.pjEmplType = pjEmplType;
	}

	public String getPjEncumbOverride() {
		return this.pjEncumbOverride;
	}

	public void setPjEncumbOverride(String pjEncumbOverride) {
		this.pjEncumbOverride = pjEncumbOverride;
	}

	public Date getPjEntryDate() {
		return this.pjEntryDate;
	}

	public void setPjEntryDate(Date pjEntryDate) {
		this.pjEntryDate = pjEntryDate;
	}

	public String getPjEstabid() {
		return this.pjEstabid;
	}

	public void setPjEstabid(String pjEstabid) {
		this.pjEstabid = pjEstabid;
	}

	public BigDecimal getPjExemptHoursMonth() {
		return this.pjExemptHoursMonth;
	}

	public void setPjExemptHoursMonth(BigDecimal pjExemptHoursMonth) {
		this.pjExemptHoursMonth = pjExemptHoursMonth;
	}

	public String getPjExemptJobLbr() {
		return this.pjExemptJobLbr;
	}

	public void setPjExemptJobLbr(String pjExemptJobLbr) {
		this.pjExemptJobLbr = pjExemptJobLbr;
	}

	public Date getPjExpectedEndDate() {
		return this.pjExpectedEndDate;
	}

	public void setPjExpectedEndDate(Date pjExpectedEndDate) {
		this.pjExpectedEndDate = pjExpectedEndDate;
	}

	public Date getPjExpectedReturnDt() {
		return this.pjExpectedReturnDt;
	}

	public void setPjExpectedReturnDt(Date pjExpectedReturnDt) {
		this.pjExpectedReturnDt = pjExpectedReturnDt;
	}

	public String getPjFicaStatusEe() {
		return this.pjFicaStatusEe;
	}

	public void setPjFicaStatusEe(String pjFicaStatusEe) {
		this.pjFicaStatusEe = pjFicaStatusEe;
	}

	public String getPjFlsaStatus() {
		return this.pjFlsaStatus;
	}

	public void setPjFlsaStatus(String pjFlsaStatus) {
		this.pjFlsaStatus = pjFlsaStatus;
	}

	public Date getPjForcePublish() {
		return this.pjForcePublish;
	}

	public void setPjForcePublish(Date pjForcePublish) {
		this.pjForcePublish = pjForcePublish;
	}

	public BigDecimal getPjFte() {
		return this.pjFte;
	}

	public void setPjFte(BigDecimal pjFte) {
		this.pjFte = pjFte;
	}

	public String getPjFullPartTime() {
		return this.pjFullPartTime;
	}

	public void setPjFullPartTime(String pjFullPartTime) {
		this.pjFullPartTime = pjFullPartTime;
	}

	public String getPjFunctionCd() {
		return this.pjFunctionCd;
	}

	public void setPjFunctionCd(String pjFunctionCd) {
		this.pjFunctionCd = pjFunctionCd;
	}

	public String getPjGlPayType() {
		return this.pjGlPayType;
	}

	public void setPjGlPayType(String pjGlPayType) {
		this.pjGlPayType = pjGlPayType;
	}

	public String getPjGpAsofDtExgRt() {
		return this.pjGpAsofDtExgRt;
	}

	public void setPjGpAsofDtExgRt(String pjGpAsofDtExgRt) {
		this.pjGpAsofDtExgRt = pjGpAsofDtExgRt;
	}

	public String getPjGpDfltCurrttyp() {
		return this.pjGpDfltCurrttyp;
	}

	public void setPjGpDfltCurrttyp(String pjGpDfltCurrttyp) {
		this.pjGpDfltCurrttyp = pjGpDfltCurrttyp;
	}

	public String getPjGpDfltEligGrp() {
		return this.pjGpDfltEligGrp;
	}

	public void setPjGpDfltEligGrp(String pjGpDfltEligGrp) {
		this.pjGpDfltEligGrp = pjGpDfltEligGrp;
	}

	public String getPjGpDfltExrtdt() {
		return this.pjGpDfltExrtdt;
	}

	public void setPjGpDfltExrtdt(String pjGpDfltExrtdt) {
		this.pjGpDfltExrtdt = pjGpDfltExrtdt;
	}

	public String getPjGpEligGrp() {
		return this.pjGpEligGrp;
	}

	public void setPjGpEligGrp(String pjGpEligGrp) {
		this.pjGpEligGrp = pjGpEligGrp;
	}

	public String getPjGpPaygroup() {
		return this.pjGpPaygroup;
	}

	public void setPjGpPaygroup(String pjGpPaygroup) {
		this.pjGpPaygroup = pjGpPaygroup;
	}

	public String getPjGrade() {
		return this.pjGrade;
	}

	public void setPjGrade(String pjGrade) {
		this.pjGrade = pjGrade;
	}

	public Date getPjGradeEntryDt() {
		return this.pjGradeEntryDt;
	}

	public void setPjGradeEntryDt(Date pjGradeEntryDt) {
		this.pjGradeEntryDt = pjGradeEntryDt;
	}

	public Date getPjHireDt() {
		return this.pjHireDt;
	}

	public void setPjHireDt(Date pjHireDt) {
		this.pjHireDt = pjHireDt;
	}

	public String getPjHolidaySchedule() {
		return this.pjHolidaySchedule;
	}

	public void setPjHolidaySchedule(String pjHolidaySchedule) {
		this.pjHolidaySchedule = pjHolidaySchedule;
	}

	public BigDecimal getPjHourlyRt() {
		return this.pjHourlyRt;
	}

	public void setPjHourlyRt(BigDecimal pjHourlyRt) {
		this.pjHourlyRt = pjHourlyRt;
	}

	public String getPjHourlyRtFra() {
		return this.pjHourlyRtFra;
	}

	public void setPjHourlyRtFra(String pjHourlyRtFra) {
		this.pjHourlyRtFra = pjHourlyRtFra;
	}

	public String getPjHrStatus() {
		return this.pjHrStatus;
	}

	public void setPjHrStatus(String pjHrStatus) {
		this.pjHrStatus = pjHrStatus;
	}

	public String getPjInterctrWrksCncl() {
		return this.pjInterctrWrksCncl;
	}

	public void setPjInterctrWrksCncl(String pjInterctrWrksCncl) {
		this.pjInterctrWrksCncl = pjInterctrWrksCncl;
	}

	public String getPjJobDataSrcCd() {
		return this.pjJobDataSrcCd;
	}

	public void setPjJobDataSrcCd(String pjJobDataSrcCd) {
		this.pjJobDataSrcCd = pjJobDataSrcCd;
	}

	public Date getPjJobEntryDt() {
		return this.pjJobEntryDt;
	}

	public void setPjJobEntryDt(Date pjJobEntryDt) {
		this.pjJobEntryDt = pjJobEntryDt;
	}

	public String getPjJobIndicator() {
		return this.pjJobIndicator;
	}

	public void setPjJobIndicator(String pjJobIndicator) {
		this.pjJobIndicator = pjJobIndicator;
	}

	public String getPjJobcode() {
		return this.pjJobcode;
	}

	public void setPjJobcode(String pjJobcode) {
		this.pjJobcode = pjJobcode;
	}

	public String getPjLaborAgreement() {
		return this.pjLaborAgreement;
	}

	public void setPjLaborAgreement(String pjLaborAgreement) {
		this.pjLaborAgreement = pjLaborAgreement;
	}

	public String getPjLaborFacilityId() {
		return this.pjLaborFacilityId;
	}

	public void setPjLaborFacilityId(String pjLaborFacilityId) {
		this.pjLaborFacilityId = pjLaborFacilityId;
	}

	public String getPjLaborTypeGer() {
		return this.pjLaborTypeGer;
	}

	public void setPjLaborTypeGer(String pjLaborTypeGer) {
		this.pjLaborTypeGer = pjLaborTypeGer;
	}

	public Date getPjLastDateWorked() {
		return this.pjLastDateWorked;
	}

	public void setPjLastDateWorked(Date pjLastDateWorked) {
		this.pjLastDateWorked = pjLastDateWorked;
	}

	public Date getPjLastHireDt() {
		return this.pjLastHireDt;
	}

	public void setPjLastHireDt(Date pjLastHireDt) {
		this.pjLastHireDt = pjLastHireDt;
	}

	public Timestamp getPjLastupddttm() {
		return this.pjLastupddttm;
	}

	public void setPjLastupddttm(Timestamp pjLastupddttm) {
		this.pjLastupddttm = pjLastupddttm;
	}

	public String getPjLastupdoprid() {
		return this.pjLastupdoprid;
	}

	public void setPjLastupdoprid(String pjLastupdoprid) {
		this.pjLastupdoprid = pjLastupdoprid;
	}

	public String getPjLayoffExemptFlag() {
		return this.pjLayoffExemptFlag;
	}

	public void setPjLayoffExemptFlag(String pjLayoffExemptFlag) {
		this.pjLayoffExemptFlag = pjLayoffExemptFlag;
	}

	public String getPjLayoffExemptRsn() {
		return this.pjLayoffExemptRsn;
	}

	public void setPjLayoffExemptRsn(String pjLayoffExemptRsn) {
		this.pjLayoffExemptRsn = pjLayoffExemptRsn;
	}

	public Date getPjLbrFacEntryDt() {
		return this.pjLbrFacEntryDt;
	}

	public void setPjLbrFacEntryDt(Date pjLbrFacEntryDt) {
		this.pjLbrFacEntryDt = pjLbrFacEntryDt;
	}

	public String getPjLdwOvr() {
		return this.pjLdwOvr;
	}

	public void setPjLdwOvr(String pjLdwOvr) {
		this.pjLdwOvr = pjLdwOvr;
	}

	public String getPjLocation() {
		return this.pjLocation;
	}

	public void setPjLocation(String pjLocation) {
		this.pjLocation = pjLocation;
	}

	public Date getPjLstAsgnStartDt() {
		return this.pjLstAsgnStartDt;
	}

	public void setPjLstAsgnStartDt(Date pjLstAsgnStartDt) {
		this.pjLstAsgnStartDt = pjLstAsgnStartDt;
	}

	public String getPjLumpSumPay() {
		return this.pjLumpSumPay;
	}

	public void setPjLumpSumPay(String pjLumpSumPay) {
		this.pjLumpSumPay = pjLumpSumPay;
	}

	public BigDecimal getPjMainApptNumJpn() {
		return this.pjMainApptNumJpn;
	}

	public void setPjMainApptNumJpn(BigDecimal pjMainApptNumJpn) {
		this.pjMainApptNumJpn = pjMainApptNumJpn;
	}

	public BigDecimal getPjMatriculaNbr() {
		return this.pjMatriculaNbr;
	}

	public void setPjMatriculaNbr(BigDecimal pjMatriculaNbr) {
		this.pjMatriculaNbr = pjMatriculaNbr;
	}

	public BigDecimal getPjMonthlyRt() {
		return this.pjMonthlyRt;
	}

	public void setPjMonthlyRt(BigDecimal pjMonthlyRt) {
		this.pjMonthlyRt = pjMonthlyRt;
	}

	public String getPjOfficerCd() {
		return this.pjOfficerCd;
	}

	public void setPjOfficerCd(String pjOfficerCd) {
		this.pjOfficerCd = pjOfficerCd;
	}

	public BigDecimal getPjPaidFte() {
		return this.pjPaidFte;
	}

	public void setPjPaidFte(BigDecimal pjPaidFte) {
		this.pjPaidFte = pjPaidFte;
	}

	public BigDecimal getPjPaidHours() {
		return this.pjPaidHours;
	}

	public void setPjPaidHours(BigDecimal pjPaidHours) {
		this.pjPaidHours = pjPaidHours;
	}

	public String getPjPaidHrsFrequency() {
		return this.pjPaidHrsFrequency;
	}

	public void setPjPaidHrsFrequency(String pjPaidHrsFrequency) {
		this.pjPaidHrsFrequency = pjPaidHrsFrequency;
	}

	public String getPjPaySystemFlg() {
		return this.pjPaySystemFlg;
	}

	public void setPjPaySystemFlg(String pjPaySystemFlg) {
		this.pjPaySystemFlg = pjPaySystemFlg;
	}

	public String getPjPayUnionFee() {
		return this.pjPayUnionFee;
	}

	public void setPjPayUnionFee(String pjPayUnionFee) {
		this.pjPayUnionFee = pjPayUnionFee;
	}

	public String getPjPaygroup() {
		return this.pjPaygroup;
	}

	public void setPjPaygroup(String pjPaygroup) {
		this.pjPaygroup = pjPaygroup;
	}

	public String getPjPerOrg() {
		return this.pjPerOrg;
	}

	public void setPjPerOrg(String pjPerOrg) {
		this.pjPerOrg = pjPerOrg;
	}

	public String getPjPerformGroupGer() {
		return this.pjPerformGroupGer;
	}

	public void setPjPerformGroupGer(String pjPerformGroupGer) {
		this.pjPerformGroupGer = pjPerformGroupGer;
	}

	public String getPjPoiType() {
		return this.pjPoiType;
	}

	public void setPjPoiType(String pjPoiType) {
		this.pjPoiType = pjPoiType;
	}

	public Date getPjPositionEntryDt() {
		return this.pjPositionEntryDt;
	}

	public void setPjPositionEntryDt(Date pjPositionEntryDt) {
		this.pjPositionEntryDt = pjPositionEntryDt;
	}

	public String getPjPositionNbr() {
		return this.pjPositionNbr;
	}

	public void setPjPositionNbr(String pjPositionNbr) {
		this.pjPositionNbr = pjPositionNbr;
	}

	public String getPjPositionOverride() {
		return this.pjPositionOverride;
	}

	public void setPjPositionOverride(String pjPositionOverride) {
		this.pjPositionOverride = pjPositionOverride;
	}

	public String getPjPosnChangeRecord() {
		return this.pjPosnChangeRecord;
	}

	public void setPjPosnChangeRecord(String pjPosnChangeRecord) {
		this.pjPosnChangeRecord = pjPosnChangeRecord;
	}

	public String getPjProrateCntAmt() {
		return this.pjProrateCntAmt;
	}

	public void setPjProrateCntAmt(String pjProrateCntAmt) {
		this.pjProrateCntAmt = pjProrateCntAmt;
	}

	public String getPjRegRegion() {
		return this.pjRegRegion;
	}

	public void setPjRegRegion(String pjRegRegion) {
		this.pjRegRegion = pjRegRegion;
	}

	public String getPjRegTemp() {
		return this.pjRegTemp;
	}

	public void setPjRegTemp(String pjRegTemp) {
		this.pjRegTemp = pjRegTemp;
	}

	public String getPjReportsTo() {
		return this.pjReportsTo;
	}

	public void setPjReportsTo(String pjReportsTo) {
		this.pjReportsTo = pjReportsTo;
	}

	public String getPjSalAdminPlan() {
		return this.pjSalAdminPlan;
	}

	public void setPjSalAdminPlan(String pjSalAdminPlan) {
		this.pjSalAdminPlan = pjSalAdminPlan;
	}

	public String getPjSetidDept() {
		return this.pjSetidDept;
	}

	public void setPjSetidDept(String pjSetidDept) {
		this.pjSetidDept = pjSetidDept;
	}

	public String getPjSetidEmplClass() {
		return this.pjSetidEmplClass;
	}

	public void setPjSetidEmplClass(String pjSetidEmplClass) {
		this.pjSetidEmplClass = pjSetidEmplClass;
	}

	public String getPjSetidJobcode() {
		return this.pjSetidJobcode;
	}

	public void setPjSetidJobcode(String pjSetidJobcode) {
		this.pjSetidJobcode = pjSetidJobcode;
	}

	public String getPjSetidLbrAgrmnt() {
		return this.pjSetidLbrAgrmnt;
	}

	public void setPjSetidLbrAgrmnt(String pjSetidLbrAgrmnt) {
		this.pjSetidLbrAgrmnt = pjSetidLbrAgrmnt;
	}

	public String getPjSetidLocation() {
		return this.pjSetidLocation;
	}

	public void setPjSetidLocation(String pjSetidLocation) {
		this.pjSetidLocation = pjSetidLocation;
	}

	public String getPjSetidSalary() {
		return this.pjSetidSalary;
	}

	public void setPjSetidSalary(String pjSetidSalary) {
		this.pjSetidSalary = pjSetidSalary;
	}

	public String getPjSetidSupvLvl() {
		return this.pjSetidSupvLvl;
	}

	public void setPjSetidSupvLvl(String pjSetidSupvLvl) {
		this.pjSetidSupvLvl = pjSetidSupvLvl;
	}

	public String getPjShift() {
		return this.pjShift;
	}

	public void setPjShift(String pjShift) {
		this.pjShift = pjShift;
	}

	public BigDecimal getPjShiftFactor() {
		return this.pjShiftFactor;
	}

	public void setPjShiftFactor(BigDecimal pjShiftFactor) {
		this.pjShiftFactor = pjShiftFactor;
	}

	public BigDecimal getPjShiftRt() {
		return this.pjShiftRt;
	}

	public void setPjShiftRt(BigDecimal pjShiftRt) {
		this.pjShiftRt = pjShiftRt;
	}

	public String getPjSocSecRiskCode() {
		return this.pjSocSecRiskCode;
	}

	public void setPjSocSecRiskCode(String pjSocSecRiskCode) {
		this.pjSocSecRiskCode = pjSocSecRiskCode;
	}

	public String getPjSpkCommIdGer() {
		return this.pjSpkCommIdGer;
	}

	public void setPjSpkCommIdGer(String pjSpkCommIdGer) {
		this.pjSpkCommIdGer = pjSpkCommIdGer;
	}

	public BigDecimal getPjStdHours() {
		return this.pjStdHours;
	}

	public void setPjStdHours(BigDecimal pjStdHours) {
		this.pjStdHours = pjStdHours;
	}

	public String getPjStdHrsFrequency() {
		return this.pjStdHrsFrequency;
	}

	public void setPjStdHrsFrequency(String pjStdHrsFrequency) {
		this.pjStdHrsFrequency = pjStdHrsFrequency;
	}

	public BigDecimal getPjStep() {
		return this.pjStep;
	}

	public void setPjStep(BigDecimal pjStep) {
		this.pjStep = pjStep;
	}

	public Date getPjStepEntryDt() {
		return this.pjStepEntryDt;
	}

	public void setPjStepEntryDt(Date pjStepEntryDt) {
		this.pjStepEntryDt = pjStepEntryDt;
	}

	public String getPjSupervisorId() {
		return this.pjSupervisorId;
	}

	public void setPjSupervisorId(String pjSupervisorId) {
		this.pjSupervisorId = pjSupervisorId;
	}

	public String getPjSupvLvlId() {
		return this.pjSupvLvlId;
	}

	public void setPjSupvLvlId(String pjSupvLvlId) {
		this.pjSupvLvlId = pjSupvLvlId;
	}

	public String getPjTariffAreaGer() {
		return this.pjTariffAreaGer;
	}

	public void setPjTariffAreaGer(String pjTariffAreaGer) {
		this.pjTariffAreaGer = pjTariffAreaGer;
	}

	public String getPjTariffGer() {
		return this.pjTariffGer;
	}

	public void setPjTariffGer(String pjTariffGer) {
		this.pjTariffGer = pjTariffGer;
	}

	public String getPjTaxLocationCd() {
		return this.pjTaxLocationCd;
	}

	public void setPjTaxLocationCd(String pjTaxLocationCd) {
		this.pjTaxLocationCd = pjTaxLocationCd;
	}

	public Date getPjTerminationDt() {
		return this.pjTerminationDt;
	}

	public void setPjTerminationDt(Date pjTerminationDt) {
		this.pjTerminationDt = pjTerminationDt;
	}

	public String getPjUnionCd() {
		return this.pjUnionCd;
	}

	public void setPjUnionCd(String pjUnionCd) {
		this.pjUnionCd = pjUnionCd;
	}

	public BigDecimal getPjUnionFeeAmount() {
		return this.pjUnionFeeAmount;
	}

	public void setPjUnionFeeAmount(BigDecimal pjUnionFeeAmount) {
		this.pjUnionFeeAmount = pjUnionFeeAmount;
	}

	public Date getPjUnionFeeEndDt() {
		return this.pjUnionFeeEndDt;
	}

	public void setPjUnionFeeEndDt(Date pjUnionFeeEndDt) {
		this.pjUnionFeeEndDt = pjUnionFeeEndDt;
	}

	public Date getPjUnionFeeStartDt() {
		return this.pjUnionFeeStartDt;
	}

	public void setPjUnionFeeStartDt(Date pjUnionFeeStartDt) {
		this.pjUnionFeeStartDt = pjUnionFeeStartDt;
	}

	public String getPjUnionFullPart() {
		return this.pjUnionFullPart;
	}

	public void setPjUnionFullPart(String pjUnionFullPart) {
		this.pjUnionFullPart = pjUnionFullPart;
	}

	public String getPjUnionPos() {
		return this.pjUnionPos;
	}

	public void setPjUnionPos(String pjUnionPos) {
		this.pjUnionPos = pjUnionPos;
	}

	public Date getPjUnionSeniorityDt() {
		return this.pjUnionSeniorityDt;
	}

	public void setPjUnionSeniorityDt(Date pjUnionSeniorityDt) {
		this.pjUnionSeniorityDt = pjUnionSeniorityDt;
	}

	public String getPjValue1Fra() {
		return this.pjValue1Fra;
	}

	public void setPjValue1Fra(String pjValue1Fra) {
		this.pjValue1Fra = pjValue1Fra;
	}

	public String getPjValue2Fra() {
		return this.pjValue2Fra;
	}

	public void setPjValue2Fra(String pjValue2Fra) {
		this.pjValue2Fra = pjValue2Fra;
	}

	public String getPjValue3Fra() {
		return this.pjValue3Fra;
	}

	public void setPjValue3Fra(String pjValue3Fra) {
		this.pjValue3Fra = pjValue3Fra;
	}

	public String getPjValue4Fra() {
		return this.pjValue4Fra;
	}

	public void setPjValue4Fra(String pjValue4Fra) {
		this.pjValue4Fra = pjValue4Fra;
	}

	public String getPjValue5Fra() {
		return this.pjValue5Fra;
	}

	public void setPjValue5Fra(String pjValue5Fra) {
		this.pjValue5Fra = pjValue5Fra;
	}

	public BigDecimal getPjWorkDayHours() {
		return this.pjWorkDayHours;
	}

	public void setPjWorkDayHours(BigDecimal pjWorkDayHours) {
		this.pjWorkDayHours = pjWorkDayHours;
	}

	public String getPjWppStopFlag() {
		return this.pjWppStopFlag;
	}

	public void setPjWppStopFlag(String pjWppStopFlag) {
		this.pjWppStopFlag = pjWppStopFlag;
	}

	public String getPjWrksCnclFunction() {
		return this.pjWrksCnclFunction;
	}

	public void setPjWrksCnclFunction(String pjWrksCnclFunction) {
		this.pjWrksCnclFunction = pjWrksCnclFunction;
	}

	public String getPjWrksCnclRoleChe() {
		return this.pjWrksCnclRoleChe;
	}

	public void setPjWrksCnclRoleChe(String pjWrksCnclRoleChe) {
		this.pjWrksCnclRoleChe = pjWrksCnclRoleChe;
	}

	public PsPersonRef getPsPersonRef() {
		return this.psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pjAbsenceSystemCd == null) ? 0 : pjAbsenceSystemCd.hashCode());
		result = prime * result + ((pjAccdntCdFra == null) ? 0 : pjAccdntCdFra.hashCode());
		result = prime * result + ((pjAcctCd == null) ? 0 : pjAcctCd.hashCode());
		result = prime * result + ((pjAction == null) ? 0 : pjAction.hashCode());
		result = prime * result + ((pjActionDt == null) ? 0 : pjActionDt.hashCode());
		result = prime * result + ((pjActionReason == null) ? 0 : pjActionReason.hashCode());
		result = prime * result + ((pjAddsToFteActual == null) ? 0 : pjAddsToFteActual.hashCode());
		result = prime * result + ((pjAnnlBenefBaseRt == null) ? 0 : pjAnnlBenefBaseRt.hashCode());
		result = prime * result + ((pjAnnualRt == null) ? 0 : pjAnnualRt.hashCode());
		result = prime * result + ((pjApptType == null) ? 0 : pjApptType.hashCode());
		result = prime * result + ((pjAsgnEndDt == null) ? 0 : pjAsgnEndDt.hashCode());
		result = prime * result + ((pjAsgnStartDt == null) ? 0 : pjAsgnStartDt.hashCode());
		result = prime * result + ((pjAutoEndFlg == null) ? 0 : pjAutoEndFlg.hashCode());
		result = prime * result + ((pjBargUnit == null) ? 0 : pjBargUnit.hashCode());
		result = prime * result + ((pjBasAction == null) ? 0 : pjBasAction.hashCode());
		result = prime * result + ((pjBasGroupId == null) ? 0 : pjBasGroupId.hashCode());
		result = prime * result + ((pjBenStatus == null) ? 0 : pjBenStatus.hashCode());
		result = prime * result + ((pjBenefitSystem == null) ? 0 : pjBenefitSystem.hashCode());
		result = prime * result + ((pjBorderWalker == null) ? 0 : pjBorderWalker.hashCode());
		result = prime * result + ((pjBusinessUnit == null) ? 0 : pjBusinessUnit.hashCode());
		result = prime * result + ((pjChangeAmt == null) ? 0 : pjChangeAmt.hashCode());
		result = prime * result + ((pjChangePct == null) ? 0 : pjChangePct.hashCode());
		result = prime * result + ((pjClassIndc == null) ? 0 : pjClassIndc.hashCode());
		result = prime * result + ((pjCobraAction == null) ? 0 : pjCobraAction.hashCode());
		result = prime * result + ((pjCompFrequency == null) ? 0 : pjCompFrequency.hashCode());
		result = prime * result + ((pjCompany == null) ? 0 : pjCompany.hashCode());
		result = prime * result + ((pjComprate == null) ? 0 : pjComprate.hashCode());
		result = prime * result + ((pjContractNum == null) ? 0 : pjContractNum.hashCode());
		result = prime * result + ((pjCtgRate == null) ? 0 : pjCtgRate.hashCode());
		result = prime * result + ((pjCurRtType == null) ? 0 : pjCurRtType.hashCode());
		result = prime * result + ((pjCurrencyCd == null) ? 0 : pjCurrencyCd.hashCode());
		result = prime * result + ((pjCurrencyCd1 == null) ? 0 : pjCurrencyCd1.hashCode());
		result = prime * result + ((pjDailyRt == null) ? 0 : pjDailyRt.hashCode());
		result = prime * result + ((pjDeptEntryDt == null) ? 0 : pjDeptEntryDt.hashCode());
		result = prime * result + ((pjDeptid == null) ? 0 : pjDeptid.hashCode());
		result = prime * result + ((pjDirectlyTipped == null) ? 0 : pjDirectlyTipped.hashCode());
		result = prime * result + ((pjEarnsDistType == null) ? 0 : pjEarnsDistType.hashCode());
		result = prime * result + ((pjEeoClass == null) ? 0 : pjEeoClass.hashCode());
		result = prime * result + ((pjEffdt == null) ? 0 : pjEffdt.hashCode());
		result = prime * result + (int) (pjEffseq ^ (pjEffseq >>> 32));
		result = prime * result + ((pjEligConfig1 == null) ? 0 : pjEligConfig1.hashCode());
		result = prime * result + ((pjEligConfig2 == null) ? 0 : pjEligConfig2.hashCode());
		result = prime * result + ((pjEligConfig3 == null) ? 0 : pjEligConfig3.hashCode());
		result = prime * result + ((pjEligConfig4 == null) ? 0 : pjEligConfig4.hashCode());
		result = prime * result + ((pjEligConfig5 == null) ? 0 : pjEligConfig5.hashCode());
		result = prime * result + ((pjEligConfig6 == null) ? 0 : pjEligConfig6.hashCode());
		result = prime * result + ((pjEligConfig7 == null) ? 0 : pjEligConfig7.hashCode());
		result = prime * result + ((pjEligConfig8 == null) ? 0 : pjEligConfig8.hashCode());
		result = prime * result + ((pjEligConfig9 == null) ? 0 : pjEligConfig9.hashCode());
		result = prime * result + ((pjEmplClass == null) ? 0 : pjEmplClass.hashCode());
		result = prime * result + ((pjEmplCtg == null) ? 0 : pjEmplCtg.hashCode());
		result = prime * result + ((pjEmplCtgL1 == null) ? 0 : pjEmplCtgL1.hashCode());
		result = prime * result + ((pjEmplCtgL2 == null) ? 0 : pjEmplCtgL2.hashCode());
		result = prime * result + (int) (pjEmplRcd ^ (pjEmplRcd >>> 32));
		result = prime * result + ((pjEmplStatus == null) ? 0 : pjEmplStatus.hashCode());
		result = prime * result + ((pjEmplType == null) ? 0 : pjEmplType.hashCode());
		result = prime * result + ((pjEncumbOverride == null) ? 0 : pjEncumbOverride.hashCode());
		result = prime * result + ((pjEntryDate == null) ? 0 : pjEntryDate.hashCode());
		result = prime * result + ((pjEstabid == null) ? 0 : pjEstabid.hashCode());
		result = prime * result + ((pjExemptHoursMonth == null) ? 0 : pjExemptHoursMonth.hashCode());
		result = prime * result + ((pjExemptJobLbr == null) ? 0 : pjExemptJobLbr.hashCode());
		result = prime * result + ((pjExpectedEndDate == null) ? 0 : pjExpectedEndDate.hashCode());
		result = prime * result + ((pjExpectedReturnDt == null) ? 0 : pjExpectedReturnDt.hashCode());
		result = prime * result + ((pjFicaStatusEe == null) ? 0 : pjFicaStatusEe.hashCode());
		result = prime * result + ((pjFlsaStatus == null) ? 0 : pjFlsaStatus.hashCode());
		result = prime * result + ((pjForcePublish == null) ? 0 : pjForcePublish.hashCode());
		result = prime * result + ((pjFte == null) ? 0 : pjFte.hashCode());
		result = prime * result + ((pjFullPartTime == null) ? 0 : pjFullPartTime.hashCode());
		result = prime * result + ((pjFunctionCd == null) ? 0 : pjFunctionCd.hashCode());
		result = prime * result + ((pjGlPayType == null) ? 0 : pjGlPayType.hashCode());
		result = prime * result + ((pjGpAsofDtExgRt == null) ? 0 : pjGpAsofDtExgRt.hashCode());
		result = prime * result + ((pjGpDfltCurrttyp == null) ? 0 : pjGpDfltCurrttyp.hashCode());
		result = prime * result + ((pjGpDfltEligGrp == null) ? 0 : pjGpDfltEligGrp.hashCode());
		result = prime * result + ((pjGpDfltExrtdt == null) ? 0 : pjGpDfltExrtdt.hashCode());
		result = prime * result + ((pjGpEligGrp == null) ? 0 : pjGpEligGrp.hashCode());
		result = prime * result + ((pjGpPaygroup == null) ? 0 : pjGpPaygroup.hashCode());
		result = prime * result + ((pjGrade == null) ? 0 : pjGrade.hashCode());
		result = prime * result + ((pjGradeEntryDt == null) ? 0 : pjGradeEntryDt.hashCode());
		result = prime * result + ((pjHireDt == null) ? 0 : pjHireDt.hashCode());
		result = prime * result + ((pjHolidaySchedule == null) ? 0 : pjHolidaySchedule.hashCode());
		result = prime * result + ((pjHourlyRt == null) ? 0 : pjHourlyRt.hashCode());
		result = prime * result + ((pjHourlyRtFra == null) ? 0 : pjHourlyRtFra.hashCode());
		result = prime * result + ((pjHrStatus == null) ? 0 : pjHrStatus.hashCode());
		result = prime * result + ((pjInterctrWrksCncl == null) ? 0 : pjInterctrWrksCncl.hashCode());
		result = prime * result + ((pjJobDataSrcCd == null) ? 0 : pjJobDataSrcCd.hashCode());
		result = prime * result + ((pjJobEntryDt == null) ? 0 : pjJobEntryDt.hashCode());
		result = prime * result + ((pjJobIndicator == null) ? 0 : pjJobIndicator.hashCode());
		result = prime * result + ((pjJobcode == null) ? 0 : pjJobcode.hashCode());
		result = prime * result + ((pjLaborAgreement == null) ? 0 : pjLaborAgreement.hashCode());
		result = prime * result + ((pjLaborFacilityId == null) ? 0 : pjLaborFacilityId.hashCode());
		result = prime * result + ((pjLaborTypeGer == null) ? 0 : pjLaborTypeGer.hashCode());
		result = prime * result + ((pjLastDateWorked == null) ? 0 : pjLastDateWorked.hashCode());
		result = prime * result + ((pjLastHireDt == null) ? 0 : pjLastHireDt.hashCode());
		result = prime * result + ((pjLastupddttm == null) ? 0 : pjLastupddttm.hashCode());
		result = prime * result + ((pjLastupdoprid == null) ? 0 : pjLastupdoprid.hashCode());
		result = prime * result + ((pjLayoffExemptFlag == null) ? 0 : pjLayoffExemptFlag.hashCode());
		result = prime * result + ((pjLayoffExemptRsn == null) ? 0 : pjLayoffExemptRsn.hashCode());
		result = prime * result + ((pjLbrFacEntryDt == null) ? 0 : pjLbrFacEntryDt.hashCode());
		result = prime * result + ((pjLdwOvr == null) ? 0 : pjLdwOvr.hashCode());
		result = prime * result + ((pjLocation == null) ? 0 : pjLocation.hashCode());
		result = prime * result + ((pjLstAsgnStartDt == null) ? 0 : pjLstAsgnStartDt.hashCode());
		result = prime * result + ((pjLumpSumPay == null) ? 0 : pjLumpSumPay.hashCode());
		result = prime * result + ((pjMainApptNumJpn == null) ? 0 : pjMainApptNumJpn.hashCode());
		result = prime * result + ((pjMatriculaNbr == null) ? 0 : pjMatriculaNbr.hashCode());
		result = prime * result + ((pjMonthlyRt == null) ? 0 : pjMonthlyRt.hashCode());
		result = prime * result + ((pjOfficerCd == null) ? 0 : pjOfficerCd.hashCode());
		result = prime * result + ((pjPaidFte == null) ? 0 : pjPaidFte.hashCode());
		result = prime * result + ((pjPaidHours == null) ? 0 : pjPaidHours.hashCode());
		result = prime * result + ((pjPaidHrsFrequency == null) ? 0 : pjPaidHrsFrequency.hashCode());
		result = prime * result + ((pjPaySystemFlg == null) ? 0 : pjPaySystemFlg.hashCode());
		result = prime * result + ((pjPayUnionFee == null) ? 0 : pjPayUnionFee.hashCode());
		result = prime * result + ((pjPaygroup == null) ? 0 : pjPaygroup.hashCode());
		result = prime * result + ((pjPerOrg == null) ? 0 : pjPerOrg.hashCode());
		result = prime * result + ((pjPerformGroupGer == null) ? 0 : pjPerformGroupGer.hashCode());
		result = prime * result + ((pjPoiType == null) ? 0 : pjPoiType.hashCode());
		result = prime * result + ((pjPositionEntryDt == null) ? 0 : pjPositionEntryDt.hashCode());
		result = prime * result + ((pjPositionNbr == null) ? 0 : pjPositionNbr.hashCode());
		result = prime * result + ((pjPositionOverride == null) ? 0 : pjPositionOverride.hashCode());
		result = prime * result + ((pjPosnChangeRecord == null) ? 0 : pjPosnChangeRecord.hashCode());
		result = prime * result + ((pjProrateCntAmt == null) ? 0 : pjProrateCntAmt.hashCode());
		result = prime * result + ((pjRegRegion == null) ? 0 : pjRegRegion.hashCode());
		result = prime * result + ((pjRegTemp == null) ? 0 : pjRegTemp.hashCode());
		result = prime * result + ((pjReportsTo == null) ? 0 : pjReportsTo.hashCode());
		result = prime * result + ((pjSalAdminPlan == null) ? 0 : pjSalAdminPlan.hashCode());
		result = prime * result + ((pjSetidDept == null) ? 0 : pjSetidDept.hashCode());
		result = prime * result + ((pjSetidEmplClass == null) ? 0 : pjSetidEmplClass.hashCode());
		result = prime * result + ((pjSetidJobcode == null) ? 0 : pjSetidJobcode.hashCode());
		result = prime * result + ((pjSetidLbrAgrmnt == null) ? 0 : pjSetidLbrAgrmnt.hashCode());
		result = prime * result + ((pjSetidLocation == null) ? 0 : pjSetidLocation.hashCode());
		result = prime * result + ((pjSetidSalary == null) ? 0 : pjSetidSalary.hashCode());
		result = prime * result + ((pjSetidSupvLvl == null) ? 0 : pjSetidSupvLvl.hashCode());
		result = prime * result + ((pjShift == null) ? 0 : pjShift.hashCode());
		result = prime * result + ((pjShiftFactor == null) ? 0 : pjShiftFactor.hashCode());
		result = prime * result + ((pjShiftRt == null) ? 0 : pjShiftRt.hashCode());
		result = prime * result + ((pjSocSecRiskCode == null) ? 0 : pjSocSecRiskCode.hashCode());
		result = prime * result + ((pjSpkCommIdGer == null) ? 0 : pjSpkCommIdGer.hashCode());
		result = prime * result + ((pjStdHours == null) ? 0 : pjStdHours.hashCode());
		result = prime * result + ((pjStdHrsFrequency == null) ? 0 : pjStdHrsFrequency.hashCode());
		result = prime * result + ((pjStep == null) ? 0 : pjStep.hashCode());
		result = prime * result + ((pjStepEntryDt == null) ? 0 : pjStepEntryDt.hashCode());
		result = prime * result + ((pjSupervisorId == null) ? 0 : pjSupervisorId.hashCode());
		result = prime * result + ((pjSupvLvlId == null) ? 0 : pjSupvLvlId.hashCode());
		result = prime * result + ((pjTariffAreaGer == null) ? 0 : pjTariffAreaGer.hashCode());
		result = prime * result + ((pjTariffGer == null) ? 0 : pjTariffGer.hashCode());
		result = prime * result + ((pjTaxLocationCd == null) ? 0 : pjTaxLocationCd.hashCode());
		result = prime * result + ((pjTerminationDt == null) ? 0 : pjTerminationDt.hashCode());
		result = prime * result + ((pjUnionCd == null) ? 0 : pjUnionCd.hashCode());
		result = prime * result + ((pjUnionFeeAmount == null) ? 0 : pjUnionFeeAmount.hashCode());
		result = prime * result + ((pjUnionFeeEndDt == null) ? 0 : pjUnionFeeEndDt.hashCode());
		result = prime * result + ((pjUnionFeeStartDt == null) ? 0 : pjUnionFeeStartDt.hashCode());
		result = prime * result + ((pjUnionFullPart == null) ? 0 : pjUnionFullPart.hashCode());
		result = prime * result + ((pjUnionPos == null) ? 0 : pjUnionPos.hashCode());
		result = prime * result + ((pjUnionSeniorityDt == null) ? 0 : pjUnionSeniorityDt.hashCode());
		result = prime * result + ((pjValue1Fra == null) ? 0 : pjValue1Fra.hashCode());
		result = prime * result + ((pjValue2Fra == null) ? 0 : pjValue2Fra.hashCode());
		result = prime * result + ((pjValue3Fra == null) ? 0 : pjValue3Fra.hashCode());
		result = prime * result + ((pjValue4Fra == null) ? 0 : pjValue4Fra.hashCode());
		result = prime * result + ((pjValue5Fra == null) ? 0 : pjValue5Fra.hashCode());
		result = prime * result + ((pjWorkDayHours == null) ? 0 : pjWorkDayHours.hashCode());
		result = prime * result + ((pjWppStopFlag == null) ? 0 : pjWppStopFlag.hashCode());
		result = prime * result + ((pjWrksCnclFunction == null) ? 0 : pjWrksCnclFunction.hashCode());
		result = prime * result + ((pjWrksCnclRoleChe == null) ? 0 : pjWrksCnclRoleChe.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PsJobRef)) {
			return false;
		}
		PsJobRef other = (PsJobRef) obj;
		if (pjAbsenceSystemCd == null) {
			if (other.pjAbsenceSystemCd != null) {
				return false;
			}
		} else if (!pjAbsenceSystemCd.equals(other.pjAbsenceSystemCd)) {
			return false;
		}
		if (pjAccdntCdFra == null) {
			if (other.pjAccdntCdFra != null) {
				return false;
			}
		} else if (!pjAccdntCdFra.equals(other.pjAccdntCdFra)) {
			return false;
		}
		if (pjAcctCd == null) {
			if (other.pjAcctCd != null) {
				return false;
			}
		} else if (!pjAcctCd.equals(other.pjAcctCd)) {
			return false;
		}
		if (pjAction == null) {
			if (other.pjAction != null) {
				return false;
			}
		} else if (!pjAction.equals(other.pjAction)) {
			return false;
		}
		if (pjActionDt == null) {
			if (other.pjActionDt != null) {
				return false;
			}
		} else if (!pjActionDt.equals(other.pjActionDt)) {
			return false;
		}
		if (pjActionReason == null) {
			if (other.pjActionReason != null) {
				return false;
			}
		} else if (!pjActionReason.equals(other.pjActionReason)) {
			return false;
		}
		if (pjAddsToFteActual == null) {
			if (other.pjAddsToFteActual != null) {
				return false;
			}
		} else if (!pjAddsToFteActual.equals(other.pjAddsToFteActual)) {
			return false;
		}
		if (pjAnnlBenefBaseRt == null) {
			if (other.pjAnnlBenefBaseRt != null) {
				return false;
			}
		} else if (!pjAnnlBenefBaseRt.equals(other.pjAnnlBenefBaseRt)) {
			return false;
		}
		if (pjAnnualRt == null) {
			if (other.pjAnnualRt != null) {
				return false;
			}
		} else if (!pjAnnualRt.equals(other.pjAnnualRt)) {
			return false;
		}
		if (pjApptType == null) {
			if (other.pjApptType != null) {
				return false;
			}
		} else if (!pjApptType.equals(other.pjApptType)) {
			return false;
		}
		if (pjAsgnEndDt == null) {
			if (other.pjAsgnEndDt != null) {
				return false;
			}
		} else if (!pjAsgnEndDt.equals(other.pjAsgnEndDt)) {
			return false;
		}
		if (pjAsgnStartDt == null) {
			if (other.pjAsgnStartDt != null) {
				return false;
			}
		} else if (!pjAsgnStartDt.equals(other.pjAsgnStartDt)) {
			return false;
		}
		if (pjAutoEndFlg == null) {
			if (other.pjAutoEndFlg != null) {
				return false;
			}
		} else if (!pjAutoEndFlg.equals(other.pjAutoEndFlg)) {
			return false;
		}
		if (pjBargUnit == null) {
			if (other.pjBargUnit != null) {
				return false;
			}
		} else if (!pjBargUnit.equals(other.pjBargUnit)) {
			return false;
		}
		if (pjBasAction == null) {
			if (other.pjBasAction != null) {
				return false;
			}
		} else if (!pjBasAction.equals(other.pjBasAction)) {
			return false;
		}
		if (pjBasGroupId == null) {
			if (other.pjBasGroupId != null) {
				return false;
			}
		} else if (!pjBasGroupId.equals(other.pjBasGroupId)) {
			return false;
		}
		if (pjBenStatus == null) {
			if (other.pjBenStatus != null) {
				return false;
			}
		} else if (!pjBenStatus.equals(other.pjBenStatus)) {
			return false;
		}
		if (pjBenefitSystem == null) {
			if (other.pjBenefitSystem != null) {
				return false;
			}
		} else if (!pjBenefitSystem.equals(other.pjBenefitSystem)) {
			return false;
		}
		if (pjBorderWalker == null) {
			if (other.pjBorderWalker != null) {
				return false;
			}
		} else if (!pjBorderWalker.equals(other.pjBorderWalker)) {
			return false;
		}
		if (pjBusinessUnit == null) {
			if (other.pjBusinessUnit != null) {
				return false;
			}
		} else if (!pjBusinessUnit.equals(other.pjBusinessUnit)) {
			return false;
		}
		if (pjChangeAmt == null) {
			if (other.pjChangeAmt != null) {
				return false;
			}
		} else if (!pjChangeAmt.equals(other.pjChangeAmt)) {
			return false;
		}
		if (pjChangePct == null) {
			if (other.pjChangePct != null) {
				return false;
			}
		} else if (!pjChangePct.equals(other.pjChangePct)) {
			return false;
		}
		if (pjClassIndc == null) {
			if (other.pjClassIndc != null) {
				return false;
			}
		} else if (!pjClassIndc.equals(other.pjClassIndc)) {
			return false;
		}
		if (pjCobraAction == null) {
			if (other.pjCobraAction != null) {
				return false;
			}
		} else if (!pjCobraAction.equals(other.pjCobraAction)) {
			return false;
		}
		if (pjCompFrequency == null) {
			if (other.pjCompFrequency != null) {
				return false;
			}
		} else if (!pjCompFrequency.equals(other.pjCompFrequency)) {
			return false;
		}
		if (pjCompany == null) {
			if (other.pjCompany != null) {
				return false;
			}
		} else if (!pjCompany.equals(other.pjCompany)) {
			return false;
		}
		if (pjComprate == null) {
			if (other.pjComprate != null) {
				return false;
			}
		} else if (!pjComprate.equals(other.pjComprate)) {
			return false;
		}
		if (pjContractNum == null) {
			if (other.pjContractNum != null) {
				return false;
			}
		} else if (!pjContractNum.equals(other.pjContractNum)) {
			return false;
		}
		if (pjCtgRate == null) {
			if (other.pjCtgRate != null) {
				return false;
			}
		} else if (!pjCtgRate.equals(other.pjCtgRate)) {
			return false;
		}
		if (pjCurRtType == null) {
			if (other.pjCurRtType != null) {
				return false;
			}
		} else if (!pjCurRtType.equals(other.pjCurRtType)) {
			return false;
		}
		if (pjCurrencyCd == null) {
			if (other.pjCurrencyCd != null) {
				return false;
			}
		} else if (!pjCurrencyCd.equals(other.pjCurrencyCd)) {
			return false;
		}
		if (pjCurrencyCd1 == null) {
			if (other.pjCurrencyCd1 != null) {
				return false;
			}
		} else if (!pjCurrencyCd1.equals(other.pjCurrencyCd1)) {
			return false;
		}
		if (pjDailyRt == null) {
			if (other.pjDailyRt != null) {
				return false;
			}
		} else if (!pjDailyRt.equals(other.pjDailyRt)) {
			return false;
		}
		if (pjDeptEntryDt == null) {
			if (other.pjDeptEntryDt != null) {
				return false;
			}
		} else if (!pjDeptEntryDt.equals(other.pjDeptEntryDt)) {
			return false;
		}
		if (pjDeptid == null) {
			if (other.pjDeptid != null) {
				return false;
			}
		} else if (!pjDeptid.equals(other.pjDeptid)) {
			return false;
		}
		if (pjDirectlyTipped == null) {
			if (other.pjDirectlyTipped != null) {
				return false;
			}
		} else if (!pjDirectlyTipped.equals(other.pjDirectlyTipped)) {
			return false;
		}
		if (pjEarnsDistType == null) {
			if (other.pjEarnsDistType != null) {
				return false;
			}
		} else if (!pjEarnsDistType.equals(other.pjEarnsDistType)) {
			return false;
		}
		if (pjEeoClass == null) {
			if (other.pjEeoClass != null) {
				return false;
			}
		} else if (!pjEeoClass.equals(other.pjEeoClass)) {
			return false;
		}
		if (pjEffdt == null) {
			if (other.pjEffdt != null) {
				return false;
			}
		} else if (!pjEffdt.equals(other.pjEffdt)) {
			return false;
		}
		if (pjEffseq != other.pjEffseq) {
			return false;
		}
		if (pjEligConfig1 == null) {
			if (other.pjEligConfig1 != null) {
				return false;
			}
		} else if (!pjEligConfig1.equals(other.pjEligConfig1)) {
			return false;
		}
		if (pjEligConfig2 == null) {
			if (other.pjEligConfig2 != null) {
				return false;
			}
		} else if (!pjEligConfig2.equals(other.pjEligConfig2)) {
			return false;
		}
		if (pjEligConfig3 == null) {
			if (other.pjEligConfig3 != null) {
				return false;
			}
		} else if (!pjEligConfig3.equals(other.pjEligConfig3)) {
			return false;
		}
		if (pjEligConfig4 == null) {
			if (other.pjEligConfig4 != null) {
				return false;
			}
		} else if (!pjEligConfig4.equals(other.pjEligConfig4)) {
			return false;
		}
		if (pjEligConfig5 == null) {
			if (other.pjEligConfig5 != null) {
				return false;
			}
		} else if (!pjEligConfig5.equals(other.pjEligConfig5)) {
			return false;
		}
		if (pjEligConfig6 == null) {
			if (other.pjEligConfig6 != null) {
				return false;
			}
		} else if (!pjEligConfig6.equals(other.pjEligConfig6)) {
			return false;
		}
		if (pjEligConfig7 == null) {
			if (other.pjEligConfig7 != null) {
				return false;
			}
		} else if (!pjEligConfig7.equals(other.pjEligConfig7)) {
			return false;
		}
		if (pjEligConfig8 == null) {
			if (other.pjEligConfig8 != null) {
				return false;
			}
		} else if (!pjEligConfig8.equals(other.pjEligConfig8)) {
			return false;
		}
		if (pjEligConfig9 == null) {
			if (other.pjEligConfig9 != null) {
				return false;
			}
		} else if (!pjEligConfig9.equals(other.pjEligConfig9)) {
			return false;
		}
		if (pjEmplClass == null) {
			if (other.pjEmplClass != null) {
				return false;
			}
		} else if (!pjEmplClass.equals(other.pjEmplClass)) {
			return false;
		}
		if (pjEmplCtg == null) {
			if (other.pjEmplCtg != null) {
				return false;
			}
		} else if (!pjEmplCtg.equals(other.pjEmplCtg)) {
			return false;
		}
		if (pjEmplCtgL1 == null) {
			if (other.pjEmplCtgL1 != null) {
				return false;
			}
		} else if (!pjEmplCtgL1.equals(other.pjEmplCtgL1)) {
			return false;
		}
		if (pjEmplCtgL2 == null) {
			if (other.pjEmplCtgL2 != null) {
				return false;
			}
		} else if (!pjEmplCtgL2.equals(other.pjEmplCtgL2)) {
			return false;
		}
		if (pjEmplRcd != other.pjEmplRcd) {
			return false;
		}
		if (pjEmplStatus == null) {
			if (other.pjEmplStatus != null) {
				return false;
			}
		} else if (!pjEmplStatus.equals(other.pjEmplStatus)) {
			return false;
		}
		if (pjEmplType == null) {
			if (other.pjEmplType != null) {
				return false;
			}
		} else if (!pjEmplType.equals(other.pjEmplType)) {
			return false;
		}
		if (pjEncumbOverride == null) {
			if (other.pjEncumbOverride != null) {
				return false;
			}
		} else if (!pjEncumbOverride.equals(other.pjEncumbOverride)) {
			return false;
		}
		if (pjEntryDate == null) {
			if (other.pjEntryDate != null) {
				return false;
			}
		} else if (!pjEntryDate.equals(other.pjEntryDate)) {
			return false;
		}
		if (pjEstabid == null) {
			if (other.pjEstabid != null) {
				return false;
			}
		} else if (!pjEstabid.equals(other.pjEstabid)) {
			return false;
		}
		if (pjExemptHoursMonth == null) {
			if (other.pjExemptHoursMonth != null) {
				return false;
			}
		} else if (!pjExemptHoursMonth.equals(other.pjExemptHoursMonth)) {
			return false;
		}
		if (pjExemptJobLbr == null) {
			if (other.pjExemptJobLbr != null) {
				return false;
			}
		} else if (!pjExemptJobLbr.equals(other.pjExemptJobLbr)) {
			return false;
		}
		if (pjExpectedEndDate == null) {
			if (other.pjExpectedEndDate != null) {
				return false;
			}
		} else if (!pjExpectedEndDate.equals(other.pjExpectedEndDate)) {
			return false;
		}
		if (pjExpectedReturnDt == null) {
			if (other.pjExpectedReturnDt != null) {
				return false;
			}
		} else if (!pjExpectedReturnDt.equals(other.pjExpectedReturnDt)) {
			return false;
		}
		if (pjFicaStatusEe == null) {
			if (other.pjFicaStatusEe != null) {
				return false;
			}
		} else if (!pjFicaStatusEe.equals(other.pjFicaStatusEe)) {
			return false;
		}
		if (pjFlsaStatus == null) {
			if (other.pjFlsaStatus != null) {
				return false;
			}
		} else if (!pjFlsaStatus.equals(other.pjFlsaStatus)) {
			return false;
		}
		if (pjForcePublish == null) {
			if (other.pjForcePublish != null) {
				return false;
			}
		} else if (!pjForcePublish.equals(other.pjForcePublish)) {
			return false;
		}
		if (pjFte == null) {
			if (other.pjFte != null) {
				return false;
			}
		} else if (!pjFte.equals(other.pjFte)) {
			return false;
		}
		if (pjFullPartTime == null) {
			if (other.pjFullPartTime != null) {
				return false;
			}
		} else if (!pjFullPartTime.equals(other.pjFullPartTime)) {
			return false;
		}
		if (pjFunctionCd == null) {
			if (other.pjFunctionCd != null) {
				return false;
			}
		} else if (!pjFunctionCd.equals(other.pjFunctionCd)) {
			return false;
		}
		if (pjGlPayType == null) {
			if (other.pjGlPayType != null) {
				return false;
			}
		} else if (!pjGlPayType.equals(other.pjGlPayType)) {
			return false;
		}
		if (pjGpAsofDtExgRt == null) {
			if (other.pjGpAsofDtExgRt != null) {
				return false;
			}
		} else if (!pjGpAsofDtExgRt.equals(other.pjGpAsofDtExgRt)) {
			return false;
		}
		if (pjGpDfltCurrttyp == null) {
			if (other.pjGpDfltCurrttyp != null) {
				return false;
			}
		} else if (!pjGpDfltCurrttyp.equals(other.pjGpDfltCurrttyp)) {
			return false;
		}
		if (pjGpDfltEligGrp == null) {
			if (other.pjGpDfltEligGrp != null) {
				return false;
			}
		} else if (!pjGpDfltEligGrp.equals(other.pjGpDfltEligGrp)) {
			return false;
		}
		if (pjGpDfltExrtdt == null) {
			if (other.pjGpDfltExrtdt != null) {
				return false;
			}
		} else if (!pjGpDfltExrtdt.equals(other.pjGpDfltExrtdt)) {
			return false;
		}
		if (pjGpEligGrp == null) {
			if (other.pjGpEligGrp != null) {
				return false;
			}
		} else if (!pjGpEligGrp.equals(other.pjGpEligGrp)) {
			return false;
		}
		if (pjGpPaygroup == null) {
			if (other.pjGpPaygroup != null) {
				return false;
			}
		} else if (!pjGpPaygroup.equals(other.pjGpPaygroup)) {
			return false;
		}
		if (pjGrade == null) {
			if (other.pjGrade != null) {
				return false;
			}
		} else if (!pjGrade.equals(other.pjGrade)) {
			return false;
		}
		if (pjGradeEntryDt == null) {
			if (other.pjGradeEntryDt != null) {
				return false;
			}
		} else if (!pjGradeEntryDt.equals(other.pjGradeEntryDt)) {
			return false;
		}
		if (pjHireDt == null) {
			if (other.pjHireDt != null) {
				return false;
			}
		} else if (!pjHireDt.equals(other.pjHireDt)) {
			return false;
		}
		if (pjHolidaySchedule == null) {
			if (other.pjHolidaySchedule != null) {
				return false;
			}
		} else if (!pjHolidaySchedule.equals(other.pjHolidaySchedule)) {
			return false;
		}
		if (pjHourlyRt == null) {
			if (other.pjHourlyRt != null) {
				return false;
			}
		} else if (!pjHourlyRt.equals(other.pjHourlyRt)) {
			return false;
		}
		if (pjHourlyRtFra == null) {
			if (other.pjHourlyRtFra != null) {
				return false;
			}
		} else if (!pjHourlyRtFra.equals(other.pjHourlyRtFra)) {
			return false;
		}
		if (pjHrStatus == null) {
			if (other.pjHrStatus != null) {
				return false;
			}
		} else if (!pjHrStatus.equals(other.pjHrStatus)) {
			return false;
		}
		if (pjInterctrWrksCncl == null) {
			if (other.pjInterctrWrksCncl != null) {
				return false;
			}
		} else if (!pjInterctrWrksCncl.equals(other.pjInterctrWrksCncl)) {
			return false;
		}
		if (pjJobDataSrcCd == null) {
			if (other.pjJobDataSrcCd != null) {
				return false;
			}
		} else if (!pjJobDataSrcCd.equals(other.pjJobDataSrcCd)) {
			return false;
		}
		if (pjJobEntryDt == null) {
			if (other.pjJobEntryDt != null) {
				return false;
			}
		} else if (!pjJobEntryDt.equals(other.pjJobEntryDt)) {
			return false;
		}
		if (pjJobIndicator == null) {
			if (other.pjJobIndicator != null) {
				return false;
			}
		} else if (!pjJobIndicator.equals(other.pjJobIndicator)) {
			return false;
		}
		if (pjJobcode == null) {
			if (other.pjJobcode != null) {
				return false;
			}
		} else if (!pjJobcode.equals(other.pjJobcode)) {
			return false;
		}
		if (pjLaborAgreement == null) {
			if (other.pjLaborAgreement != null) {
				return false;
			}
		} else if (!pjLaborAgreement.equals(other.pjLaborAgreement)) {
			return false;
		}
		if (pjLaborFacilityId == null) {
			if (other.pjLaborFacilityId != null) {
				return false;
			}
		} else if (!pjLaborFacilityId.equals(other.pjLaborFacilityId)) {
			return false;
		}
		if (pjLaborTypeGer == null) {
			if (other.pjLaborTypeGer != null) {
				return false;
			}
		} else if (!pjLaborTypeGer.equals(other.pjLaborTypeGer)) {
			return false;
		}
		if (pjLastDateWorked == null) {
			if (other.pjLastDateWorked != null) {
				return false;
			}
		} else if (!pjLastDateWorked.equals(other.pjLastDateWorked)) {
			return false;
		}
		if (pjLastHireDt == null) {
			if (other.pjLastHireDt != null) {
				return false;
			}
		} else if (!pjLastHireDt.equals(other.pjLastHireDt)) {
			return false;
		}
		if (pjLastupddttm == null) {
			if (other.pjLastupddttm != null) {
				return false;
			}
		} else if (!pjLastupddttm.equals(other.pjLastupddttm)) {
			return false;
		}
		if (pjLastupdoprid == null) {
			if (other.pjLastupdoprid != null) {
				return false;
			}
		} else if (!pjLastupdoprid.equals(other.pjLastupdoprid)) {
			return false;
		}
		if (pjLayoffExemptFlag == null) {
			if (other.pjLayoffExemptFlag != null) {
				return false;
			}
		} else if (!pjLayoffExemptFlag.equals(other.pjLayoffExemptFlag)) {
			return false;
		}
		if (pjLayoffExemptRsn == null) {
			if (other.pjLayoffExemptRsn != null) {
				return false;
			}
		} else if (!pjLayoffExemptRsn.equals(other.pjLayoffExemptRsn)) {
			return false;
		}
		if (pjLbrFacEntryDt == null) {
			if (other.pjLbrFacEntryDt != null) {
				return false;
			}
		} else if (!pjLbrFacEntryDt.equals(other.pjLbrFacEntryDt)) {
			return false;
		}
		if (pjLdwOvr == null) {
			if (other.pjLdwOvr != null) {
				return false;
			}
		} else if (!pjLdwOvr.equals(other.pjLdwOvr)) {
			return false;
		}
		if (pjLocation == null) {
			if (other.pjLocation != null) {
				return false;
			}
		} else if (!pjLocation.equals(other.pjLocation)) {
			return false;
		}
		if (pjLstAsgnStartDt == null) {
			if (other.pjLstAsgnStartDt != null) {
				return false;
			}
		} else if (!pjLstAsgnStartDt.equals(other.pjLstAsgnStartDt)) {
			return false;
		}
		if (pjLumpSumPay == null) {
			if (other.pjLumpSumPay != null) {
				return false;
			}
		} else if (!pjLumpSumPay.equals(other.pjLumpSumPay)) {
			return false;
		}
		if (pjMainApptNumJpn == null) {
			if (other.pjMainApptNumJpn != null) {
				return false;
			}
		} else if (!pjMainApptNumJpn.equals(other.pjMainApptNumJpn)) {
			return false;
		}
		if (pjMatriculaNbr == null) {
			if (other.pjMatriculaNbr != null) {
				return false;
			}
		} else if (!pjMatriculaNbr.equals(other.pjMatriculaNbr)) {
			return false;
		}
		if (pjMonthlyRt == null) {
			if (other.pjMonthlyRt != null) {
				return false;
			}
		} else if (!pjMonthlyRt.equals(other.pjMonthlyRt)) {
			return false;
		}
		if (pjOfficerCd == null) {
			if (other.pjOfficerCd != null) {
				return false;
			}
		} else if (!pjOfficerCd.equals(other.pjOfficerCd)) {
			return false;
		}
		if (pjPaidFte == null) {
			if (other.pjPaidFte != null) {
				return false;
			}
		} else if (!pjPaidFte.equals(other.pjPaidFte)) {
			return false;
		}
		if (pjPaidHours == null) {
			if (other.pjPaidHours != null) {
				return false;
			}
		} else if (!pjPaidHours.equals(other.pjPaidHours)) {
			return false;
		}
		if (pjPaidHrsFrequency == null) {
			if (other.pjPaidHrsFrequency != null) {
				return false;
			}
		} else if (!pjPaidHrsFrequency.equals(other.pjPaidHrsFrequency)) {
			return false;
		}
		if (pjPaySystemFlg == null) {
			if (other.pjPaySystemFlg != null) {
				return false;
			}
		} else if (!pjPaySystemFlg.equals(other.pjPaySystemFlg)) {
			return false;
		}
		if (pjPayUnionFee == null) {
			if (other.pjPayUnionFee != null) {
				return false;
			}
		} else if (!pjPayUnionFee.equals(other.pjPayUnionFee)) {
			return false;
		}
		if (pjPaygroup == null) {
			if (other.pjPaygroup != null) {
				return false;
			}
		} else if (!pjPaygroup.equals(other.pjPaygroup)) {
			return false;
		}
		if (pjPerOrg == null) {
			if (other.pjPerOrg != null) {
				return false;
			}
		} else if (!pjPerOrg.equals(other.pjPerOrg)) {
			return false;
		}
		if (pjPerformGroupGer == null) {
			if (other.pjPerformGroupGer != null) {
				return false;
			}
		} else if (!pjPerformGroupGer.equals(other.pjPerformGroupGer)) {
			return false;
		}
		if (pjPoiType == null) {
			if (other.pjPoiType != null) {
				return false;
			}
		} else if (!pjPoiType.equals(other.pjPoiType)) {
			return false;
		}
		if (pjPositionEntryDt == null) {
			if (other.pjPositionEntryDt != null) {
				return false;
			}
		} else if (!pjPositionEntryDt.equals(other.pjPositionEntryDt)) {
			return false;
		}
		if (pjPositionNbr == null) {
			if (other.pjPositionNbr != null) {
				return false;
			}
		} else if (!pjPositionNbr.equals(other.pjPositionNbr)) {
			return false;
		}
		if (pjPositionOverride == null) {
			if (other.pjPositionOverride != null) {
				return false;
			}
		} else if (!pjPositionOverride.equals(other.pjPositionOverride)) {
			return false;
		}
		if (pjPosnChangeRecord == null) {
			if (other.pjPosnChangeRecord != null) {
				return false;
			}
		} else if (!pjPosnChangeRecord.equals(other.pjPosnChangeRecord)) {
			return false;
		}
		if (pjProrateCntAmt == null) {
			if (other.pjProrateCntAmt != null) {
				return false;
			}
		} else if (!pjProrateCntAmt.equals(other.pjProrateCntAmt)) {
			return false;
		}
		if (pjRegRegion == null) {
			if (other.pjRegRegion != null) {
				return false;
			}
		} else if (!pjRegRegion.equals(other.pjRegRegion)) {
			return false;
		}
		if (pjRegTemp == null) {
			if (other.pjRegTemp != null) {
				return false;
			}
		} else if (!pjRegTemp.equals(other.pjRegTemp)) {
			return false;
		}
		if (pjReportsTo == null) {
			if (other.pjReportsTo != null) {
				return false;
			}
		} else if (!pjReportsTo.equals(other.pjReportsTo)) {
			return false;
		}
		if (pjSalAdminPlan == null) {
			if (other.pjSalAdminPlan != null) {
				return false;
			}
		} else if (!pjSalAdminPlan.equals(other.pjSalAdminPlan)) {
			return false;
		}
		if (pjSetidDept == null) {
			if (other.pjSetidDept != null) {
				return false;
			}
		} else if (!pjSetidDept.equals(other.pjSetidDept)) {
			return false;
		}
		if (pjSetidEmplClass == null) {
			if (other.pjSetidEmplClass != null) {
				return false;
			}
		} else if (!pjSetidEmplClass.equals(other.pjSetidEmplClass)) {
			return false;
		}
		if (pjSetidJobcode == null) {
			if (other.pjSetidJobcode != null) {
				return false;
			}
		} else if (!pjSetidJobcode.equals(other.pjSetidJobcode)) {
			return false;
		}
		if (pjSetidLbrAgrmnt == null) {
			if (other.pjSetidLbrAgrmnt != null) {
				return false;
			}
		} else if (!pjSetidLbrAgrmnt.equals(other.pjSetidLbrAgrmnt)) {
			return false;
		}
		if (pjSetidLocation == null) {
			if (other.pjSetidLocation != null) {
				return false;
			}
		} else if (!pjSetidLocation.equals(other.pjSetidLocation)) {
			return false;
		}
		if (pjSetidSalary == null) {
			if (other.pjSetidSalary != null) {
				return false;
			}
		} else if (!pjSetidSalary.equals(other.pjSetidSalary)) {
			return false;
		}
		if (pjSetidSupvLvl == null) {
			if (other.pjSetidSupvLvl != null) {
				return false;
			}
		} else if (!pjSetidSupvLvl.equals(other.pjSetidSupvLvl)) {
			return false;
		}
		if (pjShift == null) {
			if (other.pjShift != null) {
				return false;
			}
		} else if (!pjShift.equals(other.pjShift)) {
			return false;
		}
		if (pjShiftFactor == null) {
			if (other.pjShiftFactor != null) {
				return false;
			}
		} else if (!pjShiftFactor.equals(other.pjShiftFactor)) {
			return false;
		}
		if (pjShiftRt == null) {
			if (other.pjShiftRt != null) {
				return false;
			}
		} else if (!pjShiftRt.equals(other.pjShiftRt)) {
			return false;
		}
		if (pjSocSecRiskCode == null) {
			if (other.pjSocSecRiskCode != null) {
				return false;
			}
		} else if (!pjSocSecRiskCode.equals(other.pjSocSecRiskCode)) {
			return false;
		}
		if (pjSpkCommIdGer == null) {
			if (other.pjSpkCommIdGer != null) {
				return false;
			}
		} else if (!pjSpkCommIdGer.equals(other.pjSpkCommIdGer)) {
			return false;
		}
		if (pjStdHours == null) {
			if (other.pjStdHours != null) {
				return false;
			}
		} else if (!pjStdHours.equals(other.pjStdHours)) {
			return false;
		}
		if (pjStdHrsFrequency == null) {
			if (other.pjStdHrsFrequency != null) {
				return false;
			}
		} else if (!pjStdHrsFrequency.equals(other.pjStdHrsFrequency)) {
			return false;
		}
		if (pjStep == null) {
			if (other.pjStep != null) {
				return false;
			}
		} else if (!pjStep.equals(other.pjStep)) {
			return false;
		}
		if (pjStepEntryDt == null) {
			if (other.pjStepEntryDt != null) {
				return false;
			}
		} else if (!pjStepEntryDt.equals(other.pjStepEntryDt)) {
			return false;
		}
		if (pjSupervisorId == null) {
			if (other.pjSupervisorId != null) {
				return false;
			}
		} else if (!pjSupervisorId.equals(other.pjSupervisorId)) {
			return false;
		}
		if (pjSupvLvlId == null) {
			if (other.pjSupvLvlId != null) {
				return false;
			}
		} else if (!pjSupvLvlId.equals(other.pjSupvLvlId)) {
			return false;
		}
		if (pjTariffAreaGer == null) {
			if (other.pjTariffAreaGer != null) {
				return false;
			}
		} else if (!pjTariffAreaGer.equals(other.pjTariffAreaGer)) {
			return false;
		}
		if (pjTariffGer == null) {
			if (other.pjTariffGer != null) {
				return false;
			}
		} else if (!pjTariffGer.equals(other.pjTariffGer)) {
			return false;
		}
		if (pjTaxLocationCd == null) {
			if (other.pjTaxLocationCd != null) {
				return false;
			}
		} else if (!pjTaxLocationCd.equals(other.pjTaxLocationCd)) {
			return false;
		}
		if (pjTerminationDt == null) {
			if (other.pjTerminationDt != null) {
				return false;
			}
		} else if (!pjTerminationDt.equals(other.pjTerminationDt)) {
			return false;
		}
		if (pjUnionCd == null) {
			if (other.pjUnionCd != null) {
				return false;
			}
		} else if (!pjUnionCd.equals(other.pjUnionCd)) {
			return false;
		}
		if (pjUnionFeeAmount == null) {
			if (other.pjUnionFeeAmount != null) {
				return false;
			}
		} else if (!pjUnionFeeAmount.equals(other.pjUnionFeeAmount)) {
			return false;
		}
		if (pjUnionFeeEndDt == null) {
			if (other.pjUnionFeeEndDt != null) {
				return false;
			}
		} else if (!pjUnionFeeEndDt.equals(other.pjUnionFeeEndDt)) {
			return false;
		}
		if (pjUnionFeeStartDt == null) {
			if (other.pjUnionFeeStartDt != null) {
				return false;
			}
		} else if (!pjUnionFeeStartDt.equals(other.pjUnionFeeStartDt)) {
			return false;
		}
		if (pjUnionFullPart == null) {
			if (other.pjUnionFullPart != null) {
				return false;
			}
		} else if (!pjUnionFullPart.equals(other.pjUnionFullPart)) {
			return false;
		}
		if (pjUnionPos == null) {
			if (other.pjUnionPos != null) {
				return false;
			}
		} else if (!pjUnionPos.equals(other.pjUnionPos)) {
			return false;
		}
		if (pjUnionSeniorityDt == null) {
			if (other.pjUnionSeniorityDt != null) {
				return false;
			}
		} else if (!pjUnionSeniorityDt.equals(other.pjUnionSeniorityDt)) {
			return false;
		}
		if (pjValue1Fra == null) {
			if (other.pjValue1Fra != null) {
				return false;
			}
		} else if (!pjValue1Fra.equals(other.pjValue1Fra)) {
			return false;
		}
		if (pjValue2Fra == null) {
			if (other.pjValue2Fra != null) {
				return false;
			}
		} else if (!pjValue2Fra.equals(other.pjValue2Fra)) {
			return false;
		}
		if (pjValue3Fra == null) {
			if (other.pjValue3Fra != null) {
				return false;
			}
		} else if (!pjValue3Fra.equals(other.pjValue3Fra)) {
			return false;
		}
		if (pjValue4Fra == null) {
			if (other.pjValue4Fra != null) {
				return false;
			}
		} else if (!pjValue4Fra.equals(other.pjValue4Fra)) {
			return false;
		}
		if (pjValue5Fra == null) {
			if (other.pjValue5Fra != null) {
				return false;
			}
		} else if (!pjValue5Fra.equals(other.pjValue5Fra)) {
			return false;
		}
		if (pjWorkDayHours == null) {
			if (other.pjWorkDayHours != null) {
				return false;
			}
		} else if (!pjWorkDayHours.equals(other.pjWorkDayHours)) {
			return false;
		}
		if (pjWppStopFlag == null) {
			if (other.pjWppStopFlag != null) {
				return false;
			}
		} else if (!pjWppStopFlag.equals(other.pjWppStopFlag)) {
			return false;
		}
		if (pjWrksCnclFunction == null) {
			if (other.pjWrksCnclFunction != null) {
				return false;
			}
		} else if (!pjWrksCnclFunction.equals(other.pjWrksCnclFunction)) {
			return false;
		}
		if (pjWrksCnclRoleChe == null) {
			if (other.pjWrksCnclRoleChe != null) {
				return false;
			}
		} else if (!pjWrksCnclRoleChe.equals(other.pjWrksCnclRoleChe)) {
			return false;
		}
		if (psPersonRef == null) {
			if (other.psPersonRef != null) {
				return false;
			}
		} else if (!psPersonRef.equals(other.psPersonRef)) {
			return false;
		}
		return true;
	}
}