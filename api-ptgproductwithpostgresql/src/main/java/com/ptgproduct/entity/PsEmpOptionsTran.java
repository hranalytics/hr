/**
 * Program Name: PsEmpOptionsTran 
 *                                                                 
 * Program Description / functionality: This is Entity class for PsEmpOptionsTran
 *                            
 * Modules Impacted: Manage Employee Option
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer      Created             /Modified Date       Purpose
  *******************************************************************************
 * Astha&Bindu     04/01/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="ps_emp_options_trans")
@NamedQuery(name="PsEmpOptionsTran.findAll", query="SELECT p FROM PsEmpOptionsTran p")
public class PsEmpOptionsTran implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PsEmpOptionsTranPK id;

	@Column(name="eot_preferences")
	private String eotPreferences;

	@ManyToOne
	@JoinColumn(name="eot_optid", nullable = false, updatable = false, insertable = false)
	private PsEmpOptionsRef psEmpOptionsRef;

	@ManyToOne
	@JoinColumn(name="eot_emplid", nullable = false, updatable = false, insertable = false)
	private PsPersonRef psPersonRef;

	public PsEmpOptionsTran() {
	}

	public PsEmpOptionsTranPK getId() {
		return this.id;
	}

	public void setId(PsEmpOptionsTranPK id) {
		this.id = id;
	}

	public String getEotPreferences() {
		return this.eotPreferences;
	}

	public void setEotPreferences(String eotPreferences) {
		this.eotPreferences = eotPreferences;
	}

	public PsEmpOptionsRef getPsEmpOptionsRef() {
		return this.psEmpOptionsRef;
	}

	public void setPsEmpOptionsRef(PsEmpOptionsRef psEmpOptionsRef) {
		this.psEmpOptionsRef = psEmpOptionsRef;
	}

	public PsPersonRef getPsPersonRef() {
		return this.psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}
}