/**
 * Program Name: PsPersonNamesRefPK 
 *                                                                 
 * Program Description / functionality: This is id class for PsPersonNamesRef entity
 *                            
 * Modules Impacted: Person Names
 *                                                                    
 * Tables affected:  ps_person_names_ref                                                                    
 *                                                                                                         
 * Developer             Created             /Modified Date       Purpose
  *******************************************************************************
 * Pavani & Kiran       23/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.entity;

public class PsPersonNamesRefPK extends BaseEntity {
	private static final long serialVersionUID = 1L;
	private String psPersonRef;
	private java.util.Date pnEffdt;
	private String pnCountryNmFormat;
	private String pnNameType;

	public PsPersonNamesRefPK() {
	}

	public String getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(String psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	public java.util.Date getPnEffdt() {
		return this.pnEffdt;
	}

	public void setPnEffdt(java.util.Date pnEffdt) {
		this.pnEffdt = pnEffdt;
	}

	public String getPnCountryNmFormat() {
		return this.pnCountryNmFormat;
	}

	public void setPnCountryNmFormat(String pnCountryNmFormat) {
		this.pnCountryNmFormat = pnCountryNmFormat;
	}

	public String getPnNameType() {
		return this.pnNameType;
	}

	public void setPnNameType(String pnNameType) {
		this.pnNameType = pnNameType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PsPersonNamesRefPK)) {
			return false;
		}
		PsPersonNamesRefPK castOther = (PsPersonNamesRefPK) other;
		return this.psPersonRef.equals(castOther.psPersonRef) && this.pnEffdt.equals(castOther.pnEffdt)
				&& this.pnCountryNmFormat.equals(castOther.pnCountryNmFormat)
				&& this.pnNameType.equals(castOther.pnNameType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.psPersonRef.hashCode();
		hash = hash * prime + this.pnEffdt.hashCode();
		hash = hash * prime + this.pnCountryNmFormat.hashCode();
		hash = hash * prime + this.pnNameType.hashCode();

		return hash;
	}
}