/**
* Program Name: PsMyroleRefPK 
*                                                                 
* Program Description / functionality: This is entity class for person My roles
*                            
* Modules Impacted: My roles
*                                                                    
* Tables affected:  ps_myrole_ref                                                                    
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Rakesh               29/12/2016 
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.entity;

public class PsMyroleRefPK extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	private String psPersonRef;
	
	private String mrOrganization;
	
	public String getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(String psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	public String getMrOrganization() {
		return mrOrganization;
	}

	public void setMrOrganization(String mrOrganization) {
		this.mrOrganization = mrOrganization;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mrOrganization == null) ? 0 : mrOrganization.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PsMyroleRefPK other = (PsMyroleRefPK) obj;
		if (mrOrganization == null) {
			if (other.mrOrganization != null)
				return false;
		} else if ((!mrOrganization.equals(other.mrOrganization)) && (!psPersonRef.equals(other.psPersonRef)))
			return false;
		return true;
	}
}
