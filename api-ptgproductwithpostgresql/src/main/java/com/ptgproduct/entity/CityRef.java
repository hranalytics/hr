package com.ptgproduct.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="gis_city_ref")
public class CityRef extends BaseEntity{

	private static final long serialVersionUID = -4777211606019741466L;
	
	@Id
	@Column(name="ci_city_id") 
	private int cityId;
	
	
	
	@Column(name="ci_city_name") 
	private String cityeName;
	
	@Column(name="ci_desc") 
	private String description;
	

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ci_state_id")
	private StateRef stateRef;

	public CityRef() {

	}

	public CityRef(int cityId, String cityeName, String description, StateRef stateRef) {
		super();
		this.cityId = cityId;
		this.cityeName = cityeName;
		this.description = description;
		this.stateRef = stateRef;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getCityeName() {
		return cityeName;
	}

	public void setCityeName(String cityeName) {
		this.cityeName = cityeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public StateRef getStateRef() {
		return stateRef;
	}

	public void setStateRef(StateRef stateRef) {
		this.stateRef = stateRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cityId;
		result = prime * result + ((cityeName == null) ? 0 : cityeName.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((stateRef == null) ? 0 : stateRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CityRef other = (CityRef) obj;
		if (cityId != other.cityId)
			return false;
		if (cityeName == null) {
			if (other.cityeName != null)
				return false;
		} else if (!cityeName.equals(other.cityeName))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (stateRef == null) {
			if (other.stateRef != null)
				return false;
		} else if (!stateRef.equals(other.stateRef))
			return false;
		return true;
	}

	
	

	
	
	
	

}