package com.ptgproduct.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="gis_country_ref")
@NamedQuery(name = "CountryRef.findAll", query = "SELECT p FROM CountryRef p")
public class CountryRef extends BaseEntity{

	private static final long serialVersionUID = -4777211606019741466L;
	
	@Id
	@Column(name="co_country_name") 
	private String countryId;
	
	@Column(name="co_desc") 
	private String description;
	
	public CountryRef() {
		
	}


	public CountryRef(String countryId, String description
			) {

		this.countryId = countryId;
		this.description = description;

	}


	public String getCountryId() {
		return countryId;
	}


	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountryRef other = (CountryRef) obj;
		
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}
	
}

