/**
 * Program Name: PsPersonNamesRef 
 *                                                                 
 * Program Description / functionality: This is entity class for person names
 *                            
 * Modules Impacted: Person Names
 *                                                                    
 * Tables affected:  ps_person_names_ref                                                                    
 *                                                                                                         
 * Developer             Created             /Modified Date       Purpose
  *******************************************************************************
 * Pavani & Kiran       23/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "ps_person_names_ref")
@NamedQuery(name = "PsPersonNamesRef.findAll", query = "SELECT p FROM PsPersonNamesRef p")
@IdClass(PsPersonNamesRefPK.class)
public class PsPersonNamesRef extends BaseEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@Temporal(TemporalType.DATE)
	@Column(name = "pn_effdt")
	private java.util.Date pnEffdt;

	@Temporal(TemporalType.DATE)
	@Column(name = "ppd_enddt")
	private Date endDate;

	@Id
	@Column(name = "pn_country_nm_format")
	private String pnCountryNmFormat;

	@Id
	@Column(name = "pn_name_type")
	private String pnNameType;

	@Column(name = "pn_eff_status")
	private Boolean pnEffStatus;

	@Column(name = "pn_first_name")
	private String pnFirstName;

	@Column(name = "pn_first_name_srch")
	private String pnFirstNameSrch;

	@Column(name = "pn_last_name")
	private String pnLastName;

	@Column(name = "pn_last_name_pref_nld")
	private String pnLastNamePrefNld;

	@Column(name = "pn_last_name_srch")
	private String pnLastNameSrch;

	@Column(name = "pn_last_upddtm")
	private Timestamp pnLastUpddtm;

	@Column(name = "pn_last_updoprid")
	private String pnLastUpdoprid;

	@Column(name = "pn_middle_name")
	private String pnMiddleName;

	@Column(name = "pn_name")
	private String pnName;

	@Column(name = "pn_name_ac")
	private String pnNameAc;

	@Column(name = "pn_name_display")
	private String pnNameDisplay;

	@Column(name = "pn_name_display_srch")
	private String pnNameDisplaySrch;

	@Column(name = "pn_name_formal")
	private String pnNameFormal;

	@Column(name = "pn_name_initials")
	private String pnNameInitials;

	@Column(name = "pn_name_prefix")
	private String pnNamePrefix;

	@Column(name = "pn_name_royal_prefix")
	private String pnNameRoyalPrefix;

	@Column(name = "pn_name_royal_suffix")
	private String pnNameRoyalSuffix;

	@Column(name = "pn_name_suffix")
	private String pnNameSuffix;

	@Column(name = "pn_name_tittle")
	private String pnNameTittle;

	@Column(name = "pn_partner_last_name")
	private String pnPartnerLastName;

	@Column(name = "pn_partner_roy_prefix")
	private String pnPartnerRoyPrefix;

	@Column(name = "pn_pref_first_name")
	private String pnPrefFirstName;

	@Column(name = "pn_second_last_name")
	private String pnSecondLastName;

	@Column(name = "pn_second_last_srch")
	private String pnSecondLastSrch;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pn_emplid")
	@JsonBackReference("psPersonRef-psPersonNamesRefs")
	private PsPersonRef psPersonRef;

	public PsPersonNamesRef() {
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public java.util.Date getPnEffdt() {
		return pnEffdt;
	}

	public void setPnEffdt(java.util.Date pnEffdt) {
		this.pnEffdt = pnEffdt;
	}

	public String getPnCountryNmFormat() {
		return pnCountryNmFormat;
	}

	public void setPnCountryNmFormat(String pnCountryNmFormat) {
		this.pnCountryNmFormat = pnCountryNmFormat;
	}

	public String getPnNameType() {
		return pnNameType;
	}

	public void setPnNameType(String pnNameType) {
		this.pnNameType = pnNameType;
	}

	public Boolean getPnEffStatus() {
		return this.pnEffStatus;
	}

	public void setPnEffStatus(Boolean pnEffStatus) {
		this.pnEffStatus = pnEffStatus;
	}

	public String getPnFirstName() {
		return this.pnFirstName;
	}

	public void setPnFirstName(String pnFirstName) {
		this.pnFirstName = pnFirstName;
	}

	public String getPnFirstNameSrch() {
		return this.pnFirstNameSrch;
	}

	public void setPnFirstNameSrch(String pnFirstNameSrch) {
		this.pnFirstNameSrch = pnFirstNameSrch;
	}

	public String getPnLastName() {
		return this.pnLastName;
	}

	public void setPnLastName(String pnLastName) {
		this.pnLastName = pnLastName;
	}

	public String getPnLastNamePrefNld() {
		return this.pnLastNamePrefNld;
	}

	public void setPnLastNamePrefNld(String pnLastNamePrefNld) {
		this.pnLastNamePrefNld = pnLastNamePrefNld;
	}

	public String getPnLastNameSrch() {
		return this.pnLastNameSrch;
	}

	public void setPnLastNameSrch(String pnLastNameSrch) {
		this.pnLastNameSrch = pnLastNameSrch;
	}

	public Timestamp getPnLastUpddtm() {
		return this.pnLastUpddtm;
	}

	public void setPnLastUpddtm(Timestamp pnLastUpddtm) {
		this.pnLastUpddtm = pnLastUpddtm;
	}

	public String getPnLastUpdoprid() {
		return this.pnLastUpdoprid;
	}

	public void setPnLastUpdoprid(String pnLastUpdoprid) {
		this.pnLastUpdoprid = pnLastUpdoprid;
	}

	public String getPnMiddleName() {
		return this.pnMiddleName;
	}

	public void setPnMiddleName(String pnMiddleName) {
		this.pnMiddleName = pnMiddleName;
	}

	public String getPnName() {
		return this.pnName;
	}

	public void setPnName(String pnName) {
		this.pnName = pnName;
	}

	public String getPnNameAc() {
		return this.pnNameAc;
	}

	public void setPnNameAc(String pnNameAc) {
		this.pnNameAc = pnNameAc;
	}

	public String getPnNameDisplay() {
		return this.pnNameDisplay;
	}

	public void setPnNameDisplay(String pnNameDisplay) {
		this.pnNameDisplay = pnNameDisplay;
	}

	public String getPnNameDisplaySrch() {
		return this.pnNameDisplaySrch;
	}

	public void setPnNameDisplaySrch(String pnNameDisplaySrch) {
		this.pnNameDisplaySrch = pnNameDisplaySrch;
	}

	public String getPnNameFormal() {
		return this.pnNameFormal;
	}

	public void setPnNameFormal(String pnNameFormal) {
		this.pnNameFormal = pnNameFormal;
	}

	public String getPnNameInitials() {
		return this.pnNameInitials;
	}

	public void setPnNameInitials(String pnNameInitials) {
		this.pnNameInitials = pnNameInitials;
	}

	public String getPnNamePrefix() {
		return this.pnNamePrefix;
	}

	public void setPnNamePrefix(String pnNamePrefix) {
		this.pnNamePrefix = pnNamePrefix;
	}

	public String getPnNameRoyalPrefix() {
		return this.pnNameRoyalPrefix;
	}

	public void setPnNameRoyalPrefix(String pnNameRoyalPrefix) {
		this.pnNameRoyalPrefix = pnNameRoyalPrefix;
	}

	public String getPnNameRoyalSuffix() {
		return this.pnNameRoyalSuffix;
	}

	public void setPnNameRoyalSuffix(String pnNameRoyalSuffix) {
		this.pnNameRoyalSuffix = pnNameRoyalSuffix;
	}

	public String getPnNameSuffix() {
		return this.pnNameSuffix;
	}

	public void setPnNameSuffix(String pnNameSuffix) {
		this.pnNameSuffix = pnNameSuffix;
	}

	public String getPnNameTittle() {
		return this.pnNameTittle;
	}

	public void setPnNameTittle(String pnNameTittle) {
		this.pnNameTittle = pnNameTittle;
	}

	public String getPnPartnerLastName() {
		return this.pnPartnerLastName;
	}

	public void setPnPartnerLastName(String pnPartnerLastName) {
		this.pnPartnerLastName = pnPartnerLastName;
	}

	public String getPnPartnerRoyPrefix() {
		return this.pnPartnerRoyPrefix;
	}

	public void setPnPartnerRoyPrefix(String pnPartnerRoyPrefix) {
		this.pnPartnerRoyPrefix = pnPartnerRoyPrefix;
	}

	public String getPnPrefFirstName() {
		return this.pnPrefFirstName;
	}

	public void setPnPrefFirstName(String pnPrefFirstName) {
		this.pnPrefFirstName = pnPrefFirstName;
	}

	public String getPnSecondLastName() {
		return this.pnSecondLastName;
	}

	public void setPnSecondLastName(String pnSecondLastName) {
		this.pnSecondLastName = pnSecondLastName;
	}

	public String getPnSecondLastSrch() {
		return this.pnSecondLastSrch;
	}

	public void setPnSecondLastSrch(String pnSecondLastSrch) {
		this.pnSecondLastSrch = pnSecondLastSrch;
	}

	public PsPersonRef getPsPersonRef() {
		return this.psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pnCountryNmFormat == null) ? 0 : pnCountryNmFormat.hashCode());
		result = prime * result + ((pnEffStatus == null) ? 0 : pnEffStatus.hashCode());
		result = prime * result + ((pnEffdt == null) ? 0 : pnEffdt.hashCode());
		result = prime * result + ((pnFirstName == null) ? 0 : pnFirstName.hashCode());
		result = prime * result + ((pnFirstNameSrch == null) ? 0 : pnFirstNameSrch.hashCode());
		result = prime * result + ((pnLastName == null) ? 0 : pnLastName.hashCode());
		result = prime * result + ((pnLastNamePrefNld == null) ? 0 : pnLastNamePrefNld.hashCode());
		result = prime * result + ((pnLastNameSrch == null) ? 0 : pnLastNameSrch.hashCode());
		result = prime * result + ((pnLastUpddtm == null) ? 0 : pnLastUpddtm.hashCode());
		result = prime * result + ((pnLastUpdoprid == null) ? 0 : pnLastUpdoprid.hashCode());
		result = prime * result + ((pnMiddleName == null) ? 0 : pnMiddleName.hashCode());
		result = prime * result + ((pnName == null) ? 0 : pnName.hashCode());
		result = prime * result + ((pnNameAc == null) ? 0 : pnNameAc.hashCode());
		result = prime * result + ((pnNameDisplay == null) ? 0 : pnNameDisplay.hashCode());
		result = prime * result + ((pnNameDisplaySrch == null) ? 0 : pnNameDisplaySrch.hashCode());
		result = prime * result + ((pnNameFormal == null) ? 0 : pnNameFormal.hashCode());
		result = prime * result + ((pnNameInitials == null) ? 0 : pnNameInitials.hashCode());
		result = prime * result + ((pnNamePrefix == null) ? 0 : pnNamePrefix.hashCode());
		result = prime * result + ((pnNameRoyalPrefix == null) ? 0 : pnNameRoyalPrefix.hashCode());
		result = prime * result + ((pnNameRoyalSuffix == null) ? 0 : pnNameRoyalSuffix.hashCode());
		result = prime * result + ((pnNameSuffix == null) ? 0 : pnNameSuffix.hashCode());
		result = prime * result + ((pnNameTittle == null) ? 0 : pnNameTittle.hashCode());
		result = prime * result + ((pnNameType == null) ? 0 : pnNameType.hashCode());
		result = prime * result + ((pnPartnerLastName == null) ? 0 : pnPartnerLastName.hashCode());
		result = prime * result + ((pnPartnerRoyPrefix == null) ? 0 : pnPartnerRoyPrefix.hashCode());
		result = prime * result + ((pnPrefFirstName == null) ? 0 : pnPrefFirstName.hashCode());
		result = prime * result + ((pnSecondLastName == null) ? 0 : pnSecondLastName.hashCode());
		result = prime * result + ((pnSecondLastSrch == null) ? 0 : pnSecondLastSrch.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PsPersonNamesRef)) {
			return false;
		}
		PsPersonNamesRef other = (PsPersonNamesRef) obj;
		if (pnCountryNmFormat == null) {
			if (other.pnCountryNmFormat != null) {
				return false;
			}
		} else if (!pnCountryNmFormat.equals(other.pnCountryNmFormat)) {
			return false;
		}
		if (pnEffStatus == null) {
			if (other.pnEffStatus != null) {
				return false;
			}
		} else if (!pnEffStatus.equals(other.pnEffStatus)) {
			return false;
		}
		if (pnEffdt == null) {
			if (other.pnEffdt != null) {
				return false;
			}
		} else if (!pnEffdt.equals(other.pnEffdt)) {
			return false;
		}
		if (pnFirstName == null) {
			if (other.pnFirstName != null) {
				return false;
			}
		} else if (!pnFirstName.equals(other.pnFirstName)) {
			return false;
		}
		if (pnFirstNameSrch == null) {
			if (other.pnFirstNameSrch != null) {
				return false;
			}
		} else if (!pnFirstNameSrch.equals(other.pnFirstNameSrch)) {
			return false;
		}
		if (pnLastName == null) {
			if (other.pnLastName != null) {
				return false;
			}
		} else if (!pnLastName.equals(other.pnLastName)) {
			return false;
		}
		if (pnLastNamePrefNld == null) {
			if (other.pnLastNamePrefNld != null) {
				return false;
			}
		} else if (!pnLastNamePrefNld.equals(other.pnLastNamePrefNld)) {
			return false;
		}
		if (pnLastNameSrch == null) {
			if (other.pnLastNameSrch != null) {
				return false;
			}
		} else if (!pnLastNameSrch.equals(other.pnLastNameSrch)) {
			return false;
		}
		if (pnLastUpddtm == null) {
			if (other.pnLastUpddtm != null) {
				return false;
			}
		} else if (!pnLastUpddtm.equals(other.pnLastUpddtm)) {
			return false;
		}
		if (pnLastUpdoprid == null) {
			if (other.pnLastUpdoprid != null) {
				return false;
			}
		} else if (!pnLastUpdoprid.equals(other.pnLastUpdoprid)) {
			return false;
		}
		if (pnMiddleName == null) {
			if (other.pnMiddleName != null) {
				return false;
			}
		} else if (!pnMiddleName.equals(other.pnMiddleName)) {
			return false;
		}
		if (pnName == null) {
			if (other.pnName != null) {
				return false;
			}
		} else if (!pnName.equals(other.pnName)) {
			return false;
		}
		if (pnNameAc == null) {
			if (other.pnNameAc != null) {
				return false;
			}
		} else if (!pnNameAc.equals(other.pnNameAc)) {
			return false;
		}
		if (pnNameDisplay == null) {
			if (other.pnNameDisplay != null) {
				return false;
			}
		} else if (!pnNameDisplay.equals(other.pnNameDisplay)) {
			return false;
		}
		if (pnNameDisplaySrch == null) {
			if (other.pnNameDisplaySrch != null) {
				return false;
			}
		} else if (!pnNameDisplaySrch.equals(other.pnNameDisplaySrch)) {
			return false;
		}
		if (pnNameFormal == null) {
			if (other.pnNameFormal != null) {
				return false;
			}
		} else if (!pnNameFormal.equals(other.pnNameFormal)) {
			return false;
		}
		if (pnNameInitials == null) {
			if (other.pnNameInitials != null) {
				return false;
			}
		} else if (!pnNameInitials.equals(other.pnNameInitials)) {
			return false;
		}
		if (pnNamePrefix == null) {
			if (other.pnNamePrefix != null) {
				return false;
			}
		} else if (!pnNamePrefix.equals(other.pnNamePrefix)) {
			return false;
		}
		if (pnNameRoyalPrefix == null) {
			if (other.pnNameRoyalPrefix != null) {
				return false;
			}
		} else if (!pnNameRoyalPrefix.equals(other.pnNameRoyalPrefix)) {
			return false;
		}
		if (pnNameRoyalSuffix == null) {
			if (other.pnNameRoyalSuffix != null) {
				return false;
			}
		} else if (!pnNameRoyalSuffix.equals(other.pnNameRoyalSuffix)) {
			return false;
		}
		if (pnNameSuffix == null) {
			if (other.pnNameSuffix != null) {
				return false;
			}
		} else if (!pnNameSuffix.equals(other.pnNameSuffix)) {
			return false;
		}
		if (pnNameTittle == null) {
			if (other.pnNameTittle != null) {
				return false;
			}
		} else if (!pnNameTittle.equals(other.pnNameTittle)) {
			return false;
		}
		if (pnNameType == null) {
			if (other.pnNameType != null) {
				return false;
			}
		} else if (!pnNameType.equals(other.pnNameType)) {
			return false;
		}
		if (pnPartnerLastName == null) {
			if (other.pnPartnerLastName != null) {
				return false;
			}
		} else if (!pnPartnerLastName.equals(other.pnPartnerLastName)) {
			return false;
		}
		if (pnPartnerRoyPrefix == null) {
			if (other.pnPartnerRoyPrefix != null) {
				return false;
			}
		} else if (!pnPartnerRoyPrefix.equals(other.pnPartnerRoyPrefix)) {
			return false;
		}
		if (pnPrefFirstName == null) {
			if (other.pnPrefFirstName != null) {
				return false;
			}
		} else if (!pnPrefFirstName.equals(other.pnPrefFirstName)) {
			return false;
		}
		if (pnSecondLastName == null) {
			if (other.pnSecondLastName != null) {
				return false;
			}
		} else if (!pnSecondLastName.equals(other.pnSecondLastName)) {
			return false;
		}
		if (pnSecondLastSrch == null) {
			if (other.pnSecondLastSrch != null) {
				return false;
			}
		} else if (!pnSecondLastSrch.equals(other.pnSecondLastSrch)) {
			return false;
		}
		if (psPersonRef == null) {
			if (other.psPersonRef != null) {
				return false;
			}
		} else if (!psPersonRef.equals(other.psPersonRef)) {
			return false;
		}
		return true;
	}

}