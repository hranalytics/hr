/**
* Program Name: PsPersonNidRefPK 
*                                                                 
* Program Description / functionality: This is entity class for person national identity
*                            
* Modules Impacted: Identify
*                                                                    
* Tables affected:  ps_person_nid_ref                                                                    
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Bindu                 23/12/2016 
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.entity;

public class PsPersonNidRefPK extends BaseEntity {
	private static final long serialVersionUID = 1L;

	private String psPersonRef;
	private String pniCountry;
	private String pniNationalIdType;

	public PsPersonNidRefPK() {
	}

	public String getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(String psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	public String getPniCountry() {
		return this.pniCountry;
	}

	public void setPniCountry(String pniCountry) {
		this.pniCountry = pniCountry;
	}

	public String getPniNationalIdType() {
		return this.pniNationalIdType;
	}

	public void setPniNationalIdType(String pniNationalIdType) {
		this.pniNationalIdType = pniNationalIdType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PsPersonNidRefPK)) {
			return false;
		}
		PsPersonNidRefPK castOther = (PsPersonNidRefPK) other;
		return this.psPersonRef.equals(castOther.psPersonRef) && this.pniCountry.equals(castOther.pniCountry)
				&& this.pniNationalIdType.equals(castOther.pniNationalIdType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.psPersonRef.hashCode();
		hash = hash * prime + this.pniCountry.hashCode();
		hash = hash * prime + this.pniNationalIdType.hashCode();
		return hash;
	}
}