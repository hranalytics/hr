/**
* Program Name: PsMyroleRef 
*                                                                 
* Program Description / functionality: This is entity class for person My roles
*                            
* Modules Impacted: My roles
*                                                                    
* Tables affected:  ps_myrole_ref                                                                    
*                                                                                                         
* Developer             Created             /Modified Date       Purpose
*****************************************************************************
* Rakesh               29/12/2016 
* 
* * Associated Defects Raised : 
*
*/
package com.ptgproduct.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="ps_myrole_ref")
@NamedQuery(name="PsMyroleRef.findAll", query="SELECT p FROM PsMyroleRef p")
public class PsMyroleRef implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="mr_dept")
	private String mrDept;

	@Column(name="mr_location")
	private String mrLocation;

	@Id
	@Column(name="mr_organization")
	private String mrOrganization;

	@Column(name="mr_role_desc")
	private String mrRoleDesc;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mr_emplid")
	private PsPersonRef psPersonRef;

	public PsPersonRef getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	public PsMyroleRef() {
	}

	public String getMrDept() {
		return this.mrDept;
	}

	public void setMrDept(String mrDept) {
		this.mrDept = mrDept;
	}

	public String getMrLocation() {
		return this.mrLocation;
	}

	public void setMrLocation(String mrLocation) {
		this.mrLocation = mrLocation;
	}

	public String getMrOrganization() {
		return this.mrOrganization;
	}

	public void setMrOrganization(String mrOrganization) {
		this.mrOrganization = mrOrganization;
	}

	public String getMrRoleDesc() {
		return this.mrRoleDesc;
	}

	public void setMrRoleDesc(String mrRoleDesc) {
		this.mrRoleDesc = mrRoleDesc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mrDept == null) ? 0 : mrDept.hashCode());
		result = prime * result + ((mrLocation == null) ? 0 : mrLocation.hashCode());
		result = prime * result + ((mrOrganization == null) ? 0 : mrOrganization.hashCode());
		result = prime * result + ((mrRoleDesc == null) ? 0 : mrRoleDesc.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PsMyroleRef other = (PsMyroleRef) obj;
		if (mrDept == null) {
			if (other.mrDept != null)
				return false;
		} else if (!mrDept.equals(other.mrDept))
			return false;
		if (mrLocation == null) {
			if (other.mrLocation != null)
				return false;
		} else if (!mrLocation.equals(other.mrLocation))
			return false;
		if (mrOrganization == null) {
			if (other.mrOrganization != null)
				return false;
		} else if (!mrOrganization.equals(other.mrOrganization))
			return false;
		if (mrRoleDesc == null) {
			if (other.mrRoleDesc != null)
				return false;
		} else if (!mrRoleDesc.equals(other.mrRoleDesc))
			return false;
		if (psPersonRef == null) {
			if (other.psPersonRef != null)
				return false;
		} else if (!psPersonRef.equals(other.psPersonRef))
			return false;
		return true;
	}
}