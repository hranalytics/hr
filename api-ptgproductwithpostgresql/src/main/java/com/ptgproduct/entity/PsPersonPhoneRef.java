/**
 * Program Name: PsPersonPhoneRef 
 *                                                                 
 * Program Description / functionality: This is entity class for Person phone Details Services
 *                            
 * Modules Impacted: Person phone Details Services
 *                                                                    
 * Tables affected: Ps_Person_Phone_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Harish       23/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "ps_person_phone_ref")
@NamedQuery(name = "PsPersonPhoneRef.findAll", query = "SELECT p FROM PsPersonPhoneRef p")
@IdClass(PsPersonPhoneRefPK.class)
public class PsPersonPhoneRef extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "pp_phone_type")
	private String ppPhoneType;

	@Column(name = "pni_extension")
	private String pniExtension;

	@Column(name = "pp_country_code")
	private String ppCountryCode;

	@Column(name = "pp_phone")
	private String ppPhone;

	@Column(name = "pp_pref_phone_flag")
	private Boolean ppPrefPhoneFlag;

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pp_emplid")

	private PsPersonRef psPersonRef;

	public PsPersonPhoneRef() {
	}

	public String getPpPhoneType() {
		return ppPhoneType;
	}

	public void setPpPhoneType(String ppPhoneType) {
		this.ppPhoneType = ppPhoneType;
	}

	public String getPniExtension() {
		return this.pniExtension;
	}

	public void setPniExtension(String pniExtension) {
		this.pniExtension = pniExtension;
	}

	public String getPpCountryCode() {
		return this.ppCountryCode;
	}

	public void setPpCountryCode(String ppCountryCode) {
		this.ppCountryCode = ppCountryCode;
	}

	public String getPpPhone() {
		return this.ppPhone;
	}

	public void setPpPhone(String ppPhone) {
		this.ppPhone = ppPhone;
	}

	public Boolean getPpPrefPhoneFlag() {
		return this.ppPrefPhoneFlag;
	}

	public void setPpPrefPhoneFlag(Boolean ppPrefPhoneFlag) {
		this.ppPrefPhoneFlag = ppPrefPhoneFlag;
	}

	public PsPersonRef getPsPersonRef() {
		return this.psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pniExtension == null) ? 0 : pniExtension.hashCode());
		result = prime * result + ((ppCountryCode == null) ? 0 : ppCountryCode.hashCode());
		result = prime * result + ((ppPhone == null) ? 0 : ppPhone.hashCode());
		result = prime * result + ((ppPhoneType == null) ? 0 : ppPhoneType.hashCode());
		result = prime * result + ((ppPrefPhoneFlag == null) ? 0 : ppPrefPhoneFlag.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PsPersonPhoneRef)) {
			return false;
		}
		PsPersonPhoneRef other = (PsPersonPhoneRef) obj;
		if (pniExtension == null) {
			if (other.pniExtension != null) {
				return false;
			}
		} else if (!pniExtension.equals(other.pniExtension)) {
			return false;
		}
		if (ppCountryCode == null) {
			if (other.ppCountryCode != null) {
				return false;
			}
		} else if (!ppCountryCode.equals(other.ppCountryCode)) {
			return false;
		}
		if (ppPhone == null) {
			if (other.ppPhone != null) {
				return false;
			}
		} else if (!ppPhone.equals(other.ppPhone)) {
			return false;
		}
		if (ppPhoneType == null) {
			if (other.ppPhoneType != null) {
				return false;
			}
		} else if (!ppPhoneType.equals(other.ppPhoneType)) {
			return false;
		}
		if (ppPrefPhoneFlag == null) {
			if (other.ppPrefPhoneFlag != null) {
				return false;
			}
		} else if (!ppPrefPhoneFlag.equals(other.ppPrefPhoneFlag)) {
			return false;
		}
		if (psPersonRef == null) {
			if (other.psPersonRef != null) {
				return false;
			}
		} else if (!psPersonRef.equals(other.psPersonRef)) {
			return false;
		}
		return true;
	}

}