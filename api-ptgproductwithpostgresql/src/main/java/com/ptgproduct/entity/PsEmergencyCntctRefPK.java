/**
 * Program Name: PsEmergencyCntctRefPK 
 *                                                                 
 * Program Description / functionality: This is composit Id class for PsEmergencyCntctRef
 *                            
 * Modules Impacted: Emergency Contact Details Services
 *                                                                    
 * Tables affected: ps_emergency_cntct_ref                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Harish      28/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */
package com.ptgproduct.entity;

public class PsEmergencyCntctRefPK extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String psPersonRef;

	private String pecContactName;

	private String pecRelationship;

	public String getPecContactName() {
		return pecContactName;
	}

	public void setPecContactName(String pecContactName) {
		this.pecContactName = pecContactName;
	}

	public String getPecRelationship() {
		return pecRelationship;
	}

	public void setPecRelationship(String pecRelationship) {
		this.pecRelationship = pecRelationship;
	}

	public String getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(String psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pecContactName == null) ? 0 : pecContactName.hashCode());
		result = prime * result + ((pecRelationship == null) ? 0 : pecRelationship.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PsEmergencyCntctRefPK other = (PsEmergencyCntctRefPK) obj;
		if (pecContactName == null) {
			if (other.pecContactName != null)
				return false;
		} else if (!pecContactName.equals(other.pecContactName))
			return false;
		if (pecRelationship == null) {
			if (other.pecRelationship != null)
				return false;
		} else if (!pecRelationship.equals(other.pecRelationship))
			return false;
		if (psPersonRef == null) {
			if (other.psPersonRef != null)
				return false;
		} else if (!psPersonRef.equals(other.psPersonRef))
			return false;
		return true;
	}

}
