/**
 * Program Name: PsEmpOptionsRef 
 *                                                                 
 * Program Description / functionality: This is Entity class for PsEmpOptionsRef
 *                            
 * Modules Impacted: Manage Employee Option
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer       Created             /Modified Date       Purpose
  *******************************************************************************
 * Astha&Bindu     04/01/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="ps_emp_options_ref")
@NamedQuery(name="PsEmpOptionsRef.findAll", query="SELECT p FROM PsEmpOptionsRef p")
public class PsEmpOptionsRef implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="eo_op_id")
	private int eoOpId;

	@Column(name="eo_optiondetails")
	private String eoOptiondetails;

	@Column(name="eo_optionsname")
	private String eoOptionsname;

	@Column(name="eo_preferences")
	private String eoPreferences;

	@OneToMany(mappedBy="psEmpOptionsRef")
	private List<PsEmpOptionsTran> psEmpOptionsTrans;

	public PsEmpOptionsRef() {
	}

	public int getEoOpId() {
		return this.eoOpId;
	}

	public void setEoOpId(int eoOpId) {
		this.eoOpId = eoOpId;
	}

	public String getEoOptiondetails() {
		return this.eoOptiondetails;
	}

	public void setEoOptiondetails(String eoOptiondetails) {
		this.eoOptiondetails = eoOptiondetails;
	}

	public String getEoOptionsname() {
		return this.eoOptionsname;
	}

	public void setEoOptionsname(String eoOptionsname) {
		this.eoOptionsname = eoOptionsname;
	}

	public String getEoPreferences() {
		return this.eoPreferences;
	}

	public void setEoPreferences(String eoPreferences) {
		this.eoPreferences = eoPreferences;
	}

	public List<PsEmpOptionsTran> getPsEmpOptionsTrans() {
		return this.psEmpOptionsTrans;
	}

	public void setPsEmpOptionsTrans(List<PsEmpOptionsTran> psEmpOptionsTrans) {
		this.psEmpOptionsTrans = psEmpOptionsTrans;
	}

	public PsEmpOptionsTran addPsEmpOptionsTran(PsEmpOptionsTran psEmpOptionsTran) {
		getPsEmpOptionsTrans().add(psEmpOptionsTran);
		psEmpOptionsTran.setPsEmpOptionsRef(this);
		return psEmpOptionsTran;
	}

	public PsEmpOptionsTran removePsEmpOptionsTran(PsEmpOptionsTran psEmpOptionsTran) {
		getPsEmpOptionsTrans().remove(psEmpOptionsTran);
		psEmpOptionsTran.setPsEmpOptionsRef(null);
		return psEmpOptionsTran;
	}
}