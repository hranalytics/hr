package com.ptgproduct.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="ps_myproxy_ref")
@NamedQuery(name="PsMyproxyRef.findAll", query="SELECT p FROM PsMyproxyRef p")
public class PsMyproxyRef implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="mp_emplid")
	private String mpEmplid;

	@Temporal(TemporalType.DATE)
	@Column(name="mp_effdt")
	private Date mpEffdt;

	@Temporal(TemporalType.DATE)
	@Column(name="mp_end_date")
	private Date mpEndDate;

	@Column(name="mp_fr_name")
	private String mpFrName;

	@Column(name="mp_lst_name")
	private String mpLstName;

	public PsMyproxyRef() {
	}

	public String getMpEmplid() {
		return this.mpEmplid;
	}

	public void setMpEmplid(String mpEmplid) {
		this.mpEmplid = mpEmplid;
	}

	public Date getMpEffdt() {
		return this.mpEffdt;
	}

	public void setMpEffdt(Date mpEffdt) {
		this.mpEffdt = mpEffdt;
	}

	public Date getMpEndDate() {
		return this.mpEndDate;
	}

	public void setMpEndDate(Date mpEndDate) {
		this.mpEndDate = mpEndDate;
	}

	public String getMpFrName() {
		return this.mpFrName;
	}

	public void setMpFrName(String mpFrName) {
		this.mpFrName = mpFrName;
	}

	public String getMpLstName() {
		return this.mpLstName;
	}

	public void setMpLstName(String mpLstName) {
		this.mpLstName = mpLstName;
	}
}