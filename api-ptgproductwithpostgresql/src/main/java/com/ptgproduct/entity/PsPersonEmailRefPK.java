/**
 * Program Name: PsPersonEmailRefPK 
 *                                                                 
 * Program Description / functionality: This is an Entity class for PsPersonEmailRefPK
 *                            
 * Modules Impacted: Manage Email Details
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Astha     20/12/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.entity;

public class PsPersonEmailRefPK extends BaseEntity {

	private static final long serialVersionUID = 1L;
	private String psPersonRef;
	private String peEAddrType;

	public String getPeEAddrType() {
		return peEAddrType;
	}

	public void setPeEAddrType(String peEAddrType) {
		this.peEAddrType = peEAddrType;
	}

	public PsPersonEmailRefPK() {
	}

	public String getPsPersonRef() {
		return psPersonRef;
	}

	public void setPsPersonRef(String psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((peEAddrType == null) ? 0 : peEAddrType.hashCode());
		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PsPersonEmailRefPK other = (PsPersonEmailRefPK) obj;
		if (peEAddrType == null) {
			if (other.peEAddrType != null)
				return false;
		} else if (!peEAddrType.equals(other.peEAddrType))
			return false;
		if (psPersonRef == null) {
			if (other.psPersonRef != null)
				return false;
		} else if (!psPersonRef.equals(other.psPersonRef))
			return false;
		return true;
	}

}
