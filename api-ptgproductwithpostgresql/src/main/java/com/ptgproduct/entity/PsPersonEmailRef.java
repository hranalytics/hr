/**
 * Program Name: PsPersonEmailRef 
 *                                                                 
 * Program Description / functionality: This is interface for PsPersonEmailRef
 *                            
 * Modules Impacted: Manage Email Details
 *                                                                    
 * Tables affected:                                                                      
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Astha     20/12/2016                               
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "ps_email_addresses_ref")
@NamedQuery(name = "PsPersonEmailRef.findAll", query = "SELECT p FROM PsPersonEmailRef p")
@IdClass(PsPersonEmailRefPK.class)
public class PsPersonEmailRef extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "pe_e_addr_type")
	private String peEAddrType;
	@Column(name = "pe_email_addr")
	private String peEmailAddr;
	@Column(name = "pe_pref_email_flag")
	private boolean pePrefEmailFlag;

	// bi-directional many-to-one association to PsPersonRef
	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pe_emplid")
	// @JsonBackReference("psPersonRef-PsPersonEmailRef")
	private PsPersonRef psPersonRef;

	public PsPersonEmailRef() {
	}

	public String getPeEAddrType() {
		return peEAddrType;
	}

	public void setPeEAddrType(String peEAddrType) {
		this.peEAddrType = peEAddrType;
	}

	public String getPeEmailAddr() {
		return peEmailAddr;
	}

	public void setPeEmailAddr(String peEmailAddr) {
		this.peEmailAddr = peEmailAddr;
	}

	public boolean getPePrefEmailFlag() {
		return pePrefEmailFlag;
	}

	public void setPePrefEmailFlag(boolean pePrefEmailFlag) {
		this.pePrefEmailFlag = pePrefEmailFlag;
	}

	public PsPersonRef getPsPersonRef() {
		return this.psPersonRef;
	}

	public void setPsPersonRef(PsPersonRef psPersonRef) {
		this.psPersonRef = psPersonRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((peEAddrType == null) ? 0 : peEAddrType.hashCode());
		result = prime * result + ((peEmailAddr == null) ? 0 : peEmailAddr.hashCode());

		result = prime * result + ((psPersonRef == null) ? 0 : psPersonRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PsPersonEmailRef)) {
			return false;
		}
		PsPersonEmailRef other = (PsPersonEmailRef) obj;
		if (peEAddrType == null) {
			if (other.peEAddrType != null) {
				return false;
			}
		} else if (!peEAddrType.equals(other.peEAddrType)) {
			return false;
		}
		if (peEmailAddr == null) {
			if (other.peEmailAddr != null) {
				return false;
			}
		} else if (!peEmailAddr.equals(other.peEmailAddr)) {
			return false;
		}

		if (psPersonRef == null) {
			if (other.psPersonRef != null) {
				return false;
			}
		} else if (!psPersonRef.equals(other.psPersonRef)) {
			return false;
		}
		return true;
	}

}
