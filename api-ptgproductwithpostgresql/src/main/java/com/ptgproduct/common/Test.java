package com.ptgproduct.common;

import java.util.Date;

import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;


public class Test {
  
  
  public static void main(String[] args) {
    
    /*System.out.println(DateUtils.getEffectiveDate());
    System.out.println(new Date(114, 10, 1));
    long diff = DateUtils.getEffectiveDate().getTime() - (new Date(114, 10, 1)).getTime();
    long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    long remDays = days%365;
    long years = days/365;
    System.out.println(years);
    long months = remDays/30;
    remDays = remDays%30;
    System.out.println(months);
    System.out.println(remDays);*/
    
    System.out.println(DateUtils.getEffectiveDate());
    System.out.println(new Date(115, 6, 1));
    Interval interval = new Interval(new Date(115, 6, 1).getTime(), DateUtils.getEffectiveDate().getTime());
    Period period = interval.toPeriod().normalizedStandard(PeriodType.yearMonthDay());
    PeriodFormatter formatter = new PeriodFormatterBuilder()
                .appendYears()
                .appendSuffix(" year", " years")
                .appendSeparator(", ")
                .appendMonths()
                .appendSuffix(" month", " months")
                .appendSeparator(", ")
                .appendDays()
                .appendSuffix(" day ", " days ")
                .toFormatter();
    System.out.println(formatter.print(period));
    System.out.println(formatter.print(period));
    System.out.println(formatter.print(period));

    
  }

}
