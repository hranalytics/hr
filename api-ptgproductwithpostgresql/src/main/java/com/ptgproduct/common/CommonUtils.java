package com.ptgproduct.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import com.ptgproduct.domain.ReturnResponse;

public class CommonUtils {

	public static List<String> getErrorMessageList(List<FieldError> list) {

		List<String> errorsList = new ArrayList<String>();
		String errorDetails = null;
		for (FieldError currentFieldError : list) {
			errorDetails = currentFieldError.getDefaultMessage();
			errorDetails = errorDetails.replaceAll("field", currentFieldError.getField());
			errorsList.add(errorDetails);
		}
		return errorsList;
	}

	public static ReturnResponse getHttpStatusResponse(String message, HttpStatus status, Object res,
			String errorCode) {
		ReturnResponse returnResponse = new ReturnResponse();
		returnResponse.setStatusMessage(message);
		returnResponse.setStatusCode(status.value() + "");
		returnResponse.setData(res);
		returnResponse.setErrorCode(errorCode);
		return returnResponse;
	}

	public static String getGenderId(String pnNamePrefix) {
		String genderId = null;
		for (Map.Entry<String, String> gender : mapGender().entrySet()) {
			if (gender.getKey().equalsIgnoreCase(pnNamePrefix)) {
				genderId = gender.getValue();
			}
		}
		return genderId;
	}

	public static Map<String, String> mapGender() {
		Map<String, String> mapGender = new HashMap<String, String>();
		mapGender.put("Mr", "Male");
		mapGender.put("Mrs", "Female");
		mapGender.put("Mrs.", "Female");
		return mapGender;
	}

}
