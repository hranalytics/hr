/**
 * 
 */
package com.ptgproduct.common;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author naresh.terli
 *
 */
public class EmployeeIdGenerator implements IdentifierGenerator {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeIdGenerator.class);

	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {

		String prefix = "EMP";
		Connection connection = session.connection();
		try {

			PreparedStatement ps = connection.prepareStatement("SELECT nextval ('public.emp_id_seq') as nextval");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				long id = rs.getInt("nextval");
				String code = prefix + org.apache.commons.lang.StringUtils.leftPad("" + id, 7, '0');
				LOGGER.debug("Generated Emp Code: " + code);
				return code;
			}

		} catch (SQLException e) {
			LOGGER.debug(e.getMessage());
			throw new HibernateException("Unable to generate Emp Code Sequence");
		}
		return null;
	}
}
