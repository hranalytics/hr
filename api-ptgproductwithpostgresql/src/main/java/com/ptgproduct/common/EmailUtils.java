package com.ptgproduct.common;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.ptgproduct.domain.PersonDetails;

public class EmailUtils {
	
	public static boolean sendEmail(PersonDetails personDetails){
		boolean status =Boolean.FALSE;
		try {
			String fromEmailaddress="nsanodiya099@gmail.com";
			String username="nsanodiya099@gmail.com";
			String password="test@gmail.com";
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class",
					"javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");

			Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username,password);
					}
				});
			String textMsg="Dear  "+personDetails.getFirstName()+" "+personDetails.getLastName()+",\n\n Your Employment is created with us.\n\nEmp id: "+personDetails.getpEmplid()+"\n\nPassword : password .\n\n Please login with your credential and change your password!";
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmailaddress));
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(personDetails.getHomeEmail()));
			message.setSubject("Payroll Analytics System");
			message.setText(textMsg);

			Transport.send(message);

			status =Boolean.TRUE;

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	return status;
	}
	
	
}
