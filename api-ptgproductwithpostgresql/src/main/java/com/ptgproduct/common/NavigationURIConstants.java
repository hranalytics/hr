/**
 *  Program Name: MyCompanyURIConstants 
 *                                                                 
 * Program Description / functionality: This is constant interface for my company controller 
 *       which contains all URI link for services.       
 *                        
 * Modules Impacted: My Company
 *                                                                    
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
 *******************************************************************************
 * *******     17/12/2016 
 * 
 * * Associated Defects Raised : 
 *
 */

package com.ptgproduct.common;

public interface NavigationURIConstants {

	public static final String SECURITY = "/v1/api";
	public static final String GET_LANDING_PAGE = "/getLandingPage";
	public static final String GET_LANDING_PAGE_BY_EMPLOYEE = "/getLandingPage/{pEmplid}";
	public static final String GET_LANDING_PAGE_STATE = "/getLandingPageState/{countryId}";
	public static final String GET_LANDING_PAGE_CITY = "/getLandingPageCity/{stateId}";
	
	public static final String GET_PERSONS = "/getPersons";
	public static final String ADD_PERSON = "/addPerson";
	public static final String UPDATE_PERSON = "/updatePerson";
	public static final String REMOVE_PERSON = "/removePerson/{pEmplid}";
	public static final String GET_PERSON_BY_ID = "/getPersonById/{pEmplid}";
	public static final String UPDATE_PERSON_ADDRESS = "/updatePersonAddress/{pEmplid}";
	public static final String REMOVE_PERSON_ADDRESS = "/removePersonAddress/{pEmplid}/{paAddressType}/{paEffdt}";
	public static final String GET_PERSON_ADDRESS_BY_ID = "/getPersonAddressById/{pEmplid}";
	public static final String UPDATE_IDENTIFY_PERSONAL_DATA = "/updateIdentifyPersonalData";
	public static final String GET_IDENTIFY_PERSONAL_DATA = "/getIdentifyPersonalData/{pEmplid}";
	public static final String UPDATE_DEVICETOKEN = "/login";
	public static final String GET_PASSWORD = "/{companyId}/{employeeId}/password";
	public static final String GET_CUSTOMID = "/{companyId}/{employeeId}/custom-id";
	public static final String UPDATE_CUSTOMLOGIN = "/{companyId}/{employeeId}/custom-id";
	public static final String CREATE_CUSTOM_ID = "/{companyId}/{employeeId}/custom-id";
	public static final String UPDATE_SECRET = "/{companyId}/{employeeId}/secret";
	public static final String GET_SECRET = "/{companyId}/{employeeId}/secret";
	public static final String CREATE_OWN_QUESTION = "/{companyId}/{employeeId}/secret";
	public static final String GET_PERSON = "/getPersonsAuth";
	public static final String INSERT_COMPANY = "/addCompany";
	public static final String GET_COMPANY = "/getCompany";
	public static final String DELETE_COMPANY = "/deleteComapny/{companyId}";
	public static final String UPDATE_COMPANY = "/updateCompany";
	public static final String GET_PERSON_NAME_BY_ID = "/getPersonNameById/{pemplId}";
	public static final String UPDATE_PERSON_NAME_BY_ID = "/updatePersonNameById/{pemplId}";
	public static final String DELETE_PERSON_NAME_BY_ID = "/deletePersonNameById/{pemplId}/{effectiveDate}/{nameType}";
	public static final String ADD_PERSON_PHONE = "/addPersonPhone";
	public static final String UPDATE_PERSON_PHONE = "/updatePersonPhone";
	public static final String GET_PERSON_PHONE_BY_ID = "/getPersonPhoneById/{pEmplid}";
	public static final String REMOVE_PERSON_PHONE_BY_ID = "/removePersonPhone/{pEmplid}/{accessType}";
	public static final String ADD_PERSON_EMERGENCY_CONTACT = "/addPersonEmergencyContact";
	public static final String GET_PERSON_EMERGENCY_CONTACT_BY_ID = "/getPersonEmergencyContactById/{pEmplid}";
	public static final String REMOVE_PERSON_EMERGENCY_CONTACT_BY_ID = "/removePersonEmergencyContact/{pEmplid}/{contactName}/{relationship}";
	public static final String UPDATE_PERSON_EMERGENCY_CONTACT = "/updatePersonEmergencyContact";
	public static final String ADD_EMAIL_INFO = "/addPersonEmailInfo";
	public static final String UPDATE_EMAIL_INFO = "/updatePersonEmailInfo";
	public static final String REMOVE_EMAIL_INFO = "/removePersonEmailInfo/{pEmplid}/{accessType}";
	public static final String GET_EMAIL_INFO_BY_ID = "/getPersonEmailInfoById/{pEmplid}";
	public static final String GET_ROLES_BY_ID = "/getMyRolesById/{pEmplId}";
	public static final String ADD_PERSON_ROLES = "/addPersonRoles";
	public static final String REMOVE_PERSON_ROLES_BY_ID = "/removePersonRoles/{pEmplid}/{Organization}";
	public static final String UPDATE_PERSONAL_DATA_BY_ID = "/updatePersonalDataById/{pEmplid}";
	public static final String GET_PERSONAL_DATA_BY_ID = "/getPersonalDataById/{pEmplid}";
	public static final String ADD_PROXY = "/addProxy";
	public static final String GET_ALL_PROXY_BY_ID = "/getProxyById/{ppEmplId}";
	public static final String GET_PROXY_BY_ID = "/getProxyById/{ppEmplId}/{effectiveDate}";
	public static final String REMOVE_PROXY_BY_ID = "/removeProxyById/{ppEmplId}/{effectiveDate}";
	public static final String UPDATE_PROXY_BY_ID = "/updateProxyById/{ppEmplId}/{effectiveDate}";
	public static final String GET_EMP_OPTIONS_BY_ID = "/getEmployeeOptionsById/{pEmplid}";
	public static final String UPDATE_EMPLOYEE_OPTIONS="/updateEmployeeOptions";

}
