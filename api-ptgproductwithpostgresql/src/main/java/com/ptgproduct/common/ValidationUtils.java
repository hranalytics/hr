package com.ptgproduct.common;

public interface ValidationUtils {

	public static final String PERSON_EMPID_NOT_NULL = "Employee Id should not be null";

	public static final String PERSON_EMPID_SIZE = "Employee Id size should be minimum 4 characters and maximum can be 11 characters and it should be start with emp";

	public static final String PERSON_EMAIL_ACCESS_TYPE_NOT_NULL = "Person Email access type should not be null";

	public static final String PERSON_EMAIL_ACCESS_TYPE_SIZE = "Person Email access type size should be minimum 1 and maximum 4 character";

	public static final String PERSON_EMAIL_NOT_NULL = "Person Email should not be null";

	public static final String PERSON_EMAIL_VALIDATION = "Enter valid Email ID";

	public static final String PERSON_EMAIL_FLAG_NOT_NULL = "Person Email flag value should not be null";

	public static final String PERSON_COMPANY_NOT_NULL = "Company Name field is required";

	public static final String PERSON_COMPANY_SIZE = "Company Name size can be min 1 and max 3 char";

	public static final String PERSON_NATIONAL_ID_SIZE = "National ID size can be max 20 char";

	public static final String PERSON_FORMOF_ADDRESS_NOT_NULL = "Form of Address field is required";

	public static final String PERSON_FIRST_NAME_NOT_NULL = "First Name field is required";

	public static final String PERSON_LAST_NAME_NOT_NULL = "Last Name field is required";

	public static final String PERSON_FIRST_NAME_SIZE = "First Name size can be max 30 char";

	public static final String PERSON_LAST_NAME_SIZE = "Last Name size can be max 30 char";

	public static final String PERSON_MIDDLE_NAME_SIZE = "Middle Name size can be max 30 char";

	public static final String PERSON_COUNTRY_ID_NOT_NULL = "Country field is required";

	public static final String PERSON_COUNTRY_ID_SIZE = "Country Size can be of max 3 char";

	public static final String PERSON_ADDRESS1_NOT_NULL = "Address1 Field is required";

	public static final String PERSON_ADDRESS2_NOT_NULL = "Address1 Field is required";

	public static final String PERSON_ADDRESS1_SIZE = "Address1 size can be of max 55 char";

	public static final String PERSON_ADDRESS2_SIZE = "Address2 size can be of max 55 char";
	
	public static final String PERSON_CITY_NOT_NULL = "City Field is required";

	public static final String PERSON_CITY_SIZE = "City size can be of max 30 char";
	
	public static final String PERSON_COUNTRY_NOT_NULL = "Country Field is required";

	public static final String PERSON_COUNTRY_SIZE = "Country size can be of max 3 char";
	
	public static final String PERSON_STATE_NOT_NULL = "State Field is required";

	public static final String PERSON_STATE_SIZE = "State size can be of max 6 char";
	
	public static final String PERSON_POSTAL_NOT_NULL = "Postal Code Field is required";

	public static final String PERSON_POSTAL_SIZE = "Postal Code size can be of max 12 char";

	public static final String PERSON_HOME_PHONE_SIZE = "Home Phone size can be of max 24 char";
	
	public static final String PERSON_HOME_EMAIL_NOT_NULL = "Home Email Field is required";

	public static final String PERSON_HOME_EMAIL_SIZE = "Home Email size can be of max 70 char";
	
	public static final String PERSON_GENDER_ID_NOT_NULL = "Gender Field is required";

	public static final String PERSON_GENDER_ID_SIZE = "Gender size can be of max 4 char";
	
	public static final String PERSON_ENTRY_REASON_NOT_NULL = "Entry Reason Field is required";

	public static final String PERSON_ENTRY_REASON_SIZE = "Entry Reason size can be of max 3 char";
	
	public static final String PERSON_EMP_TYPE_NOT_NULL = "Employee type Field is required";

	public static final String PERSON_EMP_TYPE_SIZE = "Employee type size should be of 1 char";

}
