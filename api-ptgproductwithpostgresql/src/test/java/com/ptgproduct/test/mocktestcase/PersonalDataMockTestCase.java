package com.ptgproduct.test.mocktestcase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ptgproduct.domain.PersonalData;
import com.ptgproduct.service.PersonService;

public class PersonalDataMockTestCase {
	private static PersonService personalDataMock;
	private static PersonalData personalData;

	@BeforeClass
	public static void getPersonalMockData() {
		personalDataMock = mock(PersonService.class);
		personalData = new PersonalData("EMP0001", "1994-05-20", "S", "S", "f", "1994-05-20", "A");
		when(personalDataMock.getPersonalDataById("EMP0001")).thenReturn(personalData);
		when(personalDataMock.updatePersonalData(personalData, "EMP0001")).thenReturn(personalData);
	}

	// @Test
	public void getPersonalDataMockTest() {
		PersonalData getPersonalDataMock = personalDataMock.getPersonalDataById("EMP0001");
		assertNotNull(getPersonalDataMock);
		assertEquals("EMP0001", getPersonalDataMock.getpEmplid());
		assertEquals("1994-05-20", getPersonalDataMock.getBirthDate());
		assertEquals("1994-05-20", getPersonalDataMock.getEffectiveDate());
		assertEquals("S", getPersonalDataMock.getMaritalStatus());
		assertEquals("S", getPersonalDataMock.getMilitaryStatus());
		assertEquals("F", getPersonalDataMock.getSex());
		assertEquals("A", getPersonalDataMock.getEthinicity());
	}

	//@Test
	public void updatePersonalDataMockTest() {
		PersonalData updatePersonalDataMock = personalDataMock.updatePersonalData(personalData, "EMP0001");
		assertNotNull(updatePersonalDataMock);
		assertEquals(personalData, updatePersonalDataMock);
		assertEquals("1994-05-20", updatePersonalDataMock.getBirthDate());
		assertEquals("S", updatePersonalDataMock.getMaritalStatus());
		assertEquals("f", updatePersonalDataMock.getSex());
	}
}
