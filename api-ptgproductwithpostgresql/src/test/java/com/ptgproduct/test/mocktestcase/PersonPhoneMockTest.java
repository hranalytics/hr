package com.ptgproduct.test.mocktestcase;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ptgproduct.common.CORSFilter;
import com.ptgproduct.domain.PersonAddress;
import com.ptgproduct.domain.PersonMyRoles;
import com.ptgproduct.domain.PersonName;
import com.ptgproduct.domain.PersonPhoneDetails;
import com.ptgproduct.service.PersonService;
import com.ptgproduct.test.common.TestUtil;
import com.ptgproduct.web.controller.impl.PersonControllerImpl;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/test-context.xml" })
public class PersonPhoneMockTest {
	private MockMvc mockMvc;
	@Mock
	private PersonService personService;

	@InjectMocks
	private PersonControllerImpl personControllerimpl;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(personControllerimpl).addFilters(new CORSFilter()).build();
	}

	@Test
	public void addPersonPhone() throws Exception {

		List<PersonPhoneDetails> mockList = new ArrayList<PersonPhoneDetails>();
		PersonPhoneDetails mockPhoneDetails = new PersonPhoneDetails();
		mockPhoneDetails.setpEmplid("EMP0003");
		mockPhoneDetails.setMedia("Telephone");
		mockPhoneDetails.setAccesType("HOME");
		mockPhoneDetails.setTelePhone("9999999997");
		PersonPhoneDetails mockPhoneDetails1 = new PersonPhoneDetails();
		mockPhoneDetails1.setpEmplid("EMP0003");
		mockPhoneDetails1.setMedia("Telephone");
		mockPhoneDetails1.setAccesType("OFFI");
		mockPhoneDetails1.setTelePhone("9999999997");

		PersonPhoneDetails mockPhoneDetails3 = new PersonPhoneDetails();
		mockPhoneDetails3.setpEmplid("EMP0003");
		mockPhoneDetails3.setMedia("Telephone");
		mockPhoneDetails3.setAccesType("NEER");
		mockPhoneDetails3.setTelePhone("9999999997");
		mockList.add(mockPhoneDetails);
		mockList.add(mockPhoneDetails1);

		when(personService.getPersonPhoneById("EMP0003")).thenReturn(mockList);
		when(personService.addPersonPhone(mockPhoneDetails3)).thenReturn("Inserted Successfully");
		mockMvc.perform(post("/v1/api/addPersonPhone").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(mockPhoneDetails3))).andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$._statusCode", is("200")))
				.andExpect(jsonPath("$._statusMessage", is("Inserted Successfully")));

	}

	@Test
	public void updatePersonPhone() throws Exception {

		List<PersonPhoneDetails> mockList = new ArrayList<PersonPhoneDetails>();
		PersonPhoneDetails mockPhoneDetails = new PersonPhoneDetails();
		mockPhoneDetails.setpEmplid("EMP0003");
		mockPhoneDetails.setMedia("Telephone");
		mockPhoneDetails.setAccesType("HOME");
		mockPhoneDetails.setTelePhone("9999999997");
		PersonPhoneDetails mockPhoneDetails1 = new PersonPhoneDetails();
		mockPhoneDetails1.setpEmplid("EMP0003");
		mockPhoneDetails1.setMedia("Telephone");
		mockPhoneDetails1.setAccesType("OFFI");
		mockPhoneDetails1.setTelePhone("9999999997");

		PersonPhoneDetails mockPhoneDetails3 = new PersonPhoneDetails();
		mockPhoneDetails3.setpEmplid("EMP0003");
		mockPhoneDetails3.setMedia("Telephone");
		mockPhoneDetails3.setAccesType("XYZ");
		mockPhoneDetails3.setTelePhone("9999999998");
		mockList.add(mockPhoneDetails);
		mockList.add(mockPhoneDetails1);

		when(personService.getPersonPhoneById("EMP0003")).thenReturn(mockList);
		when(personService.updatePersonPhone(mockPhoneDetails3)).thenReturn(true);
		mockMvc.perform(put("/v1/api/updatePersonPhone").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(mockPhoneDetails3))).andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$._statusCode", is("200")));

	}

	@Test
	public void getPersonPhoneById() throws Exception {
		List<PersonPhoneDetails> mockList = new ArrayList<PersonPhoneDetails>();
		PersonPhoneDetails mockPhoneDetails = new PersonPhoneDetails();
		mockPhoneDetails.setpEmplid("EMP0003");
		mockPhoneDetails.setMedia("Telephone");
		mockPhoneDetails.setAccesType("HOME");
		mockPhoneDetails.setTelePhone("9999999997");
		mockList.add(mockPhoneDetails);
		when(personService.getPersonPhoneById("EMP0003")).thenReturn(mockList);

		mockMvc.perform(get("/v1/api/getPersonPhoneById/{pEmplid}", "EMP0003")).andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.data[0].pEmplid", is("EMP0003")))
				.andExpect(jsonPath("$.data[0].accesType", is("HOME")))
				.andExpect(jsonPath("$.data[0].media", is("Telephone")))
				.andExpect(jsonPath("$.data[0].telePhone", is("9999999997")));

	}

	@Test
	public void getPersonNameMockTest() throws Exception {
		PersonName personName = new PersonName("EMP0004", "1990-01-18", "2", "p", "m", "L", "0", "0");
		when(personService.getPersonNames("EMP0004")).thenReturn(Arrays.asList(personName));
		mockMvc.perform(get("/v1/api/getPersonNameById/{pEmplid}", "EMP0004")).andExpect(status().is(200))
				.andExpect(jsonPath("$._statusCode", is("200")))
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.data[0].pemplId", is("EMP0004")))
				.andExpect(jsonPath("$.data[0].effectiveDate", is("1990-01-18")))
				.andExpect(jsonPath("$.data[0].formOfAddress", is("1")))
				.andExpect(jsonPath("$.data[0].firstName", is(personName.getFirstName())))
				.andExpect(jsonPath("$.data[0].middleName", is(personName.getMiddleName())))
				.andExpect(jsonPath("$.data[0].lastName", is(personName.getLastName())))
				.andExpect(jsonPath("$.data[0].suffix", is(personName.getSuffix())))
				.andExpect(jsonPath("$.data[0].nameType", is(personName.getNameType())));
	}

	@Test
	public void test_get_Person_Address_by_id_success() throws Exception {
		List<PersonAddress> personAddressList = new ArrayList<PersonAddress>();
		PersonAddress personAddress = new PersonAddress();
		personAddress.setpEmplid("EMP0002");
		personAddress.setPaAddress1("Hitech");
		personAddress.setPaAddress2("hyderabad");
		personAddress.setPaAddressType("Ho");
		personAddress.setPaCity("Hgy");
		personAddress.setPaCountry("IND");
		personAddress.setPaEffdt("2013-12-12");
		personAddress.setPaPostal("500038");
		personAddress.setPaState("T");

		personAddressList.add(personAddress);

		when(personService.getPersonAddressById("EMP0002")).thenReturn(personAddressList);

		mockMvc.perform(get("/v1/api/getPersonAddressById/{pEmplid}", "EMP0002")).andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.data[0].pEmplid", is("EMP0002")))
				.andExpect(jsonPath("$.data[0].paAddress1", is("Hitech")))
				.andExpect(jsonPath("$.data[0].paAddress2", is("hyderabad")))
				.andExpect(jsonPath("$.data[0].paAddressType", is("Ho")))
				.andExpect(jsonPath("$.data[0].paCity", is("Hgy")))
				.andExpect(jsonPath("$.data[0].paCountry", is("IND")))
				.andExpect(jsonPath("$.data[0].paEffdt", is("2013-12-12")))
				.andExpect(jsonPath("$.data[0].paPostal", is("500038")))
				.andExpect(jsonPath("$.data[0].paState", is("T")));

		verify(personService, times(1)).getPersonAddressById("EMP0002");
		verifyNoMoreInteractions(personService);
	}

	// ----------------------------------------------------------------------------------------
	@Test
	public void test_get_person_myroles_by_id_success() throws Exception {

		List<PersonMyRoles> myRoles = new ArrayList<PersonMyRoles>();

		PersonMyRoles pMyRoles = new PersonMyRoles();
		pMyRoles.setEmplid("0001");
		pMyRoles.setRoleDescription("rakesh");
		pMyRoles.setOrganization("ABC");
		pMyRoles.setDepartment("kjkbh");
		pMyRoles.setLocation("nellore");

		myRoles.add(pMyRoles);

		when(personService.getMyRoleById("EMP0001")).thenReturn(myRoles);

		mockMvc.perform(get("/v1/api/getMyRolesById/{pEmplId}", "EMP0001")).andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.data[0].emplid", is("0001")))
				.andExpect(jsonPath("$.data[0].roleDescription", is("rakesh")))
				.andExpect(jsonPath("$.data[0].organization", is("ABC")))
				.andExpect(jsonPath("$.data[0].department", is("kjkbh")))
				.andExpect(jsonPath("$.data[0].location", is("nellore")));

		verify(personService, times(1)).getMyRoleById("EMP0001");
		verifyNoMoreInteractions(personService);
	}

	public static String asJsonString(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void test_get_person_names_by_id_success() throws Exception {

		List<PersonName> personName = new ArrayList<PersonName>();

		PersonName pName = new PersonName();
		pName.setPemplId("EMP0001");
		pName.setFirstName("rakesh");
		pName.setLastName("ABC");
		pName.setMiddleName("kumar");
		pName.setEffectiveDate("2013-12-12");
		pName.setFormOfAddress("nellore");
		pName.setSuffix("nell");
		pName.setNameType("ore");

		personName.add(pName);

		when(personService.getPersonNames("EMP0001")).thenReturn(personName);

		mockMvc.perform(get("/v1/api/getPersonNameById/{pemplId}", "EMP0001")).andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.data[0].pemplId", is("EMP0001")))
				.andExpect(jsonPath("$.data[0].firstName", is("rakesh")))
				.andExpect(jsonPath("$.data[0].lastName", is("ABC")))
				.andExpect(jsonPath("$.data[0].middleName", is("kumar")))
				.andExpect(jsonPath("$.data[0].effectiveDate", is("2013-12-12")))
				.andExpect(jsonPath("$.data[0].formOfAddress", is("nellore")))
				.andExpect(jsonPath("$.data[0].suffix", is("nell")))
				.andExpect(jsonPath("$.data[0].nameType", is("ore")));

		verify(personService, times(1)).getPersonNames("EMP0001");
		verifyNoMoreInteractions(personService);
	}
	// ----------------------------------------------------------------------------------------

	@Test
	public void test_delete_Person_Address_success() throws Exception {
		List<PersonAddress> personAddressList = new ArrayList<PersonAddress>();
		PersonAddress personAddress = new PersonAddress();
		personAddress.setpEmplid("EMP0003");
		personAddress.setPaAddress1("Hitech");
		personAddress.setPaAddress2("hyderabad");
		personAddress.setPaAddressType("Ho");
		personAddress.setPaCity("Hgy");
		personAddress.setPaCountry("IND");
		personAddress.setPaEffdt("2013-12-12");
		personAddress.setPaPostal("500038");
		personAddress.setPaState("T");

		personAddressList.add(personAddress);

		when(personService.getPersonAddressById("EMP0003")).thenReturn(personAddressList);
		when(personService.deletePersonAddress("EMP0003", "Ho", "2013-12-12")).thenReturn(true);

		mockMvc.perform(delete("/v1/api/removePersonAddress/{pEmplid}/{paAddressType}/{paEffdt}", "EMP0003", "Ho",
				"2013-12-12")).andExpect(status().isOk());
		verify(personService, times(1)).deletePersonAddress("EMP0003", "Ho", "2013-12-12");
		verifyNoMoreInteractions(personService);
	}
	// --------------------------------------------------------------------------------------------------------------------------------

	@Test
	public void test_create_person_Roles_success() throws Exception {
		List<PersonMyRoles> myRoles = new ArrayList<PersonMyRoles>();

		PersonMyRoles pMyRoles = new PersonMyRoles();
		pMyRoles.setEmplid("EMP0001");
		pMyRoles.setRoleDescription("rakesh");
		pMyRoles.setOrganization("ABC");
		pMyRoles.setDepartment("kjkbh");
		pMyRoles.setLocation("nellore");

		myRoles.add(pMyRoles);

		when(personService.getMyRoleById("EMP0001")).thenReturn(myRoles);
		when(personService.addPersonMyRoles(pMyRoles)).thenReturn("EMP0001");

		mockMvc.perform(
				post("/v1/api/addPersonRoles").contentType(MediaType.APPLICATION_JSON).content(asJsonString(pMyRoles)))
				.andExpect(status().isOk());

		verify(personService, times(1)).addPersonMyRoles(pMyRoles);
		verifyNoMoreInteractions(personService);
	}
	// ----------------------------------------------------------------------------------------------------------------------------------

	@Test
	public void test_update_person_Address_success() throws Exception {
		List<PersonAddress> personAddressList = new ArrayList<PersonAddress>();

		PersonAddress personAddress = new PersonAddress();
		personAddress.setpEmplid("EMP0003");
		personAddress.setPaAddress1("Hitech");
		personAddress.setPaAddress2("hyderabad");
		personAddress.setPaAddressType("Ho");
		personAddress.setPaCity("Hgy");
		personAddress.setPaCountry("IND");
		personAddress.setPaEffdt("2013-12-12");
		personAddress.setPaPostal("500038");
		personAddress.setPaState("T");

		personAddressList.add(personAddress);

		when(personService.getPersonAddressById("EMP0003")).thenReturn(personAddressList);
		when(personService.updatePersonAddress(personAddress, "EMP0003")).thenReturn(true);

		mockMvc.perform(put("/v1/api/updatePersonAddress/{pEmplid}", "EMP0003").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(personAddress))).andExpect(status().isOk());

		verify(personService, times(1)).updatePersonAddress(personAddress, "EMP0003");
		verifyNoMoreInteractions(personService);
	}
	// ----------------------------------------------------------------------------------------------------------------------------------------------

	@Test
	public void test_delete_Person_MyRoles_success() throws Exception {
		List<PersonMyRoles> myRoles = new ArrayList<PersonMyRoles>();

		PersonMyRoles pMyRoles = new PersonMyRoles();
		pMyRoles.setEmplid("EMP0001");
		pMyRoles.setRoleDescription("rakesh");
		pMyRoles.setOrganization("ABC");
		pMyRoles.setDepartment("kjkbh");
		pMyRoles.setLocation("nellore");

		myRoles.add(pMyRoles);

		when(personService.getMyRoleById("EMP0003")).thenReturn(myRoles);
		when(personService.deletePersonMyRoles("EMP0003", "ABC")).thenReturn(true);

		mockMvc.perform(delete("/v1/api/removePersonRoles/{pEmplid}/{Organization}", "EMP0003", "ABC"))
				.andExpect(status().isOk());
		verify(personService, times(1)).deletePersonMyRoles("EMP0003", "ABC");
		verifyNoMoreInteractions(personService);
	}
}
