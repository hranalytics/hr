package com.ptgproduct.test.mocktestcase;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ptgproduct.common.CORSFilter;
import com.ptgproduct.domain.EmailDetails;
import com.ptgproduct.service.PersonService;
import com.ptgproduct.test.common.TestUtil;
import com.ptgproduct.web.controller.impl.PersonControllerImpl;

@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/test-context.xml" })

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonEmailMockTest {

	private MockMvc mockMvc;

	@Mock
	private PersonService personServiceMock;

	@InjectMocks
	private PersonControllerImpl personControllerimpl;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(personControllerimpl).addFilters(new CORSFilter()).build();
	}
	
	public static String asJsonString(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void getPersonEmailInfoMockTest() throws Exception {

		EmailDetails emailDetails1 = new EmailDetails("EMP0002", "HOME", "E-mail Address", "aspjain22@gmail.com",
				"true");
		EmailDetails emailDetails2 = new EmailDetails("EMP0002", "WORK", "E-mail Address", "abc@xyz.com", "true");

		when(personServiceMock.getEmailInfoById("EMP0002")).thenReturn(Arrays.asList(emailDetails1, emailDetails2));
		mockMvc.perform(get("/v1/api/getPersonEmailInfoById/{pEmplid}", "EMP0002")).andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.data", hasSize(2)))
				.andExpect(jsonPath("$.data[0].peemplId", is("EMP0002")))
				.andExpect(jsonPath("$.data[0].accessType", is("HOME")))
				.andExpect(jsonPath("$.data[0].media", is("E-mail Address")))
				.andExpect(jsonPath("$.data[0].email", is("aspjain22@gmail.com")))
				.andExpect(jsonPath("$.data[0].emailFlag", is("true")))
				.andExpect(jsonPath("$.data[1].peemplId", is("EMP0002")))
				.andExpect(jsonPath("$.data[1].accessType", is("WORK")))
				.andExpect(jsonPath("$.data[1].media", is("E-mail Address")))
				.andExpect(jsonPath("$.data[1].email", is("abc@xyz.com")))
				.andExpect(jsonPath("$.data[1].emailFlag", is("true")));
	}
	
	@Test
	public void addPersonEmailInfoMockTest() throws Exception{
		EmailDetails emailDetails1 = new EmailDetails("EMP0002", "HOME", "E-mail Address", "aspjain22@gmail.com","true");
		EmailDetails emailDetails2 = new EmailDetails("EMP0002", "WORK", "E-mail Address", "abc@xyz.com", "true");
		EmailDetails emailDetails = new EmailDetails("EMP0002", "OFFC", "E-mail Address", "abc@xyz.com", "true");
		when(personServiceMock.getEmailInfoById("EMP0002")).thenReturn(Arrays.asList(emailDetails1, emailDetails2));
		when(personServiceMock.addEmailInfo(emailDetails)).thenReturn("EMP0002");
		mockMvc.perform(
				post("/v1/api/addPersonEmailInfo").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(emailDetails)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._statusMessage",is("Inserted Successfully")));
		
		verify(personServiceMock, times(1)).addEmailInfo(emailDetails);
		verifyNoMoreInteractions(personServiceMock);
	}
	
	@Test
	public void updatePersonEmailInfoMockTest() throws Exception{
		EmailDetails emailDetails1 = new EmailDetails("EMP0002", "HOME", "E-mail Address", "aspjain22@gmail.com","true");
		EmailDetails emailDetails2 = new EmailDetails("EMP0002", "WORK", "E-mail Address", "abc@xyz.com", "true");
		EmailDetails emailDetails = new EmailDetails("EMP0002", "OFFC", "E-mail Address", "abcde@xyz.com", "true");
		when(personServiceMock.getEmailInfoById("EMP0002")).thenReturn(Arrays.asList(emailDetails1, emailDetails2));
		when(personServiceMock.updateEmailInfo(emailDetails)).thenReturn(true);
		mockMvc.perform(
				put("/v1/api/updatePersonEmailInfo").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(emailDetails)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._statusMessage",is("Updated Successfully")));
		
		verify(personServiceMock, times(1)).updateEmailInfo(emailDetails);
		verifyNoMoreInteractions(personServiceMock);
	}
	
	@Test
	public void deletePersonEmailInfoMockTest() throws Exception{
		EmailDetails emailDetails1 = new EmailDetails("EMP0002", "HOME", "E-mail Address", "aspjain22@gmail.com","true");
		EmailDetails emailDetails2 = new EmailDetails("EMP0002", "WORK", "E-mail Address", "abc@xyz.com", "true");
		when(personServiceMock.getEmailInfoById("EMP0002")).thenReturn(Arrays.asList(emailDetails1, emailDetails2));
		when(personServiceMock.removeEmailInfo("EMP0002", "HOME")).thenReturn(true);

		mockMvc.perform(delete("/v1/api/removePersonEmailInfo/{pEmplid}/{accessType}", "EMP0002", "HOME"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$._statusMessage",is("Deleted Successfully")));
		verify(personServiceMock, times(1)).removeEmailInfo("EMP0002", "HOME");
		verifyNoMoreInteractions(personServiceMock);
	}
	

}
