package com.ptgproduct.test.mocktestcase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ptgproduct.domain.PersonName;
import com.ptgproduct.service.PersonService;

public class PersonNamesMockTestCase {
	private static PersonService personNamesMock;
	private static PersonName personName;

	@BeforeClass
	public static void getPersonNameMockData() {
		personNamesMock = mock(PersonService.class);
		personName = new PersonName("EMP0004", "2014-12-12", "0", "pavani", "M", "L", "0", "0");
		when(personNamesMock.getPersonNames("EMP0004")).thenReturn(Arrays.asList(personName));
		when(personNamesMock.updatePersonNames(personName, "EMP0004")).thenReturn(personName);
		when(personNamesMock.deletePersonNames("EMP0004", "2014-12-12", "0")).thenReturn(true);
	}

	/* @Test */
	public void getPersonNameMockTest() {
		List<PersonName> getPersonNamesMock = personNamesMock.getPersonNames("EMP0004");
		assertNotNull("object is null", personNamesMock);

		for (PersonName personNameList : getPersonNamesMock) {
			assertEquals("EMP0004", personNameList.getPemplId());
			assertEquals("2014-12-12", personNameList.getEffectiveDate());
			assertEquals("0", personNameList.getFormOfAddress());
			assertEquals("pavani", personNameList.getFirstName());
			assertEquals("M", personNameList.getMiddleName());
			assertEquals("L", personNameList.getLastName());
			assertEquals("0", personNameList.getNameType());
			assertEquals("0", personNameList.getSuffix());
		}
	}

	//@Test
	public void updatepersonNameMockTests() {
		PersonName updatePersonNamesMock = personNamesMock.updatePersonNames(personName, "EMP0004");
		assertNotNull(updatePersonNamesMock);
		assertEquals(personName, updatePersonNamesMock);

		assertEquals("pavani", updatePersonNamesMock.getFirstName());
		assertEquals("L", updatePersonNamesMock.getLastName());
		assertEquals("0", updatePersonNamesMock.getSuffix());
	}
	
	@Test
	public void deletePersonNameMockTest(){
		boolean deletePersonNamesMockTest = personNamesMock.deletePersonNames("EMP0004", "2014-12-12", "0");
		assertNotNull(deletePersonNamesMockTest);
	}
}
