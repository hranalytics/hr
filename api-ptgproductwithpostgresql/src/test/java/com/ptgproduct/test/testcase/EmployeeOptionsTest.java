package com.ptgproduct.test.testcase;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptgproduct.domain.EmployeeOptions;
import com.ptgproduct.service.PersonService;


@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/test-context.xml"})

@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeeOptionsTest {

	@Autowired
	PersonService personService;

	private EmployeeOptions getData(){
		EmployeeOptions pOptions = new EmployeeOptions();
		
		pOptions.setOptionEmplId("EMP0001");
		pOptions.setOptionId(4);
		pOptions.setOptionDetails("Recieve Emails about pending Workflow");
		pOptions.setOptionName("Workflow Notications");
		pOptions.setOptionPreferences("yes");
		return pOptions; 
	}
	
	
	@Test
	public void getEmployeeOptionsById() {
		List<EmployeeOptions> employeeOptions = personService.getEmployeeOptionsById("EMP0001");
		
		EmployeeOptions pOptions = getData();
		for(EmployeeOptions employeeOption:employeeOptions){
		assertEquals(pOptions.getOptionEmplId(), employeeOption.getOptionEmplId());
		assertEquals(pOptions.getOptionId(), employeeOption.getOptionId());
		assertEquals(pOptions.getOptionName(), employeeOption.getOptionName());
		assertEquals(pOptions.getOptionDetails(), employeeOption.getOptionDetails());
		assertEquals(pOptions.getOptionPreferences(), employeeOption.getOptionPreferences());

		
		System.out.println("Successfully");
		}
			}
	
 
	
}
