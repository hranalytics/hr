package com.ptgproduct.test.testcase;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptgproduct.domain.MyProxy;
import com.ptgproduct.service.PersonService;

@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/test-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class MyProxyTest {
	@Autowired
	PersonService personService;

	String id = "KA3006";
	String effectiveDate = "2049-12-28";

	MyProxy myProxy;

	 @Test 
	public void addProxy() {
		myProxy = new MyProxy();

		myProxy.setPpEmplId("KA3006");
		myProxy.setEffectiveDate("2049-12-28");
		myProxy.setFirstName("kiran");
		myProxy.setLastNmae("nalavala");
		myProxy.setEndDate("2019-12-28");

		String s = personService.addProxy(myProxy);
		Assert.assertNotNull(s);
		List<MyProxy> myProxies = personService.getProxyById(id, effectiveDate);
		Assert.assertNotNull(myProxies);
		for (MyProxy proxy : myProxies) {
			if (proxy.getEffectiveDate().equalsIgnoreCase(
					myProxy.getEffectiveDate())) {
				Assert.assertTrue(proxy.equals(myProxy));
			}
		}
		Assert.assertEquals("inserted successfully", s);

	}

	 @Test 
	public void getProxyById() {
		List<MyProxy> myProxies = personService.getProxyById(id, effectiveDate);
		Assert.assertNotNull(myProxies);
		for (MyProxy proxy : myProxies) {
			Assert.assertEquals(proxy.getPpEmplId(), id);
		}
	}

	 @Test 
	public void updateProxy() {
		myProxy = new MyProxy();
		myProxy.setPpEmplId("KA3006");
		myProxy.setEffectiveDate("2016-12-28");
		myProxy.setFirstName("ram");
		myProxy.setLastNmae("N");
		myProxy.setEndDate("2025-12-28");

		boolean b = personService.updateProxy(myProxy, id, effectiveDate);
		Assert.assertTrue(b);

		List<MyProxy> myProxies = personService.getProxyById(id, effectiveDate);
		Assert.assertNotNull(myProxies);
		for (MyProxy proxy : myProxies) {
			if (proxy.getEffectiveDate().equalsIgnoreCase(
					myProxy.getEffectiveDate())) {
				Assert.assertEquals(proxy.getFirstName(),
						myProxy.getFirstName());
				Assert.assertEquals(proxy.getLastNmae(), myProxy.getLastNmae());
				Assert.assertEquals(proxy.getEndDate(), myProxy.getEndDate());
			}
		}
	}

	@Test
	public void deleteProxy() {
		boolean b = personService.deleteProxy(id, effectiveDate);
		Assert.assertTrue(b);
	}
}
