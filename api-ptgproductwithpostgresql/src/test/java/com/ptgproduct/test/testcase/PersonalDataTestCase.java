package com.ptgproduct.test.testcase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptgproduct.domain.PersonalData;
import com.ptgproduct.service.PersonService;

@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/test-context.xml"})

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonalDataTestCase {

	@Autowired
	PersonService personService;

	private PersonalData getPersonalTestData() {
		PersonalData personalData = new PersonalData();
		personalData.setBirthDate("1994-05-21");
		personalData.setEffectiveDate("1994-05-20");
		personalData.setEthinicity("a");
		personalData.setMaritalStatus("single");
		personalData.setMilitaryStatus("s");
		personalData.setpEmplid("EMP0001");
		personalData.setSex("f");
		return personalData;
	}

	@Test
	public void getPersonalData() {
		PersonalData personalDataGet = getPersonalTestData();

		PersonalData personalDataActual = personService.getPersonalDataById(personalDataGet.getpEmplid());
		assertNotNull(personalDataActual);
		assertEquals(personalDataGet.getBirthDate(), personalDataActual.getBirthDate());
		assertEquals(personalDataGet.getEffectiveDate(), personalDataActual.getEffectiveDate());
		assertEquals(personalDataGet.getEthinicity(), personalDataActual.getEthinicity());
		assertEquals(personalDataGet.getMaritalStatus(), personalDataActual.getMaritalStatus());
		assertEquals(personalDataGet.getMilitaryStatus(), personalDataActual.getMilitaryStatus());
		assertEquals(personalDataGet.getpEmplid(), personalDataActual.getpEmplid());
		assertEquals(personalDataGet.getSex(), personalDataActual.getSex());
	}

	@Test
	public void updatePersonalData() {
		PersonalData personalDataUpdate = getPersonalTestData();

		personalDataUpdate.setBirthDate("1994-05-20");
		personalDataUpdate.setMaritalStatus("S");
		personalDataUpdate.setSex("F");
		PersonalData actualPersonalData = personService.updatePersonalData(personalDataUpdate,
				personalDataUpdate.getpEmplid());
		assertEquals("1994-05-20", actualPersonalData.getBirthDate());
		assertEquals("S", actualPersonalData.getMaritalStatus());
		assertEquals("F", actualPersonalData.getSex());
	}

}
