package com.ptgproduct.test.testcase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptgproduct.domain.PersonMyRoles;
import com.ptgproduct.service.PersonService;




@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/test-context.xml"})

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonMyRolesTest {

	@Autowired
	PersonService personService;
	
	private PersonMyRoles getData(){
		PersonMyRoles myroles = new PersonMyRoles();
		
		myroles.setEmplid("EMP0002");
		myroles.setDepartment("kjkbh");
		myroles.setLocation("nellore");
		myroles.setOrganization("km");
		myroles.setRoleDescription("resoo");
		
		
		return myroles; 
	}

	
	//@Test
	public void getPersonMyRolesByIdValidInput() {
		List<PersonMyRoles> personMyRoles = personService.getMyRoleById("EMP0002");
		PersonMyRoles myroles = getData();
		for(PersonMyRoles personMyRole:personMyRoles){
		assertTrue(personMyRole.equals(myroles));
		}
	}
	
	//@Test
	public void getPersonMyRolesByIdInvalidInput() {
		List<PersonMyRoles> personMyRoles = personService.getMyRoleById("EMP002");
		for(PersonMyRoles personMyRole:personMyRoles){
		assertNull(personMyRole);
		}
	}
	
	//@Test
	public void addPersonMyRolesValidInput() {
		PersonMyRoles myroles = getData();
		assertNotNull(myroles);
		String res = personService.addPersonMyRoles(myroles);
		assertNotNull(res);
		List<PersonMyRoles> pMyroles = personService.getMyRoleById(myroles.getEmplid());
		assertNotNull(pMyroles);
		for(PersonMyRoles myrole:pMyroles){
			if(myrole.getOrganization().equalsIgnoreCase(myroles.getOrganization())){
				assertTrue(myrole.equals(myroles));
			}
		}
		assertEquals("success",res);
	}
	
	//@Test(expected=DataAccessException.class)
		public void addPersonMyRolesInvalidInput() {
			PersonMyRoles myroles = getData();
			myroles.setEmplid(null);
			personService.addPersonMyRoles(myroles);
		}
	
	//@Test
	public void removePersonMyRolesByIdValidInput() {
		PersonMyRoles myroles = getData();
		boolean flag = personService.deletePersonMyRoles(myroles.getEmplid(), myroles.getOrganization());
		assertTrue(flag);
		List<PersonMyRoles> personPhoneDetails = personService.getMyRoleById(myroles.getEmplid());
		assertFalse(personPhoneDetails.contains(myroles));
	}
	
	//@Test
	public void removePersonMyRolesByIdInvalidInput() {
		PersonMyRoles myroles = getData();
		myroles.setEmplid("EMP002");
		boolean flag = personService.deletePersonMyRoles(myroles.getEmplid(), myroles.getOrganization());
		assertFalse(flag);
	}
	
}
