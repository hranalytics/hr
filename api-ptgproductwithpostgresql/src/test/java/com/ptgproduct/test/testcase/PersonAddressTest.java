package com.ptgproduct.test.testcase;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptgproduct.domain.PersonAddress;
import com.ptgproduct.service.PersonService;


@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/test-context.xml"})

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonAddressTest {

	@Autowired
	PersonService personService;

	private PersonAddress getData(){
		PersonAddress pAddress = new PersonAddress();
		
		pAddress.setpEmplid("EMP0002");
		pAddress.setPaAddress1("Hitech");
		pAddress.setPaAddress2("hyderabad");
		pAddress.setPaAddressType("Ho");
		pAddress.setPaCity("Hgy");
		pAddress.setPaCountry("I");
		pAddress.setPaEffdt("2013-12-12");
		pAddress.setPaPostal("500038");
		pAddress.setPaState("T");
		return pAddress; 
	}
	
	
	@Test
	public void getPersonAddressById() {
		List<PersonAddress> personAddresses = personService.getPersonAddressById("EMP0002");
		
		PersonAddress pAddress = getData();
		for(PersonAddress personAddress:personAddresses){
		assertEquals(pAddress.getpEmplid(), personAddress.getpEmplid());
		assertEquals(pAddress.getPaAddress1(), personAddress.getPaAddress1());
		assertEquals(pAddress.getPaAddress2(), personAddress.getPaAddress2());
		assertEquals(pAddress.getPaAddressType(), personAddress.getPaAddressType());
		assertEquals(pAddress.getPaCity(), personAddress.getPaCity());
		assertEquals(pAddress.getPaCountry(), personAddress.getPaCountry());
		assertEquals(pAddress.getPaEffdt(), personAddress.getPaEffdt());
		assertEquals(pAddress.getPaPostal(), personAddress.getPaPostal());
		assertEquals(pAddress.getPaState(), personAddress.getPaState());
		
		System.out.println("Successfully");
		}
			}
	
	@Test
	public void updatePersonAddress() {
		PersonAddress personAddress = getData();
		personAddress.setPaPostal("123456");
		boolean flag = personService.updatePersonAddress(personAddress,personAddress.getpEmplid());
		assertTrue(flag);
		List<PersonAddress> personAddresses = personService.getPersonAddressById(personAddress.getpEmplid());
		for (PersonAddress pAddress : personAddresses) {
			if ((pAddress.getpEmplid().equals(personAddress.getpEmplid()))
					&& (pAddress.getPaAddressType().equals(personAddress.getPaAddressType()))) {
				assertEquals("123456", pAddress.getPaPostal());
			}
		}
		System.out.println("Successfully");
	}

	@Test
	public void removePersonAddressById() {
		PersonAddress pAddress = getData();
		boolean flag = personService.deletePersonAddress(pAddress.getpEmplid(), pAddress.getPaAddressType(), pAddress.getPaEffdt());
		assertTrue(flag);
		List<PersonAddress> personAddress = personService.getPersonAddressById(pAddress.getpEmplid());
		assertFalse(personAddress.contains(pAddress));
		System.out.println("Successfully");
	}
}
