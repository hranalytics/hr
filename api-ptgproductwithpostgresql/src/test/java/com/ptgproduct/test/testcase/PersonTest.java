package com.ptgproduct.test.testcase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptgproduct.domain.EmailDetails;
import com.ptgproduct.domain.PersonDetails;
import com.ptgproduct.domain.PersonEmergencyContactDetails;
import com.ptgproduct.domain.PersonPhoneDetails;
import com.ptgproduct.exception.PtgproductParseException;
import com.ptgproduct.service.PersonService;

@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/test-context.xml" })

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonTest {

	@Autowired
	PersonService personService;

	private PersonDetails getPersonDetailData(int flagValue) {
		PersonDetails pDetails = new PersonDetails();

		if (flagValue == 0) {
			pDetails.setpEmplid("EMP0014");
		}
		pDetails.setCompanyId("123");
		pDetails.setNationalId("0");
		pDetails.setFormOfAddressId("abcd");
		pDetails.setFirstName("aa");
		pDetails.setLastName("bb");
		pDetails.setMiddleName("gg");
		pDetails.setCountryId("IND");
		pDetails.setAddress1("abcdfsfhdhhfgh");
		pDetails.setAddress2("aabcghghfjfgjfhjfhjfhjfh");
		pDetails.setCity("HYDhjhjghkgjkgjkjhkjk");
		pDetails.setCountry("IND");
		pDetails.setStateId("abcdef");
		pDetails.setPostalCode("500003");
		pDetails.setHomePhone("9896545");
		pDetails.setHomeEmail("abc@xyz.com");
		pDetails.setGenderId("M");
		pDetails.setEthnicityId(null);
		pDetails.setMilitryStatus("K");
		pDetails.setEntryDate("1990-01-02");
		pDetails.setEntryReason("Rea");
		pDetails.setEmplType("R");
		pDetails.setEmpReqTemp("0");
		pDetails.setEmpClass("E");
		pDetails.setAnnual_wages("224545454545.000");
		pDetails.setBenefitClassId("7");
		pDetails.setStdHours("993.32");
		pDetails.setBussinessTitle("SE");
		pDetails.setJobCode("4");
		pDetails.setFlsaStatus("T");
		pDetails.setSupervisorId("1");
		pDetails.setCompensationBasicId("C");
		pDetails.setCompensationRate("3");
		pDetails.setDepartmentId("9");
		pDetails.setWorkLocationId("2");
		pDetails.setPayGroupsIds("8");
		pDetails.setAlternateEmpId("1234");
		pDetails.setWokfEmailAddress("abc@xyz.com");
		pDetails.setLevel("1");
		pDetails.setSponsor("k");
		pDetails.setVacation("1");
		pDetails.setSick("1");
		pDetails.setPersonalTime("");
		pDetails.setFloatingHolidays("0");
		pDetails.setDateOfBirth("1990-01-02");
		pDetails.setIsBased("true");
		pDetails.setEmpVariableHour("0");
		pDetails.setEmpJobDes("0");

		if (flagValue == 1) {
			if (pDetails.getHomeEmail() == null) {
				pDetails.setHomeEmail("abc@xyz.com");
			}
			if (pDetails.getWokfEmailAddress() == null) {
				pDetails.setWokfEmailAddress("abc@xyz.com");
			}
		}

		return pDetails;
	}

	private EmailDetails getEmailData() {
		EmailDetails emailDetails = new EmailDetails();
		emailDetails.setPeemplId("EMP0005");
		emailDetails.setAccessType("HOME");
		emailDetails.setMedia("E-mail Address");
		emailDetails.setEmail("abc@xyz.com");
		emailDetails.setEmailFlag("true");
		return emailDetails;
	}

	private PersonPhoneDetails getPhoneData() {
		PersonPhoneDetails phoneDetails = new PersonPhoneDetails();
		phoneDetails.setpEmplid("EMP0005");
		phoneDetails.setAccesType("WORK");
		phoneDetails.setMedia("Telephone");
		phoneDetails.setTelePhone("9896545");
		return phoneDetails;
	}

	private PersonEmergencyContactDetails getPersonEmergencyContactData() {
		PersonEmergencyContactDetails pDetails = new PersonEmergencyContactDetails();
		pDetails.setEmplid("EMP0005");
		pDetails.setContactName("asss");
		pDetails.setPrimaryContact("Yes");
		pDetails.setRelationship("Sibling");
		pDetails.setSameAddressEmpl("No");
		pDetails.setCountry("India");
		pDetails.setAddress1("sdfsadfsdf");
		pDetails.setAddress2("sdfsadf");
		pDetails.setAddress3("dfsdfsadfsa");
		pDetails.setCity("rwerwer");
		pDetails.setState("Telangana");
		pDetails.setPostal("werwer");
		pDetails.setSamePhoneEmpl("No");
		pDetails.setPhone("9494001785");
		pDetails.setNum1("9494001785");
		pDetails.setNum2("9494001785");
		return pDetails;
	}

	// JUnit test case for Person Details module

	// @Test
	public void addPersonValidInput() throws PtgproductParseException {
		PersonDetails pDetails = getPersonDetailData(1);
		String pEmplid = personService.addPerson(pDetails);
		assertNotNull(pEmplid);
		pDetails.setpEmplid(pEmplid);
		PersonDetails personDetails = personService.getPersonById(pEmplid);
		assertTrue(personDetails.equals(pDetails));
	}

	// @Test(expected=PtgproductParseException.class)
	public void addPersonInvalidInput() throws PtgproductParseException {
		PersonDetails personDetails = getPersonDetailData(1);
		personDetails.setDateOfBirth("1990/03/03");
		personDetails.setEntryDate("abc");
		personService.addPerson(personDetails);
	}

	// @Test(expected=DataAccessException.class)
	public void addPersonException() throws PtgproductParseException {
		personService.addPerson(null);
	}

	 //@Test
	public void getPersonsValid() {
		PersonDetails pDetails = getPersonDetailData(0);
		List<PersonDetails> personDetails = personService.getAllPersons();
		assertNotNull(personDetails);
		for (PersonDetails details : personDetails) {
			if(details.getpEmplid().equalsIgnoreCase(pDetails.getpEmplid())){
				assertTrue("Person Details Equals", details.equals(pDetails));
			}
		}		
	}

	// @Test
	public void getPersonsInvalid() {
		PersonDetails pDetails = getPersonDetailData(0);
		pDetails.setCity("2");
		List<PersonDetails> personDetails = personService.getAllPersons();
		System.out.println(personDetails);
		for (PersonDetails details : personDetails) {
			if(details.getpEmplid().equalsIgnoreCase(pDetails.getpEmplid())){
				assertFalse("Person Details Equals", details.equals(pDetails));
			}
		}
	}

	// @Test
	public void getPersonByIdValidInput() {
		PersonDetails pDetails = getPersonDetailData(0);
		PersonDetails personDetails = personService.getPersonById(pDetails.getpEmplid());
		assertNotNull(personDetails);
		assertTrue("Person Details By Id Equals", personDetails.equals(pDetails));
	}

	// @Test
	public void getPersonByIdInvalidInput() {
		PersonDetails pDetails = getPersonDetailData(0);
		pDetails.setpEmplid("EMP003");
		PersonDetails personDetails = personService.getPersonById(pDetails.getpEmplid());
		System.out.println("pdetail value" + personDetails);
		assertNull(personDetails);
	}

	// @Test(expected=DataAccessException.class)
	public void getPersonByIdException() {
		personService.getPersonById(null);
	}

	// @Test
	public void updatePersonValidInput() throws PtgproductParseException {
		PersonDetails pDetails = getPersonDetailData(0);
		pDetails.setStdHours("992.32");
		pDetails.setVacation("2");
		pDetails.setSick("2");
		pDetails.setPersonalTime("");
		pDetails.setFloatingHolidays("1");
		pDetails.setHomeEmail("t1@xyz.com");
		pDetails.setWokfEmailAddress("t2@xyz.com");
		boolean flag = personService.updatePerson(pDetails);
		assertTrue(flag);
		PersonDetails personDetails = personService.getPersonById(pDetails.getpEmplid());
		assertEquals("992.32", personDetails.getStdHours());
		assertEquals("2", personDetails.getVacation());
		assertEquals("2", personDetails.getSick());
		assertEquals("", personDetails.getPersonalTime());
		assertEquals("1", personDetails.getFloatingHolidays());
		assertEquals("t1@xyz.com", personDetails.getHomeEmail());
		assertEquals("t2@xyz.com", personDetails.getWokfEmailAddress());
	}

	// @Test(expected = PtgproductParseException.class)
	public void updatePersonInvalidInput() throws PtgproductParseException {
		PersonDetails pDetails = getPersonDetailData(0);
		pDetails.setEntryDate("12/12/2009");
		personService.updatePerson(pDetails);
	}

	// @Test
	public void removePersonValidInput() {
		PersonDetails pDetails = getPersonDetailData(0);
		boolean flag = personService.deletePerson(pDetails.getpEmplid());
		assertTrue(flag);
		List<PersonDetails> personDetails = personService.getAllPersons();
		assertFalse(personDetails.contains(pDetails));
	}

	// @Test
	public void removePersonInvalidInput() {
		PersonDetails pDetails = getPersonDetailData(0);
		pDetails.setpEmplid("EMP002");
		boolean flag = personService.deletePerson(pDetails.getpEmplid());
		assertFalse(flag);
	}

	// @Test(expected=IllegalArgumentException.class)
	public void removePersonException() {
		boolean flag = personService.deletePerson(null);
		System.out.println(flag);
	}

	// JUnit test case for Person Email module

	// @Test
	public void addEmailInfoValidInput() {
		EmailDetails eDetails = getEmailData();
		assertNotNull(eDetails);
		String pEmplid = personService.addEmailInfo(eDetails);
		Boolean flag = Boolean.parseBoolean(pEmplid);
		assertTrue(flag);
		List<EmailDetails> emailDetails = personService.getEmailInfoById(eDetails.getPeemplId());
		assertNotNull(emailDetails);
		assertTrue(emailDetails.contains(eDetails));
	}

	// @Test(expected=DataAccessException.class)
	public void addEmailInfoInvalidInput() {
		EmailDetails eDetails = getEmailData();
		eDetails.setPeemplId(null);
		personService.addEmailInfo(eDetails);
	}

	// @Test(expected=NullPointerException.class)
	public void addEmailInfoNullPointerException() {
		EmailDetails eDetails = getEmailData();
		eDetails.setPeemplId("EMP002");
		personService.addEmailInfo(eDetails);
	}

	// @Test
	public void getEmailInfoByIdValidInput() {
		EmailDetails eDetails = getEmailData();
		List<EmailDetails> emailDetails = personService.getEmailInfoById(eDetails.getPeemplId());
		assertNotNull(emailDetails);
		for (EmailDetails emailDetails2 : emailDetails) {
			if (emailDetails2.getAccessType().equals(eDetails.getAccessType())) {
				assertTrue("Email Information By Id Equals", emailDetails2.equals(eDetails));
			}
		}
	}

	// @Test
	public void getEmailInfoByIdInvalidInput() {
		EmailDetails eDetails = getEmailData();
		eDetails.setPeemplId("EMP002");
		List<EmailDetails> emailDetails = personService.getEmailInfoById(eDetails.getPeemplId());
		for (EmailDetails emailDetails2 : emailDetails) {
			assertNull(emailDetails2);
		}

	}

	// @Test(expected=DataAccessException.class)
	public void getEmailInfoByIdException() {
		personService.getEmailInfoById(null);
	}

	// @Test
	public void updateEmailInfoValidInput() {
		EmailDetails eDetails = getEmailData();
		eDetails.setEmail("t1@xyz.com");
		eDetails.setEmailFlag("false");
		boolean flag = personService.updateEmailInfo(eDetails);
		assertTrue(flag);
		List<EmailDetails> emailDetails = personService.getEmailInfoById(eDetails.getPeemplId());
		for (EmailDetails emailDetail : emailDetails) {
			if ((emailDetail.getPeemplId().equals(eDetails.getPeemplId()))
					&& (emailDetail.getAccessType().equals(eDetails.getAccessType()))) {
				assertEquals("t1@xyz.com", emailDetail.getEmail());
				assertEquals("false", emailDetail.getEmailFlag());
			}
		}
	}

	// @Test(expected=NullPointerException.class)
	public void updateEmailInfoInvalidInput() {
		EmailDetails eDetails = getEmailData();
		eDetails.setPeemplId("EMP002");
		personService.updateEmailInfo(eDetails);
	}

	// @Test
	public void removeEmailInfoValidInput() {
		EmailDetails eDetails = getEmailData();
		boolean flag = personService.removeEmailInfo(eDetails.getPeemplId(), eDetails.getAccessType());
		assertTrue(flag);
		List<EmailDetails> emailDetails = personService.getEmailInfoById(eDetails.getPeemplId());
		assertFalse(emailDetails.contains(eDetails));
	}

	// @Test
	public void removeEmailInfoInvalidInput() {
		EmailDetails eDetails = getEmailData();
		eDetails.setPeemplId("EMP002");
		boolean flag = personService.removeEmailInfo(eDetails.getPeemplId(), eDetails.getAccessType());
		assertFalse(flag);
	}

	// JUnit test case for Person phone module

	// @Test
	public void addPersonPhoneValidInput() {
		PersonPhoneDetails phoneDetails = getPhoneData();
		assertNotNull(phoneDetails);
		String pEmplid = personService.addPersonPhone(phoneDetails);
		assertTrue(Boolean.parseBoolean(pEmplid));
		List<PersonPhoneDetails> personPhoneDetails = personService.getPersonPhoneById(phoneDetails.getpEmplid());
		assertNotNull(personPhoneDetails);
		assertTrue(personPhoneDetails.contains(phoneDetails));
	}

	// @Test(expected=NullPointerException.class)
	public void addPersonPhoneInvalidInput() {
		PersonPhoneDetails phoneDetails = getPhoneData();
		phoneDetails.setpEmplid("EMP002");
		personService.addPersonPhone(phoneDetails);
	}

	// @Test(expected = NullPointerException.class)
	public void addPersonPhoneException() {
		personService.addPersonPhone(null);

	}

	// @Test
	public void getPersonPhoneByIdValidInput() {
		PersonPhoneDetails pDetails = getPhoneData();
		List<PersonPhoneDetails> personPhoneDetails = personService.getPersonPhoneById(pDetails.getpEmplid());
		assertNotNull(personPhoneDetails);
		for (PersonPhoneDetails personPhoneDetails2 : personPhoneDetails) {
			if (personPhoneDetails2.getAccesType().equals(pDetails.getAccesType())) {
				assertTrue(personPhoneDetails2.equals(pDetails));
			}
		}
	}

	// @Test
	public void getPersonPhoneByIdInvalidInput() {
		PersonPhoneDetails pDetails = getPhoneData();
		pDetails.setpEmplid("EMP002");
		List<PersonPhoneDetails> personPhoneDetails = personService.getPersonPhoneById(pDetails.getpEmplid());
		for (PersonPhoneDetails personPhoneDetails2 : personPhoneDetails) {
			assertNull(personPhoneDetails2);
		}
	}

	// @Test(expected=DataAccessException.class)
	public void getPersonPhoneByIdException() {
		personService.getPersonPhoneById(null);
	}

	// @Test
	public void updatePersonPhoneValidInput() {
		PersonPhoneDetails phoneDetails = getPhoneData();
		phoneDetails.setTelePhone("123456");
		boolean flag = personService.updatePersonPhone(phoneDetails);
		assertTrue(flag);
		List<PersonPhoneDetails> personPhoneDetails = personService.getPersonPhoneById(phoneDetails.getpEmplid());
		for (PersonPhoneDetails pDetails : personPhoneDetails) {
			if ((pDetails.getpEmplid().equals(phoneDetails.getpEmplid()))
					&& (pDetails.getAccesType().equals(phoneDetails.getAccesType()))) {
				assertEquals("123456", pDetails.getTelePhone());
			}
		}
	}

	// @Test(expected=NullPointerException.class)
	public void updatePersonPhoneInvalidInput() {
		PersonPhoneDetails phoneDetails = getPhoneData();
		phoneDetails.setpEmplid("EMP002");
		personService.updatePersonPhone(phoneDetails);
	}

	// @Test
	public void removePersonPhoneByIdValidInput() {
		PersonPhoneDetails pDetails = getPhoneData();
		boolean flag = personService.deletePersonPhone(pDetails.getpEmplid(), pDetails.getAccesType());
		assertTrue(flag);
		List<PersonPhoneDetails> personPhoneDetails = personService.getPersonPhoneById(pDetails.getpEmplid());
		assertFalse(personPhoneDetails.contains(pDetails));
	}

	// @Test
	public void removePersonPhoneByIdInvalidInput() {
		PersonPhoneDetails pDetails = getPhoneData();
		pDetails.setpEmplid("EMP002");
		boolean flag = personService.deletePersonPhone(pDetails.getpEmplid(), pDetails.getAccesType());
		assertFalse(flag);
	}

	// JUnit test case for Person Emergency Contact module
	// @Test
	public void addPersonEmergencyContactValidInput() {
		PersonEmergencyContactDetails pContactDetails = getPersonEmergencyContactData();
		assertNotNull(pContactDetails);
		String pEmplid = personService.addPersonEmergencyConact(pContactDetails);
		assertTrue(Boolean.parseBoolean(pEmplid));
		List<PersonEmergencyContactDetails> emergencyContactDetails = personService
				.getPersonEmergencyContactById(pContactDetails.getEmplid());
		assertNotNull(emergencyContactDetails);
		assertTrue(emergencyContactDetails.contains(pContactDetails));
	}

	// @Test(expected=NullPointerException.class)
	public void addPersonEmergencyContactInvalidInput() {
		PersonEmergencyContactDetails pContactDetails = getPersonEmergencyContactData();
		pContactDetails.setEmplid("EMP002");
		personService.addPersonEmergencyConact(pContactDetails);
	}

	// @Test(expected=NullPointerException.class)
	public void addPersonEmergencyContactException() {
		personService.addPersonEmergencyConact(null);
	}

	// @Test
	public void getPersonEmergencyContactByIdValidInput() {
		PersonEmergencyContactDetails pContactDetails = getPersonEmergencyContactData();
		List<PersonEmergencyContactDetails> emergencyContactDetails = personService
				.getPersonEmergencyContactById(pContactDetails.getEmplid());
		assertNotNull(emergencyContactDetails);
		for (PersonEmergencyContactDetails pDetails : emergencyContactDetails) {
			if ((pDetails.getEmplid().equals(pContactDetails.getEmplid())
					&& (pDetails.getContactName().equals(pContactDetails.getContactName()))
					&& (pDetails.getRelationship().equals(pContactDetails.getRelationship())))) {
				assertTrue(pDetails.equals(pContactDetails));
			}
		}
	}

	// @Test
	public void getPersonEmergencyContactByIdInvalidInput() {
		PersonEmergencyContactDetails pContactDetails = getPersonEmergencyContactData();
		pContactDetails.setEmplid("EMP002");
		List<PersonEmergencyContactDetails> emergencyContactDetails = personService
				.getPersonEmergencyContactById(pContactDetails.getEmplid());
		for (PersonEmergencyContactDetails pDetails : emergencyContactDetails) {
			assertNull(pDetails);
		}
	}

	// @Test(expected=DataAccessException.class)
	public void getPersonEmergencyContactByIdException() {
		personService.getPersonEmergencyContactById(null);
	}

	// @Test
	public void updatePersonEmergencyContactByIdValidInput() {
		PersonEmergencyContactDetails pContactDetails = getPersonEmergencyContactData();
		pContactDetails.setAddress1("abcdefg");
		boolean flag = personService.updatePersonEmergencyContact(pContactDetails);
		assertTrue(flag);
		List<PersonEmergencyContactDetails> emergencyContactDetails = personService
				.getPersonEmergencyContactById(pContactDetails.getEmplid());
		for (PersonEmergencyContactDetails pDetails : emergencyContactDetails) {
			if ((pDetails.getEmplid().equals(pContactDetails.getEmplid())
					&& (pDetails.getContactName().equals(pContactDetails.getContactName()))
					&& (pDetails.getRelationship().equals(pContactDetails.getRelationship())))) {
				assertEquals("abcdefg", pDetails.getAddress1());
			}
		}
	}

	// @Test(expected=NullPointerException.class)
	public void updatePersonEmergencyContactByIdInvalidInput() {
		PersonEmergencyContactDetails pContactDetails = getPersonEmergencyContactData();
		pContactDetails.setEmplid("EMP002");
		personService.updatePersonEmergencyContact(pContactDetails);
	}

	// @Test
	public void removePersonEmergencyContactByIdValidInput() {
		PersonEmergencyContactDetails pContactDetails = getPersonEmergencyContactData();
		boolean flag = personService.deletePersonEmergencyContact(pContactDetails.getEmplid(),
				pContactDetails.getContactName(), pContactDetails.getRelationship());
		assertTrue(flag);
		List<PersonEmergencyContactDetails> emergencyContactDetails = personService
				.getPersonEmergencyContactById(pContactDetails.getEmplid());
		assertNotNull(emergencyContactDetails);
		assertFalse(emergencyContactDetails.contains(pContactDetails));
	}

	// @Test
	public void removePersonEmergencyContactByIdInvalidInput() {
		PersonEmergencyContactDetails pContactDetails = getPersonEmergencyContactData();
		pContactDetails.setEmplid("EMP002");
		boolean flag = personService.deletePersonEmergencyContact(pContactDetails.getEmplid(),
				pContactDetails.getContactName(), pContactDetails.getRelationship());
		assertFalse(flag);
	}

}
