package com.ptgproduct.test.testcase;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptgproduct.domain.IdentifyPersonalData;
import com.ptgproduct.service.PersonService;


@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/test-context.xml"})

@RunWith(SpringJUnit4ClassRunner.class)
public class IdentifyPersonalDataTest {

	@Autowired
	PersonService personService;

	private IdentifyPersonalData getData(){
		IdentifyPersonalData pdata = new IdentifyPersonalData();
		
		pdata.setEmployeeId("EMP0001");
		pdata.setSocSecUsId("123");
		pdata.setEmployeeStatus("0");
		pdata.setAlternateEmpid("0");
		pdata.setCitizenshipStatus("0");
		return pdata; 
	}
	
	
	@Test
	public void getIdentifyPersonalDataById() {
		List<IdentifyPersonalData> identifyPersonalDatas = personService.getIdentifyPersonalDataById("EMP0001");
		
		IdentifyPersonalData pdata = getData();
		for(IdentifyPersonalData identifyPersonalData:identifyPersonalDatas){
		assertEquals(pdata.getEmployeeId(), identifyPersonalData.getEmployeeId());
		assertEquals(pdata.getSocSecUsId(), identifyPersonalData.getSocSecUsId());
		assertEquals(pdata.getEmployeeStatus(), identifyPersonalData.getEmployeeStatus());
		assertEquals(pdata.getAlternateEmpid(), identifyPersonalData.getAlternateEmpid());
		assertEquals(pdata.getCitizenshipStatus(), identifyPersonalData.getCitizenshipStatus());

		
		System.out.println("Successfully");
		}
			}
	
	@Test
		public void updateIdentifyPersonalData() {
		IdentifyPersonalData identifyPersonalData = getData();
		identifyPersonalData.setSocSecUsId("123");
			boolean flag = personService.updateIdentifyPersonalData(identifyPersonalData);
			assertTrue(flag);
			List<IdentifyPersonalData> pDatas = personService.getIdentifyPersonalDataById(identifyPersonalData.getEmployeeId());
			for (IdentifyPersonalData pData : pDatas) {
				if ((pData.getEmployeeId().equals(identifyPersonalData.getEmployeeId()))
						&& (pData.getSocSecUsId().equals(identifyPersonalData.getSocSecUsId()))) {
					assertEquals("123", pData.getSocSecUsId());
				}
			}
			System.out.println("Successfully");
		}
	
}
