package com.ptgproduct.test.testcase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptgproduct.domain.PersonName;
import com.ptgproduct.service.PersonService;

@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/test-context.xml"})

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonNameTestCase {

	@Autowired
	PersonService personService;

	private PersonName getPersonNameData() {
		PersonName personName = new PersonName();
		personName.setEffectiveDate("2014-12-12");
		personName.setFirstName("pavani");
		personName.setFormOfAddress("0");
		personName.setLastName("L");
		personName.setMiddleName("M");
		personName.setNameType("0");
		personName.setPemplId("EMP0003");
		personName.setSuffix("0");
		return personName;
	}

	@Test
	public void getPersonNameById() {
		PersonName personNameData = getPersonNameData();
		List<PersonName> personName = personService.getPersonNames(personNameData.getPemplId());
		assertNotNull(personNameData);
		for (PersonName personName2 : personName) {
			if ((personName2.getEffectiveDate().equalsIgnoreCase(personNameData.getEffectiveDate()))
					&& (personName2.getFormOfAddress().equalsIgnoreCase(personNameData.getFormOfAddress()))
					&& (personName2.getNameType().equalsIgnoreCase(personNameData.getNameType()))) {
				assertTrue("person names testcase matched", personName2.equals(personNameData));
			}
		}
	}

	@Test
	public void updatePersonName() {
		PersonName personNameUpdate = getPersonNameData();
		personNameUpdate.setFirstName("pavani");
		personNameUpdate.setLastName("madhineni");
		personNameUpdate.setSuffix("M");

		PersonName actualData = personService.updatePersonNames(personNameUpdate, personNameUpdate.getPemplId());

		assertEquals("pavani", actualData.getFirstName());
		assertEquals("madhineni", actualData.getLastName());
		assertEquals("M", actualData.getSuffix());

	}

	@Test
	public void deletePersonName() {
		PersonName personNameDelete = getPersonNameData();
		boolean flag = personService.deletePersonNames(personNameDelete.getPemplId(),
				personNameDelete.getEffectiveDate(), personNameDelete.getNameType());
		assertTrue(flag);

	}
}
